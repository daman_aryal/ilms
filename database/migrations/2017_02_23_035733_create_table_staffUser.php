<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableStaffUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('staff_users', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');

            $table->unsignedInteger('member_type_id');
            $table->foreign('member_type_id')->references('id')->on('member_type')->onUpdate('cascade')->onDelete('cascade');

            $table->string('member_code');

            $table->date('date_of_birth');

            $table->string('permanent_address');

            $table->string('temporary_address');

            $table->unsignedInteger('department_id');

            $table->string('designation');

            $table->string('job_type');

            $table->date('joined_date');

            $table->date('valid_till');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('staff_users');
    }
}
