<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableStocks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('stocks', function (Blueprint $table) {
            $table->string('title')->nullable();
            $table->string('edition')->nullable();
            $table->string('isbn')->nullable();
            $table->unsignedInteger('publication_id')->nullable();
            $table->date('publication_date')->nullable();
            $table->text('plot_summary')->nullable();
            $table->string('language')->nullable();
            $table->unsignedInteger('sector_id')->nullable();
            $table->unsignedInteger('subject_id')->nullable();
            $table->unsignedInteger('genre_id')->nullable();
            $table->string('stock_format')->nullable();
            $table->unsignedInteger('pages')->nullable();
            $table->string('dimension')->nullable();
            $table->string('dewey')->nullable();
            $table->string('country')->nullable();

            $table->string('editor')->nullable();
            $table->string('region')->nullable();
            $table->string('volume')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('stocks', function (Blueprint $table) {
            //
        });
    }
}
