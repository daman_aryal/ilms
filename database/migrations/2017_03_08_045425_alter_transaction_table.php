<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTransactionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
         Schema::table('transactions', function (Blueprint $table) {
            $table->string('location_id')->nullable()->after('renew_to');
            $table->string('comp_name')->nullable()->after('location_id');
            $table->string('mac_address')->nullable()->after('comp_name');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
         Schema::table('transactions', function($table) {
        $table->dropColumn('comp_name');
        $table->dropColumn('location_id');
        $table->dropColumn('mac_address');
        });
    }
}
