<?php

use App\StockType;
use Illuminate\Database\Seeder;

class StockTypes extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       $stockType = [
	    	[
	    	'id'=>1,
	    	'type' => 'Book'
	    	],[
	    	'id'=>2,
	    	'type' => 'CD'
	    	],
    	];

		/**
		 * [BadMethodCallException] 
		 * This cache store does not support tagging
		 * @https://github.com/Zizaco/entrust/issues/468
		**/              

		DB::table('stock_types')->delete();
		foreach ($stockType as $key => $value) {
			StockType::create($value);
		}
    }
}
