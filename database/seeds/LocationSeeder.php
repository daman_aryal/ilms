<?php

use Illuminate\Database\Seeder;
use App\Location;
class LocationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $locations = [
             [
               'id'=>1,
               'title' => 'central',
               'description' => 'This is central library.',
               'status' => '1',
               'slug' => 'central-0',
             ],
             [
             	'id' => 2,
                'title' => 'hospital',
               'description' => 'This is hospital library.',
               'status' => '1',
               'slug' => 'hospital-0',
             ],
         ];

        DB::table('locations')->delete();
   			foreach ($locations as $key => $location) {
           	Location::create($location);
        }
    }
}
