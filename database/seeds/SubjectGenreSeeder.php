<?php

use Illuminate\Database\Seeder;
use App\Subject;
use App\Genre;

class SubjectGenreSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
     public function run()
     {
         $subjects = [
             [
               'id'=>1,
               'subject_name' => 'English',
             ],
             [
               'id'=>2,
               'subject_name' => 'Physics',
             ],
             [
               'id'=>3,
               'subject_name' => 'Chemistry',
             ],
             [
               'id'=>4,
               'subject_name' => 'Biology',
             ],
             [
               'id'=>5,
               'subject_name' => 'Zoology',
             ],
             [
               'id'=>6,
               'subject_name' => 'Computer Network',
             ]
         ];

         $genres = [
             [
               'id'=>1,
               'genre_name' => 'Sports',
             ],
             [
               'id'=>2,
               'genre_name' => 'Programming',
             ],
             [
               'id'=>3,
               'genre_name' => 'Human Biology',
             ],
             [
               'id'=>4,
               'genre_name' => 'Psychology',
             ],
             [
               'id'=>5,
               'genre_name' => 'Anthropology',
             ],
             [
               'id'=>6,
               'genre_name' => 'Human Brains',
             ]
         ];

 		/**
 		 * [BadMethodCallException]
 		 * This cache store does not support tagging
 		 * @https://github.com/Zizaco/entrust/issues/468
 		**/

   	  DB::table('subjects')->delete();
   		foreach ($subjects as $key => $subject) {
           Subject::create($subject);
         }

     DB::table('genres')->delete();
     foreach ($genres as $key => $genre) {
         Genre::create($genre);
       }


   }
}
