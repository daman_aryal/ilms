<?php

use App\Role;
use App\UserType;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = [
            [
              'id'=>1,
              'name' => 'su',
              'display_name' => 'Super User',
              'description' => 'Super user have all permission'
            ],
            [
              'id'=>2,
              'name' => 'admin',
              'display_name' => 'Admin',
              'description' => 'admin can mgmt libarian, student, vendor/supplier'
            ],
            [
              'id'=>3,
              'name' => 'librarian',
              'display_name' => 'Librarian',
              'description' => 'Librarian can mgmt user a/c, stock mgmt'
            ],
            [
              'id'=>4,
              'name' => 'student',
              'display_name' => 'Student',
              'description' => 'student can search borrow book'
            ],
            [
              'id'=>5,
              'name' => 'vendor',
              'display_name' => 'Vendor',
              'description' => 'vendor provide stock'
            ]
        ];
		
		/**
		 * [BadMethodCallException] 
		 * This cache store does not support tagging
		 * @https://github.com/Zizaco/entrust/issues/468
		**/              

  	  DB::table('user_type')->delete();
  		foreach ($role as $key => $value) {
          UserType::create($value);
        }

   
  }
}
