<?php

use Illuminate\Database\Seeder;
use App\Permission;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permission = [
        	[
                'name' => 'user-create',
        		'display_name' => 'Create User',
        		'description' => 'can create user',
                'catagory' => 'user-catagory'
        	],
        	[
        		'name' => 'user-edit',
        		'display_name' => 'Edit User',
        		'description' => 'can edit the user',
                'catagory' => 'user-catagory'
        	],
        	[
        		'name' => 'user-delete',
        		'display_name' => 'Delete user',
        		'description' => 'can delete the user',
                'catagory' => 'user-catagory'
        	],
            [
                'name' => 'user-show',
                'display_name' => 'Show the admin-user list',
                'description' => 'can see the admin-user list',
                'catagory' => 'admin-user-catagory'
            ],
            // admin-user-category
            [
                'name' => 'admin-user-create',
                'display_name' => 'Create admin-user',
                'description' => 'can create admin-user',
                'catagory' => 'admin-user-catagory'
            ],
            [
                'name' => 'admin-user-edit',
                'display_name' => 'Edit admin-user',
                'description' => 'can edit the admin-user',
                'catagory' => 'admin-user-catagory'
            ],
            [
                'name' => 'admin-user-delete',
                'display_name' => 'Delete admin-user',
                'description' => 'can delete the admin-user',
                'catagory' => 'admin-user-catagory'
            ],
            [
                'name' => 'admin-user-show',
                'display_name' => 'Show the admin-user list',
                'description' => 'can see the admin-user list',
                'catagory' => 'admin-user-catagory'
            ],
            /*role define*/
            [
                'name' => 'role-create',
                'display_name' => 'Create role',
                'description' => 'can create role',
                'catagory' => 'role-catagory'
            ],
            [
                'name' => 'role-edit',
                'display_name' => 'Edit role',
                'description' => 'can edit the role',
                'catagory' => 'role-catagory'
            ],
            [
                'name' => 'role-delete',
                'display_name' => 'Delete role',
                'description' => 'can delete the role',
                'catagory' => 'role-catagory'
            ],
            [
                'name' => 'role-show',
                'display_name' => 'Show the user role',
                'description' => 'can see the user role',
                'catagory' => 'role-catagory'
            ],
            /*stock permission*/
            [
                'name' => 'stock-create',
                'display_name' => 'Create stock',
                'description' => 'can create stock',
                'catagory' => 'stock-catagory'
            ],
            [
                'name' => 'stock-edit',
                'display_name' => 'Edit stock',
                'description' => 'can edit the stock',
                'catagory' => 'stock-catagory'
            ],
            [
                'name' => 'stock-delete',
                'display_name' => 'Delete stock',
                'description' => 'can delete the stock',
                'catagory' => 'stock-catagory'
            ],
            [
                'name' => 'stock-show',
                'display_name' => 'Show the stock list',
                'description' => 'can see the stock list',
                'catagory' => 'stock-catagory'
            ],
            /*request rule define*/
            [
                'name' => 'request-create',
                'display_name' => 'Create request',
                'description' => 'can create request',
                'catagory' => 'request-catagory'
            ],
            [
                'name' => 'request-edit',
                'display_name' => 'Edit request',
                'description' => 'can edit the request',
                'catagory' => 'request-catagory'
            ],
            [
                'name' => 'request-delete',
                'display_name' => 'Delete request',
                'description' => 'can delete the request',
                'catagory' => 'request-catagory'
            ],
            [
                'name' => 'request-show',
                'display_name' => 'Show the request',
                'description' => 'can see the request',
                'catagory' => 'request-catagory'
            ],
        ];

        
		/**
		 * [BadMethodCallException] 
		 * This cache store does not support tagging
		 * @https://github.com/Zizaco/entrust/issues/468
		**/                 
  		
        DB::table('permissions')->delete();
        foreach ($permission as $key => $value) {
           Permission::create($value);
        }
    }
}