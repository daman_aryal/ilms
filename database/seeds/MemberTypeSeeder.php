<?php

use Illuminate\Database\Seeder;

class MemberTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $memberType = [
        			[
        				'id' => 1,
        				'title' => 'Staff',
        				'code' => 'SF',
        				'slug' => 'staff-0',
        				'description' => 'This is staff.',
        				'status' => 1,
        			],
        			[
        				'id' => 2,
        				'title' => 'Student',
        				'code' => 'ST',
        				'slug' => 'student-0',
        				'description' => 'This is student.',
        				'status' => 1,
        			],
        			[
        				'id' => 3,
        				'title' => 'Teacher',
        				'code' => 'TE',
        				'slug' => 'teacher-0',
        				'description' => 'This is Teacher Description.',
        				'status' => 1,
        			],
        			[
        				'id' => 4,
        				'title' => 'Depatment',
        				'code' => 'DP',
        				'slug' => 'department-0',
        				'description' => 'This is Department.',
        				'status' => 1,
        			],
        ];

        DB::table('member_type')->delete();
    	foreach ($memberType as $key => $value) {
       		App\MemberType::create($value);
   		}

   		$policies = [
   				[
   						'id' => 1,
   						'no_of_days' => 10,
   						'no_of_books' => 5,
   						'per_day_for_week' => 2,
   						'per_day_more_than_week' => 5,
   						'description' => '10 days 5 books Rs. 2/day for 1 week and Rs. 5/day for more than 1 week',
              'grace_days'=>0
   				],
   				[
   						'id' => 2,
   						'no_of_days' => 10,
   						'no_of_books' => 2,
   						'per_day_for_week' => 2,
   						'per_day_more_than_week' => 5,
   						'description' => '10 days 2 books Rs. 2/day for 1 week and Rs. 5/day for more than 1 week',
              'grace_days'=>1
   				],
   				[
   						'id' => 3,
   						'no_of_days' => 30,
   						'no_of_books' => 5,
   						'per_day_for_week' => 2,
   						'per_day_more_than_week' => 5,
   						'description' => '30 days 5 books Rs. 2/day for 1 week and Rs. 5/day for more than 1 week',
              'grace_days'=>2
   				],
   				[
   						'id' => 4,
   						'no_of_days' => 180,
   						'no_of_books' => 50,
   						'per_day_for_week' => 2,
   						'per_day_more_than_week' => 5,
   						'description' => '180 days 50 books Rs. 2/day for 1 week and Rs. 5/day for more than 1 week',
              'grace_days'=>3
   				],

   		];

      DB::table('policies')->delete();
    	foreach ($policies as $key => $value) {
       		App\Policy::create($value);
   		}

      $memberTypePolicy = [
          [
              'id' => 1,
              'member_type_id' => 1,
              'policy_id' => 2,
          ],
          [
              'id' => 2,
              'member_type_id' => 2,
              'policy_id' => 1,
          ],
          [
              'id' => 3,
              'member_type_id' => 3,
              'policy_id' => 3,
          ],
          [
              'id' => 4,
              'member_type_id' => 4,
              'policy_id' => 4,
          ],

      ];

      DB::table('member_type_policies')->delete();
      foreach ($memberTypePolicy as $key => $value) {
          App\MemberTypePolicy::create($value);
      }


    }
}
