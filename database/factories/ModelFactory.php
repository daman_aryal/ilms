<?php

DB::table('stock_types')->delete();
$factory->define(App\StockType::class, function (Faker\Generator $faker) {
   	return [
        'type' => $faker->word,
    ];
});

$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
        'address' => $faker->address,
        'contact' => $faker->phoneNumber,
        'active' => $faker->boolean,
        'first_login' => $faker->boolean,
    ];
});

//Publication
DB::table('publications')->delete();
$factory->define(App\Publication::class, function (Faker\Generator $faker) {
    return [
        'publication_name' =>$faker->name,
    ];
});
