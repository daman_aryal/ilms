@inject('role_form', 'App\Injection\RoleForm')

<div class="row leftmargin">
   <div class="col-xs-12 col-sm-12 col-md-12">
      <div class="form-group form-md-line-input">
         <label class="col-md-2 control-label" for="form_control_1"> Role Name </label>
         <div class="col-md-10">
            @if($role)
               {!! Form::text('name', null, array('placeholder' => 'Name','class' => 'form-control','disabled' =>  true)) !!}
            @else
               {!! Form::text('name', null, array('placeholder' => 'Name','class' => 'form-control')) !!}
            @endif
            <div class="form-control-focus"> </div>
         </div>
      </div>
   </div>
   <div class="col-xs-12 col-sm-12 col-md-12">
      <div class="form-group form-md-line-input">
         <label class="col-md-2 control-label" for="form_control_1">Display Name</label>
         <div class="col-md-10">
            {!! Form::text('display_name', null, array('placeholder' => 'Display Name','class' => 'form-control')) !!}
            <div class="form-control-focus"> </div>
         </div>
      </div>
   </div>
   <div class="col-xs-12 col-sm-12 col-md-12">
      <div class="form-group form-md-line-input">
         <label class="col-md-2 control-label" for="form_control_1">Description</label>
         <div class="col-md-10">
            {!! Form::textarea('description', null, array('placeholder' => 'Description','class' => 'form-control','style'=>'height:100px')) !!}
            <div class="form-control-focus"> </div>
         </div>
      </div>
   </div>
   <div class="col-xs-12 col-sm-12 col-md-12">
      <hr>
      <div class="form-group">
         <div class="row">
            {{-- User model --}}
            <div class="col-md-3">
               <div class="form-group form-md-checkboxes">
                  <label class="col-md-12 control-label textalign" for="form_control_1"> <strong>User</strong> </label>
                  @foreach($role_form->user_model() as $id => $value)
                  <div class="col-md-12">
                     <div class="md-checkbox-list">
                        <div class="md-checkbox">
                           @if(isset($id) && isset($role_id))    
                           {{ Form::checkbox('permission[]', $id, $role_form->checked_permission($id, $role_id)? true :false, array('id'=>'checkbox1_'.$id, 'class' => 'md-check')) }}    
                           @else
                           {{ Form::checkbox('permission[]', $id,false, array('id'=>'checkbox1_'.$id, 'class' => 'md-check')) }}
                           @endif
                           <label for="checkbox1_{{$id}}">
                              <span></span>
                              <span class="check"></span>
                              <span class="box"></span> {{$value}} 
                           </label>
                        </div>
                     </div>
                  </div>
                  @endforeach
               </div>
            </div>
            {{-- Role model --}}
            <div class="col-md-3">
               <div class="form-group form-md-checkboxes">
                  <label class="col-md-12 control-label textalign" for="form_control_1"> <strong>Role</strong> </label>
                  @foreach($role_form->role_model() as $id => $value)
                  <div class="col-md-12">
                     <div class="md-checkbox-list">
                        <div class="md-checkbox">
<<<<<<< HEAD
                            @if(isset($id) && isset($role_id))    
                            {{ Form::checkbox('permission[]', $id, $role_form->checked_permission($id, $role_id)? true : false, array('id'=>'checkbox_'.$id, 'class' => 'md-check')) }}    
                            @else
                            {{ Form::checkbox('permission[]', $id,false, array('id'=>'checkbox1_'.$id, 'class' => 'md-check')) }}
                            @endif
                            <label for="checkbox1_{{$id}}">
                                <span></span>
                                <span class="check"></span>
                                <span class="box"></span> {{$value}} </label>
                            </div>
=======
                           @if(isset($id) && isset($role_id))    
                           {{ Form::checkbox('permission[]', $id, $role_form->checked_permission($id, $role_id)? true :false, array('id'=>'checkbox1_'.$id, 'class' => 'md-check')) }}    
                           @else
                           {{ Form::checkbox('permission[]', $id,false, array('id'=>'checkbox1_'.$id, 'class' => 'md-check')) }}
                           @endif
                           <label for="checkbox1_{{$id}}">
                              <span></span>
                              <span class="check"></span>
                              <span class="box"></span> {{$value}} 
                           </label>
>>>>>>> 3048c40f61220d557f360aa7074850b435f91197
                        </div>
                     </div>
                  </div>
                  @endforeach
               </div>
            </div>
            {{-- stock model --}}
            <div class="col-md-3">
               <div class="form-group form-md-checkboxes">
                  <label class="col-md-12 control-label textalign" for="form_control_1"> <strong>Stock</strong> </label>
                  @foreach($role_form->stock_model() as $id => $value)
                  <div class="col-md-12">
                     <div class="md-checkbox-list">
                        <div class="md-checkbox">
                           @if(isset($id) && isset($role_id))    
                           {{ Form::checkbox('permission[]', $id, $role_form->checked_permission($id, $role_id)? true :false, array('id'=>'checkbox1_'.$id, 'class' => 'md-check')) }}    
                           @else
                           {{ Form::checkbox('permission[]', $id,false, array('id'=>'checkbox1_'.$id, 'class' => 'md-check')) }}
                           @endif
                           <label for="checkbox1_{{$id}}">
                              <span></span>
                              <span class="check"></span>
                              <span class="box"></span> {{$value}} 
                           </label>
                        </div>
                     </div>
                  </div>
                  @endforeach
               </div>
            </div>
            {{-- request model --}}
            <div class="col-md-3">
               <div class="form-group form-md-checkboxes">
                  <label class="col-md-12 control-label textalign" for="form_control_1"> <strong>Request</strong> </label>
                  @foreach($role_form->request_model() as $id => $value)
                  <div class="col-md-12">
                     <div class="md-checkbox-list">
                        <div class="md-checkbox">
                           @if(isset($id) && isset($role_id))    
                           {{ Form::checkbox('permission[]', $id, $role_form->checked_permission($id, $role_id)? true :false, array('id'=>'checkbox1_'.$id, 'class' => 'md-check')) }}    
                           @else
                           {{ Form::checkbox('permission[]', $id,false, array('id'=>'checkbox1_'.$id, 'class' => 'md-check')) }}
                           @endif
                           <label for="checkbox1_{{$id}}">
                              <span></span>
                              <span class="check"></span>
                              <span class="box"></span> {{$value}} 
                           </label>
                        </div>
                     </div>
                  </div>
                  @endforeach
               </div>
            </div>
         </div>
         <hr>
         <div class="row">
            <div class="col-md-12">
               <div class="form-group form-md-radios typecontainerinner">
                  <div class="portlet light bordered typecontainer">
                     <label> User Type </label>
                     <div class="md-radio-inline">

                        @foreach($role_form->user_type() as $value)

                        <div class="md-radio">
                           @if(isset($id) && isset($role_id))
                           @if ($value->id == $role->user_type_id)
                           {{ Form::radio('user_type', $value->id, $role->user_type_id, array('id'=>'radio'.$value->id, 'class' => 'md-radiobtn','disabled' =>  true)) }}
                           @else
                           {{ Form::radio('user_type', $value->id, false, array('id'=>'radio'.$value->id, 'class' => 'md-radiobtn','disabled' =>  true)) }}   
                           @endif
                           @else
                           {{ Form::radio('user_type', $value->id, false, array('id'=>'radio'.$value->id, 'class' => 'md-radiobtn')) }}
                           @endif
                           <label for="radio{{$value->id}}">
                              <span></span>
                              <span class="check"></span>
                              <span class="box"></span> {{ $value->display_name }}
                           </label>
                        </div> 
                        @endforeach
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="col-xs-12 col-sm-12 col-md-12">
            <button type="submit" class="btn blue">Save</button>
            <a href="{{ url('/role') }}" class="btn btn-default">Cancel</a>
         </div>
      </div>
   </div>
</div>