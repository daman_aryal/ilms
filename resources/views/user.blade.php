@extends('layout')

@section('title','User Dashboard')


@section('headercss')
    <link href="../assets/layouts/layout3/css/layout.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/layouts/layout3/css/themes/default.min.css" rel="stylesheet" type="text/css" id="style_color" />
    <link href="../assets/layouts/layout3/css/custom.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/pages/css/search.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/global/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../css/normalize.css">
    <link rel="stylesheet" href="../css/main.css">
     <link rel="stylesheet" href="../css/owl.carousel.css">
    <link rel="stylesheet" href="../css/owl.theme.css">
    <link rel="stylesheet" href="../css/owl.transitions.css">
@endsection

@section('body')
@include('partials.user.header')
<!-- BEGIN CONTAINER -->
<div class="page-container">
        <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <!-- BEGIN PAGE CONTENT BODY -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE BREADCRUMBS -->
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <a href="index.html">Home</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <span>Dashboard</span>
                    </li>
                </ul>
                <!-- END PAGE BREADCRUMBS -->
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="page-content-inner">
                    <div class="search-page search-content-4">
                        <div class="row">
                            <div class="search-bar bordered bottommargin searchbox_user">
                                <div class="row">
                                    <div class="col-lg-8">
                                        <div class="input-group">
                                            <input class="button user_search" placeholder="I am Looking for ... " />
                                        </div>
                                    </div>
                                    <div class="col-lg-4 extra-buttons">
                                        <button id="advsearch" class="btn grey-cararra font-blue btncolor pull-right advdesign" type="button">Advanced Search</button>
                                    </div>
                                </div>
                                <div id="advoption" class="row advmargin" style="display:none">
                                    <div class="col-md-3">
                                        <label for="select2-single-input-sm" class="control-label">Filter By</label>
                                        <select id="select2-single-input-sm" class="form-control input-sm select2-multiple boarderradius">
                                                <option>All</option>
                                                <option>Featured</option>
                                                <option>Most Popular</option>
                                                <option>Top Rated</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <label for="select2-single-input-sm" class="control-label">Author</label>
                                        <select id="select2-single-input-sm" class="form-control input-sm select2-multiple boarderradius">
                                            <option value="AZ">Yug Pathak</option>
                                            <option value="CO">Jitendra Sahayogee</option>
                                            <option value="ID">Gobinda Mainali</option>
                                            <option value="MT">Siddhicharan</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <label for="select2-single-input-sm" class="control-label">Publication</label>
                                        <select id="select2-single-input-sm" class="form-control input-sm select2-multiple boarderradius">
                                            <option value="AZ">Kathalaya Publications Pvt. Ltd</option>
                                            <option value="CO">Srijana International Publication Pvt. Ltd.</option>
                                            <option value="ID">IdahoKAMANA NEWS PUBLICATIONS PVT. LTD.</option>
                                            <option value="MT">Readmore Publishers and Distributors</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row spacing">
                        <div class="col-md-6 col-sm-6">  
                            <div class="shelf owned">
                                <ul id="owl-example" class=" books clear owl-carousel">
                                    <li id="html5webdesigners " class="book">
                                        <img class="front" src="img/covers/bookapart-1.jpg" alt="HTML5 for Web Designers">
                                        <div class="back">
                                            <div class="p10">
                                                <div class="rating star-3">
                                                    <ol>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                    </ol>
                                                </div>
                                                <dl>
                                                <dt>Topic: HTML</dt>
                                                </dl>
                                                <p class="description">
                                                    Practical introduction into HTML5. Great examples, very easy to understand.
                                                </p>
                                            </div>
                                        </div>
                                    </li>
                                    <li id="css3webdesigners" class="book">
                                        <img class="front" src="img/covers/bookapart-2.jpg" alt="CSS3 for Web Designers">
                                        <div class="back">
                                            <div class="p10">
                                                <div class="rating star-4">
                                                    <ol>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                    </ol>
                                                </div>
                                                <dl>
                                                    <dt>Topic: CSS3</dt>
                                                </dl>
                                                <p class="description">
                                                    Practical introduction into the very basics of CSS3. Great examples, very easy to understand. Less interesting if you've read Handcrafted CSS before.
                                                </p>

                                            </div>
                                        </div>
                                    </li>
                                    <li id="contentstrategy" class="book">
                                        <img class="front" src="img/covers/bookapart-3.jpg" alt="The Elements of Content Strategy">
                                        <div class="back">
                                            <div class="p10">
                                                <div class="rating star-3">
                                                    <ol>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                    </ol>
                                                </div>
                                                <dl>
                                                    <dt>Topic: Strategy</dt>

                                                </dl>
                                                <p class="description">
                                                    Practical introduction into the very basics of CSS3. Great examples, very easy to understand.
                                                </p>

                                            </div>
                                        </div>
                                    </li>
                                    <li id="responsivedesign" class="book">
                                        <img class="front" src="img/covers/bookapart-4.jpg" alt="Responsive Web Design">
                                        <div class="back">
                                            <div class="p10">
                                                <div class="rating star-5">
                                                    <ol>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                    </ol>
                                                </div>
                                                <dl>
                                                    <dt>Topic: Responsive Design</dt> 

                                                </dl>
                                                <p class="description">
                                                    One of the first books on Responsive Design, and I believe still one of the best i've read! Extremely practical, well explained and many techniques still work.
                                                </p>

                                            </div>
                                        </div>
                                    </li>
                                    <li id="mobilefirst" class="book">
                                        <img class="front" src="img/covers/bookapart-6.jpg" alt="Mobile First">
                                        <div class="back">
                                            <div class="p10">
                                                <div class="rating star-5">
                                                    <ol>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                    </ol>
                                                </div>
                                                <dl>
                                                    <dt>Topic: Mobile First (responsive) Design</dt>

                                                </dl>
                                                <p class="description">
                                                    Another very well written and extremely useful book. It pretty much set a new baseline for the projects I work on nowadays. I recommend reading Responsive Web Design first.
                                                </p>

                                            </div>
                                        </div>
                                    </li>
                                    <li id="responsivedesign" class="book">
                                        <img class="front" src="img/covers/bookapart-4.jpg" alt="Responsive Web Design">
                                        <div class="back">
                                            <div class="p10">
                                                <div class="rating star-5">
                                                    <ol>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                    </ol>
                                                </div>
                                                <dl>
                                                    <dt>Topic: Responsive Design</dt> 

                                                </dl>
                                                <p class="description">
                                                    One of the first books on Responsive Design, and I believe still one of the best i've read! Extremely practical, well explained and many techniques still work.
                                                </p>

                                            </div>
                                        </div>
                                    </li>
                                    <li id="mobilefirst" class="book">
                                        <img class="front" src="img/covers/bookapart-6.jpg" alt="Mobile First">
                                        <div class="back">
                                            <div class="p10">
                                                <div class="rating star-5">
                                                    <ol>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                    </ol>
                                                </div>
                                                <dl>
                                                    <dt>Topic: Mobile First (responsive) Design</dt>

                                                </dl>
                                                <p class="description">
                                                    Another very well written and extremely useful book. It pretty much set a new baseline for the projects I work on nowadays. I recommend reading Responsive Web Design first.
                                                </p>

                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="shelf wanted">
                                <ul id="owl-example1" class=" books clear owl-carousel">
                                    <li class="book">
                                        <img src="img/covers/testable-js.jpeg" alt="Testable Javascript">
                                        <div class="back">
                                            <div class="p10">
                                                <div class="rating star-3">
                                                    <ol>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                    </ol>
                                                </div>
                                                <dl>
                                                    <dt>Topic:</dt>
                                                    <dd>Javascript</dd>
                                                    <dt>Year:</dt>
                                                    <dd>2013</dd>
                                                </dl>
                                                <p class="description">
                                                    Thought this book might be useful. Not very excited though....
                                                </p>
                                                <a href="http://shop.oreilly.com/product/0636920024699.do?code=WKJVS">Visit website</a>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="book">
                                        <img src="img/covers/bookapart-11.jpg" alt="SASS for Web Designers">
                                        <div class="back">
                                            <div class="p10">
                                                <div class="rating star-5">
                                                    <ol>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                    </ol>
                                                </div>
                                                <dl>
                                                    <dt>Topic:</dt>
                                                    <dd>SASS</dd>
                                                    <dt>Year:</dt>
                                                    <dd>2013 (TBA)</dd>
                                                </dl>
                                                <p class="description">
                                                    Really looking forward to this book! I'm a great fan of SASS and can't to read about in this book...
                                                </p>
                                                <a href="http://www.abookapart.com/products/sass-for-web-designers">Visit website</a>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="book">
                                        <img src="img/covers/testable-js.jpeg" alt="Testable Javascript">
                                        <div class="back">
                                            <div class="p10">
                                                <div class="rating star-3">
                                                    <ol>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                    </ol>
                                                </div>
                                                <dl>
                                                    <dt>Topic:</dt>
                                                    <dd>Javascript</dd>
                                                    <dt>Year:</dt>
                                                    <dd>2013</dd>
                                                </dl>
                                                <p class="description">
                                                    Thought this book might be useful. Not very excited though....
                                                </p>
                                                <a href="http://shop.oreilly.com/product/0636920024699.do?code=WKJVS">Visit website</a>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="book">
                                        <img src="img/covers/bookapart-11.jpg" alt="SASS for Web Designers">
                                        <div class="back">
                                            <div class="p10">
                                                <div class="rating star-5">
                                                    <ol>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                    </ol>
                                                </div>
                                                <dl>
                                                    <dt>Topic:</dt>
                                                    <dd>SASS</dd>
                                                    <dt>Year:</dt>
                                                    <dd>2013 (TBA)</dd>
                                                </dl>
                                                <p class="description">
                                                    Really looking forward to this book! I'm a great fan of SASS and can't to read about in this book...
                                                </p>
                                                <a href="http://www.abookapart.com/products/sass-for-web-designers">Visit website</a>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="book">
                                        <img src="img/covers/testable-js.jpeg" alt="Testable Javascript">
                                        <div class="back">
                                            <div class="p10">
                                                <div class="rating star-3">
                                                    <ol>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                    </ol>
                                                </div>
                                                <dl>
                                                    <dt>Topic:</dt>
                                                    <dd>Javascript</dd>
                                                    <dt>Year:</dt>
                                                    <dd>2013</dd>
                                                </dl>
                                                <p class="description">
                                                    Thought this book might be useful. Not very excited though....
                                                </p>
                                                <a href="http://shop.oreilly.com/product/0636920024699.do?code=WKJVS">Visit website</a>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="book">
                                        <img src="img/covers/dom-enlightenment.jpg" alt="DOM Enlightenment">
                                        <div class="back">
                                            <div class="p10">
                                                <div class="rating star-3">
                                                    <ol>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                    </ol>
                                                </div>
                                                <dl>
                                                    <dt>Topic:</dt>
                                                    <dd>HTML &amp; JS</dd>
                                                    <dt>Year:</dt>
                                                    <dd>2013</dd>
                                                </dl>
                                                <p class="description">
                                                    Thought this book might be useful. Not very excited though....
                                                </p>
                                                <a href="http://shop.oreilly.com/product/0636920027690.do?code=WKJVS">Visit website</a>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="book">
                                        <img src="img/covers/bookapart-11.jpg" alt="SASS for Web Designers">
                                        <div class="back">
                                            <div class="p10">
                                                <div class="rating star-5">
                                                    <ol>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                    </ol>
                                                </div>
                                                <dl>
                                                    <dt>Topic:</dt>
                                                    <dd>SASS</dd>
                                                    <dt>Year:</dt>
                                                    <dd>2013 (TBA)</dd>
                                                </dl>
                                                <p class="description">
                                                    Really looking forward to this book! I'm a great fan of SASS and can't to read about in this book...
                                                </p>
                                                <a href="http://www.abookapart.com/products/sass-for-web-designers">Visit website</a>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="book">
                                        <img src="img/covers/js-enlightenment.jpg" alt="Javascript Enlightenment">
                                        <div class="back">
                                            <div class="p10">
                                                <div class="rating star-3">
                                                    <ol>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                    </ol>
                                                </div>
                                                <dl>
                                                    <dt>Topic:</dt>
                                                    <dd>Javascript</dd>
                                                    <dt>Year:</dt>
                                                    <dd>2013</dd>
                                                </dl>
                                                <p class="description">
                                                    Thought this book might be useful. Not very excited though....
                                                </p>
                                                <a href="http://shop.oreilly.com/product/0636920027713.do?code=WKJVS">Visit website</a>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="book" >
                                        <img src="img/covers/smacss.png" alt="SMACSS">
                                        <div class="back">
                                            <div class="p10">
                                                <div class="rating star-4">
                                                    <ol>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                    </ol>
                                                </div>
                                                <dl>
                                                    <dt>Topic:</dt>
                                                    <dd>CSS</dd>
                                                    <dt>Year:</dt>
                                                    <dd>2012</dd>
                                                </dl>
                                                <p class="description">
                                                    Already ordered this one :) should be on its way by now!
                                                </p>
                                                <a href="http://smacss.com/">Visit website</a>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="book">
                                        <img src="img/covers/dom-enlightenment.jpg" alt="DOM Enlightenment">
                                        <div class="back">
                                            <div class="p10">
                                                <div class="rating star-3">
                                                    <ol>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                    </ol>
                                                </div>
                                                <dl>
                                                    <dt>Topic:</dt>
                                                    <dd>HTML &amp; JS</dd>
                                                    <dt>Year:</dt>
                                                    <dd>2013</dd>
                                                </dl>
                                                <p class="description">
                                                    Thought this book might be useful. Not very excited though....
                                                </p>
                                                <a href="http://shop.oreilly.com/product/0636920027690.do?code=WKJVS">Visit website</a>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="shelf owned">
                                <ul id="owl-example2" class=" books clear owl-carousel">
                                    <li id="javascriptguide" class="book" >
                                        <img class="front" src="img/covers/javascript-definitive-guide.jpg" alt="Javascript - The Definitive Guide">
                                        <div class="back">
                                            <div class="p10">
                                                <div class="rating star-3">
                                                    <ol>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                    </ol>
                                                </div>
                                                <dl>
                                                    <dt>Topic:</dt> <dd>Javascript</dd>
                                                    <dt>Year:</dt> <dd>2010</dd>
                                                </dl>
                                                <p class="description">
                                                    Great Javascript reference, especially in the current days where everyone uses jQuery and tends to forget about "native" Javascript.
                                                </p>
                                                <a href="http://www.abookapart.com/products/content-strategy-for-mobile">Visit website</a>
                                            </div>
                                        </div>
                                    </li>
                                    <li id="handcraftedcss" class="book" >
                                        <img class="front" src="img/covers/handcraftedcss.jpg" alt="Handcrafted CSS">
                                        <div class="back">
                                            <div class="p10">
                                                <div class="rating star-4">
                                                    <ol>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                    </ol>
                                                </div>
                                                <dl>
                                                    <dt>Topic:</dt> <dd>CSS</dd>
                                                    <dt>Year:</dt>  <dd>2010</dd>
                                                </dl>
                                                <p class="description">
                                                    Great book on CSS. Same author as the previous mentioned CSS for Webdesigners, and the book has the same qualities! 
                                                </p>
                                                <a href="http://www.handcraftedcss.com/">Visit website</a>
                                            </div>
                                        </div>
                                    </li>
                                    <li id="javascriptguide" class="book" >
                                        <img class="front" src="img/covers/javascript-definitive-guide.jpg" alt="Javascript - The Definitive Guide">
                                        <div class="back">
                                            <div class="p10">
                                                <div class="rating star-3">
                                                    <ol>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                    </ol>
                                                </div>
                                                <dl>
                                                    <dt>Topic:</dt> <dd>Javascript</dd>
                                                    <dt>Year:</dt> <dd>2010</dd>
                                                </dl>
                                                <p class="description">
                                                    Great Javascript reference, especially in the current days where everyone uses jQuery and tends to forget about "native" Javascript.
                                                </p>
                                                <a href="http://www.abookapart.com/products/content-strategy-for-mobile">Visit website</a>
                                            </div>
                                        </div>
                                    </li>
                                    <li id="html5canvasgame" class="book"  >
                                        <img class="front" src="img/covers/html5-canvas.jpg" alt="HTML5 Canvas - For Games and Entertainment">
                                        <div class="back">
                                            <div class="p10">
                                                <div class="rating star-3">
                                                    <ol>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                    </ol>
                                                </div>
                                                <dl>
                                                    <dt>Topic:</dt> <dd>CANVAS</dd>
                                                    <dt>Year:</dt>  <dd>2011</dd>
                                                </dl>
                                                <p class="description">

                                                </p>
                                                <a href="http://www.link.com/">Visit website</a>
                                            </div>
                                        </div>
                                    </li>
                                    <li id="dontmakemethink" class="book" >
                                        <img src="img/covers/dont-make-me-think.jpg" alt="Don&quot;t make me think">
                                        <div class="back">
                                            <div class="p10">
                                                <div class="rating star-5">
                                                    <ol>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                    </ol>
                                                </div>
                                                <dl>
                                                    <dt>Topic:</dt>
                                                    <dd>Usability</dd>
                                                    <dt>Year:</dt>
                                                    <dd>2006</dd>
                                                </dl>
                                                <p class="description">
                                                    An absolute must-read for any developer and designer!
                                                </p>
                                                <a href="http://www.link.com">Visit website</a>
                                            </div>
                                        </div>
                                    </li>
                                    <li id="javascriptguide" class="book" >
                                        <img class="front" src="img/covers/javascript-definitive-guide.jpg" alt="Javascript - The Definitive Guide">
                                        <div class="back">
                                            <div class="p10">
                                                <div class="rating star-3">
                                                    <ol>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                    </ol>
                                                </div>
                                                <dl>
                                                    <dt>Topic:</dt> <dd>Javascript</dd>
                                                    <dt>Year:</dt> <dd>2010</dd>
                                                </dl>
                                                <p class="description">
                                                    Great Javascript reference, especially in the current days where everyone uses jQuery and tends to forget about "native" Javascript.
                                                </p>
                                                <a href="http://www.abookapart.com/products/content-strategy-for-mobile">Visit website</a>
                                            </div>
                                        </div>
                                    </li>
                                    <li id="dontmakemethink" class="book" >
                                        <img src="img/covers/dont-make-me-think.jpg" alt="Don&quot;t make me think">
                                        <div class="back">
                                            <div class="p10">
                                                <div class="rating star-5">
                                                    <ol>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                    </ol>
                                                </div>
                                                <dl>
                                                    <dt>Topic:</dt>
                                                    <dd>Usability</dd>
                                                    <dt>Year:</dt>
                                                    <dd>2006</dd>
                                                </dl>
                                                <p class="description">
                                                    An absolute must-read for any developer and designer!
                                                </p>
                                                <a href="http://www.link.com">Visit website</a>
                                            </div>
                                        </div>
                                    </li>
                                    <li id="html5canvasgame" class="book"  >
                                        <img class="front" src="img/covers/html5-canvas.jpg" alt="HTML5 Canvas - For Games and Entertainment">
                                        <div class="back">
                                            <div class="p10">
                                                <div class="rating star-3">
                                                    <ol>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                    </ol>
                                                </div>
                                                <dl>
                                                    <dt>Topic:</dt> <dd>CANVAS</dd>
                                                    <dt>Year:</dt>  <dd>2011</dd>
                                                </dl>
                                                <p class="description">

                                                </p>
                                                <a href="http://www.link.com/">Visit website</a>
                                            </div>
                                        </div>
                                    </li>
                                    <li id="dontmakemethink" class="book" >
                                        <img src="img/covers/dont-make-me-think.jpg" alt="Don&quot;t make me think">
                                        <div class="back">
                                            <div class="p10">
                                                <div class="rating star-5">
                                                    <ol>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                    </ol>
                                                </div>
                                                <dl>
                                                    <dt>Topic:</dt>
                                                    <dd>Usability</dd>
                                                    <dt>Year:</dt>
                                                    <dd>2006</dd>
                                                </dl>
                                                <p class="description">
                                                    An absolute must-read for any developer and designer!
                                                </p>
                                                <a href="http://www.link.com">Visit website</a>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="shelf owned">
                                <ul id="owl-example3" class=" books clear owl-carousel">
                                    <li id="html5webdesigners " class="book">
                                        <img class="front" src="img/covers/bookapart-1.jpg" alt="HTML5 for Web Designers">
                                        <div class="back">
                                            <div class="p10">
                                                <div class="rating star-3">
                                                    <ol>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                    </ol>
                                                </div>
                                                <dl>
                                                <dt>Topic: HTML</dt>
                                                </dl>
                                                <p class="description">
                                                    Practical introduction into HTML5. Great examples, very easy to understand.
                                                </p>
                                            </div>
                                        </div>
                                    </li>
                                    <li id="css3webdesigners" class="book">
                                        <img class="front" src="img/covers/bookapart-2.jpg" alt="CSS3 for Web Designers">
                                        <div class="back">
                                            <div class="p10">
                                                <div class="rating star-4">
                                                    <ol>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                    </ol>
                                                </div>
                                                <dl>
                                                    <dt>Topic: CSS3</dt>
                                                </dl>
                                                <p class="description">
                                                    Practical introduction into the very basics of CSS3. Great examples, very easy to understand. Less interesting if you've read Handcrafted CSS before.
                                                </p>

                                            </div>
                                        </div>
                                    </li>
                                    <li id="contentstrategy" class="book">
                                        <img class="front" src="img/covers/bookapart-3.jpg" alt="The Elements of Content Strategy">
                                        <div class="back">
                                            <div class="p10">
                                                <div class="rating star-3">
                                                    <ol>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                    </ol>
                                                </div>
                                                <dl>
                                                    <dt>Topic: Strategy</dt>

                                                </dl>
                                                <p class="description">
                                                    Practical introduction into the very basics of CSS3. Great examples, very easy to understand.
                                                </p>

                                            </div>
                                        </div>
                                    </li>
                                    <li id="responsivedesign" class="book">
                                        <img class="front" src="img/covers/bookapart-4.jpg" alt="Responsive Web Design">
                                        <div class="back">
                                            <div class="p10">
                                                <div class="rating star-5">
                                                    <ol>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                    </ol>
                                                </div>
                                                <dl>
                                                    <dt>Topic: Responsive Design</dt> 

                                                </dl>
                                                <p class="description">
                                                    One of the first books on Responsive Design, and I believe still one of the best i've read! Extremely practical, well explained and many techniques still work.
                                                </p>

                                            </div>
                                        </div>
                                    </li>
                                    <li id="mobilefirst" class="book">
                                        <img class="front" src="img/covers/bookapart-6.jpg" alt="Mobile First">
                                        <div class="back">
                                            <div class="p10">
                                                <div class="rating star-5">
                                                    <ol>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                    </ol>
                                                </div>
                                                <dl>
                                                    <dt>Topic: Mobile First (responsive) Design</dt>

                                                </dl>
                                                <p class="description">
                                                    Another very well written and extremely useful book. It pretty much set a new baseline for the projects I work on nowadays. I recommend reading Responsive Web Design first.
                                                </p>

                                            </div>
                                        </div>
                                    </li>
                                    <li id="responsivedesign" class="book">
                                        <img class="front" src="img/covers/bookapart-4.jpg" alt="Responsive Web Design">
                                        <div class="back">
                                            <div class="p10">
                                                <div class="rating star-5">
                                                    <ol>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                    </ol>
                                                </div>
                                                <dl>
                                                    <dt>Topic: Responsive Design</dt> 

                                                </dl>
                                                <p class="description">
                                                    One of the first books on Responsive Design, and I believe still one of the best i've read! Extremely practical, well explained and many techniques still work.
                                                </p>

                                            </div>
                                        </div>
                                    </li>
                                    <li id="mobilefirst" class="book">
                                        <img class="front" src="img/covers/bookapart-6.jpg" alt="Mobile First">
                                        <div class="back">
                                            <div class="p10">
                                                <div class="rating star-5">
                                                    <ol>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                    </ol>
                                                </div>
                                                <dl>
                                                    <dt>Topic: Mobile First (responsive) Design</dt>

                                                </dl>
                                                <p class="description">
                                                    Another very well written and extremely useful book. It pretty much set a new baseline for the projects I work on nowadays. I recommend reading Responsive Web Design first.
                                                </p>

                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <!-- BEGIN PORTLET-->
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="portlet light bordered">
                                        <div class="portlet-title">
                                            <div class="caption">
                                                <i class="icon-cursor font-purple"></i>
                                                <span class="caption-subject font-purple bold uppercase">General Stats</span>
                                            </div>
                                            <div class="actions">
                                                <a href="javascript:;" class="btn btn-sm btn-circle red easy-pie-chart-reload">
                                                    <i class="fa fa-repeat"></i> Reload </a>
                                            </div>
                                        </div>
                                        <div class="portlet-body">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="easy-pie-chart">
                                                        <div class="number transactions" data-percent="55">
                                                            <span>+55</span>% </div>
                                                        <a class="title" href="javascript:;"> Transactions
                                                            <i class="icon-arrow-right"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="margin-bottom-10 visible-sm"> </div>
                                                <div class="col-md-4">
                                                    <div class="easy-pie-chart">
                                                        <div class="number visits" data-percent="85">
                                                            <span>+85</span>% </div>
                                                        <a class="title" href="javascript:;"> New Visits
                                                            <i class="icon-arrow-right"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="margin-bottom-10 visible-sm"> </div>
                                                <div class="col-md-4">
                                                    <div class="easy-pie-chart">
                                                        <div class="number bounce" data-percent="46">
                                                            <span>-46</span>% </div>
                                                        <a class="title" href="javascript:;"> Bounce
                                                            <i class="icon-arrow-right"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div> 
                                </div>
                                <div class="col-md-12">
                                    <div class="portlet light calendar bordered">
                                        <div class="portlet-title ">
                                            <div class="caption">
                                                <i class="icon-calendar font-green-sharp"></i>
                                                <span class="caption-subject font-green-sharp bold uppercase">Feeds</span>
                                            </div>
                                        </div>
                                        <div class="portlet-body">
                                            <div id="calendar"> </div>
                                        </div>
                                    </div>
                                </div>
                            </div> 
                        </div> 
                    </div>
                </div>    
            </div>
            <!-- END PAGE CONTENT INNER -->
        </div>
    </div>
        <!-- END PAGE CONTENT BODY -->
        <!-- END CONTENT BODY -->
</div>
@include('partials.user.footer')
@endsection

@section('footerscript')
<!-- BEGIN THEME LAYOUT SCRIPTS -->
<script src="../assets/layouts/layout3/scripts/layout.min.js" type="text/javascript"></script>
<script src="../assets/layouts/layout3/scripts/demo.min.js" type="text/javascript"></script>
<script src="../js/owl.carousel.js" type="text/javascript"></script>
<!-- END THEME LAYOUT SCRIPTS -->

@endsection