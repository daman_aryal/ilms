<div class="row">

	<div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group form-md-line-input">
            <label class="col-md-2 control-label" for="form_control_1">Days :</label>
            <div class="col-md-7">
               {!! Form::text('days', null, array('placeholder' => 'Days','class' => 'form-control')) !!}
                <div class="form-control-focus"> </div>
            </div>
            <label class="col-md-1 control-label" for="form_control_1">Active :</label>
            <div class="col-md-2">
              {{ Form::checkbox('status', 1, old('status'), [ 'class' => 'make-switch', 'data-size' => 'make-switch' ] ) }}
            </div>
       </div>
   </div>

   <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group form-md-line-input">
            <label class="col-md-2 control-label" for="form_control_1">Books :</label>
            <div class="col-md-10">
            {!! Form::text('books', null, array('placeholder' => 'Books','class' => 'form-control')) !!}
            <div class="form-control-focus"> </div>
           </div>
       </div>
   </div>
   <div class="clearfix"></div>
   <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group form-md-line-input">
            <label class="col-md-2 control-label" for="form_control_1">Fine :</label>
            <div class="col-md-10">
            {!! Form::text('description',null, array('placeholder' => 'Description','class' => 'form-control', 'rows'=>'3')) !!}
            <div class="form-control-focus"> </div>
           </div>
       </div>
   </div>


    <div class="col-xs-12 col-sm-12 col-md-12 text-right" style="padding-top: 25px;">
	<button type="submit" class="btn btn-primary">Submit</button>
  </div>
</div>