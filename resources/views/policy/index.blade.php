@extends('adminMaster')
@section('title','Policy')
@section('headerscript')
<style>
	.float{
		position:fixed;
		width:60px;
		height:60px;
		bottom:40px;
		right:40px;
		background-color:#3fd5c0;
		color:#FFF;
		border-radius:50px !important;
		text-align:center;
		z-index: 9999;
	}
	.float:hover{

		transition: all 1s;
		transform: translateZ(10px);

    transform: rotateY(-180deg);
		box-shadow: 0 14px 28px rgba(0,0,0,0.25), 0 10px 10px rgba(0,0,0,0.22);
		color:#fff;
	}

	.my-float{
		font-size: 30px;
		margin-top: 25px;
		margin-left: 2px;
	}
</style>
@endsection

@section('page-content')
	<div class="page-content-wrapper">
	<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<div class="portlet light bordered">
				<div class="portlet-title">
					<div class="caption font-green-haze">
						<i class="icon-social-dropbox font-green-haze"></i>
						<span class="caption-subject bold uppercase">Member Type Setup </span>
					</div>

				 <div class="tools"> </div>
				</div>
				@if ($message = Session::get('success'))
					<div class="alert alert-success">
						<p>{{ $message }}</p>
					</div>
				@endif

				 <table class="table table-striped table-bordered table-hover" id="sample_1">
					<thead>
						<tr>
						<div class="row">
						<th>No</th>
						<th>Name of Policy</th>
						<th>Action</th>
						</div>
					</tr>

					</thead>
					<tbody>
							@foreach ($policy as $key => $member)
								@if ($member->status == 1)
								<tr>
								@else
									<tr style="background-color: #ffebee !important;" >
								@endif
									<td class="col-md-1">{{ $key+1 }}</td>
									<td class="col-md-2">{{ $member->title }}</td>
									<td class="col-md-2">{{ $member->code }}</td>
									<td class="col-md-2">
										<a class="btn btn-primary btn-xs btn-circle fa fa-edit" href="{{ url('policy/'.$member->slug.'/edit') }}"></a>
										{!! Form::open(['url' => '/policy/'.$member->slug,'method' => 'DELETE','style'=>'display:inline']) !!}
										<button class="fa fa-trash btn-xs btn btn-circle red" type="submit" ></button>
										{!! Form::close() !!}
									</td>
								</tr>
							@endforeach
					</tbody>

				</table>
				{{-- {!! $members->render() !!} --}}
				<!-- END SAMPLE FORM PORTLET-->
			</div>
		</div>
		<a href="{{ url('policy/create') }}" class="float" title="Create New Member Type">
			<i class="fa fa-plus my-float"></i>
		</a>
	</div>
@endsection
