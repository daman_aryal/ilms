@extends('adminMaster')
@section('page-content')
  <div class="page-content-wrapper">
  <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
      <div class="portlet light bordered">
          <div class="portlet-title">
              <div class="caption font-green-haze">
                  <i class="icon-settings font-green-haze"></i>
                  <span class="caption-subject bold uppercase">Member Type Creation</span>
              </div>
              <div class="actions">
                        <a class="btn btn-primary" href="{{ url('memberType') }}"> Back</a>
                    </div>
          </div>
          <div class="portlet-body form">
           @if (count($errors) > 0)
               <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                  <ul>
                     @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                     @endforeach
                  </ul>
              </div>
          @endif
              {!! Form::open(array('route' => 'membertype.store','method'=>'POST')) !!}
              @include('membertype.form')
              {!! Form::close() !!}
          </div>
      </div>  
    </div>
  </div>  
@endsection
		