@extends('adminMaster')
@section('page-content')
	<div class="page-content-wrapper">
	<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
		    <div class="portlet light bordered">
		        <div class="portlet-title">
		            <div class="caption font-green-haze">
		                <i class="icon-settings font-green-haze"></i>
		                <span class="caption-subject bold uppercase">Transacion Detail</span>
		            </div>
				</div>
		        @if ($message = Session::get('success'))
					<div class="alert alert-success">
						<p>{{ $message }}</p>
					</div>
				@endif
		        <table class="table table-bordered">

					<tr>
						<th>S.N</th>
						<th>Stock</th>
						<th>Status</th>
						<th>Reserved by</th>
						<th>Approved By</th>
						<th>Reserved from</th>
						<th>Reserved to</th>
						<th>Action</th>
					</tr>
				@foreach ($lts_transaction_status_paginate as $lts)
				<tr>
					<td>{{ $lts->id }}</td>
					<td>{{$lts->stock_detail->title}}</td>
					{{-- reserve request --}}
					@if($lts->status == 0)
						<td><label class="label label-danger">Reserve Request</label></td>
					@endif

					{{-- request dispatch --}}
					@if($lts->status == 2)
						<td><label class="label label-info">Request Dispatched</label></td>
					@endif

					{{-- request dispatch --}}
					@if($lts->status == 3)
						<td><label class="label label-info">Retured</label></td>
					@endif

					<td>{{$lts->reserved_by->name}}</td>
					<td>
						@if($lts->modified_by !== null)
							{{$lts->modified_by->name}}
						@endif
					</td>
					<td>{{ $lts->book_from }}</td>
					<td>{{ $lts->book_to}}</td>
					<td>
						<a class="btn btn-primary" href="{{ url('transaction/dispatch/'.$lts->id.'/'.$lts->stock_id) }}">Renew</a>
					</td>
				</tr>
				@endforeach
				{{$lts_transaction_status_paginate->render()}}

				</table>

			</div>
		</div>
	</div>
@endsection
