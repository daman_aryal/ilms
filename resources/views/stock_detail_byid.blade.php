<div class="portlet light bordered">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12">
			<div class="form-group">
				<strong>Stock Type:</strong>
				{{ $stock_detail->stock->id }}
			</div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-12">
			<div class="form-group">
				<strong>Stock Title:</strong>
				{{ $stock_detail->title }}
			</div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-12">
			<div class="form-group">
				<strong>Publication:</strong>
				{{$stock_detail->publication->publication_name}}
			</div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-12">
			<div class="form-group">
				<strong>Created by:</strong>
				{{$stock_detail->user_create->name}}
			</div>
		</div>
	</div>
</div>

