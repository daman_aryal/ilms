@extends('userMaster')
@section('title','My Records')
@section('page-content')
	<div class="page-content-wrapper">
        <div class="page-content">
            <div class="container">
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <a href="{{url('/home')}}">Home</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <span>Records</span>
                    </li>
                </ul>
                <div class="page-content-inner">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="caption font-dark">
                                            {{-- <i class="icon-settings font-dark"></i> --}}
                                            <span class="caption-subject bold uppercase">Books Status</span>
                                        </div>
                                        <div class="tools"> </div>
                                    </div>
                                    <div class="portlet-body">
                                        <table class="table table-striped table-bordered table-hover" id="sample_1">
                                        <thead>
                                                <tr>
                                                <th>No</th>
                                                <th>Title</th>
                                                <th>Requested on</th>
                                                <th>Status</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach ($booksUserIssued as $key => $bookStatus)
                                               @foreach ($bookStatus as $element)
                                                <tr>
                                                    <td>{{$key+1}}</td>
                                                    <td>{{ $element->code}}</td>
                                                    <td>{{date('d M,Y', strtotime($element->created_at))}}</td>
                                                    <td><button class="btn btn-sucess">Pending</button></td>
                                                </tr>
                                               @endforeach
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="caption font-dark">
                                            {{-- <i class="icon-settings font-dark"></i> --}}
                                            <span class="caption-subject bold uppercase">Books You Have</span>
                                        </div>
                                        <div class="tools"> </div>
                                    </div>
                                    <div class="portlet-body">
                                        <table class="table table-striped table-bordered table-hover" id="sample_1">
                                        <thead>
                                                <tr>
                                                <th>No</th>
                                                <th>Title</th>
                                                <th>Borrowed on</th>
                                                <th>Return due Date</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                                    @foreach($books_user_have as $key=>$have)
                                                    {{-- {{dd($checkTransaction)}} --}}
                                                        <tr @if ($have->book_to < Carbon\Carbon::now()->toDateString())  style="background:#ea4747;color:#fff;"   @endif>
                                                            <td>{{ $key+1 }}</td>
                                                            <td>{{$stock_info->getStockDetail($have->stock_detail_id)}}</td>
                                                            <td>{{date('Y-m-d',strtotime($have->book_from))}}</td>
                                                            <td>{{date('Y-m-d',strtotime($have->book_to))}}&nbsp;&nbsp;&nbsp;@if ($have->book_to < Carbon\Carbon::now()->toDateString()) 
                                                            @php
                                                                $todayDate = Carbon\Carbon::now();
                                                                $datetime2 = Carbon\Carbon::parse($have->book_to);
                                                                $day = $todayDate->diffInDays($datetime2);
                                                                $priceDay = $checkTransaction->getFine(Auth::user()->id)->per_day_for_week;
                                                                $priceWeek = $checkTransaction->getFine(Auth::user()->id)->per_day_more_than_week;
                                                                $fine = 0;
                                                                if ($day < 7)
                                                                    $fine = $day * $priceDay;
                                                                else
                                                                    $fine = $day * $priceWeek;
                                                            @endphp
                                                                <span><strong>{{$day}} Days Ago</strong></span>    
                                                                <span>and Fine is <strong>RS. {{$fine}}</strong></span>
                                                                
                                                                @endif 


                                                                </td>
                                                        </tr>    
                                                    
                                                        

                                                    @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="caption font-dark">
                                            {{-- <i class="icon-settings font-dark"></i> --}}
                                            <span class="caption-subject bold uppercase">Books You Returned</span>
                                        </div>
                                        <div class="tools"> </div>
                                    </div>
                                    <div class="portlet-body">
                                        <table class="table table-striped table-bordered table-hover" id="sample_1">
                                             <thead>
                                                <tr>
                                                    <th> S No.</th>
                                                    <th> Book </th>
                                                    <th> Status </th>
                                                    <th> Issued date </th>
                                                    <th> Returned date </th>
                                                </tr>
                                            </thead>
                                            <tbody>
											@foreach ($returned as $key => $return)
												<tr>
													<td> {{ $key +1}} </td>
													<td> {{ $return->stock_detail->title}} </td>
													<td> Returned </td>
													<td> {{ $return->issuedDate($return->reference_id)}} </td>
													<td> {{ $return->created_at->toDateString()}} </td>
												</tr>
											@endforeach

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
