@extends('userMaster')
@section('title','Book Detail')
@section('page-content')
	 <div class="page-content-wrapper">
        <div class="page-content">
            <div class="container">
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <a href="{{url('/home')}}">Home</a>
                        <i class="fa fa-circle"></i>
                    </li>

                    <li>
                        <a href="{{url('/home/book')}}">Book</a>
                        <i class="fa fa-circle"></i>

                    </li>
                    <li>
                        <span>Book Detail</span>
                    </li>
                </ul>
                <div class="page-content-inner">
                    <div class="book-detail-pg">
                        <div class="overlay-image">
                            <img src="/{{$getStockMaster->getBookImage()}}" alt="Book Cover">
                            <div class="bk-bg-color"></div>
                        </div>
                        <div class="bk-desc activated">
                            <h2>{{$book->title}}</h2>
                            {{-- {{dd()}} --}}
                            @foreach ($stock::find($getStockMaster->id)->rel_author as $authorName)
                            <p class="author">{{$authorName->author_name}}</p>
                            @endforeach
                            <p class="published">{{date('d M,Y', strtotime($book->publication_date))}}</p>
                            <p class="synopsis">{{$book->plot_summary}}</p>
                            <div class="container">
                            {{-- <em>Book Code : {{$book->code}}</em> --}}
                               <div class="row btn-margin">
                                @if ($book->transactionStatus == 2)
                                   {{--  <button class="book_button" id="getReserve">Not Available</button> --}}
                                    <a class="book_button" href="#getBookModal" data-toggle="modal"      data-stock-id="{{$getStockMaster->id}}">Reserve this book</a>
                                   {{--  @if ($book->transactionStatus == 0)
                                        <button class="book_button" id="getReserve">Not Available</button>
                                        <a class="book_button" href="#getBookModal" data-toggle="modal"      data-stock-id="{{$book->id}}">Reserve this book</a>
                                    @else
                                        <a class="book_button" href="#getBookModal" data-toggle="modal"      data-stock-id="{{$book->id}}">Reserve this book</a>
                                    @endif --}}
                                @elseif($book->transactionStatus == 0)
                                    <span class="help-block"> Select date: </span>
                                    <div class="input-group input-large date-picker input-daterange" data-date="2012-11-11" data-date-format="yyyy-mm-dd">
                                        <input type="text" class="form-control date_from" id="date_from_borrow" name="from" data-date-format="yyyy-mm-dd" data-date-start-date="+0d">
                                        <span class="input-group-addon"> to </span>
                                        <input type="text" class="form-control date_to" id="date_to_borrow" data-date-format="yyyy-mm-dd" name="to" data-date-start-date="+0d">
                                    </div><br>
                                    <button class="book_button" id="getBook" data-stock-id="{{$getStockMaster->id}}"> Borrow Book</button>
                                
                                @endif
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>
                <div id="getBookModal" class="modal fade" role="dialog" data-focus-on="input:first">
                 {!! Form::open(['id'=>'transaction_reserve', 'class'=>'form-horizontal form-bordered'])!!}
                 <input type="hidden" name="bookId" value="{{$getStockMaster->id}}">
                    <div class="modal-header header-bg">
                        <h3 class="modal-title header-font" style="">Reserve Book</h3>
                    </div>
                    <div class="modal-body modal-padding">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="col-md-12">
                                    <p><strong>Title:</strong>{{$book->title}}</p>
                                     @foreach ($stock::find($getStockMaster->id)->rel_author as $authorName)
                                    <p><strong>Author Name:</strong>{{$authorName->author_name}}</p>
                                    @endforeach
                                </div>
                                 <div class="col-md-4">
                                
                                    <span class="help-block"> Select date </span>
                                    <div class="input-group input-large date-picker input-daterange" data-date="2012-11-11" data-date-format="yyyy-mm-dd">
                                        <input type="text" class="form-control date_from" id="date_from" name="from" data-date-format="yyyy-mm-dd" data-date-start-date="+0d">
                                        <span class="input-group-addon"> to </span>
                                        <input type="text" class="form-control date_to" id="date_to" data-date-format="yyyy-mm-dd" name="to" data-date-start-date="+0d">
                                    </div>
                                    <div class="invalid_date"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="footer-margin ">
                        <div class="modal-footer">
                            <button id="reserve_book" type="submit" class="book_button">Submit</button>
                            <button type="button" class="book_button" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                    {!! Form::close()!!}
                </div>
                </div>
            <div class="alert-fix">
                <div class="alert alert-success pull-right collapse">
                    <strong>Successfully Submitted</strong> the reserve request
                </div>
            </div>
        </div>
    </div>
@endsection
@section('footer')
<script type="text/javascript">
    $('#date_from').datepicker();
    $('#date_to').datepicker();
    $('#date_from_borrow').datepicker();
    $('#date_to_borrow').datepicker();
</script>
@endsection