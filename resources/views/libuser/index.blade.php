@extends('adminMaster')
@section('title','Library User Management')
@section('headerscript')
<style>
	.float{
		position:fixed;
		width:60px;
		height:60px;
		bottom:40px;
		right:40px;
		background-color:#3fd5c0;
		color:#FFF;
		border-radius:50px !important;
		text-align:center;
		z-index: 9999;
	}
	.float:hover{
		transition: all 1s;
		transform: translateZ(10px);
    	transform: rotateY(-180deg);
		box-shadow: 0 14px 28px rgba(0,0,0,0.25), 0 10px 10px rgba(0,0,0,0.22);
		color:#fff;
	}

	.my-float{
		font-size: 30px;
		margin-top: 25px;
		margin-left: 2px;
	}
	.portlet.light .dataTables_wrapper .dt-buttons {
	    margin-top: -138px!important;
	}
</style>
@endsection

@section('page-content')
	<div class="page-content-wrapper">
	<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<div class="page-bar">
	            <ul class="page-breadcrumb">
	                <li>
	                    <a href="{{ url('dashboard') }}">Home</a>
	                    <i class="fa fa-circle"></i>
	                </li>
	                <li>
	                    <span>Library User Management</span>
	                </li>
	            </ul>
	        </div>
	        <div class="page-title"></div>
			<div class="portlet light bordered">
				<div class="portlet-title">
					<div class="caption font-green-haze">
						<i class="icon-social-dropbox font-green-haze"></i>
						<span class="caption-subject bold uppercase">Library User Management</span>
					</div>
				</div>
				@if ($message = Session::get('success'))
					<div class="alert alert-success">
						<p>{{ $message }}</p>
					</div>
				@endif
       			<ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#example1-tab1" aria-controls="example1-tab1" role="tab" data-toggle="tab">Staff</a></li>
        <li role="presentation"><a href="#example1-tab2" aria-controls="example1-tab2" role="tab" data-toggle="tab">Student</a></li>
         <li role="presentation"><a href="#example1-tab3" aria-controls="example1-tab3" role="tab" data-toggle="tab">Department</a></li>
          <li role="presentation"><a href="#example1-tab4" aria-controls="example1-tab4" role="tab" data-toggle="tab">Teacher</a></li>
    </ul>

    <!-- Tab panes -->
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane fade in active" id="example1-tab1">
            <table id="example1-tab1-dt" class="table table-striped table-bordered table-condensed" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Sn</th>
                        <th>Name</th>
                        <th>Position</th>
                        <th>Department</th>
                        <th>Member Type</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
        					@php
        					$i =1;
        					@endphp
        					@foreach ($staffUser as $getUser)
        						@if (!empty($getUser->staffUser))
        							@foreach ($getUser->staffUser as $key => $getBasicUser)
                      @if ($getBasicUser->member_type_id == 1)
        							<tr>
        								<td>{{$i}}</td>
        								<td>{{$getBasicUser->first_name}}&nbsp;{{$getBasicUser->last_name}}</td>
        								<td>{{$getBasicUser->designation}}</td>	
        								@foreach ($department as $key => $memberTypeValue)
        									@if ($getBasicUser->department_id == $key)
        										<td><label class="btn btn-success">{{$memberTypeValue}}</label></td>	
        									@endif
        								@endforeach
        								@foreach ($user as $allUser)
                          @if ($allUser->id == $getBasicUser->user_id)
                          <td>{{$allUser->code}}</td>
                            @if ($allUser->active == 1)
                              <td>
                                  <label class="btn btn-success">Acitve</label>
                              </td>
                            @else
                              <td>
                                  <label class="btn btn-danger">Inactive</label>    
                              </td>

                            @endif
                          @endif
                        @endforeach
        								<td><a  class="btn btn-primary btn-sm" href="{{ url('/') }}/staffUser/edit/{{$getUser->id}}"><i class="fa fa-edit"></i> Edit</a></td>
        							</tr>
        							@php
        								$i++;
        							@endphp
                      @endif
        							@endforeach	
        						@endif
        					@endforeach
                </tbody>
            </table>
        </div>
        
        <div role="tabpanel" class="tab-pane fade" id="example1-tab2">
            <table id="example1-tab2-dt" class="table table-striped table-bordered table-condensed" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Sn</th>
                        <th>Name</th>
                        <th>Batch</th>
                        <th>Program</th>
                        <th>Member Code</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                  @php
                  $i =1;
                  @endphp
                  @foreach ($basicUser as $getUser)
                    @if (!empty($getUser->basicUser))
                      @foreach ($getUser->basicUser as $key => $getBasicUser)
                      <tr>
                        <td>{{$i}}</td>
                        <td>{{$getBasicUser->first_name}}&nbsp;{{$getBasicUser->last_name}}</td>
                        @foreach ($year as $yearName)
                          @if ($yearName->id == $getBasicUser->year_id )
                            <td>{{$yearName->batch}}</td> 
                            
                          @endif
                        @endforeach
                        @foreach ($program as $key => $memberTypeValue)
                          @if ($getBasicUser->program_id == $key)
                            <td><label class="btn btn-success">{{$memberTypeValue}}</label></td>  
                          @endif
                        @endforeach
                        @foreach ($user as $allUser)
                          @if ($allUser->id == $getBasicUser->user_id)
                          <td>{{$allUser->code}}</td>
                            @if ($allUser->active == 1)
                              <td>
                                  <label class="btn btn-success">Acitve</label>
                              </td>
                            @else
                              <td>
                                  <label class="btn btn-danger">Inactive</label>    
                              </td>

                            @endif
                          @endif
                        @endforeach
                        <td><a  class="btn btn-primary btn-sm" href="{{ url('/') }}/staffUser/edit/{{$getUser->id}}"><i class="fa fa-edit"></i> Edit</a></td>
                      </tr>
                      @php
                        $i++;
                      @endphp
                      @endforeach 
                    @endif
                  @endforeach
                </tbody>
            </table>
        </div>
        <div role="tabpanel" class="tab-pane fade" id="example1-tab3">
            <table id="example1-tab3-dt" class="table table-striped table-bordered table-condensed" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Sn</th>
                        <th>Name</th>
                        <th>Position</th>
                        <th>Department</th>
                        <th>Member Code</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                  @php
                  $i =1;
                  @endphp
                  @foreach ($staffUser as $getUser)
                    @if (!empty($getUser->staffUser))
                      @foreach ($getUser->staffUser as $key => $getBasicUser)
                      @if ($getBasicUser->member_type_id == 4)
                      <tr>
                        <td>{{$i}}</td>
                        <td>{{$getBasicUser->first_name}}&nbsp;{{$getBasicUser->last_name}}</td>
                        <td>{{$getBasicUser->designation}}</td> 
                        @foreach ($department as $key => $memberTypeValue)
                          @if ($getBasicUser->department_id == $key)
                            <td><label class="btn btn-success">{{$memberTypeValue}}</label></td>  
                          @endif
                        @endforeach
                        @foreach ($user as $allUser)
                          @if ($allUser->id == $getBasicUser->user_id)
                          <td>{{$allUser->code}}</td>
                            @if ($allUser->active == 1)
                              <td>
                                  <label class="btn btn-success">Acitve</label>
                              </td>
                            @else
                              <td>
                                  <label class="btn btn-danger">Inactive</label>    
                              </td>

                            @endif
                          @endif
                        @endforeach
                        <td><a  class="btn btn-primary btn-sm" href="{{ url('/') }}/staffUser/edit/{{$getUser->id}}"><i class="fa fa-edit"></i> Edit</a></td>
                      </tr>
                      @php
                        $i++;
                      @endphp
                      @endif
                      @endforeach 
                    @endif
                  @endforeach
                </tbody>
            </table>
        </div>
        <div role="tabpanel" class="tab-pane fade" id="example1-tab4">
            <table id="example1-tab4-dt" class="table table-striped table-bordered table-condensed" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Sn</th>
                        <th>Name</th>
                        <th>Position</th>
                        <th>Department</th>
                        <th>Member Code</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                  @php
                  $i =1;
                  @endphp
                  @foreach ($staffUser as $getUser)
                    @if (!empty($getUser->staffUser))
                      @foreach ($getUser->staffUser as $key => $getBasicUser)
                      @if ($getBasicUser->member_type_id == 3)
                      <tr>
                        <td>{{$i}}</td>
                        <td>{{$getBasicUser->first_name}}&nbsp;{{$getBasicUser->last_name}}</td>
                        <td>{{$getBasicUser->designation}}</td> 
                        @foreach ($department as $key => $memberTypeValue)
                          @if ($getBasicUser->department_id == $key)
                            <td><label class="btn btn-success">{{$memberTypeValue}}</label></td>  
                          @endif
                        @endforeach
                        @foreach ($user as $allUser)
                          @if ($allUser->id == $getBasicUser->user_id)
                          <td>{{$allUser->code}}</td>
                            @if ($allUser->active == 1)
                              <td>
                                  <label class="btn btn-success">Acitve</label>
                              </td>
                            @else
                              <td>
                                  <label class="btn btn-danger">Inactive</label>    
                              </td>

                            @endif
                          @endif
                        @endforeach
                        <td><a  class="btn btn-primary btn-sm" href="{{ url('/') }}/staffUser/edit/{{$getUser->id}}"><i class="fa fa-edit"></i> Edit</a></td>
                      </tr>
                      @php
                        $i++;
                      @endphp
                      @endif
                      @endforeach 
                    @endif
                  @endforeach
                </tbody>
            </table>
        </div>
    </div>
			    
			</div>
		</div>
		<a href="{{ url('libUser/create') }}" class="float" title="Create New User">
			<i class="fa fa-plus my-float"></i>
		</a>
	</div>
@endsection
@section('page-script')
<script>
$( document ).ready(function() {
	$('#example1-tab1-dt').DataTable({
      columns: [
         { width: '20%' },
         { width: '20%' },
         { width: '20%' },
         { width: '10%' },
         { width: '15%' },
         { width: '15%' }
      ]
   });

   $('#example1-tab2-dt').DataTable({
      columns: [
         { width: '20%' },
         { width: '20%' },
         { width: '20%' },
         { width: '10%' },
         { width: '15%' },
         { width: '15%' }
      ]
   });
   $('#example1-tab3-dt').DataTable({
      columns: [
         { width: '20%' },
         { width: '20%' },
         { width: '20%' },
         { width: '10%' },
         { width: '15%' },
         { width: '15%' }
      ]
   });$('#example1-tab4-dt').DataTable({
      columns: [
         { width: '20%' },
         { width: '20%' },
         { width: '20%' },
         { width: '10%' },
         { width: '15%' },
         { width: '15%' }
      ]
   });
   
   $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
      $($.fn.dataTable.tables(true)).DataTable()
         .columns.adjust();
   });  
}); 
</script>
@endsection
