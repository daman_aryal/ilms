@extends('adminMaster')
@section('title','Library User Management')
@section('headerscript')
<style>
    @media (min-width: 992px){
        .hori-mar-lb{
            margin-left: 18px;
        }
        .hori-mar-inp{
            margin-left: 43px;
        }
    }
    @media (max-width: 1200px){
        .div-form>div{
            padding-left: 20px;
        }
        .img-upload{
            margin-top: 20px;
            padding-left: 35px !important;
        }
    }
</style>
@endsection
@section('page-content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="{{ url('dashboard') }}">Home</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="{{ url('libUser') }}">Library User Management</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Create</span>
                </li>
            </ul>
        </div>
        <div class="page-title"></div>
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-green-haze">
                    <i class="icon-settings font-green-haze"></i>
                    <span class="caption-subject bold uppercase">Library User Management</span>
                </div>

                <div class="pull-right">
                    <a class="btn btn-success" 	href="{{ url('/libUser') }}"><i class="fa fa-arrow-left "></i> Back</a>
                </div>

                @include('partials.admin.quicksidebar')
                <!-- END QUICK SIDEBAR -->

            </div>
            @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
            @endif
            @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <div class="row div-form">
                <div class="col-md-6">
                    <div class="form-group form-md-line-input">
                        <label class="col-md-3 control-label hori-mar-lb" for="member">Member type:</label>
                        <div class="col-md-7 hori-mar-inp">
                            {!! Form::select('member_type',$member_types,( isset($user->basicUser[0]->member_type_id) ? $user->basicUser[0]->member_type_id : null ), array('class' => 'form-control','id' => 'member','placeholder'=>"Enter the member type")) !!}
                            <div class="form-control-focus"> </div>
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <div class="staffForm hidden">
                {!! Form::open(['url' => 'staffUser/store','method'=>'POST','files'=>true,'id'=>'staffUserForm']) !!}
                @include('staff.form')

                <div class="portlet light bordered">
                    <!-- email and password sections-->
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group form-md-line-input">
                            <label class="col-md-2 control-label" for="form_control_1">Email:</label>
                            <div class="col-md-10">
                            {!! Form::text('email', null, array('placeholder' => 'Email','class' => 'form-control')) !!}
                            <div class="form-control-focus"> </div>
                           </div>
                       </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12">
                      <div class="form-group form-md-line-input">
                          <label class="col-md-2 control-label" for="form_control_1">Password:</label>
                          <div class="col-md-10">
                              <div class="input-group">
                                  <div class="input-group-control">
                                      {!! Form::password('password', array('placeholder' => 'Password','class' => 'form-control','id' => 'password')) !!}
                                      <div class="form-control-focus"> </div>
                                  </div>
                                  <span class="input-group-btn btn-right">
                                      <a class="btn green-haze" id="generate">Generate</a>
                                  </span>
                              </div>
                          </div>
                      </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group form-md-line-input">
                            <label class="col-md-2 control-label" for="form_control_1">Password Confirmation:</label>
                            <div class="col-md-10">
                             {!! Form::password('password_confirm', array('placeholder' => 'Confirm Password','class' => 'form-control', 'id'=>'password_confirm')) !!}    
                            <div class="form-control-focus"> </div>
                           </div>
                       </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12" id="generated" style="display: none">
                        <div class="form-group form-md-line-input">
                            <label class="col-md-2 control-label" for="form_control_1">Generated Password:</label>
                            <div class="col-md-10">
                              <span id="view_pass"></span>    
                            <div class="form-control-focus"> </div>
                           </div>
                       </div>
                    </div>

                    <!-- form action buttons-->

                    {!! Form::hidden('member_type_id',null, array('class' => 'form-control','id' => 'memberType','placeholder'=>"Enter the Member Type",'readonly'=>'true')) !!}
                    <div class="form-actions">
                        <div class="row div-form">
                            <div class="col-md-offset-2 col-md-10">
                                <button type="submit" id="staffFormSubmit" class="btn blue">Save</button>
                                <a href="{{ url('/libUser') }}" class="btn default">Cancel</a>
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
            <div class="basicForm hidden">
                {!! Form::open(['url' => 'basicUser/store','method'=>'POST','files'=>true,'id'=>'basicUserForm']) !!}
                @include('basicuser.form')

                <div class="portlet light bordered">
                    <!-- email and password sections-->
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group form-md-line-input">
                            <label class="col-md-2 control-label" for="form_control_1">Email:</label>
                            <div class="col-md-10">
                            {!! Form::text('email', null, array('placeholder' => 'Email','class' => 'form-control')) !!}
                            <div class="form-control-focus"> </div>
                           </div>
                       </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12">
                      <div class="form-group form-md-line-input">
                          <label class="col-md-2 control-label" for="form_control_1">Password:</label>
                          <div class="col-md-10">
                              <div class="input-group">
                                  <div class="input-group-control">
                                      {!! Form::password('password', array('placeholder' => 'Password','class' => 'form-control','id' => 'spassword')) !!}
                                      <div class="form-control-focus"> </div>
                                  </div>
                                  <span class="input-group-btn btn-right">
                                      <a class="btn green-haze" id="sgenerate">Generate</a>
                                  </span>
                              </div>
                          </div>
                      </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group form-md-line-input">
                            <label class="col-md-2 control-label" for="form_control_1">Password Confirmation:</label>
                            <div class="col-md-10">
                             {!! Form::password('password_confirm', array('placeholder' => 'Confirm Password','class' => 'form-control', 'id'=>'spassword_confirm')) !!}    
                            <div class="form-control-focus"> </div>
                           </div>
                       </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12" id="sgenerated" style="display: none">
                        <div class="form-group form-md-line-input">
                            <label class="col-md-2 control-label" for="form_control_1">Generated Password:</label>
                            <div class="col-md-10">
                              <span id="sview_pass"></span>    
                            <div class="form-control-focus"> </div>
                           </div>
                       </div>
                    </div>

                    <!-- form action buttons-->

                    {!! Form::hidden('member_type_id',null, array('class' => 'form-control','id' => 'memberType','placeholder'=>"Enter the Member Type",'readonly'=>'true', 'value'=>'')) !!}
                    <div class="form-actions">
                        <div class="row div-form">
                            <div class="col-md-offset-2 col-md-10">
                                <button type="submit" id="basicFormSubmit" class="btn blue">Save</button>
                                <a href="{{ url('/libUser') }}" class="btn default">Cancel</a>
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>

            </div>

        </div>
    </div>
</div>
@endsection


@section('footerscript')
{!! Html::script('assets/global/scripts/app.min.js') !!}
{!! Html::script('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') !!}
{!! Html::script('assets/pages/scripts/components-date-time-pickers.min.js') !!}
{!! Html::script('assets/global/scripts/datatable.js') !!}
{!! Html::script('assets/global/plugins/datatables/datatables.min.js') !!}
{!! Html::script('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') !!}
{!! Html::script('assets/pages/scripts/table-datatables-buttons.min.js') !!}
<!-- BEGIN THEME LAYOUT SCRIPTS -->
<script src="../assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script>
<script src="../assets/layouts/layout/scripts/demo.min.js" type="text/javascript"></script>
<!-- END THEME LAYOUT SCRIPTS -->

{{-- <script>
$( "#memberType" ).change(function() {
console.log(this.value);
});
</script> --}}
<script type="text/javascript">
    $("#basicFormSubmit").click(function(e){  
        e.preventDefault();
        $("#basicUserForm").submit();
    });  
    $("#staffFormSubmit").click(function(e){
        e.preventDefault();  
        $("#staffUserForm").submit();
    });	
    $('#year').on('change', function (e) {
        e.preventDefault();

        var year_id = $(this).val();
        console.log(year_id);
        $.ajax({
            url: '../api/year/get_batch',
            type: "GET",
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            delay: 250,
            data: {year_id: year_id},
            success: function( msg ) {
                $('input[name="batch_id"]').val(msg).prop('readonly', true);

            }
        });
    });

    $('#member').on('change', function (e) {
        $('#password').val("");
        $('#password_confirm').val("");
        $('#spassword').val("");
        $('#spassword_confirm').val("");

// e.preventDefault();

var member = $(this).val();
$('#memberType').empty();
$('input[name="member_type_id"]').val(member);
//  $('#memberType').val(member);

// console.log($('#memberType').val());
if(member == 2){
    $('.basicForm').removeClass('hidden');
    if (!$('.staffForm').hasClass('hidden')) {
        $('.staffForm').addClass('hidden');
        $('#sgenerated').hide();
    }
}else if( member == 1 || member == 4 || member == 3){
    $('#generated').hide();
    $('.staffForm').removeClass('hidden');
    if (!$('.basicForm').hasClass('hidden')) {
        $('.basicForm').addClass('hidden');
    }
}else{
    if (!$('.basicForm').hasClass('hidden')) {
        $('.basicForm').addClass('hidden');
    }
    if (!$('.staffForm').hasClass('hidden')) {
        $('.staffForm').addClass('hidden');
    }
}
// $.ajax({
//     url: '../api/member_type/get_code',
//     type: "GET",
//     headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
//     data: {member_type_id: member},
//     success: function( msg ) {
//      // var member = $(this).val();
//      $('#memberType').empty();
//      // $('#memberType').val(member);

//       $('input[name="code"]').val(msg.code).prop('readonly', true);
//       $('input[name="member_code"]').val(msg.member_code).prop('readonly', true);
//     }
// });
});

    $('#program').on('change', function (e) {
        e.preventDefault();
        var program_id = $(this).val();
        $('#class')
        .find('option')
        .remove()
        .end()
        .append('<option value="" disabled selected>Select Class</option>');
        $('#section')
        .find('option')
        .remove()
        .end()
        .append('<option value="" disabled selected>Select Class</option>');

//get subject according to the sector selected
$.getJSON("/api/program/getClass?program_id="+program_id, function(data) {
    console.log(data);

//add options in subject
// $("#class").append('<option value disabled selected></option>');
$.each(data, function(){
    $("#class").append('<option value="'+ this.id +'">'+ this.title +'</option>');
});
});


});

    $('#class').on('change', function (e) {
        e.preventDefault();
        var class_id = $(this).val();
// $('#class')
//   .find('option')
//   .remove()
//   .end()
//   .append('<option value="" disabled selected>Select Class</option>');

//get subject according to the sector selected
$.getJSON("/api/program/getSection?class_id="+class_id, function(data) {
// console.log(data);

//add options in subject
// $("#class").append('<option value disabled selected></option>');
$.each(data, function(){
    $("#section").append('<option value="'+ this.id +'">'+ this.title +'</option>');
});
});


});
</script>

@endsection