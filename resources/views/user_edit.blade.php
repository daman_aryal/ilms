@extends('adminMaster')
@section('title','User | Edit')

@section('page-content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="{{ url('dashboard') }}">Home</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="{{ url('adminUser') }}">Application User Management</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Edit</span>
                </li>
            </ul>
        </div>
        <div class="page-title"></div>
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-green-haze">
                    <i class="icon-settings font-green-haze"></i>
                    <span class="caption-subject bold uppercase">Edit User</span>
                </div>
                <div class="actions">
                    <a class="btn btn-primary" href="{{ url('adminUser') }}"><i class="fa fa-arrow-left "></i> Back</a>
                </div>
            </div>
            <div class="portlet-body form">
                @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                {!! Form::model($user, ['method' => 'POST','url' => ['adminUser/update/'.$user->id]]) !!}
                @include('user_form')
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection
@section('footerscript')
<script type="text/javascript">
    
 $('#generate').on('click',function(e){
         e.preventDefault();
            $.ajax({
                url: '/generatePass',
                type: "POST",
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success: function( result ) {
                    $('#password_confirm').val(result);
                    $('#password').val(result);
                    $('#view_pass').html(result);
                }
            });
    });
</script>
@endsection