@extends('userMaster')
@section('title','My Profile')
@section('page-content')
	<div class="page-content-wrapper">
        <div class="page-content">
            <div class="container">
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <a href="{{url('/student')}}">Home</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <span>Profile Edit</span>
                    </li>
                </ul>
                @if (count($errors) > 0)

                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                @if (Session::has('message'))
                   <div class="alert alert-info">{{ Session::get('message') }}</div>
                @endif
                <div class="page-content-inner">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="caption font-dark">
                                            {{-- <i class="icon-settings font-dark"></i> --}}
                                            <span class="caption-subject bold uppercase">Edit Profile</span>
                                        </div>
                                       
                                    </div>
                                    <div class="portlet-body">
                                        {!! Form::open(['url' => '/updateProfile','method'=>'POST','id'=>'basicUserForm']) !!}
                                        @include('partials.user.form_edit_profile')
                                        {!!Form::close()!!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('footerscript')
<script type="text/javascript">
    
 $('#generate').on('click',function(e){
         e.preventDefault();
            $.ajax({
                url: '/generatePass',
                type: "POST",
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success: function( result ) {
                    $('#password_confirm').val(result);
                    $('#password').val(result);
                    $('#view_pass').html(result);
                    $('#generated').show();
                }
            });
    });
</script>
@endsection
