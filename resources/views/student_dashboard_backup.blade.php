@extends('adminMaster')
@section('page-content')
	<div class="page-content-wrapper">
	<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
		    <div class="portlet light bordered">
		        <div class="portlet-title">
		            <div class="caption font-green-haze">
		                <i class="icon-settings font-green-haze"></i>
		                <span class="caption-subject bold uppercase">Student Dashboard</span>
		            </div>
				</div>
		       <div class="success_hidden" hidden="true">
					<div class="alert alert-danger">
			          	<strong><p class="success"></p></strong>
			           	successfully reserve!<br><br>
			        </div>
				</div>
		        <table class="table table-stripe">
					
					<tr>
						<td><strong>S.N</strong></td>
						<td><strong>Title</strong></td>
						<td><strong>Publication</strong></td>
						<td><strong>Created By</strong></td>
						<td><strong>Booked Till</strong></td>
						<td><strong>Stock Id</strong></td>
						<td><strong>Quantity</strong></td>
						<td><strong>Action</strong></td>
					</tr>

					@foreach($stock_details_with_status as $s)
						<tr>
							<td>{{$s->id}}</td>
							<td>{{$s->title}}</td>
							<td>{{$s->publication->publication_name}}</td>
							<td>{{$s->user_create->name}}</td>
							<td>{{$s->to}}</td>
							<td>{{$s->stock_id}}</td>
							<td>{{$s->stock->quantity}}</td>
							

							<td>
								@if($s->transaction !== null && $s->transaction->created_by == Auth::user()->id)
									
									{{-- if satus is 0, its pending for book --}}
									@if($s->transaction->status == 0)
										<a class="reserve_btn btn red" disabled>
											Pending ...
										</a>	
									@endif

									@if($s->transaction->status == 2)
										<a class="reserve_btn btn blue" 
											href="#reserve_form" 
											data-toggle="modal"
											data-stock-id="{{$s->id}}">
											Renew
										</a>
									@endif
									
									
								@else
									{{-- {{dd($s->id)}} --}}
									<a class="reserve_btn btn blue" 
									href="#reserve_form" 
									data-toggle="modal"
									data-stock-id="{{$s->id}}"
										>Reserve
									</a>
								@endif
							</td>
						</tr>
					@endforeach

					<div id="reserve_form" class="modal fade" role="dialog" aria-hidden="true">
						<div class="modal-content">
							
							{{-- modal header --}}
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
								<h4 class="modal-title">Stock Detail</h4>
							</div>

							{{-- modal body --}}
							<div class="modal-body" style="padding-bottom: 60px!important">

								<div id="stock_detail_byid">
									{{-- stock detail here --}}
								</div>

								<div class="form-body">
								{!! Form::open(['id'=>'transaction_reserve', 'class'=>'form-horizontal form-bordered'])!!}
									
									@if($s->transaction !== null 
									&& $s->transaction->created_by == Auth::user()->id)
									
									{{-- if this form is for renvew --}}
									{!!Form::hidden('ref_id', $s->reference_id)!!}
									@endif
									
									<div class="form-group">
										<label class="control-label col-md-3">Reserve Date</label>
										<div class="col-md-4">
											<div class="input-group input-large date-picker input-daterange" data-date="10/11/2012" data-date-format="yyyy-mm-dd">
												<input type="text" class="form-control" name="from">
												<span class="input-group-addon"> to </span>
												<input type="text" class="form-control" name="to"> 
											</div>
											<div class="invalid_date"></div>
										</div>
									</div>

									{{-- ajax error handle --}}
									<div class="error_hidden" hidden="true">
										<div class="alert alert-danger">
											<strong>Whoops!</strong> There were some problems with your input.<br><br>
											<ul class="error">
											</ul>
										</div>
									</div>
								</div>
							</div>

							{{-- modal footer --}}

							<div class="modal-footer">
								<button class="btn dark btn-outline" data-dismiss="modal" aria-hidden="true">Close</button>
								<input class="btn dark btn-outline" type="submit" value="Reserve">
							</div>

							{!! Form::close()!!}
						</div>
					</div>
				</table>

			<!-- END SAMPLE FORM PORTLET-->
			</div>
		</div>
	</div>
@endsection
@section('page-script')
	<script src="{{ url('js/transaction.js') }}"></script>
@endsection