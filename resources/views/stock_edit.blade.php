
@extends('adminMaster')
@section('title','Stock | Edit')
@section('page-content')
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <a href="{{ url('dashboard') }}">Home</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <a href="{{ url('stock') }}">Stock</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <span>Edit</span>
                    </li>
                </ul>
            </div>
            <div class="page-title"></div>
            <div class="row">
                <div class="col-md-12">
                            <!-- BEGIN SAMPLE FORM PORTLET-->
                        <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption font-green-haze">
                                <i class="icon-settings font-green-haze"></i>
                                <span class="caption-subject bold uppercase">Edit Stock</span>
                            </div>
                            <div class="actions">
                                <a class="btn btn-primary" href="{{ url('/stock') }}"> Back</a>
                            </div>
                        </div>
                       {!! Form::model($stock, ['method' => 'POST','url' => ['stock/update',$stock->id]]) !!}
                        @include('stock_form')
                        {!! Form::close() !!}
                    </div>
                    <!-- END SAMPLE FORM PORTLET-->
                </div>
            </div>
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
@endsection
