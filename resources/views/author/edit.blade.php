@extends('adminMaster')
@section('title','Author | Edit')

@section('page-content')
	<div class="page-content-wrapper">
	<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<div class="page-bar">
	            <ul class="page-breadcrumb">
	                <li>
	                    <a href="{{ url('dashboard') }}">Home</a>
	                    <i class="fa fa-circle"></i>
	                </li>
	                <li>
	                    <a href="{{ url('setting') }}">Setting</a>
	                    <i class="fa fa-circle"></i>
	                </li>
	                <li>
	                    <a href="{{ url('author') }}">Author</a>
	                    <i class="fa fa-circle"></i>
	                </li>
	                <li>
	                	<span>Edit</span>
	                </li>
	            </ul>
	        </div>
	        <div class="page-title"></div>
			<div class="portlet light bordered">
				<div class="portlet-title">
					<div class="caption font-green-haze">
					    <i class="icon-settings font-green-haze"></i>
					    <span class="caption-subject bold uppercase">Author Edit</span>
					</div>
					<div class="actions">
                        <a class="btn btn-primary" href="{{ url('author') }}"><i class="fa fa-arrow-left "></i> Back</a>
                    </div>
				</div>
				<div class="portlet-body form">
				@if (count($errors) > 0)
					<div class="alert alert-danger">
						<strong>Whoops!</strong> There were some problems with your input.<br><br>
						<ul>
							@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
				@endif
				{!! Form::model($author, ['url' => ['author', $author->slug],'method'=>'PUT','files'=>true, 'novalidate'=>'']) !!}
					@include('author.form')
				{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>	
@endsection
@section('page-script')
  {!! Html::script('assets/global/plugins/jquery.min.js') !!}
  {!! Html::script('js/stock_create.js') !!}
  {!! Html::script('js/toastr.min.js') !!}
  {!! Html::script('js/typeahead.bundle.min.js') !!}
  {!! Html::script('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') !!}
  {!! Html::script('assets/pages/scripts/components-date-time-pickers.min.js') !!}
  {!! Html::script('js/dropify.min.js') !!}
@endsection