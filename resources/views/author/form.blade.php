<div class="row">

	<div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group form-md-line-input">
            <div class="col-md-9">
                <div class="col-xs-12 col-sm-12 col-md-12">
                  <div class="form-group form-md-line-input">
                      <label class="col-md-2 control-label" for="form_control_1">Name:</label>
                      <div class="col-md-10">
                      {!! Form::text('author_name', null, array('placeholder' => 'Name','class' => 'form-control')) !!}
                      <div class="form-control-focus"> </div>
                     </div>
                  </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                  <div class="form-group form-md-line-input">
                      <label class="col-md-2 control-label" for="form_control_1">Link:</label>
                      <div class="col-md-10">
                      {!! Form::text('link', null, array('placeholder' => 'Link','class' => 'form-control')) !!}
                      <div class="form-control-focus"> </div>
                     </div>
                  </div>
                </div>

                 <div class="clearfix"></div>
                 <div class="col-xs-12 col-sm-12 col-md-12">
                      <div class="form-group form-md-line-input">
                          <label class="col-md-2 control-label" for="form_control_1">Personal Information:</label>
                          <div class="col-md-10">
                          {!! Form::textarea('persona_information',null, array('class' => 'form-control', 'rows'=>'3')) !!}
                          <div class="form-control-focus"> </div>
                         </div>
                     </div>
                 </div>
            </div>
            <div class="col-md-3">
                <div class="mt-list-container list-simple ext-1 col-md-12" style="padding: 15px">
                    <div class="pull-left" style="margin-right: 20px;">
                        @if(isset($author) && ! is_null($author->images))
                        <input type="file" name="author_image" id="authorImage" class="dropify" data-default-file="{{ asset($author->images->path) }}" data-height="175" data-allowed-file-extensions = "png jpeg jpg" data-max-file-size='2M'>
                        @else
                        {!! Form::file('author_image',['id'=>'authorImage','class' => 'dropify','data-height' => '175','data-allowed-file-extensions'=>'png jpeg jpg','data-max-file-size'=>'2M']) !!}
                        @endif
                         <label for="input-file-now-custom-2">Author Image:</label>
                   </div>
                </div>
            </div>
       </div>
   </div>
   
            
    <div class="col-xs-12 col-sm-12 col-md-12" style="padding-top: 25px;">
       <button type="submit" class="btn btn-primary col-md-offset-2">Save</button>
       <a href="{{ url('/author') }}" class="btn btn-default">Cancel</a>

  </div>
</div>