@extends('adminMaster')
@section('title','Author | Index')

@section('headerscript')
<style>
	.float{
		position:fixed;
		width:60px;
		height:60px;
		bottom:40px;
		right:40px;
		background-color:#3fd5c0;
		color:#FFF;
		border-radius:50px !important;
		text-align:center;
		z-index: 9999;
	}
	.float:hover{

		transition: all 1s;
		transform: translateZ(10px);

    transform: rotateY(-180deg);
		box-shadow: 0 14px 28px rgba(0,0,0,0.25), 0 10px 10px rgba(0,0,0,0.22);
		color:#fff;
	}

	.my-float{
		font-size: 30px;
		margin-top: 25px;
		margin-left: 2px;
	}
</style>
@endsection

@section('page-content')
	<div class="page-content-wrapper">
	<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<div class="page-bar">
	            <ul class="page-breadcrumb">
	                <li>
	                    <a href="{{ url('dashboard') }}">Home</a>
	                    <i class="fa fa-circle"></i>
	                </li>
	                <li>
	                    <a href="{{ url('setting') }}">Setting</a>
	                    <i class="fa fa-circle"></i>
	                </li>
	                <li>
	                	<span>Author</span>
	                </li>
	            </ul>
	        </div>
	        <div class="page-title"></div>
			<div class="portlet light bordered">
				<div class="portlet-title">
					<div class="caption font-green-haze">
						<i class="icon-social-dropbox font-green-haze"></i>
						<span class="caption-subject bold uppercase">Author Setup</span>
					</div>

				 <div class="tools"> </div>
				</div>
				@if ($message = Session::get('success'))
					<div class="alert alert-success">
						<p>{{ $message }}</p>
					</div>
				@endif

				 <table class="table table-striped table-bordered table-hover" id="sample_1">
					<thead>
						<tr>
							<div class="row">
							<th class="col-md-1">No</th>
							<th class="col-md-2">Name</th>
							<th class="col-md-2">Link</th>
							<th class="col-md-2">Action</th>
							</div>
						</tr>

					</thead>
					<tbody>
							@foreach ($authors as $key => $author)
								<tr>
									<td class="col-md-1">{{ $key+1 }}</td>
									<td class="col-md-2">{{ $author->author_name }}</td>
									<td class="col-md-2">{{ $author->link }}</td>
									<td class="col-md-2">
										<a class="btn btn-primary btn-sm" href="{{ url('author/'.$author->slug.'/edit') }}"><i class="fa fa-edit"></i> Edit</a>
										{!! Form::open(['url' => '/author/'.$author->slug,'method' => 'DELETE','style'=>'display:inline']) !!}
							            	{!! Form::button('<i class="fa fa-trash-o"></i> Delete', ['type' => 'submit','class' => 'btn btn-danger btn-sm']) !!}
							        	{!! Form::close() !!}
									</td>
								</tr>
							@endforeach
					</tbody>

				</table>
				{{-- {!! $authors->render() !!} --}}
				<!-- END SAMPLE FORM PORTLET-->
			</div>
		</div>
		<a href="{{ url('author/create') }}" class="float" title="Create New Author">
			<i class="fa fa-plus my-float"></i>
		</a>
	</div>
@endsection


