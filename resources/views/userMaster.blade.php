@extends('userlayout')
@section('headercss')
    {!! Html::style('assets/global/plugins/select2/css/select2-bootstrap.min.css') !!}
    {!! Html::style('assets/global/plugins/select2/css/select2.min.css') !!}
    <link href="{{ url('latestcss/assets/layouts/layout3/css/layout.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ url('latestcss/assets/layouts/layout3/css/themes/default.min.css') }}" rel="stylesheet" type="text/css" id="style_color" />
    <link href="{{ url('latestcss/assets/layouts/layout3/css/custom.min.css') }}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{url('latestcss/css/demo.css')}}">
    <link rel="stylesheet" href="{{url('latestcss/css/owl.carousel.css')}}">
    <link rel="stylesheet" href="{{url('latestcss/css/owl.theme.css')}}">
    <link rel="stylesheet" href="{{url('latestcss/css/owl.transitions.css')}}">
    <!-- <link rel="stylesheet" type="text/css" href=".../css/cs-select.css" /> -->
    <link rel="stylesheet" type="text/css" href="{{url('latestcss/css/cs-skin-slide.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{url('latestcss/css/main.css')}}" />
    <link rel="stylesheet" href="{{ url('latestcss/css/book.css') }}">
    <link rel="stylesheet" href="{{ url('../latestcss/css/book_list_modal.css')}}">
    <link href="{{ url('latestcss/assets/pages/css/search.min.css')}}" rel="stylesheet" type="text/css" />
       <link media="all" type="text/css" rel="stylesheet" href="{{ url('css/toastr.min.css') }}">


    <link rel="stylesheet" href="{{ url('latestcss/css/typehead.css')}}">

   <link href="{{ url('latestcss/assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ url('latestcss/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
        {{-- <link href="{{url('assets/global/plugins/typeahead/typeahead.css')}}" rel="stylesheet" type="text/css" /> --}}

          <style>
  .tt-query {
    -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
       -moz-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
  }

  .select2-container--focus,.select2-container--open,.select2-container--below,.select2-search__field{
    width: 355px !important;
  }

  .tt-hint {
    color: #999
  }

  .tt-menu {    /* used to be tt-dropdown-menu in older versions */
    width: 900px;
    margin-top: 4px;
    padding: 4px 0;
    background-color: #fff;
    border: 1px solid #ccc;
    border: 1px solid rgba(0, 0, 0, 0.2);
    -webkit-border-radius: 4px;
       -moz-border-radius: 4px;
            border-radius: 4px;
    -webkit-box-shadow: 0 5px 10px rgba(0,0,0,.2);
       -moz-box-shadow: 0 5px 10px rgba(0,0,0,.2);
            box-shadow: 0 5px 10px rgba(0,0,0,.2);
  }



/*.select2-result-repository__avatar img {
  height: 60px;
}*/

  .tt-suggestion {
    padding: 3px 20px;
    line-height: 24px;
  }

  .tt-highlight  {
    /*font-weight: normal;*/
    color: #72aecc !important;
  }

  .tt-suggestion.tt-cursor,.tt-suggestion:hover {
    color: #fff;
    /*background-color: #72aecc;*/

  }

  .tt-suggestion p {
    margin: 0;
  }

  </style>

@endsection

@section('body')
    @include('partials.user.header')
    <!-- BEGIN CONTAINER -->
    @yield('page-content')

    @include('partials.user.footer')
@endsection

@section('footerscript')
<!-- BEGIN THEME LAYOUT SCRIPTS -->

<script src="{{ url('latestcss/assets/layouts/layout3/scripts/demo.min.js') }}" type="text/javascript"></script>
<script src="{{ url('latestcss/js/owl.carousel.js') }}" type="text/javascript"></script>
<script src="{{ url('latestcss/js/classie.js') }}"></script>
{{-- <script src="{{ url('latestcss/js/selectFx.js') }}"></script> --}}
<script src="{{ url('latestcss/assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
<script src="{{ url('latestcss/assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
<script src="{{ url('latestcss/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
{{-- <script src="{{ url('path') }}" type="text/javascript"></script> --}}

<script src="{{url('latestcss/assets/global/plugins/typeahead/handlebars.min.js')}}" type="text/javascript"></script>
<script src="{{url('latestcss/assets/global/plugins/typeahead/typeahead.bundle.min.js')}}" type="text/javascript"></script>
<script src="{{url('latestcss/js/custom-typeahead.js')}}"></script>
<script src="{{url('latestcss/assets/global/scripts/app.min.js')}}" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Shuffle/4.1.0/shuffle.min.js"></script>
<script src="{{url('latestcss/js/book_list_modal.js')}}"></script>

<script>
    (function() {
        [].slice.call( document.querySelectorAll( 'select.cs-select' ) ).forEach( function(el) {
            new SelectFx(el);
        } );
    })();
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/typeahead.js/0.11.1/typeahead.bundle.min.js"></script>
<script src="{{ url('js/toastr.min.js') }}"></script>

<script>
   jQuery(document).ready(function($) {
        $("#search").click(function(){

            window.location = "{{url('/home/book')}}";

        });
        $(document).on("click", "#submit1", function(event){
            $('.main-container').removeClass('prevent-scroll');
            $('.main-overlay').fadeOut();
            $('.main-overlay').find('.overlay-details').remove();
            $('.alert-success').show().fadeOut(5000);
        });
        $(document).on("click", "#submit2", function(event){
            $('.modal').modal('toggle');
            $('.alert-success2').show().fadeOut(5000);
        });

        $(document).on("click", "#getThisBook", function(event){
            // $('.modal').modal('toggle');
            $('.getBookMsg').show().fadeOut(7000);
            $('#getThisBook').html('Requested');
            $('#getThisBook').removeClass('getBookMsg');


        });
        var engine = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.whitespace('stock_detail'),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            remote:{
                    url: '{{ url('home/autocomplete') }}?stock_detail=%QUERY',
                    wildcard: '%QUERY'
                }
        });
        $(".user_search").typeahead({
            hint: true,
            highlight: true,
            minLength: 1
        }, {
            source: engine.ttAdapter(),
            // This will be appended to "tt-dataset-" to form the class name of the suggestion menu.
            name: 'stock_detail',
            // the key from the array we want to display (name,id,email,etc...)
            displayKey: 'title',
             templates: {
                    empty: [
                        '<div class="empty-message text-center"><h4>No Book Available..</h4></div>'
                    ],
                    header: [
                        '<div class="list-group search-results-dropdown">'
                    ],
                    suggestion: function (data) {
                    return '<div class="media">\
                              <div class="media-left media-middle">\
                                <div class="media-left media-middle" style="border-bottom: 1px black;">\
                                <a href="{{url('book/detail')}}/'+data.code+'"> </a>\
                              </div>\
                              <div class="media-body">\
                                <a href="{{url('book/detail')}}/'+data.code+'"><h4 class="media-heading">"'+data.title+'"</h4> By <span>'+data.authorName+'</span> <span>'+data.edition+'</span></a> \
                                <a href="{{url('book/detail')}}/'+data.code+'"><p style="font-size:15px;"> '+data.plot_summary+'</p></a>\
                              </div><hr>\
                            </div>'
                }
            }
            });
    });
</script>
<script>
    $(document).ready(function() {
    $('#bootstrapModalFullCalendar').fullCalendar({
        events: [{"title":"Free Pizza","allday":"false","borderColor":"#5173DA","color":"#99ABEA","textColor":"#000000","description":"<p>This is just a fake description for the Free Pizza.</p><p>Nothing to see!</p>","start":"2016-11-29T10:23:03","end":"2016-11-29T11:23:03"},{"title":"DNUG Meeting","allday":"false","borderColor":"#5173DA","color":"#99ABEA","textColor":"#000000","description":"<p>This is just a fake description for the DNUG Meeting.</p><p>Nothing to see!</p>","start":"2016-11-30T10:23:03","end":"2016-11-30T11:23:03"},{"title":"Staff Meeting","allday":"false","borderColor":"#5173DA","color":"#99ABEA","textColor":"#000000","description":"<p>This is just a fake description for the Staff Meeting.</p><p>Nothing to see!</p>","start":"2016-12-01T10:23:03","end":"2016-12-03T10:23:03"},{"title":"Poker Night","allday":"false","borderColor":"#5173DA","color":"#99ABEA","textColor":"#000000","description":"<p>This is just a fake description for the Poker Night.</p><p>Nothing to see!</p>","start":"2016-11-17T10:23:03","end":"2016-11-17T11:23:03"},{"title":"Beer Garden","allday":"false","borderColor":"#5173DA","color":"#99ABEA","textColor":"#000000","description":"<p>This is just a fake description for the Beer Garden.</p><p>Nothing to see!</p>","start":"2016-12-03T10:23:03","end":"2016-12-03T11:23:03"},{"title":"XBox Tourney","allday":"false","borderColor":"#5173DA","color":"#99ABEA","textColor":"#000000","description":"<p>This is just a fake description for the XBox Tourney.</p><p>Nothing to see!</p>","start":"2016-12-04T10:23:03","end":"2016-12-06T10:23:03"},{"title":"Pool Party","allday":"false","borderColor":"#5173DA","color":"#99ABEA","textColor":"#000000","description":"<p>This is just a fake description for the Pool Party.</p><p>Nothing to see!</p>","start":"2016-12-05T10:23:03","end":"2016-12-05T11:23:03"},{"title":"Staff Meeting","allday":"false","borderColor":"#5173DA","color":"#99ABEA","textColor":"#000000","description":"<p>This is just a fake description for the Staff Meeting.</p><p>Nothing to see!</p>","start":"2016-11-21T10:23:03","end":"2016-11-21T11:23:03"},{"title":"Poker Night","allday":"false","borderColor":"#5173DA","color":"#99ABEA","textColor":"#000000","description":"<p>This is just a fake description for the Poker Night.</p><p>Nothing to see!</p>","start":"2016-12-13T10:23:03","end":"2016-12-15T10:23:03"},{"title":"Hackathon","allday":"false","borderColor":"#5173DA","color":"#99ABEA","textColor":"#000000","description":"<p>This is just a fake description for the Hackathon.</p><p>Nothing to see!</p>","start":"2016-12-08T10:23:03","end":"2016-12-08T11:23:03"},{"title":"Beta Testing","allday":"false","borderColor":"#5173DA","color":"#99ABEA","textColor":"#000000","description":"<p>This is just a fake description for the Beta Testing.</p><p>Nothing to see!</p>","start":"2016-12-09T10:23:03","end":"2016-12-09T11:23:03"},{"title":"Perl Meetup","allday":"false","borderColor":"#820F20","color":"#A6113C","textColor":"#ffffff","description":"<p>This is just a fake description for the Perl Meetup.</p><p>Nothing to see!</p>","start":"2016-11-29T10:23:03","end":"2016-11-29T12:23:03"},{"title":"Node.js Meetup","allday":"false","borderColor":"#820F20","color":"#A6113C","textColor":"#ffffff","description":"<p>This is just a fake description for the Node.js Meetup.</p><p>Nothing to see!</p>","start":"2016-11-30T10:23:03","end":"2016-11-30T12:23:03"},{"title":"Javascript Meetup","allday":"false","borderColor":"#820F20","color":"#A6113C","textColor":"#ffffff","description":"<p>This is just a fake description for the Javascript Meetup.</p><p>Nothing to see!</p>","start":"2016-12-01T10:23:03","end":"2016-12-02T10:23:03"},{"title":"HTML Meetup","allday":"false","borderColor":"#820F20","color":"#A6113C","textColor":"#ffffff","description":"<p>This is just a fake description for the HTML Meetup.</p><p>Nothing to see!</p>","start":"2016-11-25T10:23:03","end":"2016-11-25T12:23:03"},{"title":"CSS Meetup","allday":"false","borderColor":"#820F20","color":"#A6113C","textColor":"#ffffff","description":"<p>This is just a fake description for the CSS Meetup.</p><p>Nothing to see!</p>","start":"2016-12-03T10:23:03","end":"2016-12-03T12:23:03"}],
        header: {
            left: '',
            center: 'prev title next',
            right: ''
        },
        eventClick:  function(event, jsEvent, view) {
            // console.log(event.start);
            var dateStart = event.start.format('YYYY-MM-DD');
            var dateEnd = event.end.format('YYYY-MM-DD');

            var timeDate = event.start.format('h(:mm)t');
            // console.log(newdate._d);
            // var dates  = late.toDate();
            $('#modalTitle').html(event.title);
            $('#showDateStart').html(dateStart);
            $('#showDateEnd').html(dateEnd);
            $('#timeDate').html(timeDate);
            $('#modalBody').html(event.description);
            $('#fullCalModal').modal();
        }
    });
});
</script>
@yield('footer')
<!-- END THEME LAYOUT SCRIPTS -->
@endsection
