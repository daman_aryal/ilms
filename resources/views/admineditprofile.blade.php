@extends('adminMaster')
@section('title','Application User Management')
@section('headerscript')
<style>
	.float{
		position:fixed;
		width:60px;
		height:60px;
		bottom:40px;
		right:40px;
		background-color:#3fd5c0;
		color:#FFF;
		border-radius:50px !important;
		text-align:center;
		z-index: 9999;
	}
	.float:hover{

		transition: all 1s;
		transform: translateZ(10px);

    transform: rotateY(-180deg);
		box-shadow: 0 14px 28px rgba(0,0,0,0.25), 0 10px 10px rgba(0,0,0,0.22);
		color:#fff;
	}

	.my-float{
		font-size: 30px;
		margin-top: 25px;
		margin-left: 2px;
	}
</style>
@endsection

@section('page-content')
	<div class="page-content-wrapper">
	<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <a href="{{ url('dashboard') }}">Home</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <span>Change Passwords</span>
                    </li>
                </ul>
            </div>
            <div class="page-title"></div>
			<div class="portlet light bordered">
				<div class="portlet-title">
					<div class="caption font-green-haze">
						<i class="icon-social-dropbox font-green-haze"></i>
						<span class="caption-subject bold uppercase">Change My Password</span>
					</div>

				 	<div class="tools"> </div>
				</div>
				@if (Session::has('message'))
                   <div class="alert alert-info">{{ Session::get('message') }}</div>
                @endif
				{!! Form::open(['url' => '/updateProfile','method'=>'POST','id'=>'basicUserForm']) !!}
                	@include('partials.user.form_edit_profile')
                {!!Form::close()!!}
			</div>
		</div>
		
	</div>
@endsection
