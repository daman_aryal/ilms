@extends('adminMaster')
@section('title','User | Show')

@section('page-content')
	<div class="page-content-wrapper">
	<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<div class="page-bar">
	            <ul class="page-breadcrumb">
	                <li>
	                    <a href="{{ url('dashboard') }}">Home</a>
	                    <i class="fa fa-circle"></i>
	                </li>
	                <li>
	                    <a href="{{ url('adminUser') }}">Application User Management</a>
	                    <i class="fa fa-circle"></i>
	                </li>
	                <li>
	                    <span>Show</span>
	                </li>
	            </ul>
	        </div>
	        <div class="page-title"></div>
		    <div class="portlet light bordered">
				<div class="col-lg-12 margin-tb">
					<div class="pull-left">
						<h2>Show User</h2>
					</div>
					<div class="pull-right">
						<a class="btn btn-primary" href="{{ url('adminUser') }}"><i class="fa fa-arrow-left "></i> Back</a>
					</div>
				</div>

				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12">
						<div class="form-group">
							<strong>Name:</strong>
							{{ $user->name }}
						</div>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-12">
						<div class="form-group">
							<strong>Email:</strong>
							{{ $user->email }}
						</div>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-12">
						<div class="form-group">
							<strong>Roles:</strong>
							@if(!empty($user->roles))
							@foreach($user->roles as $v)
							<label class="label label-success">{{ $v->display_name }}</label>
							@endforeach
							@endif
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection