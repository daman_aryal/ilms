@extends('layout')

@section('headercss')
 <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="{{ url('assets/layouts/layout/css/layout.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ url('assets/layouts/layout/css/themes/light.min.css') }}" rel="stylesheet" type="text/css" id="style_color" />
        <link href="{{ url('assets/layouts/layout/css/custom.min.css') }}" rel="stylesheet" type="text/css" />
         <link rel="stylesheet" href="{{ url('css/dropify.min.css') }}">
         <link href="{{url('assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{url('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')}}" rel="stylesheet" type="text/css" />
        {{-- https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.8/css/materialize.min.css --}}
        {{-- <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.8/css/materialize.min.css"> --}}

        @yield('headerscript')
         <style>

         	.btn-md {
			    font-size: 16px;
			    padding: 12px 26px 10px;
				margin-top: 20px;


			}
			.btn-md.blue-outline {
			    color: #FFF;
			    background-color: #3598dc;
			    border: none;
			    border-radius: 2px;
			        box-shadow: 0 1px 3px rgba(0,0,0,.1),0 1px 2px rgba(0,0,0,.18);
			}
			.btn-md.blue-outline:hover{
			    color: #FFF;
			    background-color: #217ebd;
			      border: none;
			    border-radius: 2px;
			    box-shadow: 0 3px 6px rgba(0,0,0,.2),0 3px 6px rgba(0,0,0,.26);
			}

         	.btn-md-new{
         		/*padding:10px 5px !important;*/
         		margin-left: 20px;
         		margin-bottom: 5px;
         		margin-top: 8px !important;
         	}
         	 .btn-md {
                font-size: 16px;
                padding: 12px 26px 10px;
            }
            .btn-footer{
                font-size: 12px;
                padding: 8px 15px 8px;
            }
            .blue-outline {
                color: #FFF;
                background-color: #3598dc;
                border: none;
                border-radius: 2px;
                    box-shadow: 0 1px 3px rgba(0,0,0,.1),0 1px 2px rgba(0,0,0,.18);
            }
            .blue-outline:hover{
                color: #FFF;
                background-color: #217ebd;
                  border: none;
                border-radius: 2px;
                box-shadow: 0 3px 6px rgba(0,0,0,.2),0 3px 6px rgba(0,0,0,.26);
            }
            .modal-padding{
                padding: 25px;
                padding-bottom:0px;
            }
            .modal-header.bg{
                background-color: #72aecc;
            }
            .font-color{
                color:white;
                font-weight: 400;
            }

            .footer-margin{
                margin-top: 20px;
            }
            .btn-margin{
                margin-right: 5px;
            }
            .md-fab.md-fab-accent {
    background: #7cb342;
}
.md-fab {
    box-sizing: border-box;
    width: 64px;
    height: 64px;
    border-radius: 50%;
    background: #fff;
    color: #727272;
    display: block;
    box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);
    -webkit-transition: box-shadow 280ms cubic-bezier(0.4, 0, 0.2, 1);
    transition: box-shadow 280ms cubic-bezier(0.4, 0, 0.2, 1);
    border: none;
    position: relative;
    text-align: center;
    cursor: pointer;
}
.md-fab.md-fab-accent > i {
    color: #fff;
}
.md-fab > i {
    font-size: 36px;
    line-height: 64px;
    height: inherit;
    width: inherit;
    position: absolute;
    left: 0;
    top: 0;
    color: #727272;
}

         </style>


        <!-- END THEME LAYOUT STYLES -->
@endsection

@section('body')
	<!-- BEGIN HEADER -->
	@include('partials.admin.header')
	<!-- END HEADER -->
	<!-- BEGIN HEADER & CONTENT DIVIDER -->
	<div class="clearfix"></div>
	<!-- END HEADER & CONTENT DIVIDER -->
	<!-- BEGIN CONTAINER -->
	<div class="page-container">
	@include('partials.admin.sidebar')
		<!-- BEGIN CONTENT -->

	@yield('page-content')

	<!-- BEGIN QUICK SIDEBAR -->
	{{-- @include('partials.admin.quicksidebar') --}}
	<!-- END QUICK SIDEBAR -->

	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER -->
	@include('partials.admin.footer')
@endsection
@section('footerscript')
<!-- BEGIN THEME LAYOUT SCRIPTS -->

{{-- {!! Html::script('js/dropify.min.js') !!} --}}

{{-- <script src="{{ url('assets/layouts/layout/scripts/layout.min.js') }}" type="text/javascript"></script> --}}
{{-- <script src="{{ url('assets/layouts/layout/scripts/demo.min.js') }}" type="text/javascript"></script> --}}

 {{-- <script src="{{ url('js/dropify.min.js') }}"></script> --}}

   {{-- <script src="{{url('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}" type="text/javascript"></script> --}}

{{-- <script src="{{url('assets/global/scripts/app.min.js') }}" type="text/javascript"></script> --}}

{{-- <script src="{{url('assets/pages/scripts/components-date-time-pickers.min.js')}}" type="text/javascript"></script> --}}
{{-- <script src="{{url('assets/global/scripts/datatable.js')}}" type="text/javascript"></script> --}}
{{-- <script src="{{url('assets/global/plugins/datatables/datatables.min.js')}}" type="text/javascript"></script> --}}
{{-- <script src="{{url('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')}}" type="text/javascript"></script> --}}
{{-- <script src="{{url('assets/pages/scripts/table-datatables-buttons.min.js')}}" type="text/javascript"></script> --}}

{{-- https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.8/js/materialize.min.js --}}
{{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.8/js/materialize.min.js"></script> --}}

@yield('page-script')
{!! Html::script('assets/layouts/layout/scripts/layout.min.js') !!}
<!-- END THEME LAYOUT SCRIPTS -->
@endsection
