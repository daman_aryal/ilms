<div class="row">

	<div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group form-md-line-input">
            <label class="col-md-2 control-label" for="form_control_1">Title :</label>
            <div class="col-md-7">
               {!! Form::text('title', null, array('placeholder' => 'Title','class' => 'form-control')) !!}
                <div class="form-control-focus"> </div>
            </div>
            <label class="col-md-1 control-label" for="form_control_1">Active :</label>
            <div class="col-md-2">
              {{ Form::checkbox('status', 1, old('status'), [ 'class' => 'make-switch', 'data-size' => 'make-switch' ] ) }}
            </div>
       </div>
   </div>

   <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group form-md-line-input">
            <label class="col-md-2 control-label" for="form_control_1">Code :</label>
            <div class="col-md-10">
            {!! Form::text('code', null, array('placeholder' => 'Code','class' => 'form-control')) !!}
            <div class="form-control-focus"> </div>
           </div>
       </div>
   </div>
   <div class="clearfix"></div>
   <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group form-md-line-input">
            <label class="col-md-2 control-label" for="form_control_1">Description :</label>
            <div class="col-md-10">
            {!! Form::textarea('description',null, array('placeholder' => 'Description','class' => 'form-control', 'rows'=>'3')) !!}
            <div class="form-control-focus"> </div>
           </div>
       </div>
   </div>
   <div class="col-xs-12 col-sm-12 col-md-12">
               <div class="form-group form-md-checkboxes">
                  <label class="col-md-12 control-label textalign" for="form_control_1"> <strong>Policy:</strong> </label>
                  @if(isset($member))
                        @foreach($policy as $id => $value)
                          <div class="col-md-12">
                             <div class="md-checkbox-list">
                                <div class="md-checkbox">
                                  @foreach($member->policy as $data)
                                   <input type="checkbox" id="checkbox1_{{$value->id}}" name="policy[]" @if($data->id == $id) checked @endif value={{$value->id}} class="md-check">
                                  @endforeach
                                   <label for="checkbox1_{{$value->id}}">
                                      <span></span>
                                      <span class="check"></span>
                                      <span class="box"></span> {{$value->description}} 
                                   </label>
                                </div>
                             </div>
                          </div>
                        @endforeach
                  @else
                  @foreach($policy as $id => $value)
                  <div class="col-md-12">
                     <div class="md-checkbox-list">
                        <div class="md-checkbox">

                           {{ Form::checkbox('policy[]', $value->id,false, array('id'=>'checkbox1_'.$value->id, 'class' => 'md-check')) }}
                           <label for="checkbox1_{{$value->id}}">

                              <span></span>
                              <span class="check"></span>
                              <span class="box"></span> {{$value->description}} 
                           </label>
                        </div>
                     </div>
                  </div>
                  @endforeach
                  @endif
               </div>
            </div>

   <div class="col-xs-12 col-sm-12 col-md-12" style="padding-top: 25px;">
       <button type="submit" class="btn btn-primary col-md-offset-2">Save</button>
       <a href="{{ url('/membertype') }}" class="btn btn-default">Cancel</a>

  </div>
</div>