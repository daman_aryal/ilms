
   <div class="form-body">
      <div class="row div-form">
         <div class="col-md-8">
            <div class="form-group form-md-line-input">
               <label class="col-md-3 control-label" for="first_name">First Name:</label>
               <div class="col-md-7">
                  {!! Form::text('first_name',( isset($user->staffUser[0]->first_name) ? $user->staffUser[0]->first_name : null ), array('class' => 'form-control','id' => 'first_name','placeholder'=>'Enter your first name')) !!}
                  <div class="form-control-focus"> </div>
               </div>
            </div>
            <div class="form-group form-md-line-input">
               <label class="col-md-3 control-label" for="last_name">Last Name:</label>
               <div class="col-md-7">
                  {!! Form::text('last_name',( isset($user->staffUser[0]->last_name) ? $user->staffUser[0]->last_name : null ), array('class' => 'form-control','id' => 'last_name','placeholder'=>'Enter your last name')) !!}
                  <div class="form-control-focus"> </div>
               </div>
            </div>
            {{-- <div class="form-group form-md-line-input">
               <label class="col-md-3 control-label" for="date_of_birth">Date of Birth:</label>
               <div class="col-md-7">
                  {!! Form::date('date_of_birth',( isset($user->staffUser[0]->date_of_birth) ? $user->staffUser[0]->date_of_birth : null ), array('class' => 'form-control','id' => 'date_of_birth','placeholder'=>'Enter your date of birth')) !!}
                  <div class="form-control-focus"> </div>
               </div>
            </div> --}}
            <div class="form-group form-md-line-input">
                <label class="control-label col-md-3">Date of Birth:</label>
                <div class="col-md-7">
                  {!! Form::text('date_of_birth',( isset($user->staffUser[0]->date_of_birth) ? $user->staffUser[0]->date_of_birth : null ), array('class' => 'form-control date-picker','id' => 'date_of_birth','placeholder'=>'YYYY-MM-DD', 'data-date-format' => 'yyyy-mm-dd')) !!}
                    {{-- <input class="form-control date-picker" size="16" type="text" placeholder="DD/MM/YYYY"> --}}
                    <div class="form-control-focus"></div>
                </div>
            </div>
         </div>

         @if($edit)
            <div class="col-md-2 img-upload">
               <input id="user_image" class="dropify" data-height="140" data-width="100" data-allowed-file-extensions="png jpeg jpg" data-max-file-size="2M" data-default-file="{{( isset($user->images->path) ? '/'.$user->images->path : null )}}" name="user_image" type="file">
            </div>

         @else
            <div class="col-md-2 img-upload">
               <input id="user_image" class="dropify" data-height="140" data-width="100" data-allowed-file-extensions="png jpeg jpg" data-max-file-size="2M" data-default-file="" name="user_image" type="file">
            </div>
         @endif

      </div>
      <div class="form-group form-md-line-input full-row-fg">
      <label class="col-md-2 control-label" for="contact">Contact no:</label>
         <div class="col-md-10">
            {!! Form::number('contact',null, array('class' => 'form-control','id' => 'contact','placeholder'=>"Enter your contact no")) !!}
            <div class="form-control-focus"> </div>
         </div>
      </div>

      <div class="row div-form">
         <div class="col-md-6">
            <div class="form-group form-md-line-input">
               <label class="col-md-3 control-label" for="permanent_address">Permanent Address:</label>
               <div class="col-md-7 hori-mar-inp">
                  {!! Form::text('permanent_address',( isset($user->staffUser[0]->permanent_address) ? $user->staffUser[0]->permanent_address : null ), array('class' => 'form-control','id' => 'permanent_address','placeholder'=>"Enter Permanent Address")) !!}
                  <div class="form-control-focus"> </div>
               </div>
            </div>
         </div>

         
         <div class="col-md-6">
            <div class="form-group form-md-line-input">
               <label class="col-md-3 control-label" for="temporary_address">Temporary Address:</label>
               <div class="col-md-7 hori-mar-inp">
                  {!! Form::text('temporary_address', ( isset($user->staffUser[0]->temporary_address) ? $user->staffUser[0]->temporary_address : null ), array('class' => 'form-control','id' => 'temporary_address','placeholder'=>"Enter Temporary Address")) !!}
                  <div class="form-control-focus"> </div>
               </div>
            </div>
         </div>

      </div>


      <div class="row div-form">
         <div class="col-md-6">
            <div class="form-group form-md-line-input">
               <label class="col-md-3 control-label" for="department_name">Department Name:</label>
               <div class="col-md-7 hori-mar-inp">
                  {!! Form::select('department_id',$department,( isset($user->staffUser[0]->department_id) ? $user->staffUser[0]->department_id : null ), array('class' => 'form-control','id' => 'department','placeholder'=>"Select Department Name")) !!}
                  <div class="form-control-focus"> </div>
               </div>
            </div>
         </div>
         
         <div class="col-md-6">
            <div class="form-group form-md-line-input">
               <label class="col-md-3 control-label" for="batch">Designation:</label>
               <div class="col-md-7 hori-mar-inp">
                  {!! Form::text('designation', ( isset($user->staffUser[0]->designation) ? $user->staffUser[0]->designation : null ), array('class' => 'form-control','id' => 'batch','placeholder'=>"Enter the Designation")) !!}
                  <div class="form-control-focus"> </div>
               </div>
            </div>
         </div>

      </div>

      <div class="row div-form">
         <div class="col-md-6">
            <div class="form-group form-md-line-input">
               <label class="col-md-3 control-label" for="job_type">Job Type:</label>
               <div class="col-md-7 hori-mar-inp">
                  {!! Form::select('job_type',['temporary' => 'Temporary', 'permanent' => 'Permanent', 'contract' => 'Contract'],( isset($user->staffUser[0]->job_type) ? $user->staffUser[0]->job_type : null ), array('class' => 'form-control','id' => 'job_type','placeholder'=>"Select Job Type")) !!}
                  <div class="form-control-focus"> </div>
               </div>
            </div>
         </div>

         
         <div class="col-md-6">
            <div class="form-group form-md-line-input">
                <label class="control-label col-md-3">Jointed Date:</label>
                <div class="col-md-7 hori-mar-inp">
                  {!! Form::text('jointed_date',( isset($user->staffUser[0]->joined_date) ? $user->staffUser[0]->joined_date : null ), array('class' => 'form-control date-picker','id' => 'jointed_date','placeholder'=>'YYYY-MM-DD', 'data-date-format' => 'yyyy-mm-dd')) !!}
                    <div class="form-control-focus"></div>
                </div>
            </div>
         </div>

      </div>

      {{-- <div class="form-group form-md-line-input full-row-fg">
      <label class="col-md-2 control-label" for="valid_till">Valid Till:</label>
         <div class="col-md-6">
            {!! Form::date('valid_till',( isset($user->staffUser[0]->valid_till) ? $user->staffUser[0]->valid_till : null ), array('class' => 'form-control','id' => 'valid','placeholder'=>"Enter the valid date")) !!}
            <div class="form-control-focus"> </div>
         </div>
      </div>
 --}}
    <div class="form-group form-md-line-input">
        <label class="control-label col-md-2">Valid Till:</label>
        <div class="col-md-7">
          {!! Form::text('valid_till',( isset($user->staffUser[0]->valid_till) ? $user->staffUser[0]->valid_till : null ), array('class' => 'form-control date-picker','id' => 'valid','placeholder'=>'YYYY-MM-DD', 'data-date-format' => 'yyyy-mm-dd')) !!}
            <div class="form-control-focus"></div>
        </div>
    </div>

      <div class="row div-form">
         <div class="col-md-6">
            <div class="form-group form-md-line-input">
               {{-- <label class="col-md-3 control-label" for="member_code">Member Code:</label> --}}
               <div class="col-md-7 hori-mar-inp">
                  {!! Form::hidden('member_code',null, array('class' => 'form-control','id' => 'member_code','placeholder'=>"Enter the member code",'readonly'=>'true')) !!}
                   <div class="form-control-focus"> </div>
               </div>
            </div>
         </div>
      </div>
   </div>
