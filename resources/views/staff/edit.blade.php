@extends('adminMaster')
@section('title','Staff | Edit')

@section('headerscript')
   <style>
      @media (min-width: 992px){
         .hori-mar-lb{
         margin-left: 18px;
         }
         .hori-mar-inp{
            margin-left: 43px;
         }
      }
      @media (max-width: 1200px){
         .div-form>div{
            padding-left: 20px;
         }
         .img-upload{
            margin-top: 20px;
            padding-left: 35px !important;
         }
      }
   </style>
@endsection
@section('page-content')

<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
  <!-- BEGIN CONTENT BODY -->
  <!-- BEGIN CONTENT BODY -->
  <div class="page-content">
    <div class="portlet light bordered">
      <div class="portlet-title">
        <div class="caption font-green-haze">
          <i class="icon-settings font-green-haze"></i>
          <span class="caption-subject bold uppercase">Create New Staff User</span>
        </div>
        <div class="actions">
          <a class="btn btn-primary" href="{{ url('staffUser') }}"><i class="fa fa-arrow-left "></i> Back</a>
        </div>
      </div>

      <div class="portlet-body form">
        @if (count($errors) > 0)
        <div class="alert alert-danger">
          <strong>Whoops!</strong> There were some problems with your input.<br><br>
          <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
          </ul>
        </div>
        @endif
        {{-- {{ dd($user) }} --}}
        {!! Form::model($user,['url' => 'staffUser/update/'."$user->id",'method'=>'POST','file'=>'true', 'novalidate '=>'','enctype' => 'multipart/form-data','id'=>'basicUserForm']) !!}
        @include('staff.form')
        @include('partials.admin.email_password')
<div class="form-actions">
                <div class="row div-form">
                   <div class="col-md-offset-2 col-md-10">
                       <a href="{{ url('/libUser') }}" class="btn default">Cancel</a>
                       <button type="submit" id="staffFormSubmit" class="btn blue">Save</button>
                   </div>
                </div>
              </div>
        {!! Form::close() !!}
        <!-- END CONTENT BODY -->
      </div>
    </div>
  </div>
</div>
<!-- END CONTENT -->
@endsection
@section('page-script')
  {!! Html::script('js/dropify.min.js') !!}
  <script type="text/javascript">

    $(function() {
        var edit = {{ $edit }};
        if (edit) {
          $('#member').prop('disabled', true);
          $('#member_code').prop('disabled', true);

        }
    });
  $('#member').on('change', function (e) {
      // e.preventDefault();

      var member = $(this).val();
      console.log(member);
      $.ajax({
          url: '../api/member_type_staff/get_code',
          type: "GET",
          headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
          data: {member_type_id: member},
          success: function( msg ) {
            $('input[name="code"]').val(msg.code).prop('readonly', true);
            $('input[name="member_code"]').val(msg.member_code).prop('readonly', true);
          }
      });
  });

  </script>

@endsection
