@extends('adminMaster')
@section('title','Staff | Index')

@section('headerscript')
<style>
	.float{
		position:fixed;
		width:60px;
		height:60px;
		bottom:40px;
		right:40px;
		background-color:#3fd5c0;
		color:#FFF;
		border-radius:50px !important;
		text-align:center;
		z-index: 9999;
	}
	.float:hover{

		transition: all 1s;
		transform: translateZ(10px);

    transform: rotateY(-180deg);
		box-shadow: 0 14px 28px rgba(0,0,0,0.25), 0 10px 10px rgba(0,0,0,0.22);
		color:#fff;
	}

	.my-float{
		font-size: 30px;
		margin-top: 25px;
		margin-left: 2px;
	}
</style>
@endsection

@section('page-content')
	<div class="page-content-wrapper">
	<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<div class="portlet light bordered">
				<div class="portlet-title">
					<div class="caption font-green-haze">
						<i class="icon-social-dropbox font-green-haze"></i>
						<span class="caption-subject bold uppercase">Staff User Management</span>
					</div>

				 <div class="tools"> </div>
				</div>
				@if ($message = Session::get('success'))
					<div class="alert alert-success">
						<p>{{ $message }}</p>
					</div>
				@endif

				 <table class="table table-striped table-bordered table-hover" id="sample_1">
					<thead>
						<tr>
						<th>SN.</th>
						<th>First Name</th>
						<th>Last Name</th>
						<th>Action</th>
					</tr>

					</thead>
					<tbody>
					@php
						$i =1;
					@endphp
					@foreach ($allUser as $getUser)
						@if (!empty($getUser->staffUser))
							@foreach ($getUser->staffUser as $key => $getBasicUser)
							<tr>
								<td>{{$i}}</td>
								<td>{{$getBasicUser->first_name}}</td>	
								<td>{{$getBasicUser->last_name}}</td>	
								<td><a href="{{ url('/') }}/staffUser/edit/{{$getUser->id}}"><i class="icon-pencil"></i> Edit</a></td>	
							</tr>
							@php
								$i++;
							@endphp
							@endforeach	
						@else
							{{dd('thhere')}}

						@endif

						
					@endforeach
									
					</tbody>

				</table>

				{{-- {!! $stock->render() !!} --}}
				<!-- END SAMPLE FORM PORTLET-->
			</div>
		</div>
		<a href="{{ url('staffUser/create') }}" class="float" title="Create New Stock">
			<i class="fa fa-plus my-float"></i>
		</a>
	</div>
@endsection
