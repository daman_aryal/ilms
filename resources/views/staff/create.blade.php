@extends('adminMaster')
@section('title','Staff | Create')

@section('headerscript')
   <style>
      @media (min-width: 992px){
        .hori-mar-inp{
          margin-left: 5px;
        }
      }
      @media (max-width: 1200px){
         .div-form>div{
            padding-left: 20px;
         }
         .img-upload{
            margin-top: 20px;
            width: 20%;         }
      }
   </style>
@endsection
@section('page-content')

<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<!-- BEGIN CONTENT BODY -->
	<!-- BEGIN CONTENT BODY -->
	<div class="page-content">
		<div class="portlet light bordered">
			<div class="portlet-title">
				<div class="caption font-green-haze">
					<i class="icon-settings font-green-haze"></i>
					<span class="caption-subject bold uppercase">Create New Staff User</span>
				</div>
				<div class="actions">
					<a class="btn btn-primary" href="{{ url('staffUser') }}"><i class="fa fa-arrow-left "></i> Back</a>
				</div>
			</div>

			<div class="portlet-body form">
				@if (count($errors) > 0)
				<div class="alert alert-danger">
					<strong>Whoops!</strong> There were some problems with your input.<br><br>
					<ul>
						@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
				@endif
				{!! Form::open(['url' => 'staffUser/store','method'=>'POST','file'=>'true', 'novalidate '=>'','enctype' => 'multipart/form-data','id'=>'basicUserForm']) !!}
				@include('staff.form')
				@include('partials.admin.email_password')
				{!! Form::close() !!}			    
				<!-- END CONTENT BODY -->
			</div>
		</div>
	</div>
</div>
<!-- END CONTENT -->
@endsection
@section('page-script')
  {!! Html::script('js/dropify.min.js') !!}
  <script type="text/javascript">
  $('#member').on('change', function (e) {
      // e.preventDefault();

      var member = $(this).val();
      // console.log(member);
      $.ajax({
          url: '../api/member_type_staff/get_code',
          type: "GET",
          headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
          data: {member_type_id: member},
          success: function( msg ) {
            $('input[name="code"]').val(msg.code).prop('readonly', true);
            $('input[name="member_code"]').val(msg.member_code).prop('readonly', true);
          }
      });
  });

  </script>

@endsection
