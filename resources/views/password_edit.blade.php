@extends('adminMaster')
@section('title','Password | Reset')

@section('page-content')
  <div class="page-content-wrapper">
  <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
      <div class="portlet light bordered">
        <div class="portlet-title">
          <div class="caption font-green-haze">
            <i class="icon-settings font-green-haze"></i>
            <span class="caption-subject bold uppercase">Change Password</span>
          </div>
        </div>
        <div class="portlet-body form">
          @if (count($errors) > 0)
            <div class="alert alert-danger">
              <strong>Whoops!</strong> There were some problems with your input.<br><br>
              <ul>
                @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                @endforeach
              </ul>
            </div>
          @endif
          @if (session('password_updated'))
            <div class="alert alert-danger">
              {{session('password_updated')}}
            </div>
          @endif
          @if (session('password_not_match'))
            <div class="alert alert-danger">
              {{session('password_not_match')}}
            </div>
          @endif
          <div class="row leftmargin">
            {!! Form::open(['url' => 'password/update/']) !!}
              <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group form-md-line-input">
                  <label class="col-md-2 control-label" for="form_control_1">Old Password</label>
                  <div class="col-md-10">
                    {!! Form::password('old_password', ['class'=>'form-control', 'placeholder' => 'password reset', 'autocomplete' => 'off']) !!}
                      <div class="form-control-focus"> </div>
                    @if(session('password_not_match'))
                      <p>{{session('password_not_match')}}</p>
                    @endif
                  </div>
                </div>
              </div>

              <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group form-md-line-input">
                  <label class="col-md-2 control-label" for="form_control_1">Password</label>
                  <div class="col-md-10">
                    {!! Form::password('password', array('placeholder' => 'Password','class' => 'form-control')) !!}
                    <div class="form-control-focus"> </div>
                  </div>
                </div>
              </div>

              <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group form-md-line-input">
                  <label class="col-md-2 control-label" for="form_control_1">Password Confirmation:</label>
                  <div class="col-md-10">
                    {!! Form::password('password_confirm', array('placeholder' => 'Confirm Password','class' => 'form-control')) !!}    
                  <div class="form-control-focus"> </div>
                  </div>
                </div>
              </div>
              <div class="col-xs-12 col-sm-12 col-md-12 text-center">
              {!! Form::submit('update', ['class'=>'btn blue pull-right']) !!}
              </div>
            {!! Form::close() !!}
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
