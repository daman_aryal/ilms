@inject('user_form', 'App\Injection\UserForm')
<div class="row form-body">
   <div class="col-xs-12 col-sm-12 col-md-12">
      <div class="form-group form-md-line-input">
         <label class="col-md-2 control-label" for="form_control_1">Name:</label>
         <div class="col-md-10">
            {!! Form::text('name', null, array('placeholder' => 'Name','class' => 'form-control')) !!}
            <div class="form-control-focus"> </div>
         </div>
      </div>
   </div>
   <div class="col-xs-12 col-sm-12 col-md-12">
      <div class="form-group form-md-line-input">
         <label class="col-md-2 control-label" for="form_control_1">Email:</label>
         <div class="col-md-10">
            {!! Form::text('email', null, array('placeholder' => 'Email','class' => 'form-control')) !!}
            <div class="form-control-focus"> </div>
         </div>
      </div>
   </div>
   <div class="col-xs-12 col-sm-12 col-md-12">
    <div class="form-group form-md-line-input">
        <label class="col-md-2 control-label" for="password">Password:</label>
        <div class="col-md-10">
            <div class="input-group">
                <div class="input-group-control">
                    {!! Form::password('password', array('placeholder' => 'Password','class' => 'form-control','id' => 'password')) !!}
                    <div class="form-control-focus"> </div>
                </div>
                <span class="input-group-btn btn-right">
                    <a class="btn green-haze" id="generate">Generate</a>
                </span>
            </div>
        </div>
    </div>
  </div>
   <div class="col-xs-12 col-sm-12 col-md-12">
      <div class="form-group form-md-line-input">
         <label class="col-md-2 control-label" for="form_control_1">Re-Password:</label>
         <div class="col-md-10">
            {!! Form::password('password_confirm', array('placeholder' => 'Confirm Password','class' => 'form-control','id' =>'password_confirm')) !!}    
            <div class="form-control-focus"> </div>
         </div>
      </div>
   </div>
   <div class="col-xs-12 col-sm-12 col-md-12" id="generated" style="display: none">
      <div class="form-group form-md-line-input">
          <label class="col-md-2 control-label" for="form_control_1">Generated Password:</label>
          <div class="col-md-10">
            <span id="view_pass"></span>    
          <div class="form-control-focus"> </div>
         </div>
     </div>
  </div>
   <div class="col-xs-12 col-sm-12 col-md-12">
      <div class="form-group form-md-line-input">
         <label class="col-md-2 control-label" for="form_control_1">Address:</label>
         <div class="col-md-10">
            {!! Form::text('address', null, array('placeholder' => 'Address','class' => 'form-control')) !!} <div class="form-control-focus"> </div>
         </div>
      </div>
   </div>
   <div class="col-xs-12 col-sm-12 col-md-12">
      <div class="form-group form-md-line-input">
         <label class="col-md-2 control-label" for="form_control_1">Contact:</label>
         <div class="col-md-10">
            {!! Form::text('contact', null, array('placeholder' => 'Contact','class' => 'form-control')) !!}
            <div class="form-control-focus"> </div>
         </div>
      </div>
   </div>


   {{-- <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group form-md-line-input">
            <label class="col-md-2 control-label" for="form_control_1">Roll no:</label>
            <div class="col-md-10">
            {!! Form::text('code',null, array('placeholder' => 'Roll no','class' => 'form-control')) !!}
            <div class="form-control-focus"> </div>
           </div>
       </div>
   </div> --}}
   <div class="col-xs-12 col-sm-12 col-md-12">
      <div class="form-group form-md-line-input">
         <label class="col-md-2 control-label" for="form_control_1">Role:</label>
         <div class="col-md-8">
         @if (isset($user->roles))
           @foreach ($user->roles as $element)
           {{-- {{dd($element->id)}} --}}
           {!! Form::select('role[]', $user_form->admin(), $element->id, ['class' => 'form-control', 'placeholder' => 'Select user type']) !!}
          @endforeach
        @else
          {!! Form::select('role[]',$user_form->admin(), null, ['class' => 'form-control', 'placeholder' => 'Select Role']) !!}
        @endif
        
            <div class="form-control-focus"> </div>
         </div>
         <div class="col-md-1"></div>
      </div>
      @if (isset($user))
        @if ($user->id != 1)
          <div class="row div-form">
         <div class="col-md-6">
            <div class="form-group form-md-line-input">
              <label class="col-md-4 control-label" for="section">Location :</label>
              <div class="col-md-7 hori-mar-inp">
              @if (isset($pivotLocation))
                  {!! Form::select('location_id',$locations, $pivotLocation->location_id, array('class' => 'form-control','id' => 'section','placeholder'=>"Select Location")) !!}
              @else  
              {!! Form::select('location_id',$locations,(isset($user->location_id) ? $user->location_id : null ), array('class' => 'form-control','id' => 'section','placeholder'=>"Select Location")) !!}
              @endif
                  <div class="form-control-focus"> </div>
              </div>

            </div>
         </div>
         <div class="clearfix"></div>
      </div>
        @endif
      @else
      <div class="row div-form">
         <div class="col-md-6">
            <div class="form-group form-md-line-input">
              <label class="col-md-4 control-label" for="section">Location :</label>
              <div class="col-md-7 hori-mar-inp">
              @if (isset($pivotLocation))
                  {!! Form::select('location_id',$locations, $pivotLocation->location_id, array('class' => 'form-control','id' => 'section','placeholder'=>"Select Location")) !!}
              @else  
              {!! Form::select('location_id',$locations,(isset($user->location_id) ? $user->location_id : null ), array('class' => 'form-control','id' => 'section','placeholder'=>"Select Location")) !!}
              @endif
                  <div class="form-control-focus"> </div>
              </div>

            </div>
         </div>
         <div class="clearfix"></div>
      </div>
      @endif
      
      

   </div>
</div>
<div class="form-actions">
   <div class="row">
      <div class="col-md-offset-2 col-md-10">
         <button type="submit" class="btn btn-primary">Save</button>
         {{-- <a href="{{ url('/adminUser') }}" class="btn btn-default">Cancel</a> --}}
      </div>
   </div>
</div>