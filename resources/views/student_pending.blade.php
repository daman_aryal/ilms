@extends('userMaster')
@section('title','Book Issued')
@section('page-content')
	<div class="page-content-wrapper">
        <div class="page-content">
            <div class="container">
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <a href="{{url('/home')}}">Home</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <span>Issued Books</span>
                    </li>
                </ul>
                <div class="page-content-inner">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="caption font-dark">
                                            {{-- <i class="icon-settings font-dark"></i> --}}
                                            <span class="caption-subject bold uppercase">Books You Returned</span>
                                        </div>
                                        <div class="tools"> </div>
                                    </div>
                                    <div class="portlet-body">
                                        <table class="table table-striped table-bordered table-hover" id="sample_1">
                                        <thead>
                                                <tr>
                                                <th>No</th>
                                                <th>Title</th>
                                                <th>Requested on</th>
                                                <th>Requested for date</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>1</td>
                                                    <td>Web Design</td>
                                                    <td>Nov 10, 2016</td>
                                                    <td>Nov 25, 2016</td>
                                                </tr> 
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection