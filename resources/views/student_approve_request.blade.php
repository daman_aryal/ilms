@extends('userMaster')
@section('title','Request Book')
@section('page-content')
	<div class="page-content-wrapper">
        <div class="page-content">
            <div class="container">
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <a href="{{url('/home')}}">Home</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <span>Requested Book</span>
                    </li>
                </ul>
                <div class="page-content-inner">

                    @if (count($errors) > 0)

                        <div class="alert alert-danger">
                            <strong>Whoops!</strong> There were some problems with your input.<br><br>
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet light portlet-fit portlet-form bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class=" icon-layers font-red"></i>
                                        <span class="caption-subject font-red sbold uppercase">Request Book</span>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <!-- BEGIN FORM-->
                                    <form action="/student/request" method="POST" id="requestForm">
                                        {!! csrf_field()!!}
                                        <div class="form-body">
                                            <div class="form-group form-md-line-input form-md-floating-label col-md-4" >
                                                <input type="text" name="title" class="form-control" id="material" style="margin-top: 17px" data-validation="required" data-validation-error-msg="You did not enter a title.">
                                                <label for="material">Title of the Material</label>
                                                <span style="color:red" class="error"></span>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group form-md-line-input form-md-floating-label col-md-4 " >
                                                <input type="text" name="publication" class="form-control" id="publication" style="margin-top: 17px">
                                                <label for="publication">Publication</label>
                                                <span style="color:red" class="error"></span>
                                            </div>
                                            <div class="col-md-2"></div>   
                                            <div class="form-group form-md-line-input form-md-floating-label col-md-4" >
                                                <input type="text" name="author" class="form-control" id="author" style="margin-top: 17px">
                                                <label for="form_control_1">Author</label>
                                                <span style="color:red" class="error"></span>
                                            </div>
                                            <div class="clearfix"></div>

                                            <div class="form-group form-md-line-input form-md-floating-label col-md-6" >
                                               <input type="url" name="url" class="form-control" style="margin-top: 17px;">
                                                <label for="deatil">Url</label>
                                                <span style="color:red" class="error"></span>
                                            </div>
                                            <div class="clearfix"></div>  

                                            <div class="form-group form-md-line-input form-md-floating-label col-md-12" >
                                                <textarea class="form-control" name="description" id="description"></textarea>
                                                <label for="description">Detail</label>
                                                <span style="color:red" class="error"></span>
                                            </div>
                                            <div class="clearfix"></div>  

                                            <div class="form-group form-md-line-input form-md-floating-label col-md-4">
                                                <label for="user_name">Tag Name</label>
                                                                <select id="select2-button-addons-single-input-group-sm" name="user_name[]" class="form-control userSelect" multiple>
                                                                </select> 
                                                                <p><em>Tag minimum 10 users.</em></p>
                                                <span style="color:red" class="error"></span>
                                            </div>                                         
                                        </div>
                                        <div class="form-actions">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    {{-- <a href="javascript:;" class="btn green-haze">Submit</a> --}}
                                                    <button type="submit" class="btn green-haze">Submit</button>
                                                    <a href="javascript:;" class="btn red-intense">Cancel</a>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <!-- END FORM-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('footerscript')
  {!! Html::script('assets/global/plugins/jquery.min.js') !!}
  {!! Html::script('assets/global/plugins/bootstrap/js/bootstrap.min.js') !!}
  {!! Html::script('assets/global/plugins/select2/js/select2.full.min.js') !!}
  {!! Html::script('js/tag.user.js') !!}
  {!! Html::script('js/request.js') !!}
  <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>

  <script>
    
    $("#requestForm").validate({
        errorClass: "myErrorClass",
      ignore: [],       
      rules: {
        title: {
              required: true,
              minlength: 4,
             },
        publication: {
            required: true,
            minlength: 3,
        },

        author: {
            required: true,
            minlength: 3,

        },
        user_name: {
            required: true,
            minlength: 3,
        },
      },
      messages: {
        title: "Please Enter Title of the book.",
      },
      errorPlacement: function(error, element) {
          // if(element.attr("name") == "title") {
            error.appendTo( element.parent("div").find('.error') );
          // } else {
          //   error.insertAfter(element);
          // }

         //  switch (element) {
         //    case element.attr("name") === 'title':
         //        error.appendTo( element.parent("div").find('.error') );
         //        break;
         //    case element.attr("name") === 'location':
         //        error.insertAfter($("#location"));
         //        break;
         //    default:
         //        //nothing
         // }


        },

                //When there is an error normally you just add the class to the element.
        // But in the case of select2s you must add it to a UL to make it visible.
        // The select element, which would otherwise get the class, is hidden from
        // view.
        // highlight: function (element, errorClass, validClass) {
        //     var elem = $(element);
        //     if (elem.hasClass("select2-offscreen")) {
        //         $("#select2-button-addons-single-input-group-sm" + elem.attr("id") + " ul").addClass(errorClass);
        //     } else {
        //         elem.addClass(errorClass);
        //     }
        // },

        //When removing make the same adjustments as when adding
        // unhighlight: function (element, errorClass, validClass) {
        //     var elem = $(element);
        //     if (elem.hasClass("select2-offscreen")) {
        //         $("#select2-button-addons-single-input-group-sm" + elem.attr("id") + " ul").removeClass(errorClass);
        //     } else {
        //         elem.removeClass(errorClass);
        //     }
        // }
    
    });

  </script>

@endsection