@extends('adminMaster')
@section('title','Stock | Create')
@section('headerscript')
    {!! Html::style('css/toastr.min.css') !!}
    {!! Html::style('css/loader.css') !!}
    {!! Html::style('assets/global/plugins/select2/css/select2-bootstrap.min.css') !!}
    {!! Html::style('assets/global/plugins/select2/css/select2.min.css') !!}
    {!! Html::style('assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css') !!}
    {!! Html::style('assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css') !!}
    {!! Html::style('assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css') !!}

    
    <style>
        .tt-query {
            -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
            -moz-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
        }
        .select2-container--focus,.select2-container--open,.select2-container--below,.select2-search__field{
            width: 355px !important;
        }
        .tt-hint {
            color: #999
        }
        .tt-menu {    /* used to be tt-dropdown-menu in older versions */
            width: 300px;
            margin-top: 4px;
            padding: 4px 0;
            background-color: #fff;
            border: 1px solid #ccc;
            border: 1px solid rgba(0, 0, 0, 0.2);
            -webkit-border-radius: 4px;
            -moz-border-radius: 4px;
            border-radius: 4px;
            -webkit-box-shadow: 0 5px 10px rgba(0,0,0,.2);
            -moz-box-shadow: 0 5px 10px rgba(0,0,0,.2);
            box-shadow: 0 5px 10px rgba(0,0,0,.2);
        }
        /*.select2-result-repository__avatar img {
        height: 60px;
        }*/
        .tt-suggestion {
            padding: 3px 20px;
            line-height: 24px;
        }
        .tt-highlight  {
            /*font-weight: normal;*/
            color: #72aecc !important;
        }
        .tt-suggestion.tt-cursor,.tt-suggestion:hover {
            color: #fff;
            background-color: #72aecc;
        }
        .tt-suggestion p {
            margin: 0;
        }
    </style>
@endsection

@section('page-content')
    {{-- Author MODAL STARTS --}}
    <div id="authorModal" class="modal authorModal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" data-focus-on="input:first" >
        <div class="modal-header bg">
            <h4 class="modal-title font-color" style="">Author Detail Info</h4>
        </div>
        <div class="modal-body modal-padding">
            {!! Form::open(['method'=>'POST','files'=>true, 'novalidate '=>'','id' => 'authorForm']) !!}
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group form-md-line-input form-md-floating-label has-success" id="authorInput">
                            <input type="text" name="authorName" class="form-control" id="authorName">
                            <label for="authorName">Name</label>
                            <span class="help-block" id="authorError"></span>
                        </div>
                        <div class="form-group form-md-line-input form-md-floating-label has-success">
                            <input type="text" class="form-control" name= "authorLink" id="authorLink">
                            <label for="authorLink">Link</label>
                        </div>
                    </div>
                    <div id="fountainG" style="display: none;">
                        <div id="fountainG_1" class="fountainG"></div>
                        <div id="fountainG_2" class="fountainG"></div>
                        <div id="fountainG_3" class="fountainG"></div>
                        <div id="fountainG_4" class="fountainG"></div>
                        <div id="fountainG_5" class="fountainG"></div>
                        <div id="fountainG_6" class="fountainG"></div>
                        <div id="fountainG_7" class="fountainG"></div>
                        <div id="fountainG_8" class="fountainG"></div>
                    </div>
                    <div class="col-md-6">
                        <div class="mt-list-container list-simple ext-1 col-md-12">
                            <div class="pull-right">
                                {{-- <input name = "author_image" type="file" id="input-file-now-custom-2" class="dropify" data-height="175" /> --}}
                                {!! Form::file('author_image',['id'=>'authorImage','class' => 'dropify','data-height' => '175','data-allowed-file-extensions'=>'png jpeg jpg','data-max-file-size'=>'2M']) !!}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group form-md-line-input form-md-floating-label has-success">
                            <textarea class="form-control" name="authorAbout" id="authorAbout" rows="3"></textarea>
                            <label for="form_control_1">About</label>
                        </div>
                    </div>
                </div>
                <div class="row footer-margin">
                    <div class="modal-footer">
                        <button type="button" class="btn-footer blue-outline btn-margin" id="addAuthor">Add</button>
                        {{--  {!! Form::submit('Add',['class'=>'btn-footer blue-outline btn-margin', 'id' => 'addAuthor']) !!} --}}
                        <button type="button" class="btn-footer blue-outline" id="authorCancel" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        {!! Form::close() !!}
    </div>
    {{-- Author MODAL ENDS --}}
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <a href="{{ url('dashboard') }}">Home</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <a href="{{ url('stock') }}">Stock</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <span>Create</span>
                    </li>
                </ul>
            </div>
            <div class="page-title"></div>
            <!-- BEGIN SAMPLE FORM PORTLET-->
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-green-haze">
                       <i class="icon-settings font-green-haze"></i>
                       <span class="caption-subject bold uppercase">Create New Stock</span>
                    </div>
                    <div class="actions">
                       <a class="btn btn-primary" href="{{ url('stock') }}"><i class="fa fa-arrow-left "></i> Back</a>
                    </div>
                </div>

                {!! Form::open(['url' => 'stock/store','method'=>'POST','file'=>'true', 'novalidate '=>'','enctype' => 'multipart/form-data','id'=>'stockForm']) !!}
                    @include('stock_form')
                {!! Form::close() !!}
            </div>
            <!-- END SAMPLE FORM PORTLET-->
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
@endsection
@section('page-script')
<script src="{{ url('assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}" type="text/javascript"></script>
{!! Html::script('assets/global/plugins/jquery.min.js') !!}
{!! Html::script('js/stock_create.js') !!}
{!! Html::script('js/toastr.min.js') !!}
{!! Html::script('js/typeahead.bundle.min.js') !!}
{!! Html::script('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') !!}
{!! Html::script('assets/pages/scripts/components-date-time-pickers.min.js') !!}
{!! Html::script('js/dropify.min.js') !!}


{{-- {!! Html::script('js/select2.full.min.js') !!} --}}
{{-- <script src="{{url('../assets/global/plugins/select2/js/select2.min.js')}}" type="text/javascript"></script> --}}
{{-- <script src="../assets/global/plugins/jquery.min.js" type="text/javascript"></script> --}}
{!! Html::script('assets/global/plugins/bootstrap/js/bootstrap.min.js') !!}
{!! Html::script('assets/global/plugins/select2/js/select2.full.min.js') !!}
{!! Html::script('assets/pages/scripts/components-select2.js') !!}
{{-- {!! Html::script('js/publication.js') !!} --}}
{{-- <script src="../assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script> --}}
{{-- <script src="../assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script> --}}
{{-- <script src="../assets/global/scripts/app.min.js" type="text/javascript"></script> --}}
{{-- <script src="../assets/pages/scripts/components-select2.js" type="text/javascript"></script> --}}
<script type="text/javascript">
    $('#sectorSelect').on('change', function (e) {
        e.preventDefault();
        var sector_id = $(this).val();

        $('#genreSelect')
        .find('option')
        .remove()
        .end();

        //clear options first, for selection of next option 
        $('#subjectSelect')
        .find('option')
        .remove()
        .end();

        //get subject according to the sector selected
        $.getJSON("/api/subject/getsubject?sector_id="+sector_id, function(data) {

            //add options in subject
            $("#subjectSelect").append('<option value disabled selected>Select Subject</option>');
            $.each(data, function(){
                $("#subjectSelect").append('<option value="'+ this.id +'">'+ this.subject_name +'</option>');
            });
        });

        //get sector according to the sector selected
        $.getJSON("/api/genre/getgenre?sector_id="+sector_id, function(data) {

            //add options in subject
            $("#genreSelect").append('<option value disabled selected>Select Genre</option>');
            $.each(data, function(){
                $("#genreSelect").append('<option value="'+ this.id +'">'+ this.genre_name +'</option>');
            });
        });
    });

</script>
@endsection
