@extends('userMaster')
@section('title','Book List')
@section('page-content')
    <div class="page-content-wrapper">
      <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <div id="main-container" class="container">
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <a href="{{url('/user')}}">Home</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <span>Books</span>
                    </li>
                </ul>
                <div class="page-container">
                <div class="results">
                    Search Results for : <strong>Your Query Here..</strong>
                </div> <br>   
                   <div class="media">
                      <div class="media-left media-middle">
                        <a href="{{url('book/detail')}}">
                          <img class="media-object" src="{{url('img/a.jpg')}}" alt="" height=100 width=75>
                        </a>
                      </div>
                      <div class="media-body">
                        <h4 class="media-heading"><a href="{{url('book/detail')}}">You Book Title</a></h4>
                        <p style="font-size:15px;">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                      </div><hr>
                    </div>
                    <div class="media">
                      <div class="media-left media-middle">
                        <a href="{{url('book/detail')}}">
                          <img class="media-object" src="{{url('img/a.jpg')}}" alt="" height=100 width=75>
                        </a>
                      </div>
                      <div class="media-body">
                        <h4 class="media-heading"><a href="{{url('book/detail')}}">You Book Title</a></h4>
                        <p style="font-size:15px;">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                      </div><hr>
                    </div>
                    <div class="media">
                      <div class="media-left media-middle">
                        <a href="{{url('book/detail')}}">
                          <img class="media-object" src="{{url('img/a.jpg')}}" alt="" height=100 width=75>
                        </a>
                      </div>
                      <div class="media-body">
                        <h4 class="media-heading"><a href="{{url('book/detail')}}">You Book Title</a></h4>
                        <p style="font-size:15px;">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                      </div><hr>
                    </div><div class="media">
                      <div class="media-left media-middle">
                        <a href="{{url('book/detail')}}">
                          <img class="media-object" src="{{url('img/a.jpg')}}" alt="" height=100 width=75>
                        </a>
                      </div>
                      <div class="media-body">
                        <h4 class="media-heading"><a href="{{url('book/detail')}}">You Book Title</a></h4>
                        <p style="font-size:15px;">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                      </div><hr>
                    </div>
                </div>
                <!-- /main -->

                <div class="main-overlay">
                    <div class="overlay-full"></div>
                </div>

            </div>

        </div>
    </div>
@endsection
