@extends('adminMaster')
@section('title','Direct Dispatch')
@section('headerscript')
    {!! Html::style('css/selec2full.min.css') !!}
    {!! Html::style('assets/global/plugins/select2/css/select2-bootstrap.min.css') !!}
    {!! Html::style('assets/global/plugins/select2/css/select2.min.css') !!}
    {!! Html::style('css/toastr.min.css') !!}
    <style>
        .tt-query {
            -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
            -moz-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
        }
        .select2-container--focus,.select2-container--open,.select2-container--below,.select2-search__field{
            width: 355px !important; 
        }

        .tt-hint {
            color: #999
        }
        .tt-menu {    /* used to be tt-dropdown-menu in older versions */
            width: 300px;
            margin-top: 4px;
            padding: 4px 0;
            background-color: #fff;
            border: 1px solid #ccc;
            border: 1px solid rgba(0, 0, 0, 0.2);
            -webkit-border-radius: 4px;
            -moz-border-radius: 4px;
            border-radius: 4px;
            -webkit-box-shadow: 0 5px 10px rgba(0,0,0,.2);
            -moz-box-shadow: 0 5px 10px rgba(0,0,0,.2);
            box-shadow: 0 5px 10px rgba(0,0,0,.2);
        }
        /*.select2-result-repository__avatar img {
        height: 60px;
        }*/
        .tt-suggestion {
            padding: 3px 20px;
            line-height: 24px;
        }
        .tt-highlight  {
            /*font-weight: normal;*/
            color: #72aecc !important;
        }
        .tt-suggestion.tt-cursor,.tt-suggestion:hover {
            color: #fff;
            background-color: #72aecc;
        }
        .tt-suggestion p {
            margin: 0;
        }
    </style>
@endsection
@section('page-content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="{{ url('dashboard') }}">Home</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Direct Dispatch</span>
                </li>
            </ul>
        </div>
        <div class="page-title"></div>
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-green-haze">
                    <i class="icon-settings font-green-haze"></i>
                    <span class="caption-subject bold uppercase">Direct Dispatch</span>
                </div>          
            </div>
            <div class="portlet-body form">
                @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                {!! Form::open(array('route' => 'dispatch.store','method'=>'POST','id'=>'dispatchForm')) !!}
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group form-md-line-input">
                                <label class="col-md-2 control-label" for="form_control_1">User:</label>
                                <div class="col-md-7">
                                   {{-- {!! Form::select('user', [], array('placeholder' => 'User Name','class' => 'form-control userSelect')) !!} --}}
                                   <select name="user" placeholder='User Name' class="form-control userSelect" id="userName">
                                       
                                   </select>
                                    <div class="form-control-focus"> </div>
                                </div>
                           </div>
                       </div>
                      
                      <div class="clearfix"></div>
                      <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group form-md-line-input">
                                <label class="col-md-2 control-label" for="form_control_1">Stock:</label>
                                <div id="stockName" class="col-md-7 form-group form-md-line-input has-success">
                                   {!! Form::text('title', null, array('placeholder' => 'Stock Name','class' => 'form-control','id'=>'title')) !!}
                                    <div class="form-control-focus"> </div>
                                </div>
                           </div>
                       </div>
                       <div class="clearfix"></div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group form-md-line-input">
                                    <label class="control-label col-md-2">From:</label>
                                    <div class="col-md-4">
                                      {!! Form::text('from',null , array('class' => 'form-control date-picker','id' => 'date_from','placeholder'=>'YYYY-MM-DD', 'data-date-format' => 'yyyy-mm-dd','required'=>'')) !!}
                                        {{-- <input class="form-control date-picker" size="16" type="text" placeholder="DD/MM/YYYY"> --}}
                                        <div class="form-control-focus"></div>
                                    </div>
                                    {{-- <label class="col-md-2 control-label" for="form_control_1">From:</label>
                                    <div class="col-md-4">
                                        {!! Form::date('from', null, array('placeholder' => 'From Date', 'class' => 'form-control datefield form-control-inline input-medium date-picker','id'=>'date_from','required'=>'')) !!}
                                        <div class="form-control-focus"> </div>
                                    </div> --}}
                                        {{-- <input class="form-control form-control-inline input-medium date-picker" size="16" type="text" value="">

                                         {!! Form::text('title', null, array('placeholder' => 'Title','class' => 'form-control')) !!}
                                        } --}}
                                        <label class="control-label col-md-2">To:</label>
                                    <div class="col-md-4">

                                      {!! Form::text('to',null , array('class' => 'form-control date-picker','id' => 'date_to','placeholder'=>'YYYY-MM-DD', 'data-date-format' => 'yyyy-mm-dd','required'=>'')) !!}

                                     {{--  {!! Form::text('to',null , array('class' => 'form-control date-picker','id' => 'to','placeholder'=>'YYYY-MM-DD', 'data-date-format' => 'yyyy-mm-dd','required'=>'')) !!} --}}
                                       {{--  <input class="form-control date-picker" id="to" type="text" name="to" date-date-format = "yyyy-mm-dd" placeholder="YYYY-MM-DD">
                                        <div class="form-control-focus"></div> --}}
                                    </div>
                                </div>
                            </div>
                        <div class="col-xs-12 col-sm-12 col-md-12" style="padding-top: 25px;">
                           <button type="submit" id="dispatchBook" class="btn btn-primary col-md-offset-2">Dispatch</button>
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>  
    </div>
</div>  
@endsection

@section('footerscript')
    {!! Html::script('js/toastr.min.js') !!}
    {{-- {!! Html::script('assets/global/plugins/jquery.min.js') !!} --}}
    {!! Html::script('assets/global/plugins/select2/js/select2.full.min.js') !!}
    {{-- {!! Html::script('assets/global/plugins/bootstrap/js/bootstrap.min.js') !!} --}}
    {!! Html::script('js/jquery.validate.min.js') !!}
    <script type="text/javascript">



        // Set the "bootstrap" theme as the default theme for all Select2
        // widgets.
        //
        // @see https://github.com/select2/select2/issues/2927
        $.fn.select2.defaults.set("theme", "bootstrap");

        var placeholder = "Select User";

        $(".select2, .select2-multiple").select2({
          
            placeholder: placeholder,
            width: null
        });

        $(".select2-allow-clear").select2({
            allowClear: true,
            placeholder: placeholder,
            width: null
        });

        // @see https://select2.github.io/examples.html#data-ajax
        function formatRepo(repo) {
            if (repo.loading) return repo.text;

            var markup = "<div class='select2-result-repository__meta'><div class='select2-result-repository__title'>" + repo.name + " [ "+ repo.code + "]" + "</div>";

            // if (repo.persona_information) {
            //     markup += "<div class='select2-result-repository__description'>" + repo.persona_information + "</div>";
            // }

            return markup;
        }

        function formatRepoSelection(repo) {
            return repo.name || repo.text;
        }

        $("#userName").select2({
            tags:false,
            width: "off",
            ajax: {
                // url: "https://api.github.com/search/repositories",
                url: "/api/user/get_users",
                dataType: 'json',
                delay: 250,
                data: function(params) {
                    return {
                        user_name: params.term, // search term
                        page: params.page
                    };
                },
                processResults: function(data, page) {
                    // parse the results into the format expected by Select2.
                    // since we are using custom formatting functions we do not need to
                    // alter the remote JSON data
                    return {

                        results: data.items
                    };
                },
                cache: !0
            },
            escapeMarkup: function(markup) {
                return markup;
            }, // let our custom formatter work
            minimumInputLength: 1,
            templateResult: formatRepo,
            templateSelection: formatRepoSelection
        });

        $("button[data-select2-open]").click(function() {
            $("#" + $(this).data("select2-open")).select2("open");
        });

        $(":checkbox").on("click", function() {
          // console.log("clicked");
            $(this).parent().nextAll("select").prop("disabled", !this.checked);
        });

        // copy Bootstrap validation states to Select2 dropdown
        //
        // add .has-waring, .has-error, .has-succes to the Select2 dropdown
        // (was #select2-drop in Select2 v3.x, in Select2 v4 can be selected via
        // body > .select2-container) if _any_ of the opened Select2's parents
        // has one of these forementioned classes (YUCK! ;-))
        $(".select2, .select2-multiple, .select2-allow-clear, .authorSelect").on("select2:open", function() {
            if ($(this).parents("[class*='has-']").length) {
                var classNames = $(this).parents("[class*='has-']")[0].className.split(/\s+/);

                for (var i = 0; i < classNames.length; ++i) {
                    if (classNames[i].match("has-")) {
                        $("body > .select2-container").addClass(classNames[i]);
                    }
                }
            }
        });

        $(".js-btn-set-scaling-classes").on("click", function() {
            $("#select2-multiple-input-sm, #select2-single-input-sm").next(".select2-container--bootstrap").addClass("input-sm");
            $("#select2-multiple-input-lg, #select2-single-input-lg").next(".select2-container--bootstrap").addClass("input-lg");
            $(this).removeClass("btn-primary btn-outline").prop("disabled", true);
        });


        $('#dispatchBook').on('click',function(e){
            e.preventDefault();
            // var dataCode = $('#getBook').attr('data-stock-id');
            var from = $('#date_from').val();
            var to =$('#date_to').val();
            var user = $('#userName').val();
            var title = $('#title').val();
            $.ajax({
              url: '../../dispatch/store',
              type: "POST",
              timeout: 250,
              async: false,
              headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
              data: {user: user, from: from, to: to, title: title,},
              success: function( result ) {

                switch(result.dispatched){
                    case "success":
                        $('#getBook').attr('disabled','true');
                        
                        // $("#userName").val();
                        $('#dispatchForm').trigger("reset");
                        toastr.success(result.msg, 'Success:');
                        // location.href = "/dashboard";
                        break;

                    case "unsuccess":
                        toastr.error(result.msg, 'Failed:');
                        break;

                    case "error":
                        $('#stockName').removeClass('has-success');
                        $('#stockName').addClass('has-error');
                        $("#title").focus();
                        toastr.warning(result.msg, 'Failed:');
                        break;

                    case "damaged":
                    case "unavailable":
                        toastr.error(result.msg, 'Failed:');
                        break;

                    default:
                        toastr.error(result.msg, 'Failed: The Reuest Cannot be Proceed. Please Contact System Admin! ');

                }

                // if (result.dispatched === "success") {
                //   toastr.success(result.msg, 'Success: ');
                //   $('#getBook').attr('disabled','true');
                //   location.href = "/dashboard";
                  
                // }else if(result.dispatched === "unsuccess"){
                //     toastr.error(result.msg, 'Failed:');
                // }else if(result.dispatched === "error"){
                //     $('#stockName').removeClass('has-success');
                //     $('#stockName').addClass('has-error');
                //     $("#title").focus();
                //     toastr.warning(result.msg, 'Failed:');
                // }
                // else{
                //   toastr.error(result.msg, 'Failed: The Reuest Cannot be Proceed. Please Contact System Admin! ');
                // }
              }
            });
        });


        //form validation
        $("#myform").validate({
          rules: {
            user: {required: true},
            title: {
              required: true,
            },
            from: {required: true},
            to: {required: true},
          },
          messages: {
            user: "Please specify your name",
          }
        });

    </script>
@endsection
