@extends('adminMaster')
@section('title','Year | Create')

@section('page-content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="{{ url('dashboard') }}">Home</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="{{ url('setting') }}">Setting</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="{{ url('year') }}">Year and Batch</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Create</span>
                </li>
            </ul>
        </div>
        <div class="page-title"></div>
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-green-haze">
                    <i class="icon-settings font-green-haze"></i>
                    <span class="caption-subject bold uppercase">Year and Batch Creation</span>
                </div>
                <div class="actions">
                    <a class="btn btn-primary" href="{{ url('year') }}"><i class="fa fa-arrow-left "></i> Back</a>
                </div>
            </div>
            <div class="portlet-body form">
                @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                {!! Form::open(array('route' => 'year.store','method'=>'POST')) !!}
                @include('year.form')
                {!! Form::close() !!}
            </div>
        </div>  
    </div>
</div>  
@endsection
