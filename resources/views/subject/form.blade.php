<div class="row">

	<div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group form-md-line-input">
            <label class="col-md-2 control-label" for="form_control_1">Title:</label>
            <div class="col-md-7">
               {!! Form::text('subject_name', null, array('placeholder' => 'Title','class' => 'form-control')) !!}
                <div class="form-control-focus"> </div>
            </div>
            <label class="col-md-1 control-label" for="form_control_1">Active</label>
            <div class="col-md-2">
              {{ Form::checkbox('active', 1, old('active'), [ 'class' => 'make-switch', 'data-size' => 'make-switch' ] ) }}
            </div>
       </div>
   </div>

   <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group form-md-line-input">
            <label class="col-md-2 control-label" for="form_control_1">Sector:</label>
            <div class="col-md-8">
           {!! Form::select('sector_id',$sectors, null, ['class' => 'form-control','placeholder' => 'Select Sector']) !!}
            <div class="form-control-focus"> </div>
           </div>
           <div class="col-md-1"></div>
       </div>
   </div>
  
 <div class="col-xs-12 col-sm-12 col-md-12" style="padding-top: 25px;">
       <button type="submit" class="btn btn-primary col-md-offset-2">Save</button>
       <a href="{{ url('/subject') }}" class="btn btn-default">Cancel</a>

  </div>
</div>