@extends('adminMaster')
@section('title','Fine Management')
@section('headerscript')
    {!! Html::style('css/selec2full.min.css') !!}
    {!! Html::style('assets/global/plugins/select2/css/select2-bootstrap.min.css') !!}
    {!! Html::style('assets/global/plugins/select2/css/select2.min.css') !!}
    {!! Html::style('css/toastr.min.css') !!}
    <style>
        .tt-query {
            -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
            -moz-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
        }
        .select2-container--focus,.select2-container--open,.select2-container--below,.select2-search__field{
            width: 355px !important; 
        }

        .tt-hint {
            color: #999
        }
        .tt-menu {    /* used to be tt-dropdown-menu in older versions */
            width: 300px;
            margin-top: 4px;
            padding: 4px 0;
            background-color: #fff;
            border: 1px solid #ccc;
            border: 1px solid rgba(0, 0, 0, 0.2);
            -webkit-border-radius: 4px;
            -moz-border-radius: 4px;
            border-radius: 4px;
            -webkit-box-shadow: 0 5px 10px rgba(0,0,0,.2);
            -moz-box-shadow: 0 5px 10px rgba(0,0,0,.2);
            box-shadow: 0 5px 10px rgba(0,0,0,.2);
        }
        /*.select2-result-repository__avatar img {
        height: 60px;
        }*/
        .tt-suggestion {
            padding: 3px 20px;
            line-height: 24px;
        }
        .tt-highlight  {
            /*font-weight: normal;*/
            color: #72aecc !important;
        }
        .tt-suggestion.tt-cursor,.tt-suggestion:hover {
            color: #fff;
            background-color: #72aecc;
        }
        .tt-suggestion p {
            margin: 0;
        }
    </style>
@endsection
@section('page-content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="{{ url('dashboard') }}">Home</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Fine Management</span>
                </li>
            </ul>
        </div>
        <div class="page-title"></div>
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-green-haze">
                    <i class="icon-settings font-green-haze"></i>
                    <span class="caption-subject bold uppercase">Fine Management</span>
                </div>          
            </div>
            <div class="portlet-body form">
                @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                {!! Form::open(array('route' => 'fine.payment','method'=>'POST','id'=>'paymentForm')) !!}
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group form-md-line-input">
                                <label class="col-md-2 control-label" for="form_control_1">User:</label>
                                <div class="col-md-7">
                                   {{-- {!! Form::select('user', [], array('placeholder' => 'User Name','class' => 'form-control userSelect')) !!} --}}
                                   <select name="user" placeholder='User Name' class="form-control userSelect" id="userName">
                                       
                                   </select>
                                    <div class="form-control-focus"> </div>
                                </div>
                           </div>
                       </div>
                      
                      <div class="clearfix"></div>
                      <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group form-md-line-input">
                                <label class="col-md-2 control-label" for="form_control_1">Total Amount:</label>
                                <label class="col-md-2 control-label" id="total"></label>
                                   {{-- {!! Form::text('total', null, array('class' => 'form-control','id'=>'total')) !!} --}}
                                    <div class="form-control-focus"> </div>
                           </div>
                       </div>
                       <div class="clearfix"></div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-2 control-label" for="form_control_1">Due Amount:</label>
                                    <label style="color: red;" class="col-md-2 control-label" id="due"></label>
{{-- 
                                       {!! Form::text('due', null, array('class' => 'form-control','id'=>'due')) !!}
                                        <div class="form-control-focus"> </div> --}}
                                </div>
                            </div>
                        <div class="clearfix"></div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-2 control-label" for="form_control_1">Payment Amount:</label>
                                    <div id="stockName" class="col-md-7 form-group form-md-line-input has-success">
                                       {!! Form::text('payment', null, array('placeholder' => 'payment amount ','class' => 'form-control','id'=>'payment')) !!}
                                        <div class="form-control-focus"> </div>
                                    </div>
                                </div>
                            </div>
                        <div class="col-xs-12 col-sm-12 col-md-12" style="padding-top: 25px;">
                           <button type="Submit" id="submitPayment" class="btn btn-primary col-md-offset-2">Submit</button>
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>  
    </div>
</div>  
@endsection

@section('footerscript')
    {!! Html::script('js/toastr.min.js') !!}
    {{-- {!! Html::script('assets/global/plugins/jquery.min.js') !!} --}}
    {!! Html::script('assets/global/plugins/select2/js/select2.full.min.js') !!}
    {{-- {!! Html::script('assets/global/plugins/bootstrap/js/bootstrap.min.js') !!} --}}
    {!! Html::script('js/jquery.validate.min.js') !!}
    <script type="text/javascript">



        // Set the "bootstrap" theme as the default theme for all Select2
        // widgets.
        //
        // @see https://github.com/select2/select2/issues/2927
        $.fn.select2.defaults.set("theme", "bootstrap");

        var placeholder = "Select User";

        $(".select2, .select2-multiple").select2({
          
            placeholder: placeholder,
            width: null
        });

        $(".select2-allow-clear").select2({
            allowClear: true,
            placeholder: placeholder,
            width: null
        });

        // @see https://select2.github.io/examples.html#data-ajax
        function formatRepo(repo) {
            if (repo.loading) return repo.text;

            var markup = "<div class='select2-result-repository__meta'><div class='select2-result-repository__title'>" + repo.name + " [ "+ repo.code + "]" + "</div>";

            // if (repo.persona_information) {
            //     markup += "<div class='select2-result-repository__description'>" + repo.persona_information + "</div>";
            // }

            return markup;
        }

        function formatRepoSelection(repo) {
            return repo.name || repo.text;
        }

        $("#userName").select2({
            tags:false,
            width: "off",
            ajax: {
                // url: "https://api.github.com/search/repositories",
                url: "/api/user/get_users",
                dataType: 'json',
                delay: 250,
                data: function(params) {
                    return {
                        user_name: params.term, // search term
                        page: params.page
                    };
                },
                processResults: function(data, page) {
                    // parse the results into the format expected by Select2.
                    // since we are using custom formatting functions we do not need to
                    // alter the remote JSON data
                    return {

                        results: data.items
                    };
                },
                cache: !0
            },
            escapeMarkup: function(markup) {
                return markup;
            }, // let our custom formatter work
            minimumInputLength: 1,
            templateResult: formatRepo,
            templateSelection: formatRepoSelection
        });

        $("button[data-select2-open]").click(function() {
            $("#" + $(this).data("select2-open")).select2("open");
        });

        $(":checkbox").on("click", function() {
          // console.log("clicked");
            $(this).parent().nextAll("select").prop("disabled", !this.checked);
        });

        // copy Bootstrap validation states to Select2 dropdown
        //
        // add .has-waring, .has-error, .has-succes to the Select2 dropdown
        // (was #select2-drop in Select2 v3.x, in Select2 v4 can be selected via
        // body > .select2-container) if _any_ of the opened Select2's parents
        // has one of these forementioned classes (YUCK! ;-))
        $(".select2, .select2-multiple, .select2-allow-clear, .authorSelect").on("select2:open", function() {
            if ($(this).parents("[class*='has-']").length) {
                var classNames = $(this).parents("[class*='has-']")[0].className.split(/\s+/);

                for (var i = 0; i < classNames.length; ++i) {
                    if (classNames[i].match("has-")) {
                        $("body > .select2-container").addClass(classNames[i]);
                    }
                }
            }
        });

        $(".js-btn-set-scaling-classes").on("click", function() {
            $("#select2-multiple-input-sm, #select2-single-input-sm").next(".select2-container--bootstrap").addClass("input-sm");
            $("#select2-multiple-input-lg, #select2-single-input-lg").next(".select2-container--bootstrap").addClass("input-lg");
            $(this).removeClass("btn-primary btn-outline").prop("disabled", true);
        });

        $('#userName').on('change',function(e){
            var name = $(this).val();
            $.ajax({
                url: '/fine/userDetail',
                type: "GET",
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                data: {user: name},
                success: function( result ) {
                    $('#total').html(result.total_amount);
                    $('#due').html(result.due);
                }
            });
        });

        // $('#submitPayment').on('click',function(e){
        //     e.preventDefault();
        //     // var dataCode = $('#getBook').attr('data-stock-id');
        //     var payment = $('#payment').val();
        //     var user =$('#userName').val();
        //     $.ajax({
        //         url: '/fine/payment',
        //         type: "POST",
        //         headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        //         data: {user: user,payment:payment},
        //         success: function( result ) {
        //             if(result.status == "true") {
                       
        //                 toastr.success(result.msg, 'Success:');
        //                 location.reload();
        //             }
        //             else{
        //                 toastr.error(result.msg, 'Failed:');

        //             }
        //         }
        //     });
        // });
       

    </script>
@endsection
