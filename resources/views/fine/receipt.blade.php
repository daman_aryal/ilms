<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Report</title>
    <link href="../latestcss/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
	 <link href="{{ url('latestcss/assets/global/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />

</head>
<body>
<a style="float: left; font-size: 50px;" class="hide_print" href="{{url('/')}}/fine"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i></a>
	<a style="float: right; font-size: 50px;" class="hide_print" id="print"><i class="fa fa-print" aria-hidden="true"></i></a>
	<div class="container-fluid text-center">
		<div class="row">
			<div class="col-md-12">
				<img src="{{ url('../assets/layouts/layout/img/cmclogo.png') }}" alt="" class="pull-left" style="height: 150px;margin-top: 10px">
				<h4>Chitwan Medical College (P) Ltd</h4>
				<h3>Chitwan Medical College Teaching Hospital</h3>
				<h5>{{$data['address']}}</h5>
				<h5>Phone No: 977-56-532933(Hospital) 977-56-592366(College) 977-1-400504(Kathmandu)</h5>
				<h5>Email: info@cmc.edu.np Website: www.cmc.edu.np</h5>
			</div>
			<div class="clearfix"></div>
		</div>
		<hr>
		<div id="title"><h3>Fine Receipt</h3></div>
	</div>
	<div id="detail">
		<div>
			<label for="library">Library:</label> {{$data['location']}}  {{-- print hospital or central --}}
			<span class="pull-right"><label for="date">Date:</label> {{Carbon\Carbon::now()}} </span> <br>
			<label for="name">Name:</label> {{$data['user']}}
			@if($data['program'] != null) 
			<span class="pull-right"><label for="program">Program:</label> {{$data['program']}} </span><br>
			@endif
			@if($data['department'] != null) 
			<span class="pull-right"><label for="department">Department:</label> {{$data['department']}} </span><br>
			@endif
			<label for="member_code">Member Code:</label> {{$data['user_type']}} <br>
 		</div>
	</div>
	<div class="clearfix"></div>
	<div id="Content">
		<h5>Cash Received of Rs. {{$data['amount']}} for Library fine.</h5>
		<table class="table ">
			<tr>
				<td>Total Amount due:</td>
				<td style="text-align: right;">Rs. {{$data['total']}}</td>
			</tr>
			<tr>
				<td>Amount Received:</td>
				<td style="text-align: right;">Rs. {{$data['amount']}}</td>
			</tr>
			<tr>
				<td>Balance Due:</td>
				<td style="text-align: right;">Rs. {{$data['due']}}</td>
			</tr>
		</table>
	</div>
	<div id="footer" style="margin-top: 30px">
		<div>________________</div>
		<label for="user">User:</label>{{$data['received_by']}}
		<div class="pull-right" style="font-size: 12px"><label for="current_date">Printed Date:</label> {{$data['received_date']}}</div>
	</div>
</body>
<footer>
	<script src="{{ url('latestcss/js/jquery.min.js') }}"></script>
	<script type="text/javascript">
	$('#print').on('click',function(e){
		$('.hide_print').hide();
			window.print();
	});

	</script>
</footer>
</html>