@extends('adminMaster')
@section('title','Transaction | Index')
@section('headerscript')
	{!! Html::style('css/toastr.min.css') !!}
@endsection
@section('page-content')

		<div id="dispatchModal" class="modal dispatchModal fade" tabindex="-1" data-focus-on="input:first" >
			<div class="modal-header bg">
					<h4 class="modal-title font-color" style="">Dispatch Detail Info</h4>
			</div>
			<div class="modal-body modal-padding">
					<div class="row">
							<div class="col-md-6">
									 <div class="form-group form-md-line-input form-md-floating-label has-success" id="dispatchInput">
											<input type="text" name="code" class="form-control" id="codeName1" required>
											<label for="form_control_1">Book Code</label>
											<span class="help-block" id="dispatchError"></span>
									</div>
							</div>
					</div>
					<div class="row footer-margin">
							<div class="modal-footer">
									<button type="button" class="btn-footer blue-outline btn-margin" id="dispatchBook">Save</button>
									<button type="button" class="btn-footer blue-outline" data-dismiss="modal">Cancel</button>
							</div>
					</div>
			</div>
		</div>
		<div id="returnModal" class="modal returnModal fade" tabindex="-1" data-focus-on="input:first" >
			<div class="modal-header bg">
					<h4 class="modal-title font-color" style="">Return Detail Info</h4>
			</div>
			<div class="modal-body modal-padding">
					<div class="row">
							<div class="col-md-6">

									 <div class=" form-group form-md-line-input form-md-floating-label has-success" id="returnInput">
											<input type="text" name="code" class="form-control" id="codeName2" required>
											<label>Book Code</label>
											<span class="help-block" id="returnError"></span>

									</div>
									
							</div>
					</div>
					<div class="row footer-margin">
							<div class="modal-footer">
									<button type="button" class="btn-footer blue-outline btn-margin" id="returnBook">Save</button>
									<button type="button" class="btn-footer blue-outline" data-dismiss="modal">Cancel</button>
							</div>
					</div>
			</div>
		</div>
	<div class="page-content-wrapper">
	<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
		    <div class="portlet light bordered">
		        <div class="portlet-title">
		            <div class="caption font-green-haze">
		                <i class="icon-settings font-green-haze"></i>
		                <span class="caption-subject bold uppercase">Transaction Detail</span>
		            </div>
		        </div>
		        @if ($message = Session::get('success'))
					<div class="alert alert-success">
						<p>{{ $message }}</p>
					</div>
				@endif
		        <table class="table table-bordered">
					<tr>
						<th>S.N</th>
						<th>Reserved By</th>
						<th>Book Code</th>
						<th>Book Name</th>
						<th>Status</th>
						<th>Reserved from</th>
						<th>Reserved to</th>
						<th width="280px">Action</th>
					</tr>
				@foreach ($transaction as $key => $t)
				<tr>
					<td>{{ ++$key }}</td>
					<td>{{ $t->reserved_by->name}}</td>
					<td>{{ $t->code}}</td>
					<td>{{ $stock_detail->getStockTitle($t->stock->id)}}</td>

					{{-- reserve request --}}
					@if($t->status == 0)
						<td><label class="label label-danger">Reserve Request</label></td>
					@endif

					{{-- request dispatch --}}
					@if($t->status == 1)
						<td><label class="label label-info">Issued</label></td>
					@endif

					{{-- request dispatch --}}
					@if($t->status == 2)
						<td><label class="label label-info">Request Dispatched</label></td>
					@endif

					{{-- request dispatch --}}
					@if($t->status == 3)
						<td><label class="label label-info">Request Dispatched Renewed</label></td>
					@endif

					{{-- request dispatch --}}
					@if($t->status == 4)
						<td><label class="label label-info">Returned</label></td>
					@endif


					<td>{{ $t->book_from->toDateString() }}</td>
					<td>{{ $t->book_to->toDateString()}}</td>
					<td>
						{{-- <a class="btn btn-info" href="">Show</a> --}}
						@if( $t->status==1)
							{{-- <a class="btn btn-primary" id="dispatch" href="{{ url('transaction/dispatch/'.$t->id.'/'.$t->stock_id.'/'.$t->stock_detail_id) }}">Dispatch</a> --}}
							<a class="btn btn-primary dispatch" id="{{$t->id.'/'.$t->stock_id.'/'.$t->stock_detail_id}}"  href="#dispatchModal" data-toggle="modal" >Dispatch</a>
						@endif

						@if($t->status == 2 || $t->status == 3)
							<a class="btn btn-primary return" date_to="{{$t->book_to}}" id="{{$t->id.'/'.$t->stock_id.'/'.$t->stock_detail_id.'/'.$t->user_id}}" {{--  href="{{ url('transaction/returned/'.) }}" --}} href="#returnModal" data-toggle="modal">Return</a>
						@endif

					</td>
				</tr>
				@endforeach
				{{$transaction->render()}}
				</table>

			<!-- END SAMPLE FORM PORTLET-->
			</div>
		</div>
	</div>
@endsection

@section('footerscript')
	{!! Html::script('js/toastr.min.js') !!}
	<script>

	$('.return').on('click',function(e){
		var fine_amount = $('#fine_amount').val();
		$('#fine_modal').val(fine_amount);
		if (fine_amount > 0) {
			$('.paid').show();
		}else{
			$('.paid').hide();
		}

	});
	$(document).bind('keypress', function(e) {
		if({{$t->status}} == 1){
            if(e.keyCode==13){
                 $('#dispatchBook').trigger('click');
             }
		}
		if ({{$t->status}} == 2 || {{$t->status}} == 3){
			if(e.keyCode==13){
				console.log("here");
                 $('#returnBook').trigger('click');
             }	
		}
        });
	$('#dispatchBook').on('click', function (e) {
			e.preventDefault();

			var codeName = $('#codeName1').val();
			var url_path = $('.dispatch').attr('id');
			$.ajax({
			    url: '../api/dispatch/'+url_path,
			    type: "POST",
			    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
			    data: {code: codeName},
			    success: function( result ) {
					if (result.status == "error") {
						toastr.error(result.msg, 'Failed:');
					}else{
						toastr.success(result.msg, 'Success:');
						location.reload();
					}
			    }
			});
	});

	$('#returnBook').on('click', function (e) {
			e.preventDefault();
			
			var codeName = $('#codeName2').val();
			var to = $('.return').attr('date_to');
			var url_path = $('.return').attr('id');


				$.ajax({
				    url: '../api/return/'+url_path,
				    type: "GET",
				    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
				    data: {code: codeName, date_to: to},
				    success: function( result ) {
								if (result.status == "error") {
									toastr.error(result.msg, 'Failed:');
								}else{
									toastr.success(result.msg, 'Success:');
									location.reload();
								}
				    }
				});
	});
	</script>
@endsection
