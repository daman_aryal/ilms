<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->
<!-- <head> -->
    <meta charset="utf-8" />
    <title>@yield('title')</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta content="" name="description" />
    <meta content="" name="author" />
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
    <link href="{{ url('latestcss/assets/global/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ url('latestcss/assets/global/plugins/simple-line-icons/simple-line-icons.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ url('latestcss/assets/global/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ url('latestcss/assets/global/plugins/uniform/css/uniform.default.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ url('latestcss/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ url('latestcss/assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ url('latestcss/assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css') }}" rel="stylesheet" type="text/css" />
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="{{ url('latestcss/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ url('latestcss/assets/global/plugins/morris/morris.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ url('latestcss/assets/global/plugins/fullcalendar/fullcalendar.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ url('latestcss/assets/global/plugins/jqvmap/jqvmap/jqvmap.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ url('latestcss/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet" type="text/css" />
    {{-- <link href="../assets/global/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet" type="text/css" /> --}}
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN THEME GLOBAL STYLES -->
    <link href="{{ url('latestcss/assets/global/css/components.min.css') }}" rel="stylesheet" id="style_components" type="text/css" />
    <link href="{{ url('latestcss/assets/global/css/plugins.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- <link href="../assets/pages/css/search.min.css" rel="stylesheet" type="text/css" /> -->
    <!-- END THEME GLOBAL STYLES -->
    <!-- BEGIN THEME LAYOUT STYLES -->

    {{-- <link href="{{ url('latestcss/css/bootstrap-responsive.css') }}" rel="stylesheet"> --}}
    <link rel="stylesheet" href="{{ url('../latestcss/css/demo.css') }}">
    <link rel="stylesheet" href="{{ url('latestcss/css/jquery.flipster.min.css') }}">
    <link rel='stylesheet prefetch' href='{{ url('https://fonts.googleapis.com/css?family=Antic') }}'>
    <!-- END THEME LAYOUT STYLES -->
    <link rel="shortcut icon" href="{{ url('favicon.ico') }}" />
<!--     <link rel="stylesheet" href="../css/normalize.css">
    <link rel="stylesheet" href="../css/main.css">  -->
    <link rel="stylesheet" href="{{ url('latestcss/css/custom.css') }}">
    <style>
        label.myErrorClass {
            color: red;
            font-size: 11px;
            /*    font-style: italic;*/
            display: block;
        }
        @media(max-width: 992px){
            .sm-menu{
                display: block!important;
            }
            .lg-menu{
                display:none;
            }
        }
    </style>
    @yield('headercss')
</head>
<!-- END HEAD -->
<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
@yield('body')

<script src="{{ url('latestcss/js/jquery.min.js') }}"></script>
<script src="{{ url('latestcss/js/custom.js') }}"></script>
<!-- BEGIN CORE PLUGINS -->
<script src="{{ url('../latestcss/assets/global/plugins/jquery.min.js') }}" type="text/javascript"></script>
<script src="{{ url('../latestcss/assets/global/plugins/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
<script src="{{ url('../latestcss/assets/global/plugins/js.cookie.min.js') }}" type="text/javascript"></script>
<script src="{{ url('../latestcss/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js') }}" type="text/javascript"></script>
<script src="{{ url('../latestcss/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js') }}" type="text/javascript"></script>
<script src="{{ url('../latestcss/assets/global/plugins/jquery.blockui.min.js') }}" type="text/javascript"></script>
<script src="{{ url('../latestcss/assets/global/plugins/uniform/jquery.uniform.min.js') }}" type="text/javascript"></script>
<script src="{{ url('../latestcss/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="{{ url('../latestcss/assets/global/plugins/moment.min.js') }}" type="text/javascript"></script>
<script src="{{ url('../latestcss/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js') }}" type="text/javascript"></script>
<script src="{{ url('../latestcss/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
<script src="{{ url('../latestcss/assets/global/plugins/morris/morris.min.js') }}" type="text/javascript"></script>
<script src="{{ url('../latestcss/assets/global/plugins/morris/raphael-min.js') }}" type="text/javascript"></script>
<script src="{{ url('../latestcss/assets/global/plugins/counterup/jquery.waypoints.min.js') }}" type="text/javascript"></script>
<script src="{{ url('../latestcss/assets/global/plugins/counterup/jquery.counterup.min.js') }}" type="text/javascript"></script>
<script src="{{ url('../latestcss/assets/global/plugins/fullcalendar/fullcalendar.min.js') }}" type="text/javascript"></script>
<script src="{{ url('../latestcss/assets/global/plugins/flot/jquery.flot.min.js') }}" type="text/javascript"></script>
<script src="{{ url('../latestcss/assets/global/plugins/flot/jquery.flot.resize.min.js') }}" type="text/javascript"></script>
<script src="{{ url('../latestcss/assets/global/plugins/flot/jquery.flot.categories.min.js') }}" type="text/javascript"></script>
<script src="{{ url('../latestcss/assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js') }}" type="text/javascript"></script>
<script src="{{ url('../latestcss/assets/global/plugins/jquery.sparkline.min.js') }}" type="text/javascript"></script>
<script src="{{url('../assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js')}}" type="text/javascript"></script>
<script src="{{url('../assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js')}}" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="{{ url('../latestcss/assets/global/scripts/app.min.js') }}" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="{{ url('../latestcss/assets/pages/scripts/dashboard.min.js') }}" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script src="{{ url('../latestcss/assets/layouts/layout3/scripts/layout.min.js') }}" type="text/javascript"></script>

<script src="{{ url('../latestcss/assets/layouts/global/scripts/quick-sidebar.min.js') }}" type="text/javascript"></script>

<!-- <script src="jquery.flipster.min.js"></script> -->
<script src="{{ url('../latestcss/js/vendor/modernizr-2.6.2.min.js') }}"></script>
@yield('footerscript')
</body>
</html>
