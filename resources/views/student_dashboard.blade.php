@extends('userMaster')
@section('title','User Dashboard')
@section('page-content')
<div class="page-container">
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <!-- BEGIN PAGE CONTENT BODY -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE BREADCRUMBS -->
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <a href="{{ url('/home') }}">Home</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <span>Dashboard</span>
                    </li>
                </ul>
                <!-- END PAGE BREADCRUMBS -->
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="page-content-inner">
                    <div class="search-page search-content-4">
                        <div class="row">
                            <div class="input-group" style="margin-bottom: 15px;">
                                <div class="col-md-12">
                                    <input class="button user_search" placeholder="I am Looking for ..." style="border-radius: 0 !important; height: 55px"/>
                                    <button id="search" class="search-btn pull-right" type="button">
                                    <i class="glyphicon glyphicon-search"></i> Search</button>
                                </div>
                            </div>
                            {{-- Advance Search --}}
                            {{-- <div class="row" style="margin-left: -13px; margin-top: 10px">
                                <a id="advsearch">Advanced Search</a>
                            </div>
                            <div id="advoption" class="row advmargin" style="display:none" >
                               <div class="md-checkbox-inline">
                                    <div class="md-checkbox">
                                        <input type="checkbox" id="checkbox2_4" name="checkboxes2[]" value="1" class="md-check" checked>
                                        <label for="checkbox2_4">
                                            <span></span>
                                            <span class="check"></span>
                                            <span class="box"></span> All </label>
                                    </div>
                                    <div class="md-checkbox">
                                        <input type="checkbox" id="checkbox2_5" name="checkboxes2[]" value="1" class="md-check">
                                        <label for="checkbox2_5">
                                            <span></span>
                                            <span class="check"></span>
                                            <span class="box"></span> Author </label>
                                    </div>
                                    <div class="md-checkbox">
                                        <input type="checkbox" id="checkbox2_6" name="checkboxes2[]" value="1" class="md-check">
                                        <label for="checkbox2_6">
                                            <span></span>
                                            <span class="check"></span>
                                            <span class="box"></span> Publication</label>
                                    </div>
                                </div>
                            </div> --}}
                        </div>
                    </div>
                    <div class="row spacing">
                        <div class="col-md-6 col-sm-6 sm-bottom-margin">
                            <div class="shelf owned">
                                <ul id="owl-example" class=" books clear owl-carousel">
                                    @if(isset($featured_stock))
                                        @foreach($featured_stock as $featured)
                                            <li class="book">
                                                <a href="{{ url('book/detail') }}/CMC-{{$featured->stock_id}}">
                                                    <img src="/{{ $featured->stock->getBookImage()}}" alt="{{ $featured->stock->rel_stockDetail[0]->title}}">
                                                </a>
                                                <div class="back">
                                                    <div class="p10">
                                                     <dl>
                                                            <strong>Title:</strong>
                                                            {{ $featured->stock->rel_stockDetail[0]->title}}
                                                        </dl>
                                                        <dl>
                                                            <strong>Year:</strong>
                                                            {{ $featured->stock->rel_stockDetail[0]->publication_date->format('Y') }}
                                                        </dl>
                                                        <p class="description" align="justify">
                                                            <strong>Summary:</strong>
                                                            {{ substr($featured->stock->rel_stockDetail[0]->plot_summary, 0,55)}}{{ strlen($featured->stock->rel_stockDetail[0]->plot_summary) > 55 ? '...' : "" }}
                                                        </p>
                                                    </div>
                                                </div>
                                            </li> 
                                        @endforeach    
                                        
                                    @endif
                                </ul>
                            </div>
                            <div class="shelf wanted">
                                <ul id="owl-example1" class=" books clear owl-carousel">
                                @if (isset($latest_books))
                                    @foreach($latest_books as $key => $latest_book)
                                    <li class="book">
                                        @if (isset($latest_book->images[0]))
                                            @if(isset($latest_book->images[0]->path))
                                        <a href="{{ url('book/detail') }}/CMC-{{$latest_book->id}}">
                                            <img src="/{{ $latest_book->images[0]->path}}">
                                        </a>
                                        @else
                                            <a href="{{ url('book/detail') }}">
                                            <img src="/img/no-cover.jpg">
                                        </a>
                                        @endif   
                                        @endif
                                        
                                        <div class="back">
                                            <div class="p10">
                                               {{--  <div class="rating star-3">
                                                    <ol>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                    </ol>
                                                </div> --}}
                                               <dl>
                                                    <strong>Title:</strong>
                                                    @if (isset($latest_book->rel_stockDetail[0]))
                                                    {{ $latest_book->rel_stockDetail[0]->title}}
                                                        {{-- expr --}}
                                                    @endif
                                                </dl>
                                                <dl>
                                                    <strong>Year:</strong>
                                                    @if (isset($latest_book->rel_stockDetail[0]))
                                                        {{-- expr --}}
                                                    {{ $latest_book->rel_stockDetail[0]->publication_date->format('Y') }}
                                                    @endif
                                                </dl>
                                                <p class="description" align="justify">

                                                    <strong>Summary:</strong>
                                                    @if (isset($latest_book->rel_stockDetail[0]))
                                                        {{-- expr --}}
                                                    {{ substr($latest_book->rel_stockDetail[0]->plot_summary, 0,55)}}{{ strlen($latest_book->rel_stockDetail[0]->plot_summary) > 55 ? '...' : "" }}
                                                    @endif
                                                </p>
                                            </div>
                                        </div>
                                    </li>
                                    @endforeach    
                                @endif
                                
                                </ul>
                            </div>
                            <div class="shelf owned">
                                <ul id="owl-example2" class=" books clear owl-carousel">
                                    <li id="javascriptguide" class="book">
                                        <a href="{{ url('book/detail') }}">
                                            <img class="front" src="img/covers/javascript-definitive-guide.jpg" alt="Javascript - The Definitive Guide">
                                        </a>
                                        <div class="back">
                                            <div class="p10">
                                                <div class="rating star-3">
                                                    <ol>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                    </ol>
                                                </div>
                                                <dl>
                                                    <strong>Title:</strong>
                                                    Javascript
                                                    <strong>year:</strong>
                                                    2012
                                                </dl>
                                                <p class="description">
                                                    Great Javascript reference, especially in the current days where everyone uses jQuery and tends to forget about "native" Javascript.
                                                </p>
                                            </div>
                                        </div>
                                    </li>
                                    <li id="handcraftedcss" class="book">
                                        <a href="{{ url('book/detail') }}">
                                            <img class="front" src="img/covers/handcraftedcss.jpg" alt="Handcrafted CSS">
                                        </a>
                                        <div class="back">
                                            <div class="p10">
                                                <div class="rating star-4">
                                                    <ol>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                    </ol>
                                                </div>
                                                <dl>
                                                    <strong>Title:</strong>
                                                    Cascading Style Sheets
                                                    <strong>year:</strong>
                                                    2012
                                                </dl>
                                                <p class="description">
                                                    Great book on CSS. Same author as the previous mentioned CSS for Webdesigners, and the book has the same qualities!
                                                </p>
                                            </div>
                                        </div>
                                    </li>
                                    <li id="javascriptguide" class="book">
                                        <a href="{{ url('book/detail') }}">
                                            <img class="front" src="img/covers/javascript-definitive-guide.jpg" alt="Javascript - The Definitive Guide">
                                        </a>
                                        <div class="back">
                                            <div class="p10">
                                                <div class="rating star-3">
                                                    <ol>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                    </ol>
                                                </div>
                                                <dl>
                                                    <strong>Title:</strong>
                                                    Javascript
                                                    <strong>year:</strong>
                                                    2012
                                                </dl>
                                                <p class="description">
                                                    Great Javascript reference, especially in the current days where everyone uses jQuery and tends to forget about "native" Javascript.
                                                </p>
                                            </div>
                                        </div>
                                    </li>
                                    <li id="html5canvasgame" class="book">
                                        <a href="{{ url('book/detail') }}">
                                            <img class="front" src="img/covers/html5-canvas.jpg" alt="HTML5 Canvas - For Games and Entertainment">
                                        </a>
                                        <div class="back">
                                            <div class="p10">
                                                <div class="rating star-3">
                                                    <ol>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                    </ol>
                                                </div>
                                                <dl>
                                                   <strong>Title:</strong>
                                                    HTML CANVAS
                                                    <strong>year:</strong>
                                                    2011
                                                </dl>
                                                <p class="description">
                                                Thought this book might be useful. Not very excited though....
                                                </p>
                                            </div>
                                        </div>
                                    </li>
                                    <li id="dontmakemethink" class="book">
                                        <a href="{{ url('book/detail') }}">
                                            <img src="img/covers/dont-make-me-think.jpg" alt="Don&quot;t make me think">
                                        </a>
                                        <div class="back">
                                            <div class="p10">
                                                <div class="rating star-5">
                                                    <ol>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                    </ol>
                                                </div>
                                                <dl>
                                                    <strong>Title:</strong>
                                                    Usablility
                                                    <strong>year:</strong>
                                                    2006
                                                </dl>
                                                <p class="description">
                                                    An absolute must-read for any developer and designer!
                                                </p>
                                            </div>
                                        </div>
                                    </li>
                                    <li id="javascriptguide" class="book">
                                        <a href="{{ url('book/detail') }}">
                                            <img class="front" src="img/covers/javascript-definitive-guide.jpg" alt="Javascript - The Definitive Guide">
                                        </a>
                                        <div class="back">
                                            <div class="p10">
                                                <div class="rating star-3">
                                                    <ol>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                    </ol>
                                                </div>
                                                <dl>
                                                    <strong>Title:</strong>
                                                    Javascript
                                                    <strong>year:</strong>
                                                    2010
                                                </dl>
                                                <p class="description">
                                                    Great Javascript reference, especially in the current days where everyone uses jQuery and tends to forget about "native" Javascript.
                                                </p>
                                            </div>
                                        </div>
                                    </li>
                                    <li id="dontmakemethink" class="book">
                                        <a href="{{ url('book/detail') }}">
                                            <img src="img/covers/dont-make-me-think.jpg" alt="Don&quot;t make me think">
                                        </a>
                                        <div class="back">
                                            <div class="p10">
                                                <div class="rating star-5">
                                                    <ol>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                    </ol>
                                                </div>
                                                <dl>
                                                    <strong>Title:</strong>
                                                    Usuability
                                                    <strong>year:</strong>
                                                    2012
                                                </dl>
                                                <p class="description">
                                                    An absolute must-read for any developer and designer!
                                                </p>
                                            </div>
                                        </div>
                                    </li>
                                    <li id="html5canvasgame" class="book">
                                        <a href="{{ url('book/detail') }}">
                                            <img class="front" src="img/covers/html5-canvas.jpg" alt="HTML5 Canvas - For Games and Entertainment">
                                        </a>
                                        <div class="back">
                                            <div class="p10">
                                                <div class="rating star-3">
                                                    <ol>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                    </ol>
                                                </div>
                                                <dl>
                                                    <strong>Title:</strong>
                                                    Usability
                                                    <strong>year:</strong>
                                                    2012
                                                </dl>
                                                <p class="description">

                                                </p>
                                            </div>
                                        </div>
                                    </li>
                                    <li id="dontmakemethink" class="book">
                                        <a href="{{ url('book/detail') }}">
                                            <img src="img/covers/dont-make-me-think.jpg" alt="Don&quot;t make me think">
                                        </a>
                                        <div class="back">
                                            <div class="p10">
                                                <div class="rating star-5">
                                                    <ol>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                    </ol>
                                                </div>
                                                <dl>
                                                    <strong>Title:</strong>
                                                    Usability
                                                    <strong>year:</strong>
                                                    2012
                                                </dl>
                                                <p class="description">
                                                    An absolute must-read for any developer and designer!
                                                </p>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="shelf owned">
                                <ul id="owl-example3" class=" books clear owl-carousel">
                                    <li id="html5webdesigners " class="book">
                                        <a href="{{ url('book/detail') }}">
                                            <img class="front" src="img/covers/bookapart-1.jpg" alt="HTML5 for Web Designers">
                                        </a>
                                        <div class="back">
                                            <div class="p10">
                                                <div class="rating star-3">
                                                    <ol>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                    </ol>
                                                </div>
                                                <dl>
                                                    <strong>Title:</strong>
                                                    Usability
                                                    <strong>year:</strong>
                                                    2012
                                                </dl>
                                                <p class="description">
                                                    Practical introduction into HTML5. Great examples, very easy to understand.
                                                </p>
                                            </div>
                                        </div>
                                    </li>
                                    <li id="css3webdesigners" class="book">
                                        <a href="{{ url('book/detail') }}">
                                            <img class="front" src="img/covers/bookapart-2.jpg" alt="CSS3 for Web Designers">
                                        </a>
                                        <div class="back">
                                            <div class="p10">
                                                <div class="rating star-4">
                                                    <ol>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                    </ol>
                                                </div>
                                                <dl>
                                                    <strong>Title:</strong>
                                                    Cascading Style Sheet 3
                                                    <strong>year:</strong>
                                                    2012
                                                </dl>
                                                <p class="description">
                                                    Practical introduction into the very basics of CSS3. Great examples, very easy to understand. Less interesting if you've read Handcrafted CSS before.
                                                </p>

<<<<<<< HEAD
									@if($s->transaction->status == 2)
										<a class="reserve_btn btn blue" 
											href="#reserve_form" 
											data-toggle="modal"
											data-stock-id="{{$s->id}}">
											Renew
										</a>
									@endif
									
									
								@else
									<a class="reserve_btn btn blue" 
									href="#reserve_form" 
									data-toggle="modal"
									data-stock-id="{{$s->id}}"
										>Reserve
									</a>
								@endif
							</td>
						</tr>
				
=======
                                            </div>
                                        </div>
                                    </li>
                                    <li id="contentstrategy" class="book">
                                        <a href="{{ url('book/detail') }}">
                                            <img class="front" src="img/covers/bookapart-3.jpg" alt="The Elements of Content Strategy">
                                        </a>
                                        <div class="back">
                                            <div class="p10">
                                                <div class="rating star-3">
                                                    <ol>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                    </ol>
                                                </div>
                                                <dl>
                                                    <strong>Title:</strong>
                                                    CSS 3
                                                    <strong>year:</strong>
                                                    2012
                                                </dl>
                                                <p class="description">
                                                    Practical introduction into the very basics of CSS3. Great examples, very easy to understand.
                                                </p>
                                            </div>
                                        </div>
                                    </li>
                                    <li id="responsivedesign" class="book">
                                        <a href="{{ url('book/detail') }}">
                                            <img class="front" src="img/covers/bookapart-4.jpg" alt="Responsive Web Design">
                                        </a>
                                        <div class="back">
                                            <div class="p10">
                                                <div class="rating star-5">
                                                    <ol>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                    </ol>
                                                </div>
                                                <dl>
                                                    <strong>Title:</strong>
                                                    Responsive Design
                                                    <strong>year:</strong>
                                                    2012
                                                </dl>
                                                <p class="description">
                                                    One of the first books on Responsive Design, and I believe still one of the best i've read! Extremely practical, well explained and many techniques still work.
                                                </p>
>>>>>>> 3048c40f61220d557f360aa7074850b435f91197

                                            </div>
                                        </div>
                                    </li>
                                    <li id="mobilefirst" class="book">
                                        <a href="{{ url('book/detail') }}">
                                            <img class="front" src="img/covers/bookapart-6.jpg" alt="Mobile First">
                                        </a>
                                        <div class="back">
                                            <div class="p10">
                                                <div class="rating star-5">
                                                    <ol>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                    </ol>
                                                </div>
                                                <dl>
                                                    <strong>Title:</strong>
                                                    Usability
                                                    <strong>year:</strong>
                                                    2012
                                                </dl>
                                                <p class="description">
                                                    Another very well written and extremely useful book. It pretty much set a new baseline for the projects I work on nowadays. I recommend reading Responsive Web Design first.
                                                </p>

                                            </div>
                                        </div>
                                    </li>
                                    <li id="responsivedesign" class="book">
                                        <a href="{{ url('book/detail') }}">
                                            <img class="front" src="img/covers/bookapart-4.jpg" alt="Responsive Web Design">
                                        </a>
                                        <div class="back">
                                            <div class="p10">
                                                <div class="rating star-5">
                                                    <ol>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                    </ol>
                                                </div>
                                                <dl>
                                                   <strong>Title:</strong>
                                                    Responsive Design
                                                    <strong>year:</strong>
                                                    2012
                                                </dl>
                                                <p class="description">
                                                    One of the first books on Responsive Design, and I believe still one of the best i've read! Extremely practical, well explained and many techniques still work.
                                                </p>

                                            </div>
                                        </div>
                                    </li>
                                    <li id="mobilefirst" class="book">
                                        <a href="{{ url('book/detail') }}">
                                            <img class="front" src="img/covers/bookapart-6.jpg" alt="Mobile First">
                                        </a>
                                        <div class="back">
                                            <div class="p10">
                                                <div class="rating star-5">
                                                    <ol>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                        <li>✰</li>
                                                    </ol>
                                                </div>
                                                <dl>
                                                    <strong>Title:</strong>
                                                    Mobile First Responsive Design
                                                    <strong>year:</strong>
                                                    2012
                                                </dl>
                                                <p class="description">
                                                    Another very well written and extremely useful book. It pretty much set a new baseline for the projects I work on nowadays. I recommend reading Responsive Web Design first.
                                                </p>

                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <!-- BEGIN PORTLET-->
                            <div class="portlet light bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-cursor font-purple"></i>
                                        <span class="caption-subject font-purple bold uppercase">General Stats</span>
                                    </div>
                              {{--       <div class="actions">
                                        <a href="javascript:;" class="btn btn-sm btn-circle red easy-pie-chart-reload">
                                            <i class="fa fa-repeat"></i> Reload </a>
                                    </div> --}}
                                </div>
                                <div class="portlet-body">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="easy-pie-chart">
                                                <div class="number transactions" data-percent="{{ $transaction_count }}">
                                                    <span>+{{ $transaction_count }}</span> </div>
                                                <a class="title" href="javascript:;"> Number of Transactions
                                                    <i class="icon-arrow-right"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="margin-bottom-10 visible-sm"> </div>
                                        <div class="col-md-4">
                                            <div class="easy-pie-chart">
                                                <div class="number visits" data-percent="{{ $user_count }}">
                                                    <span>+{{ $user_count }}</span> </div>
                                                <a class="title" href="javascript:;"> Number of Users
                                                    <i class="icon-arrow-right"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="margin-bottom-10 visible-sm"> </div>
                                        <div class="col-md-4">
                                            <div class="easy-pie-chart">
                                                <div class="number bounce" data-percent="{{ $book_count }}">
                                                    <span>+{{ $book_count }}</span> </div>
                                                <a class="title" href="javascript:;"> Number of Books
                                                    <i class="icon-arrow-right"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="portlet light calendar bordered">
                                <div class="portlet-title ">
                                    <div class="caption">
                                        <i class="icon-calendar font-green-sharp"></i>
                                        <span class="caption-subject font-green-sharp bold uppercase">Feeds</span>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div id="bootstrapModalFullCalendar"> </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PAGE CONTENT INNER -->
            <div id="fullCalModal" class="modal fade">
                <div class="modal-content">
                    <div class="modal-header header-bg">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span> <span class="sr-only">close</span></button>
                        <h4 id="modalTitle" class="modal-title" style="color:#fff"></h4>
                        <span >
                            <strong style="float: left;display: block;color: #fff;">Start Date : </strong>
                            <h5 id="showDateStart" style=" margin:6px; color: #fff; float: left;display: block;"></h5>
                        </span><div class="clearfix"></div>
                        <span><strong style="float: left;display: block;color: #fff;">End Date :</strong><h5 id="showDateEnd" style=" margin:6px; color: #fff; float: left;display: block;"></h5> </span><div class="clearfix"></div>
                        <span><strong style="float: left;display: block;color: #fff;">Time : </strong><h5 id="timeDate" style=" margin:6px; float: left;display: block;color: #fff;"></h5></span>
                    </div>
                    <div id="modalBody" class="modal-body"></div>
                    <div class="modal-footer">
                        <button type="button" class="book_button" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<<<<<<< HEAD
									{{-- ajax error handle --}}
									<div class="error_hidden" hidden="true">
										<div class="alert alert-danger">
											<strong>Whoops!</strong> There were some problems with your input.<br><br>
											<ul class="error">
											</ul>
										</div>
									</div>
								</div>
							</div>

							{{-- modal footer --}}

							<div class="modal-footer">
								<button class="btn dark btn-outline" data-dismiss="modal" aria-hidden="true">Close</button>
								<input class="btn dark btn-outline" type="submit" value="Reserve">
							</div>

							{!! Form::close()!!}
						</div>
					</div>
					@endforeach
				</table>

			<!-- END SAMPLE FORM PORTLET-->
			</div>
		</div>
	</div>
=======
>>>>>>> 3048c40f61220d557f360aa7074850b435f91197
@endsection
