<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Report</title>
    <link href="../latestcss/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
</head>
<body>
	<div class="container-fluid text-center">
		<div class="col-md-2">
			<img src="../img/cmc.png" alt="" style="height: 150px;margin-top: 10px; margin-left: 100px">
		</div>
		<div class="col-md-8">
			<h4><strong>Chitwan Medical College (P) Ltd</strong></h4>
			<h3><strong>Chitwan Medical College Teaching Hospital</strong></h3>
			<h5 class="detail-weight">Bharatpur 10, Chitwan, Nepal</h5>
			<h5 class="detail-weight">Phone No: 977-56-532933(Hospital) 977-56-592366(College) 977-1-400504(Kathmandu)</h5>
			<h5 class="detail-weight">Email: info@cmc.edu.np Website: www.cmc.edu.np</h5>
		</div>
		<div class="clearfix"></div>
		<hr>
		<div id="title"><h4>Title</h4></div>
	</div>
	<div class="container" id="Content">
		<div class="table-scrollable">
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                </tbody>
            </table>
        </div>
	</div>
</body>
</html>