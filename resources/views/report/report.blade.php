@extends('adminMaster')
@section('title','Report')

@section('page-content')
	<div class="page-content-wrapper">
	<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <a href="{{ url('dashboard') }}">Home</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <span>Report</span>
                    </li>
                </ul>
            </div>
            <div class="page-title"></div>
			<div class="portlet light bordered">
				<div class="portlet-title">
					<div class="caption font-green-haze">
						<i class="icon-social-dropbox font-green-haze"></i>
						<span class="caption-subject bold uppercase">Generate your report</span>
					</div>
				 <div class="tools"> </div>
				</div>
				{!! Form::open(array('class' => 'form-horizontal')) !!}
					<div class="form-body">
						<div class="form-group form-md-line-input">
			               	<label class="col-md-3 control-label" for="from">Form:</label>
			               	<div class="col-md-7">
			                  	{!! Form::date('form', null, array('class' => 'form-control','id' => 'from','placeholder'=>'From date')) !!}
			                  	<div class="form-control-focus"> </div>
			               	</div>
			            </div>

			            <div class="form-group form-md-line-input">
			               	<label class="col-md-3 control-label" for="to">To:</label>
			               	<div class="col-md-7">
			                  	{!! Form::date('to', null, array('class' => 'form-control','id' => 'to','placeholder'=>'To date')) !!}
			                  	<div class="form-control-focus"> </div>
			               	</div>
			            </div>
			            <div class="form-group form-md-line-input">
			               	<label class="col-md-3 control-label" for="user">Application User:</label>
			               	<div class="col-md-7">
			               		<select class="form-control" id="user">
                                    <option value=""></option>
                                    <option value="">Option 1</option>
                                    <option value="">Option 2</option>
                                    <option value="">Option 3</option>
                                    <option value="">Option 4</option>
                                </select>
			                   	<div class="form-control-focus"> </div>
			               	</div>
			            </div>
			        </div>
		            <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-2 col-md-10">
                                <button type="button" class="btn blue">Generate</button>
                            </div>
                        </div>
                    </div>
                {!! Form::close() !!}
			</div>
		</div>
	</div>
@endsection


