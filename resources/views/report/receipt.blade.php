<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Report</title>
    <link href="../latestcss/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
</head>
<body>
	<div class="container-fluid text-center">
		<div class="row">
			<div class="col-md-12">
				<img src="{{ url('../assets/layouts/layout/img/cmclogo.png') }}" alt="" class="pull-left" style="height: 150px;margin-top: 10px">
				<h4>Chitwan Medical College (P) Ltd</h4>
				<h3>Chitwan Medical College Teaching Hospital</h3>
				<h5>Bharatpur 10, Chitwan, Nepal</h5>
				<h5>Phone No: 977-56-532933(Hospital) 977-56-592366(College) 977-1-400504(Kathmandu)</h5>
				<h5>Email: info@cmc.edu.np Website: www.cmc.edu.np</h5>
			</div>
			<div class="clearfix"></div>
		</div>
		<hr>
		<div id="title"><h3>Fine Receipt</h3></div>
	</div>
	<div id="detail">
		<div>
			<label for="library">Library</label> Hospital/Central  {{-- print hospital or central --}}
			<span class="pull-right"><label for="date">Date:</label> 2017/01/01 17:50:00 </span> <br>
			<label for="name">Name:</label> Sonika Ranabhat 
			<span class="pull-right"><label for="program">Program:</label> MBBS </span><br>
			<label for="member_code">Member Code:</label> MBBS98 <br>
 		</div>
	</div>
	<div class="clearfix"></div>
	<div id="Content">
		<h5>Cash Received of Rs. 1000 for Library fine.</h5>
		<table class="table ">
			<tr>
				<td>Total Amount due:</td>
				<td style="text-align: right;">Rs. 1200</td>
			</tr>
			<tr>
				<td>Amount Received:</td>
				<td style="text-align: right;">Rs. 1000</td>
			</tr>
			<tr>
				<td>Balance Due:</td>
				<td style="text-align: right;">Rs. 30</td>
			</tr>
		</table>
	</div>
	<div id="footer" style="margin-top: 30px">
		<div>________________</div>
		<label for="user">User:</label>JAMUNA
		<div class="pull-right" style="font-size: 12px"><label for="current_date">Printed Date:</label> 2017/03/15 12:48 PM</div>
	</div>
</body>
</html>