<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Report</title>
    <link href="../latestcss/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
</head>
<body>
	<div class="container-fluid text-center">
		<div class="row">
			<div class="col-md-2">
				<img src="{{ url('../assets/layouts/layout/img/cmclogo.png') }}" alt="" style="height: 150px;margin-top: 10px;margin-left: 100px">
			</div>
			<div class="col-md-8">
				<h4>Chitwan Medical College (P) Ltd</h4>
				<h3>Chitwan Medical College Teaching Hospital</h3>
				<h5>Bharatpur 10, Chitwan, Nepal</h5>
				<h5>Phone No: 977-56-532933(Hospital) 977-56-592366(College) 977-1-400504(Kathmandu)</h5>
				<h5>Email: info@cmc.edu.np Website: www.cmc.edu.np</h5>
			</div>
			<div class="clearfix"></div>
		</div>
		<hr>
		<div id="title"><h3>{{$title}}@if($name)({{$name}})@endif</h3></div>
	</div>
	<div id="Content">
		@if(count($transactions)!= 0)
			<table class="table table-bordered">
					<tr>
						<th>S.N</th>
						<th>Book Name</th>
						<th>Reserved By</th>
						<th>Status</th>
						<th>Reserved From</th>
						<th>Reserved To</th>
						<th>Authorized By</th>
					</tr>
					@foreach ($transactions as $key => $transaction)
						<tr>
							<td>{{++$key}}</td>
							<td>{{$transaction->stock_detail->title}}</td>
							<td>{{$transaction->reserved_by->name}}</td>
							{{-- Book Requested --}}
							@if($transaction->status == 0)
								<td>Requested</td>
							@endif
							{{-- Book Issued --}}
							@if($transaction->status == 1)
								<td>Issued</td>
							@endif
							{{-- Book Dispatched --}}
							@if($transaction->status == 2)
								<td>Dispatched</td>
							@endif
							{{-- Book Returned --}}
							@if($transaction->status == 4)
								<td>Returned</td>
							@endif
							<td>{{$transaction->book_from->format('Y-m-d')}}</td>
							<td>{{$transaction->book_to->format('Y-m-d')}}</td>
							<td>{{$trans->getReceived($transaction->updated_by)}}</td>
						</tr>
					@endforeach
				</table>	
			@else
					<h4 class="text-center">No Data Available</h4>
						
			@endif	
	</div>
</body>
</html>