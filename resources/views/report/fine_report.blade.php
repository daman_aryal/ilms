<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Report</title>
    <link href="../latestcss/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
	 <link href="{{ url('latestcss/assets/global/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
	<style>
		.logo{
			height: 150px;
			margin-top: 10px; 
			
		}
		@media print{
			.logo{
				height: 100px;
				margin-top: 10px;
			}
		}
	</style>
</head>
<body>
	<a style="float: left; font-size: 50px;" class="hide_print" href="{{url('/')}}/fine_index"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i></a>
	<a style="float: right; font-size: 50px;" class="hide_print" id="print"><i class="fa fa-print" aria-hidden="true"></i></a>
	<div class="container-fluid text-center print">
		<div class="row">
			<div class="col-md-12">
				<img src="{{ url('../assets/layouts/layout/img/cmclogo.png') }}" alt="" class="logo pull-left">
				<h5>Chitwan Medical College (P) Ltd</h5>
				<h4>Chitwan Medical College Teaching Hospital</h4>
				<h6>{{$location}}</h6>
				<h6>Phone No: 977-56-532933(Hospital) 977-56-592366(College) 977-1-400504(Kathmandu)</h6>
				<h6>Email: info@cmc.edu.np Website: www.cmc.edu.np</h6>
			</div>
			<div class="clearfix"></div>
		</div>
		<hr>
		<div id="title">
		<h3>{{$title}}@if($name)({{$name}})@endif</h3>
		<h6>from: {{$date_from}} to {{$date_to}}</h6>
		</div>
	</div>
	<div id="Content">
		@if(count($fines)!= 0)
			<table class="table table-bordered">
					<tr>
						<th>S.N</th>
						<th>Date</th>
						<th>Code</th>
						<th>Name</th>
						{{-- <th>Mem.Type</th> --}}
						<th>Receipt No.</th>
						<th>Amount</th>
						{{-- <th>Duration</th> --}}
						<th>Received By</th>
					</tr>
					@foreach ($fines as $key => $fine)
						<tr>
							<td>{{++$key}}</td>
							<td>{{Carbon\Carbon::parse($fine->received_date)->format('Y-m-d')}}</td>
							<td>{{$fine->chargedTo->code}}</td>
							<td>{{$fine->chargedTo->name}}</td>
							{{-- {{dd($staffUser)}} --}}
							{{-- <td>{{$fine->bookFrom->book_to->format('Y-m-d')}}</td> --}}
							<td>{{$fine->receipt_no}}</td>
							<td>{{$fine->amount}}</td>
							{{-- <td>{{$fine->received_date}}</td> --}}
							<td>{{$finee->getReceived($fine->received_by)}}</td>
						</tr>
					@endforeach
				</table>	
			@else
					<h4 class="text-center">No Data Available</h4>
						
			@endif	
	</div>
</body>
<footer>
	<script src="{{ url('latestcss/js/jquery.min.js') }}"></script>
	<script type="text/javascript">
	$('#print').on('click',function(e){
		$('.hide_print').hide();
			window.print();
	});

	</script>
</footer>
</html>
