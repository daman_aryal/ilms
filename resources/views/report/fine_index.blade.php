@extends('adminMaster')
@section('title','Fine Collection Report')

@section('page-content')
<div class="page-content-wrapper">
	<!-- BEGIN CONTENT BODY -->
	<div class="page-content">
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li>
					<a href="{{ url('dashboard') }}">Home</a>
					<i class="fa fa-circle"></i>
				</li>
				<li>
					<span>Report</span>
				</li>
			</ul>
		</div>
		<div class="page-title"></div>
		<div class="portlet light bordered">
			<div class="portlet-title">
				<div class="caption font-green-haze">
					<i class="icon-social-dropbox font-green-haze"></i>
					<span class="caption-subject bold uppercase">Fine Collection report</span>
				</div>
				<div class="tools"> </div>
			</div>
			<div class="portlet-body form">
				{!! Form::open(array('url' => 'fine_report','method'=>'POST')) !!}
				<div class="form-body">
					<div class="form-group form-md-line-input">
						<label class="control-label col-md-3">From:</label>
						<div class="col-md-4">
							{!! Form::text('from',null , array('class' => 'form-control date-picker','id' => 'from','placeholder'=>'YYYY-MM-DD', 'data-date-format' => 'yyyy-mm-dd')) !!}
							<div class="form-control-focus"></div>
						</div>
						<label class="col-md-1 control-label" for="form_control_1">All:</label>
						<div class="col-md-3">
							{{ Form::checkbox('all', null, old('all'), [ 'class' => 'make-switch all', 'data-size' => 'make-switch' ] ) }}
						</div>

					</div>
					<div class="form-group form-md-line-input">
						<label class="control-label col-md-3">To:</label>
						<div class="col-md-4">
							{!! Form::text('to',null , array('class' => 'form-control date-picker','id' => 'to','placeholder'=>'YYYY-MM-DD', 'data-date-format' => 'yyyy-mm-dd')) !!}
							<div class="form-control-focus"></div>
						</div>
					</div>
					<div class="condition">
						<div class="form-group form-md-line-input">
							<label class="col-md-3 control-label" for="user">Application User:</label>
							<div class="col-md-7">
								{!! Form::select('users',$users, null, ['class' => 'form-control','placeholder' => 'Select User']) !!}
								<div class="form-control-focus"> </div>
							</div>

						</div>
						<div class="form-group form-md-line-input">
						</div>
					</div>
				</div>
				<div class="form-actions">
					<div class="row">
						<div class="col-md-offset-2 col-md-10">
							<button type="submit" class="btn blue" target="_blank">View Report</button>
						</div>
					</div>
				</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>
@endsection
@section('footerscript')
<script>
	$('.all').on('switchChange.bootstrapSwitch', function (e,state) {
		console.log(state);
		if(state == true){
			$('.condition').hide();
		}else{
			$('.condition').show();
		}
	});
	$(document).ready(function(){
		$("#from").datepicker().datepicker("setDate", new Date());
		$("#to").datepicker().datepicker("setDate", new Date());
	});
</script>
@endsection

