
   <div class="form-body">
      <div class="row div-form">
         <div class="col-md-8">
            <div class="form-group form-md-line-input">
               <label class="col-md-3 control-label" for="first_name">First Name:</label>
               <div class="col-md-7">
{{--                   {{ $user->basicUser[0]->first_name }} --}}
                  {!! Form::text('first_name',( isset($user->basicUser[0]->first_name) ? $user->basicUser[0]->first_name : null ), array('class' => 'form-control','id' => 'first_name','placeholder'=>'Enter your first name')) !!}
                  <div class="form-control-focus"> </div>
               </div>
            </div>
            <div class="form-group form-md-line-input">
               <label class="col-md-3 control-label" for="last_name">Last Name:</label>
               <div class="col-md-7">
                  {!! Form::text('last_name',( isset($user->basicUser[0]->last_name) ? $user->basicUser[0]->last_name : null ), array('class' => 'form-control','id' => 'last_name','placeholder'=>'Enter your last name')) !!}
                  <div class="form-control-focus"> </div>
               </div>
            </div>
            {{-- <div class="form-group form-md-line-input">
               <label class="col-md-3 control-label" for="date_of_birth">Date of Birth:</label>
               <div class="col-md-7">
                  {!! Form::date('date_of_birth',( isset($user->basicUser[0]->date_of_birth) ? $user->basicUser[0]->date_of_birth : null ), array('class' => 'form-control','id' => 'date_of_birth','placeholder'=>'Enter your date of birth')) !!}
                  <div class="form-control-focus"> </div>
               </div>
            </div> --}}
            <div class="form-group form-md-line-input">
                <label class="control-label col-md-3">Date of Birth:</label>
                <div class="col-md-7">
                  {!! Form::text('date_of_birth',( isset($user->basicUser[0]->date_of_birth) ? $user->basicUser[0]->date_of_birth : null ), array('class' => 'form-control date-picker','id' => 'date_of_birth','placeholder'=>'YYYY-MM-DD', 'data-date-format' => 'yyyy-mm-dd')) !!}
                    {{-- <input class="form-control date-picker" size="16" type="text" placeholder="DD/MM/YYYY"> --}}
                    <div class="form-control-focus"></div>
                </div>
            </div>
         </div>
         @if($edit)
            <div class="col-md-2 col-sm-3 img-upload">
               <input id="user_image" class="dropify" data-height="140" data-width="100" data-allowed-file-extensions="png jpeg jpg" data-max-file-size="2M" data-default-file="{{( isset($user->images->path) ? '/'.$user->images->path : null )}}" name="user_image" type="file">
            </div>

         @else
            <div class="col-md-2 col-sm-3 img-upload">
               <input id="user_image" class="dropify" data-height="140" data-width="100" data-allowed-file-extensions="png jpeg jpg" data-max-file-size="2M" data-default-file="" name="user_image" type="file">
            </div>
         @endif

      </div>
      <div class="form-group form-md-line-input full-row-fg">
      <label class="col-md-2 control-label" for="gaurdian_name">Gaurdian Name:</label>
         <div class="col-md-10">
            {!! Form::text('gaurdian_name',( isset($user->basicUser[0]->gaurdian_name) ? $user->basicUser[0]->gaurdian_name : null ), array('class' => 'form-control','id' => 'garudian','placeholder'=>"Enter your garudian's name")) !!}
            <div class="form-control-focus"> </div>
         </div>
      </div>
      <div class="form-group form-md-line-input full-row-fg">
      <label class="col-md-2 control-label" for="relation">Relation:</label>
         <div class="col-md-10">
            {!! Form::text('relation',( isset($user->basicUser[0]->relation) ? $user->basicUser[0]->relation : null ), array('class' => 'form-control','id' => 'relation','placeholder'=>"Enter your relation with gaurdian")) !!}
            <div class="form-control-focus"> </div>
         </div>
      </div>
      <div class="form-group form-md-line-input full-row-fg">
      <label class="col-md-2 control-label" for="contact">Contact no:</label>
         <div class="col-md-10">
            {!! Form::number('contact',null, array('class' => 'form-control','id' => 'contact','placeholder'=>"Enter your contact no")) !!}
            <div class="form-control-focus"> </div>
         </div>
      </div>
      <div class="form-group form-md-line-input full-row-fg">
         <label class="col-md-2 control-label" for="address">Address:</label>
         <div class="col-md-10">
            {!! Form::text('address',null, array('class' => 'form-control','id' => 'Address','placeholder'=>"Enter your Address")) !!}
            <div class="form-control-focus"> </div>
         </div>
      </div>

      {{-- {{ dd($user->images->path )}} --}}
      
     <div class="row div-form">
         <div class="col-md-6">
            <div class="form-group form-md-line-input ">
               <label class="col-md-3 control-label" for="year">Year:</label>
               <div class="col-md-7 hori-mar-inp">
                  {!! Form::select('year_id',$years,( isset($user->basicUser[0]->year_id) ? $user->basicUser[0]->year_id : null ), array('class' => 'form-control','id' => 'year','placeholder'=>"Enter the studying year")) !!}
                  <div class="form-control-focus"> </div>
               </div>
            </div>
         </div>
         <div class="col-md-6">
            <div class="form-group form-md-line-input">
               <label class="col-md-3 control-label" for="batch">Batch:</label>
               <div class="col-md-7 hori-mar-inp">
                  {!! Form::text('batch_id',( isset($batch) ? $batch : null ), array('class' => 'form-control','id' => 'batch','readonly'=>'true','placeholder'=>"Enter the Batch")) !!}
                  <div class="form-control-focus"> </div>
               </div>
            </div>
         </div>
      </div>
      <div class="row div-form">
         <div class="col-md-6">
            <div class="form-group form-md-line-input">
               <label class="col-md-3 control-label" for="program">Program:</label>
               <div class="col-md-7 hori-mar-inp">
                  {!! Form::select('program_id',$programs,( isset($user->basicUser[0]->program_id) ? $user->basicUser[0]->program_id : null ), array('class' => 'form-control','id' => 'program','placeholder'=>"Enter the program")) !!}
                   <div class="form-control-focus"> </div>
               </div>
            </div>
         </div>
         <div class="col-md-6">
            <div class="form-group form-md-line-input">
               <label class="col-md-3 control-label" for="class">Class:</label>
               <div class="col-md-7 hori-mar-inp">
                  {!! Form::select('class_id',[],( isset($user->basicUser[0]->class_id) ? $user->basicUser[0]->class_id : null ), array('class' => 'form-control','id' => 'class','placeholder'=>"Select Class")) !!}
                   <div class="form-control-focus"> </div>
               </div>
            </div>
         </div>
      </div>
      <div class="row div-form">
         <div class="col-md-6">
            <div class="form-group form-md-line-input">
               <label class="col-md-3 control-label" for="section">Section:</label>
               <div class="col-md-7 hori-mar-inp">
                  {!! Form::select('section_id',[],( isset($user->basicUser[0]->section_id) ? $user->basicUser[0]->section_id : null ), array('class' => 'form-control','id' => 'section','placeholder'=>"Select the section")) !!}
                   <div class="form-control-focus"> </div>
               </div>
            </div>
         </div>
         <div class="col-md-6">
            <div class="form-group form-md-line-input ">
               <label class="col-md-3 control-label" for="Roll">Roll no:</label>
               <div class="col-md-7 hori-mar-inp">
                  {!! Form::number('roll_number',( isset($user->basicUser[0]->roll_number) ? $user->basicUser[0]->roll_number : null ), array('class' => 'form-control','id' => 'roll','placeholder'=>"Enter the Roll number")) !!}
                   <div class="form-control-focus"> </div>
               </div>
            </div>
         </div>
      </div>
      {{-- <div class="form-group form-md-line-input full-row-fg">
      <label class="col-md-2 control-label" for="valid_till">Valid Till:</label>
         <div class="col-md-6">
            {!! Form::date('valid_till',( isset($user->basicUser[0]->valid_till) ? $user->basicUser[0]->valid_till : null ), array('class' => 'form-control','id' => 'valid','placeholder'=>"Enter the valid date")) !!}
            <div class="form-control-focus"> </div>
         </div>
      </div> --}}
      <div class="form-group form-md-line-input">
        <label class="control-label col-md-2">Valid Till:</label>
        <div class="col-md-7">
          {!! Form::text('valid_till',( isset($user->basicUser[0]->valid_till) ? $user->basicUser[0]->valid_till : null ), array('class' => 'form-control date-picker','id' => 'valid','placeholder'=>'YYYY-MM-DD', 'data-date-format' => 'yyyy-mm-dd')) !!}
            <div class="form-control-focus"></div>
        </div>
      </div>
      {{-- {!! Form::hidden('member_type_id',null, array('class' => 'form-control','id' => 'memberType','placeholder'=>"Enter the Member Type",'readonly'=>'true')) !!} --}}
      <div class="row div-form">
         <div class="col-md-6">
            <div class="form-group form-md-line-input">
               {{-- <label class="col-md-3 control-label" for="member_code">Member Code:</label> --}}
               <div class="col-md-7 hori-mar-inp">
                  {!! Form::hidden('member_code',( isset($user->basicUser[0]->member_code) ? $user->basicUser[0]->member_code : null ), array('class' => 'form-control','id' => 'member_code','placeholder'=>"Enter the member code",'readonly'=>'true')) !!}
                   <div class="form-control-focus"> </div>
               </div>
            </div>
         </div>
      </div>
   </div>
  
