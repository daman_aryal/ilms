@inject('active_nav', 'App\Injection\SideNav')
<div class="page-sidebar-wrapper">
    <div class="page-sidebar navbar-collapse collapse">
        <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
            
            @if(Auth::user()->user_type() == "su")
                @include('partials.admin.suadmin_sidebar')
            @endif

            @if(Auth::user()->user_type() == "admin")
                @include('partials.admin.admin_sidebar')
            @endif

            {{-- lihbarian --}}            
            @if(Auth::user()->user_type() == "librarian")
                @include('partials.admin.librarian_sidebar')
            @endif

            {{--studnet --}}            
            @if(Auth::user()->user_type() == "student")
                @include('partials.admin.student_sidebar')
            @endif  

        </ul>
        <!-- END SIDEBAR MENU -->
    </div>
    <!-- END SIDEBAR -->
</div>