<li class="sidebar-toggler-wrapper hide">
    <div class="sidebar-toggler"> </div>
</li>

<li class="nav-item start">
    <a href="{{ url('student/') }}" class="nav-link nav-toggle">
        <i class="icon-home"></i>
        <span class="title">Dashboard</span>
    </a>
</li>
<li class="{{$active_nav->active_nav('student/transaction/status')}} nav-item">
    <a href="{{ url('student/transaction/status') }}" class=" nav-link nav-toggle">
        <i class="icon-user-following"></i>
        <span class="title">Transaction Status</span>
    </a>
</li>
