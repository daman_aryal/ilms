        @inject('user_form', 'App\Injection\UserForm') 
            @if ($user_form->location() != null)
                <a href="javascript:;" style="font-style: italic; color: #747f8c; margin-left: 20px;">
                    Location: <span>{{$user_form->location()}}</span>
                </a>
            @endif
            <li class="{{$active_nav->active_nav('dashboard')}} nav-item">
               <a href="{{ url('dashboard') }}" class=" nav-link nav-toggle">
                    <i class="icon-home"></i>
                    <span class="title">Dashboard</span>
                </a>
            </li>
            <li class="{{$active_nav->active_nav('role')}} nav-item">
               <a href="{{ url('role/') }}" class=" nav-link nav-toggle">
                    <i class="icon-user-following"></i>
                    <span class="title">Roles Management</span>
                </a>
            </li>
            <!-- END RESPONSIVE QUICK SEARCH FORM -->
            </li>
            <li class="nav-item {{$active_nav->expand_nav('adminUser')}}{{$active_nav->expand_nav('basicUser')}}{{$active_nav->expand_nav('libUser')}}">
                <a href="javascript:;" class="nav-link nav-toggle">
                     <i class="icon-users"></i>
                    <span class="title">Users Management</span>
                    <span class="arrow {{$active_nav->span_nav('adminUser')}}{{$active_nav->span_nav('basicUser')}}{{$active_nav->span_nav('libUser')}}"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item {{$active_nav->active_nav('adminUser')}} ">
                        <a href="{{ url('adminUser/') }}" class="nav-link ">
                            <span class="title">Application User</span>
                        </a>
                    </li>
                    <li class="nav-item  {{$active_nav->active_nav('libUser')}}">
                        <a href="{{ url('libUser') }}" class="nav-link ">
                            <span class="title">Library User</span>
                        </a>
                    </li>
                   {{--  <li class="nav-item  {{$active_nav->active_nav('basicUser')}}">
                        <a href="{{ url('basicUser') }}" class="nav-link ">
                            <span class="title">Basic User Management</span>
                        </a>
                    </li> --}}
                </ul>
            </li>
            <li class="nav-item {{$active_nav->active_nav('stock')}}">
                <a href="{{ url('/stock') }}" class="nav-link nav-toggle">
                    <i class="icon-diamond"></i>
                    <span class="title">Stocks</span>
                </a>
            </li>
            <li class="nav-item {{$active_nav->active_nav('setting')}}{{$active_nav->active_nav('year')}}{{$active_nav->active_nav('sector')}}{{$active_nav->active_nav('subject')}}{{$active_nav->active_nav('genre')}}{{$active_nav->active_nav('membertype')}}{{$active_nav->active_nav('department')}}{{$active_nav->active_nav('class')}}{{$active_nav->active_nav('section')}}{{$active_nav->active_nav('program')}}">
                 <a href="{{ url('setting') }}" class="nav-link nav-toggle">
                  <i class="icon-settings"></i>
                   <span class="title"> Setting</span>
               </a>
            </li>
            <li class="nav-item {{$active_nav->expand_nav('fine_index')}}{{$active_nav->expand_nav('transaction_index')}}{{$active_nav->expand_nav('book_index')}}">
                <a href="javascript:;" class="nav-link nav-toggle">
                     <i class="icon-doc"></i>
                    <span class="title">Reports</span>
                    <span class="arrow {{$active_nav->span_nav('fine_index')}}{{$active_nav->span_nav('transaction_index')}}{{$active_nav->span_nav('book_index')}}"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item {{$active_nav->active_nav('fine_index')}} ">
                        <a href="{{ url('fine_index/') }}" class="nav-link ">
                            <span class="title">Fine Collection Report</span>
                        </a>
                    </li>
                   {{--  <li class="nav-item  {{$active_nav->active_nav('transaction_index')}}">
                        <a href="{{ url('transaction_index') }}" class="nav-link ">
                            <span class="title">Transaction Report</span>
                        </a>
                    </li> --}}
                    {{-- <li class="nav-item  {{$active_nav->active_nav('book_index')}}">
                        <a href="{{ url('book_index') }}" class="nav-link ">
                            <span class="title">Books Report</span>
                        </a>
                    </li> --}}
                </ul>
            </li>
                        
