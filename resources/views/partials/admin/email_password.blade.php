<!-- email and password sections-->
<div class="col-xs-12 col-sm-12 col-md-12">
    <div class="form-group form-md-line-input">
        <label class="col-md-2 control-label" for="form_control_1">Email:</label>
        <div class="col-md-10">
        {!! Form::text('email', null, array('placeholder' => 'Email','class' => 'form-control')) !!}
        <div class="form-control-focus"> </div>
       </div>
   </div>
</div>

<div class="col-xs-12 col-sm-12 col-md-12">
  <div class="form-group form-md-line-input">
      <label class="col-md-2 control-label" for="form_control_1">Password:</label>
      <div class="col-md-10">
          <div class="input-group">
              <div class="input-group-control">
                  {!! Form::password('password', array('placeholder' => 'Password','class' => 'form-control','id' => 'password')) !!}
                  <div class="form-control-focus"> </div>
              </div>
              <span class="input-group-btn btn-right">
                  <a class="btn green-haze" id="generate">Generate</a>
              </span>
          </div>
      </div>
  </div>
</div>

<div class="col-xs-12 col-sm-12 col-md-12">
    <div class="form-group form-md-line-input">
        <label class="col-md-2 control-label" for="form_control_1">Password Confirmation:</label>
        <div class="col-md-10">
         {!! Form::password('password_confirm', array('placeholder' => 'Confirm Password','class' => 'form-control', 'id'=>'password_confirm')) !!}    
        <div class="form-control-focus"> </div>
       </div>
   </div>
</div>

<div class="col-xs-12 col-sm-12 col-md-12" id="generated" style="display: none">
    <div class="form-group form-md-line-input">
        <label class="col-md-2 control-label" for="form_control_1">Generated Password:</label>
        <div class="col-md-10">
          <span id="view_pass"></span>    
        <div class="form-control-focus"> </div>
       </div>
   </div>
</div>

<!-- form action buttons-->
