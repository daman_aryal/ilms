
<li class="{{$active_nav->active_nav('dashboard')}} nav-item">
   <a href="{{ url('dashboard') }}" class=" nav-link nav-toggle">
        <i class="icon-home"></i>
        <span class="title">Dashboard</span>
    </a>
</li>
<li class="{{$active_nav->active_nav('role')}} nav-item">
   <a href="{{ url('role/') }}" class=" nav-link nav-toggle">
        <i class="icon-user-following"></i>
        <span class="title">Roles Management</span>
    </a>
</li>

<!-- END RESPONSIVE QUICK SEARCH FORM -->
</li>
<li class="nav-item {{$active_nav->active_nav('user')}}">
    <a href="{{ url('user/') }}" class="nav-link nav-toggle">
        <i class="icon-users"></i>
        <span class="title">Users Management</span>
    </a>
</li>


<li class="nav-item nav-item {{$active_nav->active_nav('stock')}}">
    <a href="{{ url('/stock') }}" class="nav-link nav-toggle">
        <i class="icon-diamond"></i>
        <span class="title">Stocks</span>
    </a>
</li>
<li class="nav-item  ">
    <a href="javascript:;" class="nav-link nav-toggle">
        <i class="icon-puzzle"></i>
        <span class="title">Requests</span>
    </a>
</li>
<li class="nav-item  ">
    <a href="javascript:;" class="nav-link nav-toggle">
        <i class="icon-settings"></i>
        <span class="title">Charts</span>
        <span class="arrow"></span>
    </a>
    <ul class="sub-menu">
        <li class="nav-item  ">
            <a href="#" class="nav-link ">
                <span class="title">Bar Chart</span>
            </a>
        </li>
        <li class="nav-item  ">
            <a href="form_controls_md.html" class="nav-link ">
                <span class="title">Pie Chart</span>
            </a>
        </li>
    </ul>
</li>
<li class="nav-item  ">
    <a href="javascript:;" class="nav-link nav-toggle">
        <i class="icon-bulb"></i>
        <span class="title">Tables</span>
        <span class="arrow"></span>
    </a>
    <ul class="sub-menu">
        <li class="nav-item  ">
            <a href="elements_steps.html" class="nav-link ">
                <span class="title">Pending Books</span>
            </a>
        </li>
        <li class="nav-item  ">
            <a href="elements_lists.html" class="nav-link ">
                <span class="title">Borrowed Books</span>
            </a>
        </li>
        <li class="nav-item  ">
            <a href="elements_ribbons.html" class="nav-link ">
                <span class="title">Required Books</span>
            </a>
        </li>
    </ul>
</li>


