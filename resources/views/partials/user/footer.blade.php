<!-- BEGIN PRE-FOOTER -->
<div class="page-prefooter">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12 footer-block">
                <h2>About</h2>
                <p align="justify">OLIMS also known as a Online library information management system, is a resource management system for library, used to track acquisitions, cataloging and circulation. </p>
            </div>
            <div class="col-md-3 col-sm-6 col-xs12 footer-block">
                <h2>Subscribe Email</h2>
                <div class="subscribe-form">
                    <form action="javascript:;">
                        <div class="input-group">
                            <input type="text" placeholder="mail@mavorion.com" class="form-control">
                            <span class="input-group-btn">
                                <button class="btn" type="submit">Submit</button>
                            </span>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12 footer-block">
                <h2>Follow Us On</h2>
                <ul class="social-icons">
                    <li>
                        <a href="https://www.facebook.com/mavorion" data-original-title="facebook" class="facebook"></a>
                    </li>
                    <li>
                        <a href="https://twitter.com/mavorion" data-original-title="twitter" class="twitter"></a>
                    </li>
                    <li>
                        <a href="https://www.linkedin.com/company/mavorion-systems" data-original-title="linkedin" class="linkedin"></a>
                    </li>
                </ul>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12 footer-block">
                <h2>Contacts</h2>
                <address class="margin-bottom-40"> Phone: +977-1-4483080
                    <br> Email:
                    <a href="mailto:b2b.mavorion.com">b2b@mavorion.com</a>
                </address>
            </div>
        </div>
    </div>
</div>
<!-- END PRE-FOOTER -->
<!-- BEGIN INNER FOOTER -->
<div class="page-footer">
    <div class="container"> ILMS by Mavorion Systems
    </div>
</div>
<div class="scroll-to-top">
    <i class="icon-arrow-up"></i>
</div>