<div class="page-header ">
<!-- BEGIN HEADER TOP -->
    <div class="page-header-top">
        <div class="container">
            <!-- BEGIN LOGO -->
            <div class="page-logo">
                <a href="{{url('/home')}}">
                    <img src="/img/mav.png" alt="logo" class="logo-default" style="margin-top: 13px;">
                </a>
            </div>
            <!-- END LOGO -->
            <!-- BEGIN RESPONSIVE MENU TOGGLER -->
            <a href="javascript:;" class="menu-toggler"></a>
            <!-- END RESPONSIVE MENU TOGGLER -->
            <!-- BEGIN TOP NAVIGATION MENU -->
            <div class="top-menu">
                <ul class="nav navbar-nav pull-right">

                    <!-- END NOTIFICATION DROPDOWN -->
                    <!-- BEGIN TODO DROPDOWN -->
                   {{--  <li class="dropdown dropdown-extended dropdown-tasks dropdown-dark" id="header_task_bar">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                            <i class="icon-calendar"></i>
                        
                        </a>
                        
                    </li> --}}
                    <!-- END TODO DROPDOWN -->
                    {{-- <li class="droddown dropdown-separator">
                        <span class="separator"></span>
                    </li> --}}
                    <!-- BEGIN INBOX DROPDOWN -->
                    <li class="dropdown dropdown-extended dropdown-inbox dropdown-dark" id="header_inbox_bar">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                        </a>
                    </li>
                    <!-- END INBOX DROPDOWN -->
                    <!-- BEGIN USER LOGIN DROPDOWN -->

                    <li class="dropdown dropdown-user ">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                            {{-- <img alt="" class="img-circle" src="../assets/layouts/layout3/img/avatar9.jpg"> --}}
                            <span class="username username-hide-mobile">Welcome {{Auth::user()->name}} <i class="fa fa-angle-down"></i></span>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-default">
                            <li>
                                <a href="{{ url('editProfile') }}"><i class="icon-pencil"></i>Edit Profile</a>  
                            </li>
                            <li>
                                <a href="{{ url('/logout') }}"
                                    onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                      <i class="icon-key"></i>Log Out
                                </a>
                                <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>
                    </li>
                    <!-- END USER LOGIN DROPDOWN -->
                    <!-- BEGIN NOTIFICATION DROPDOWN -->
                   {{--  <li class="dropdown dropdown-extended dropdown-notification" id="header_notification_bar">
                            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                <i class="icon-bell"></i>
                                <span class="badge badge-default"> {{ $unreadNotifications->count() }} </span>
                            </a>
                            <ul class="dropdown-menu" style="overflow-y: hidden;">
                                <li>
                                    <ul class="dropdown-menu-list scroller" style="height: 250px;" data-handle-color="#637283">
                                        @foreach($unreadNotifications as $key => $unreadNotification)
                                            <li>
                                                <a href="javascript:;">
                                                    <span class="time">just now</span>
                                                    <span class="details">
                                                        <span class="label label-sm label-icon label-success">
                                                            <i class="fa fa-plus"></i>
                                                        </span><i>{{ $unreadNotification->getUserName($unreadNotification->request_book->created_by) }}</i> requested for <strong>{{ $unreadNotification->request_book->title }}</strong> book.</span>
                                                </a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </li>
                            </ul>
                        </li> --}}
                </ul>
            </div>
            <!-- END TOP NAVIGATION MENU -->
        </div>
    </div>
<!-- END HEADER TOP -->
<!-- BEGIN HEADER MENU -->
    <div class="page-header-menu">
        <div class="container">
            <!-- BEGIN HEADER SEARCH BOX -->
            <div class="hor-menu sm-menu" style="display: none">
                <ul class="nav navbar-nav">
                    <li class="menu-dropdown classic-menu-dropdown {{ Request::is('/home') ? "active" : "" }}">
                        <a href="{{url('/home')}}"> Home
                        </a>
                    </li>
                    <li class="menu-dropdown mega-menu-dropdown {{ Request::is('/home/book') ? "active" : "" }}">
                        <a href="{{url('/home/book')}}"> Materuals
                        </a>
                    </li>
                    <li class="menu-dropdown mega-menu-dropdown {{ Request::is('/home/record') ? "active" : "" }}">
                        <a href="{{url('/home/record')}}"> My Records
                        </a>
                    </li>
                    <li class="menu-dropdown mega-menu-dropdown {{ Request::is('/home/request') ? "active" : "" }}">
                        <a href="{{url('/home/request')}}"> Make Request
                        </a>
                    </li>
                    <li class="menu-dropdown mega-menu-dropdown {{ Request::is('/home/pending') ? "active" : "" }}">
                        <a href="{{url('/home/pending')}}"> Requested
                        </a>
                    </li>
                </ul>
            </div>
            <!-- END HEADER SEARCH BOX -->
            <!-- BEGIN MEGA MENU -->
            <div class="menu-section lg-menu">
                <div class="cc-rockmenu">
                  <div class="rolling">
                        <a href="{{url('/home')}}"><figure class="rolling_icon"><i class="icon-home"></i></figure>
                        <span>Home</span><span></span><span></span></a>
                  </div>
                  <div class="rolling">
                    <a href="{{url('/home/book')}}"><figure class="rolling_icon"><i class="icon-book-open"></i></figure>
                    <span >Materials</span>
                    <span></span><span></span>
                    </a>
                  </div>
                  <div class="rolling">
                    <a href="{{url('/home/record')}}">
                    <figure class="rolling_icon"><i class="icon-notebook"></i></figure>
                    <span>My Records</span><span></span><span></span></a>
                    
                  </div>
                  <div class="rolling">
                    <a href="{{url('/home/request')}}">
                    <figure class="rolling_icon"><i class="icon-basket-loaded"></i>
                    </figure><span>Make Request</span><span></span><span></span>
                    </a>
                    
                  </div>
                   <div class="rolling">
                    <a href="{{url('/home/pending')}}">
                    <figure class="rolling_icon"><i class="icon-briefcase"></i></figure>
                    <span>Requested</span><span></span><span></span></a>
                    
                  </div>
               
                </div>
            </div>


            <!-- END MEGA MENU -->
        </div>
    </div>
<!-- END HEADER MENU -->
@if(Request::path() != 'home')
    <div class="page-header-menu" style="height: 0px !important; background-color: #eff3f8;" >
        <div class="container">
             <div class="toolbar row">
                <div class="filter-options col-xs-12 col-md-9">
                   
                </div>

                <div class="col-xs-12 col-md-3">
                    <div class="form-group">
                        <div class="input-group-control form-group form-md-line-input form-md-floating-label" style="width: 218px;display: block;float: left;margin-top: -20px;margin-bottom: 20px;">

                            {!! Form::open(['url'=>'/home/searchbook','method'=> 'post'])!!}
                            <input type="text" id="search-inner" class="form-control" name="search_book" pattern=".{3,}"   required title="3 characters minimum"> 
                             <label for="search-inner">What you are Looking For ?</label>
                        </div>
                        <span class="input-group-btn btn-right">
                            <button class="btn green-haze" type="submit"><i class="glyphicon glyphicon-search"></i> </button>
                        </span>
                            {!! Form::close() !!}
                    </div>
                </div>
               
            </div>
        </div>
    </div>


@endif


 