
   <div class="form-body">
      <div class="row div-form">
         <div class="col-md-8">
            <div class="form-group form-md-line-input">
               <label class="col-md-3 control-label hori-mar-lb" for="password_old">Old Password:</label>
               <div class="col-md-7">
                  {!! Form::password('password_old',null, array('class' => 'form-control','id' => 'passwordOld','placeholder'=>'Enter old Password', 'autocomplete' => 'off')) !!}
                  <div class="form-control-focus"> 

                  </div>
               </div>
            </div>
{{--             <div class="form-group form-md-line-input">
               <label class="col-md-3 control-label hori-mar-lb" for="password">New Password:</label>
               <div class="col-md-7">
                  {!! Form::password('password', array('class' => 'form-control ','id' => 'passwordNew','placeholder'=>'Re - Enter Your New Password ')) !!}
                  <div class="form-control-focus"> </div>
               </div>
                  <a id="generate">Generate</a>
                  <label id="view_pass"></label>
            </div> --}}

           <div class="form-group form-md-line-input">
               <label class="col-md-3 control-label" for="form_control_1">Password:</label>
               <div class="col-md-7">
                   <div class="input-group">
                       <div class="input-group-control">
                           {!! Form::password('password', array('placeholder' => 'Password','class' => 'form-control','id' => 'password')) !!}
                           <div class="form-control-focus"> </div>
                       </div>
                       <span class="input-group-btn btn-right">
                           <a class="btn green-haze" id="generate">Generate</a>
                       </span>
                   </div>
               </div>
           </div>
            <div class="form-group form-md-line-input">
               <label class="col-md-3 control-label hori-mar-lb" for="password_confirm">Confirm Password:</label>
               <div class="col-md-7">
                  {!! Form::password('password_confirm', array('class' => 'form-control','id' => 'password_confirm','placeholder'=>'Re-enter Yor New Password')) !!}
                  <div class="form-control-focus"> </div>
               </div>
            </div>
            <div id="generated" style="display: none">
                <div class="form-group form-md-line-input">
                    <label class="col-md-3 control-label" for="form_control_1">Generated Password:</label>
                    <div class="col-md-7">
                      <span id="view_pass"></span>    
                    <div class="form-control-focus"> </div>
                   </div>
               </div>
            </div>
            <div class="form-group form-md-line-input">
            </div>

            <div class="form-actions">
               <div class="col-md-offset-2 col-md-10">
                  {{-- <button type="button" class="btn default">Cancel</button> --}}
                  <button type="submit"  class="btn green">Submit</button>
                  <a href="/home" type="button" class="btn default">Cancel</a>
               </div>
            </div>
         </div>
      </div>

   </div>
