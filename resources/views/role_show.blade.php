@extends('adminMaster')
@section('title','Role | Show')

@section('page-content')
	<div class="page-content-wrapper">
	<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<div class="page-bar">
	            <ul class="page-breadcrumb">
	                <li>
	                    <a href="{{ url('dashboard') }}">Home</a>
	                    <i class="fa fa-circle"></i>
	                </li>
	                <li>
	                    <a href="{{ url('role') }}">Role</a>
	                    <i class="fa fa-circle"></i>
	                </li>
	                <li>
	                    <span>Show</span>
	                </li>
	            </ul>
	        </div>
	        <div class="page-title"></div>  
		    <div class="portlet light bordered">
				<div class="portlet-title">
					<div class="caption font-green-haze">
					    <i class="icon-settings font-green-haze"></i>
					    <span class="caption-subject bold uppercase">Role</span>
					</div>
					<div class="actions">
                        <a class="btn btn-primary" href="{{ url('role') }}"><i class="fa fa-arrow-left "></i> Back</a>
                    </div>
				</div>

				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12">
						<div class="form-group">
		                	<strong>Name:</strong>
		                	{{ $role->display_name }}
		            	</div>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-12">
						<div class="form-group">
			                <strong>Description:</strong>
			                {{ $role->description }}
		           		</div>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-12">
						<div class="form-group">
							<strong>Permissions:</strong>
							@if(!empty($rolePermissions))
							@foreach($rolePermissions as $v)
							<label class="label label-success">{{ $v->display_name }}</label>
							@endforeach
							@endif
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection