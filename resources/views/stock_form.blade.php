{{-- Publication MODAL STARTS --}}
<div id="publicationModal" class="modal publicationModal fade" tabindex="-1" data-focus-on="input:first" >
    <div class="modal-header bg">
        <h4 class="modal-title font-color" style="">Publication Detail Info</h4>
    </div>
    <div class="modal-body modal-padding">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group form-md-line-input form-md-floating-label has-success" id="publicationInput">
                    <label for="form_control_1">Publication Name</label>
                    <input type="text" name="publication_name" class="form-control" id="publicationName" required >
                    <span class="help-block" id="publicationError"></span>
                </div>
            </div>
        </div>
        <div class="row footer-margin">
            <div class="modal-footer">
                <button type="button" class="btn-footer blue-outline btn-margin" id="addPublication">Add</button>
                <button type="button" class="btn-footer blue-outline" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>
{{--Publication MODAL ENDS --}}

{{-- Subject MODAL STARTS --}}
<div id="subjectModal" class="modal fade" tabindex="-1" data-focus-on="input:first" >
    <div class="modal-header bg">
        <h4 class="modal-title font-color" style="">Subject Detail Info</h4>
    </div>
    <div class="modal-body modal-padding">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group form-md-line-input form-md-floating-label has-success" id="subjectInput">
                    <input type="text" name="subject_name" class="form-control " id="subjectName" required=''>
                    <label for="form_control_1">Subject Name</label>
                    <span class="help-block" id="subjectError"></span>
                </div>
            </div>
            <div id="error"></div>
        </div>
        <div class="row footer-margin">
            <div class="modal-footer">
                <button type="button" class="btn-footer blue-outline btn-margin" id="addSubject">Add</button>
                <button type="button" class="btn-footer blue-outline" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>
{{-- Subject MODAL ENDS --}}

{{-- Genre MODAL STARTS --}}
<div id="genreModal" class="modal genreModal fade genreModal" tabindex="-1" data-focus-on="input:first" >
    <div class="modal-header bg">
        <h4 class="modal-title font-color" style="">Genre Detail Info</h4>
    </div>
    <div class="modal-body modal-padding">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group form-md-line-input form-md-floating-label has-success" id="genreInput">
                    <input type="text" name="genre_name" class="form-control" id="genreName" required>
                    <label for="form_control_1">Genre Name</label>
                    <span class="help-block" id="genreError"></span>
                </div>
            </div>
        </div>
        <div class="row footer-margin">
            <div class="modal-footer">
                <button type="button" class="btn-footer blue-outline btn-margin" id="addGenre">Add</button>
                <button type="button" class="btn-footer blue-outline" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>
{{-- Genre MODAL ENDS --}}

<div class="clearfix"></div>
<div class="portlet-body">
    @if (count($errors) > 0)
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    @if (Session::has('message'))
    <div class="alert alert-success">{{ Session::get('message') }}</div>
    @endif
    <div class="mt-element-list">
        <div class="mt-list-head list-simple ext-1 font-white bg-green-sharp">
            <div class="list-head-title-container">
                <h3 class="list-title">Basic Info</h3>
            </div>
        </div>
        <div class="mt-list-container list-simple ext-1 col-md-12" style="padding: 15px">
            <div class="form-group form-md-line-input form-md-floating-label  col-md-12  ">
                <label for="form_control_1">Title</label>
                {!! Form::text('title', old('title'), array('class' => 'form-control')) !!}
            </div>

            <div class="form-group form-md-line-input form-md-floating-label col-md-6 ">
                <label for="form_control_1">Stock Type</label>
                {{-- {!! Form::select('stock_type',$stock_type, null, ['class' => 'form-control']) !!} --}}
                <select name="stock_type" class="form-control">
                    {{-- <option value=""></option> --}}
                    <option value="" disabled selected>Select Stock Type</option>
                    @foreach($stock_type as $key => $stock_type)
                        <option value="{{ $key }}">{{ $stock_type }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-1"></div>
            <div class="form-group form-md-line-input form-md-floating-label col-md-3 ">
                <label for="form_control_1">Quantity</label>
                {!! Form::number('quantity',old('quantity'), array('class' => 'form-control')) !!}
            </div>
            <div class="clearfix"></div>
            <div class="form-group form-md-line-input form-md-floating-label col-md-6 ">
                <label for="form_control_1">Call Number</label>
                {!! Form::text('index', old('index'), array('class' => 'form-control')) !!}
            </div>
            <div class="col-md-2"></div>
            <div class="form-group form-md-line-input form-md-floating-label col-md-6 ">
                <label for="featured">Featured Book:</label>
                <input type="checkbox" name="book_featured" class="make-switch" data-on-color="info" data-off-color="danger" data-size="small" id="featured">
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="mt-element-list" style="margin-top: 20px;">
        <div class="mt-list-head list-simple ext-1 font-white bg-green-sharp">
            <div class="list-head-title-container">
                <h3 class="list-title">Image</h3>
            </div>
        </div>
        <div class="mt-list-container list-simple ext-1 col-md-12" style="padding: 15px">
            <div class="pull-left" style="margin-right: 20px;">
                <label for="input-file-now-custom-2">Front Cover:</label>
                {!! Form::file('front',['id' => "input-file-now-custom-2", 'class' => 'dropify', 'data-height'=> '200','data-allowed-file-extensions'=>'png jpeg jpg','data-max-file-size'=>'2M']) !!}
                {{-- <input type="file" id="input-file-now-custom-2" name="front" class="dropify" data-height="200" /> --}}
            </div>
            <div class="pull-left">
                <label for="input-file-now-custom-1">Back Cover:</label>
                {!! Form::file('back',['id' => "input-file-now-custom-1", 'class' => 'dropify', 'data-height'=> '200','data-allowed-file-extensions'=>'png jpeg jpg','data-max-file-size'=>'2M']) !!}
                {{-- <input type="file" id="input-file-now-custom-1" name="back"class="dropify" data-height="200" /> --}}
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="mt-element-list" style="margin-top: 20px;">
        <div class="mt-list-head list-simple ext-1 font-white bg-green-sharp">
            <div class="list-head-title-container">
                <h3 class="list-title">Additional Info</h3>
            </div>
        </div>
        <div class="mt-list-container list-simple ext-1 col-md-12" style="padding: 15px">
            <div class="form-group form-md-line-input form-md-floating-label col-md-4">
                {{-- {!! Form::text('author_name', old('author_name'), array('class' => 'form-control typeaheadAuthorName', 'id' => 'authorNameForm','placeholder'=>'Author Name')) !!} --}}
                <label for="author_name">Author Name</label>
                <select id="select2-button-addons-single-input-group-sm" name="author_name[]" class="form-control authorSelect" multiple="">
                    {{-- <option value="asdf">asdf</option> --}}
                </select>
                {{-- <input class="form-control authorSelect" multiple=""type="hidden" id="select2-button-addons-single-input-group-sm" name="author_name[]" style="width: 100%;" /> --}}
            </div>

            {{-- <a href="#authorModal" class="btn blue-outline btn-circle btn-icon-only btn-info col-md-1" data-toggle="modal">
                <i class="fa fa-plus"></i>
            </a> --}}
            <div class="col-md-1">
            </div>
            {{-- <button class="btn-md blue-outline btn-md-new col-md-1" data-toggle="modal" href="#responsive"><i class="fa fa-plus"></i> ADD</button> --}}
            <div class="form-group form-md-line-input form-md-floating-label col-md-4">
                {!! Form::text('publication_name',old('publication_name'), array('class' => 'form-control typeaheadPublication','id' => 'publicationNameForm','placeholder'=>'Publication Name')) !!}
                {{-- <label for="form_control_1">Publication</label> --}}

            </div>
            <div class="col-md-1">
               {{--  <a href="#publicationModal" class="btn  blue-outline btn-circle btn-icon-only btn-info col-md-1" data-toggle="modal">
                    <i class="fa fa-plus"></i>
                </a> --}}
                {{-- <button class="btn-md blue-outline btn-md-new" data-toggle="modal" href="#publicationModal"><i class="fa fa-plus"></i> ADD</button> --}}
            </div>
            <div class="clearfix"></div>
            <div class="form-group form-md-line-input form-md-floating-label col-md-4">
                <label for="form_control_1">Edition</label>
                {!! Form::text('edition',old('edition'), array('class' => 'form-control')) !!}
            </div>
            {{-- <div class="clearfix"></div> --}}
            <div class="col-md-1"></div>
            <div class="form-group form-md-line-input form-md-floating-label col-md-3">
                <label for="form_control_1">Published Date</label>
                {!! Form::text('publication_date',old('publication_date'), array('class' => 'form-control date-picker', 'data-date-format' => 'yyyy-mm-dd')) !!}
            </div>
            <div class="col-md-1"></div>
            <div class="form-group form-md-line-input form-md-floating-label col-md-3">
                <label for="form_control_1">Format</label>
                {!! Form::text('stock_format',old('stock_format'), array('class' => 'form-control')) !!}
            </div>
            {{-- <div class="col-md-1"></div> --}}

            <div class="form-group form-md-line-input form-md-floating-label col-md-3  ">
                {{-- 	{!! Form::text('subject_name', old('subject'), array('class' => 'form-control typeaheadSubject', 'id' => 'subjectNameForm','placeholder'=>'Subject Name')) !!} --}}
                {{-- <label for="form_control_1">Subject</label> --}}
                <label for="form_control_1">Sector</label>
                <select name="sector" class="form-control" id="sectorSelect">
                    {{-- <option value=""></option> --}}
                    <option value="" disabled selected>Select Sector</option>
                    @foreach($sectors as $key => $sector)
                    <option value="{{ $key }}">{{ $sector }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-1"></div>

            <!-- add subject button-->
            {{-- 	<div class="col-md-1">
            <a href="#subjectModal" class="btn  blue-outline btn-circle btn-icon-only btn-info col-md-1" data-toggle="modal">
            <i class="fa fa-plus"></i>
            </a>
            </div> --}}

            <div class="form-group form-md-line-input form-md-floating-label col-md-4" id="subjectSelectDiv">
                {{-- 							{!! Form::text('genre_name', old('genre'), array('class' => 'form-control typeaheadGenre', 'id' => 'genreNameForm','placeholder'=>'Genre Name')) !!} --}}
                {{-- <label for="form_control_1">Genre</label> --}}
                <label for="form_control_1">Subject</label>
                <select name="subject" class="form-control" id="subjectSelect">
                    <option value="" disabled selected>Select Subject</option>
                    {{-- <option value="" id="Type"></option> --}}
                </select>
            </div>
            <div class="col-md-1"></div>
            <div class="form-group form-md-line-input form-md-floating-label col-md-3  ">
                {{--                            {!! Form::text('genre_name', old('genre'), array('class' => 'form-control typeaheadGenre', 'id' => 'genreNameForm','placeholder'=>'Genre Name')) !!} --}}
                {{-- <label for="form_control_1">Genre</label> --}}
                <label for="form_control_1">Genre</label>
                <select name="genre" class="form-control" id="genreSelect" placeholder="Select Genre">
                    <option value="" disabled selected>Select Genre</option>
                </select>
            </div>

            <!-- add genre button-->
            {{-- 	<div class="col-md-1">
            <a href="#genreModal" class="btn  blue-outline btn-circle btn-icon-only btn-info col-md-1" data-toggle="modal">
            <i class="fa fa-plus"></i>
            </a>
            </div> --}}

            <div class="clearfix"></div>
            <div class="form-group form-md-line-input form-md-floating-label col-md-3">
                <label for="form_control_1">Dimension</label>
                {!! Form::text('dimension',old('dimension'), array('class' => 'form-control')) !!}
            </div>
            <div class="col-md-1"></div>
            <div class="form-group form-md-line-input form-md-floating-label col-md-3">
                <label for="form_control_1">Language</label>
                {!! Form::text('language',old('language'), array('class' => 'form-control')) !!}
            </div>
            <div class="col-md-1"></div>
            <div class="form-group form-md-line-input form-md-floating-label col-md-3">
                <label for="form_control_1">ISBN</label>
                {!! Form::text('isbn', old('isbn'), array('class' => 'form-control')) !!}
            </div>
            <div class="clearfix"></div>
            <div class="form-group form-md-line-input form-md-floating-label col-md-3">
                <label for="form_control_1">Dewey</label>
                {!! Form::text('dewey',old('dewey'), array('class' => 'form-control')) !!}
            </div>
            <div class="col-md-1"></div>
            <div class="form-group form-md-line-input form-md-floating-label col-md-3">
                <label for="form_control_1">Pages</label>
                {!! Form::number('pages',old('pages'), array('class' => 'form-control')) !!}
            </div>
            <div class="col-md-1"></div>
            <div class="form-group form-md-line-input form-md-floating-label col-md-3">
                {{-- <div class="form-group form-md-line-input has-info"> --}}
                <label for="form_control_1">Country</label>
                <select name="country" class="form-control">
                    <option value=""></option>
                    <option value="Afganistan">Afghanistan</option>
                    <option value="Albania">Albania</option>
                    <option value="Algeria">Algeria</option>
                    <option value="American Samoa">American Samoa</option>
                    <option value="Andorra">Andorra</option>
                    <option value="Angola">Angola</option>
                    <option value="Anguilla">Anguilla</option>
                    <option value="Antigua &amp; Barbuda">Antigua &amp; Barbuda</option>
                    <option value="Argentina">Argentina</option>
                    <option value="Armenia">Armenia</option>
                    <option value="Aruba">Aruba</option>
                    <option value="Australia">Australia</option>
                    <option value="Austria">Austria</option>
                    <option value="Azerbaijan">Azerbaijan</option>
                    <option value="Bahamas">Bahamas</option>
                    <option value="Bahrain">Bahrain</option>
                    <option value="Bangladesh">Bangladesh</option>
                    <option value="Barbados">Barbados</option>
                    <option value="Belarus">Belarus</option>
                    <option value="Belgium">Belgium</option>
                    <option value="Belize">Belize</option>
                    <option value="Benin">Benin</option>
                    <option value="Bermuda">Bermuda</option>
                    <option value="Bhutan">Bhutan</option>
                    <option value="Bolivia">Bolivia</option>
                    <option value="Bonaire">Bonaire</option>
                    <option value="Bosnia &amp; Herzegovina">Bosnia &amp; Herzegovina</option>
                    <option value="Botswana">Botswana</option>
                    <option value="Brazil">Brazil</option>
                    <option value="British Indian Ocean Ter">British Indian Ocean Ter</option>
                    <option value="Brunei">Brunei</option>
                    <option value="Bulgaria">Bulgaria</option>
                    <option value="Burkina Faso">Burkina Faso</option>
                    <option value="Burundi">Burundi</option>
                    <option value="Cambodia">Cambodia</option>
                    <option value="Cameroon">Cameroon</option>
                    <option value="Canada">Canada</option>
                    <option value="Canary Islands">Canary Islands</option>
                    <option value="Cape Verde">Cape Verde</option>
                    <option value="Cayman Islands">Cayman Islands</option>
                    <option value="Central African Republic">Central African Republic</option>
                    <option value="Chad">Chad</option>
                    <option value="Channel Islands">Channel Islands</option>
                    <option value="Chile">Chile</option>
                    <option value="China">China</option>
                    <option value="Christmas Island">Christmas Island</option>
                    <option value="Cocos Island">Cocos Island</option>
                    <option value="Colombia">Colombia</option>
                    <option value="Comoros">Comoros</option>
                    <option value="Congo">Congo</option>
                    <option value="Cook Islands">Cook Islands</option>
                    <option value="Costa Rica">Costa Rica</option>
                    <option value="Cote DIvoire">Cote D'Ivoire</option>
                    <option value="Croatia">Croatia</option>
                    <option value="Cuba">Cuba</option>
                    <option value="Curaco">Curacao</option>
                    <option value="Cyprus">Cyprus</option>
                    <option value="Czech Republic">Czech Republic</option>
                    <option value="Denmark">Denmark</option>
                    <option value="Djibouti">Djibouti</option>
                    <option value="Dominica">Dominica</option>
                    <option value="Dominican Republic">Dominican Republic</option>
                    <option value="East Timor">East Timor</option>
                    <option value="Ecuador">Ecuador</option>
                    <option value="Egypt">Egypt</option>
                    <option value="El Salvador">El Salvador</option>
                    <option value="Equatorial Guinea">Equatorial Guinea</option>
                    <option value="Eritrea">Eritrea</option>
                    <option value="Estonia">Estonia</option>
                    <option value="Ethiopia">Ethiopia</option>
                    <option value="Falkland Islands">Falkland Islands</option>
                    <option value="Faroe Islands">Faroe Islands</option>
                    <option value="Fiji">Fiji</option>
                    <option value="Finland">Finland</option>
                    <option value="France">France</option>
                    <option value="French Guiana">French Guiana</option>
                    <option value="French Polynesia">French Polynesia</option>
                    <option value="French Southern Ter">French Southern Ter</option>
                    <option value="Gabon">Gabon</option>
                    <option value="Gambia">Gambia</option>
                    <option value="Georgia">Georgia</option>
                    <option value="Germany">Germany</option>
                    <option value="Ghana">Ghana</option>
                    <option value="Gibraltar">Gibraltar</option>
                    <option value="Great Britain">Great Britain</option>
                    <option value="Greece">Greece</option>
                    <option value="Greenland">Greenland</option>
                    <option value="Grenada">Grenada</option>
                    <option value="Guadeloupe">Guadeloupe</option>
                    <option value="Guam">Guam</option>
                    <option value="Guatemala">Guatemala</option>
                    <option value="Guinea">Guinea</option>
                    <option value="Guyana">Guyana</option>
                    <option value="Haiti">Haiti</option>
                    <option value="Hawaii">Hawaii</option>
                    <option value="Honduras">Honduras</option>
                    <option value="Hong Kong">Hong Kong</option>
                    <option value="Hungary">Hungary</option>
                    <option value="Iceland">Iceland</option>
                    <option value="India">India</option>
                    <option value="Indonesia">Indonesia</option>
                    <option value="Iran">Iran</option>
                    <option value="Iraq">Iraq</option>
                    <option value="Ireland">Ireland</option>
                    <option value="Isle of Man">Isle of Man</option>
                    <option value="Israel">Israel</option>
                    <option value="Italy">Italy</option>
                    <option value="Jamaica">Jamaica</option>
                    <option value="Japan">Japan</option>
                    <option value="Jordan">Jordan</option>
                    <option value="Kazakhstan">Kazakhstan</option>
                    <option value="Kenya">Kenya</option>
                    <option value="Kiribati">Kiribati</option>
                    <option value="Korea North">Korea North</option>
                    <option value="Korea Sout">Korea South</option>
                    <option value="Kuwait">Kuwait</option>
                    <option value="Kyrgyzstan">Kyrgyzstan</option>
                    <option value="Laos">Laos</option>
                    <option value="Latvia">Latvia</option>
                    <option value="Lebanon">Lebanon</option>
                    <option value="Lesotho">Lesotho</option>
                    <option value="Liberia">Liberia</option>
                    <option value="Libya">Libya</option>
                    <option value="Liechtenstein">Liechtenstein</option>
                    <option value="Lithuania">Lithuania</option>
                    <option value="Luxembourg">Luxembourg</option>
                    <option value="Macau">Macau</option>
                    <option value="Macedonia">Macedonia</option>
                    <option value="Madagascar">Madagascar</option>
                    <option value="Malaysia">Malaysia</option>
                    <option value="Malawi">Malawi</option>
                    <option value="Maldives">Maldives</option>
                    <option value="Mali">Mali</option>
                    <option value="Malta">Malta</option>
                    <option value="Marshall Islands">Marshall Islands</option>
                    <option value="Martinique">Martinique</option>
                    <option value="Mauritania">Mauritania</option>
                    <option value="Mauritius">Mauritius</option>
                    <option value="Mayotte">Mayotte</option>
                    <option value="Mexico">Mexico</option>
                    <option value="Midway Islands">Midway Islands</option>
                    <option value="Moldova">Moldova</option>
                    <option value="Monaco">Monaco</option>
                    <option value="Mongolia">Mongolia</option>
                    <option value="Montserrat">Montserrat</option>
                    <option value="Morocco">Morocco</option>
                    <option value="Mozambique">Mozambique</option>
                    <option value="Myanmar">Myanmar</option>
                    <option value="Nambia">Nambia</option>
                    <option value="Nauru">Nauru</option>
                    <option value="Nepal">Nepal</option>
                    <option value="Netherland Antilles">Netherland Antilles</option>
                    <option value="Netherlands">Netherlands (Holland, Europe)</option>
                    <option value="Nevis">Nevis</option>
                    <option value="New Caledonia">New Caledonia</option>
                    <option value="New Zealand">New Zealand</option>
                    <option value="Nicaragua">Nicaragua</option>
                    <option value="Niger">Niger</option>
                    <option value="Nigeria">Nigeria</option>
                    <option value="Niue">Niue</option>
                    <option value="Norfolk Island">Norfolk Island</option>
                    <option value="Norway">Norway</option>
                    <option value="Oman">Oman</option>
                    <option value="Pakistan">Pakistan</option>
                    <option value="Palau Island">Palau Island</option>
                    <option value="Palestine">Palestine</option>
                    <option value="Panama">Panama</option>
                    <option value="Papua New Guinea">Papua New Guinea</option>
                    <option value="Paraguay">Paraguay</option>
                    <option value="Peru">Peru</option>
                    <option value="Phillipines">Philippines</option>
                    <option value="Pitcairn Island">Pitcairn Island</option>
                    <option value="Poland">Poland</option>
                    <option value="Portugal">Portugal</option>
                    <option value="Puerto Rico">Puerto Rico</option>
                    <option value="Qatar">Qatar</option>
                    <option value="Republic of Montenegro">Republic of Montenegro</option>
                    <option value="Republic of Serbia">Republic of Serbia</option>
                    <option value="Reunion">Reunion</option>
                    <option value="Romania">Romania</option>
                    <option value="Russia">Russia</option>
                    <option value="Rwanda">Rwanda</option>
                    <option value="St Barthelemy">St Barthelemy</option>
                    <option value="St Eustatius">St Eustatius</option>
                    <option value="St Helena">St Helena</option>
                    <option value="St Kitts-Nevis">St Kitts-Nevis</option>
                    <option value="St Lucia">St Lucia</option>
                    <option value="St Maarten">St Maarten</option>
                    <option value="St Pierre &amp; Miquelon">St Pierre &amp; Miquelon</option>
                    <option value="St Vincent &amp; Grenadines">St Vincent &amp; Grenadines</option>
                    <option value="Saipan">Saipan</option>
                    <option value="Samoa">Samoa</option>
                    <option value="Samoa American">Samoa American</option>
                    <option value="San Marino">San Marino</option>
                    <option value="Sao Tome &amp; Principe">Sao Tome &amp; Principe</option>
                    <option value="Saudi Arabia">Saudi Arabia</option>
                    <option value="Senegal">Senegal</option>
                    <option value="Serbia">Serbia</option>
                    <option value="Seychelles">Seychelles</option>
                    <option value="Sierra Leone">Sierra Leone</option>
                    <option value="Singapore">Singapore</option>
                    <option value="Slovakia">Slovakia</option>
                    <option value="Slovenia">Slovenia</option>
                    <option value="Solomon Islands">Solomon Islands</option>
                    <option value="Somalia">Somalia</option>
                    <option value="South Africa">South Africa</option>
                    <option value="Spain">Spain</option>
                    <option value="Sri Lanka">Sri Lanka</option>
                    <option value="Sudan">Sudan</option>
                    <option value="Suriname">Suriname</option>
                    <option value="Swaziland">Swaziland</option>
                    <option value="Sweden">Sweden</option>
                    <option value="Switzerland">Switzerland</option>
                    <option value="Syria">Syria</option>
                    <option value="Tahiti">Tahiti</option>
                    <option value="Taiwan">Taiwan</option>
                    <option value="Tajikistan">Tajikistan</option>
                    <option value="Tanzania">Tanzania</option>
                    <option value="Thailand">Thailand</option>
                    <option value="Togo">Togo</option>
                    <option value="Tokelau">Tokelau</option>
                    <option value="Tonga">Tonga</option>
                    <option value="Trinidad &amp; Tobago">Trinidad &amp; Tobago</option>
                    <option value="Tunisia">Tunisia</option>
                    <option value="Turkey">Turkey</option>
                    <option value="Turkmenistan">Turkmenistan</option>
                    <option value="Turks &amp; Caicos Is">Turks &amp; Caicos Is</option>
                    <option value="Tuvalu">Tuvalu</option>
                    <option value="Uganda">Uganda</option>
                    <option value="Ukraine">Ukraine</option>
                    <option value="United Arab Erimates">United Arab Emirates</option>
                    <option value="United Kingdom">United Kingdom</option>
                    <option value="United States of America">United States of America</option>
                    <option value="Uraguay">Uruguay</option>
                    <option value="Uzbekistan">Uzbekistan</option>
                    <option value="Vanuatu">Vanuatu</option>
                    <option value="Vatican City State">Vatican City State</option>
                    <option value="Venezuela">Venezuela</option>
                    <option value="Vietnam">Vietnam</option>
                    <option value="Virgin Islands (Brit)">Virgin Islands (Brit)</option>
                    <option value="Virgin Islands (USA)">Virgin Islands (USA)</option>
                    <option value="Wake Island">Wake Island</option>
                    <option value="Wallis &amp; Futana Is">Wallis &amp; Futana Is</option>
                    <option value="Yemen">Yemen</option>
                    <option value="Zaire">Zaire</option>
                    <option value="Zambia">Zambia</option>
                    <option value="Zimbabwe">Zimbabwe</option>
                </select>

                {{-- </div> --}}
            </div>
            <div class="clearfix"></div>
            <div class="form-group form-md-line-input form-md-floating-label col-md-12">
                <label for="form_control_1">Summary </label>
                {!! Form::textarea('plot_summary',old('plot_summary'), array('class' => 'form-control', 'rows'=>'3')) !!}
            </div>
        </div>
    </div>
</div>
<div class="form-actions">
    <div class="row">
       <div class="col-md-11 col-md-offset-1">
            <button class="btn btn-md blue" type="submit">Save</button>
            <a href="{{ url('/stock') }}" class="btn btn-md default">Cancel</a>
       </div>
    </div>
</div>
