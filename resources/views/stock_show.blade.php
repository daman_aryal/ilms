@extends('adminMaster')
@section('title','Stock | Show')

@section('headerscript')
<style>
	.floatfab{
		position:fixed;
		width:60px;
		height:60px;
		bottom:40px;
		right:40px;
		background-color:#3fd5c0;
		color:#FFF;
		border-radius:50px !important;
		text-align:center;
		z-index: 9999;
	}
	.floatfab:hover{

		transition: all 1s;
		transform: translateZ(10px);

    transform: rotateZ(-180deg);
		box-shadow: 0 14px 28px rgba(0,0,0,0.25), 0 10px 10px rgba(0,0,0,0.22);
		color:#fff;
	}

	.my-floatfab{
		font-size: 30px;
		margin-top: 25px;
		margin-left: 2px;
	}
</style>
@endsection

@section('page-content')
	{{-- MODAL STARTS --}}
		<div id="quantityModal" class="modal quantityModal fade" tabindex="-1" data-focus-on="input:first" >
					<div class="modal-header bg">
							<h4 class="modal-title font-color" style="">Add Quantity</h4>
					</div>
					<div class="modal-body modal-padding">
							<div class="row">
									<div class="col-md-6">
											 <div class="form-group form-md-line-input form-md-floating-label has-success">
												 <form class="" action="{{ route('stockdetail.store' )}}" method="post">
													 {!! csrf_field()!!}
													 <input type="number" min="0" name="quantity" class="form-control" id="quantity" requuired>
													 <label for="form_control_1">Quantity</label>

													 <input type="hidden" value={{ $stockid }} name="stock_id">
											</div>
									</div>
							</div>
							<div class="row footer-margin">
									<div class="modal-footer">
											<button type="submit" class="btn-footer blue-outline btn-margin" id="addQuantity">Add</button>
											<button class="btn-footer blue-outline" data-dismiss="modal">Cancel</button>
										</form>
									</div>
							</div>
					</div>
			</div>
	{{-- MODAL ENDS --}}

	<div class="page-content-wrapper">
	<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <a href="{{ url('dashboard') }}">Home</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <a href="{{ url('stock') }}">Stock</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <span>Show</span>
                    </li>
                </ul>
            </div>
            <div class="page-title"></div>
			@if ($message = Session::get('success'))
				<div class="alert alert-success">
					<p>{{ $message }}</p>
				</div>
			@endif

			<div class="portlet light bordered">
				<div class="portlet-title">
					<div class="caption font-green-haze">
						<i class="icon-social-dropbox font-green-haze"></i>
						<span class="caption-subject bold uppercase">Stock Overview</span>
					</div>

					<div class="actions">
                        <a class="btn btn-primary" href="/stock"><i class="fa fa-arrow-left "></i> Back</a>
                    </div>

				 {{-- <div class="tools"> </div> --}}
				</div>

				<div class="mt-element-card mt-element-overlay">
                    <div class="row">
                        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                            <div class="mt-card-item">
                                <div class="mt-card-avatar mt-overlay-1">
                                @if(!$stock_info->images->isEmpty())
                                    <img src="/{{ $stock_info->images[0]->path }}" />
                                @else
                                	<img src="/img/no-cover.jpg" />
                                @endif
                                    {{-- <div class="mt-overlay" >
                                        <div class="mt-info">
                                            <p style="margin:10px;" align="left">{{ $stock[0]->plot_summary }}</p>
                                        </div>
                                    </div> --}}
                                </div>
                                <div class="mt-card-content">
                                    {{-- <h3 class="mt-card-name">{{ $stock[0]->title }}</h3>
                                    <p class="mt-card-desc font-grey-mint">By:
	                                    @foreach ($stock_info->rel_author as $key => $authorName)
											@if ($key == 0 )
													{{ $authorName->author_name }}
											@else
													{{ ", " }}{{ $authorName->author_name }}
											@endif
										@endforeach 

                                    </p> --}}
                                </div>
                            </div>
                        </div>
                        <div class="m-heading-1 border-green-turquoise m-bordered col-lg-9 col-md-8 col-sm-6 col-xs-12" style="height: 250px; overflow-y: scroll;">
                        	<h4><strong>Title: </strong>{{ $stock[0]->title }}</h4>
                        	<h4><strong>Author: </strong>
                        		@foreach ($stock_info->rel_author as $key => $authorName)
									@if ($key == 0 )
											{{ $authorName->author_name }}
									@else
											{{ ", " }}{{ $authorName->author_name }}
									@endif
								@endforeach 
                        	</h4>
                        	<h4><strong>Book Summary:  </strong>{{ $stock[0]->plot_summary  }}</h4>
                        	
                        </div>
                    </div>
                </div>

				 <table class="table table-striped table-bordered table-hover" id="sample_1">
					<thead>
						<tr>
						{{-- <div class="row"> --}}
						<th>S.N</th>
{{-- 						<th>Title</th> --}}
						<th>Accession Number</th>
						<th>Active Status</th>
						<th>Book Status</th>
						<th>Action</th>
						{{-- </div> --}}
					</tr>

					</thead>
					<tbody>
							@foreach ($stock as $key => $stocks)
								@if ($stocks->active == 0)
									<tr style="background-color: #ffebee ">
								@else
									<tr>
							  @endif

									<td>{{ $key+1 }}</td>
							{{-- 		<td >{{ $stocks->title }}</td> --}}
									<td>{{ $stocks->code}}</td>
									@if ($stocks->active == 1)
										<td>
												<input type="checkbox" class="make-switch active" name="active" id="{{ $stocks->id }}" data-size="small" data-on-color="info" data-off-color="danger" checked> </div>
										</td>
									@else
										<td>
												<input type="checkbox" class="make-switch active" name="active" id="{{ $stocks->id }}" data-size="small" data-on-color="info" data-off-color="danger" > </div>
										</td>

									@endif
									
									@if($stocks->available_status == 1)
										<td>
											<label class="label label-success"><i class="fa fa-arrow-down" aria-hidden="true"></i>  IN</label>
										</td>
									@else
										<td>
											<label class="label label-danger"><i class="fa fa-arrow-up" aria-hidden="true"></i>  OUT</label>
										</td>
									@endif
									
									<td >
										{{-- <a class="btn btn-xs btn-info btn-circle fa fa-eye" href="{{ url('stock/show/'.$stocks->id) }}"></a>
										<a class="btn btn-primary btn-xs btn-circle fa fa-edit" href="{{ url('stock/edit/'.$stocks->id) }}"></a> --}}
										{{-- {!! Form::open(['url' => '/stockdetail/delete/'.$stocks->id,'style'=>'display:inline']) !!}
										<button class="fa fa-trash btn-sm btn btn-circle red" type="submit" > Delete</button>
										{!! Form::close() !!} --}}

										<a href={{"/stock/barcode/".$stocks->id}} class="btn btn-sm  blue"><i class="fa fa-print"></i> Barcode</a>

										{!! Form::open(['url' => '/stockdetail/delete/'.$stocks->id,'style'=>'display:inline']) !!}
							            	{!! Form::button('<i class="fa fa-trash-o"></i> Delete', ['type' => 'submit','class' => 'btn btn-danger btn-sm']) !!}
							        	{!! Form::close() !!}
									</td>
								</tr>
							@endforeach
					</tbody>
				</table>
			</div>
		</div>



		<a type="button" href="#quantityModal" data-toggle="modal" class="floatfab" title="Create New Stock">
			<i class="fa fa-plus my-floatfab"></i>
		</a>
	</div>

@endsection

@section('page-script')
<script>
	$('.active').on('switchChange.bootstrapSwitch', function (e,state) {
		console.log(state);
		e.preventDefault();
		var id = $(this).attr('id');
		$.ajax({
				url: '../../api/stockdetail/updatestatus',
				type: "POST",
				timeout: 1000,
				headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
				data: {status: state, id:id},
				success: function( msg ) {
						console.log(msg);
				}
		});

	});
</script>
@endsection
