@extends('userMaster')
@section('title','Book List')
@section('page-content')
    <div class="page-content-wrapper">
      <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <div id="main-container" class="container">
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <a href="{{url('/home')}}">Home</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <span>Books</span>
                    </li>
                </ul>
                <div class="page-container"> 
                <p>You Searched For: <strong>{{ $query }} </strong></p>
                @if(count($results)>0)
                  @foreach($results as $book)  
                     <div class="media">
                        <div class="media-left media-middle">
                          <a href="{{url("/book/detail")}}/CMC-{{$book->stock_id}}">
                            <img class="media-object" src="/{{ $stock->getBookImage($book->stock_id)}}" alt="" height=100 width=75>
                          </a>
                        </div>
                        <div class="media-body">
                          <h4 class="media-heading"><a href="{{url("/book/detail")}}/CMC-{{$book->stock_id}}">{{$book->title}}</a></h4>
                          <p class="author">by
                                                {{$book->author_name}}

                                                 &bull; {{date('Y', strtotime($book->publication_date))}}</p>

                                              <p>{{$book->plot_summary}}</p>
                        </div><hr>
                      </div>
                  @endforeach
                  @else
                    <p>No Books Found.</p>
                @endif
                </div>
                <!-- /main -->

                <div class="main-overlay">
                    <div class="overlay-full"></div>
                </div>

            </div>

        </div>
    </div>
@endsection
@section('footer')
<script type="text/javascript">
        $('#d_from').datepicker();

        $('#d_to').datepicker();

</script>
@endsection
