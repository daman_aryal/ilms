@extends('adminMaster')
@section('title','Role | Create')

@section('page-content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="{{ url('dashboard') }}">Home</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="{{ url('role') }}">Role</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Create</span>
                </li>
            </ul>
        </div>
        <div class="page-title"></div>  
        <div class="portlet light bordered">
            <div class="page-title"></div>
            <div class="portlet-title">
                <div class="caption font-green-haze">
                    <i class="icon-settings font-green-haze"></i>
                    <span class="caption-subject bold uppercase">Create Role</span>
                </div>
                <div class="actions">
                    <a class="btn btn-primary" href="{{ url('role') }}"><i class="fa fa-arrow-left "></i> Back</a>
                </div>
            </div>
            <div class="portlet-body form">
                @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                {!! Form::open(array('url' => 'role/store','method'=>'POST')) !!}
                @include('role_form')
                {!! Form::close() !!}
            </div>
        </div>  
    </div>
</div>  
@endsection
