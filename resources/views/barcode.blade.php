@extends('adminMaster')
@section('title','Barcode')
@section('page-content')
<div class="page-content-wrapper">
  <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
      <div class="portlet light bordered">
          <div class="portlet-title">
              <div class="caption font-green-haze">
                  <i class="icon-settings font-green-haze"></i>
                  <span class="caption-subject bold uppercase">Book Code Print Section</span>
              </div>
              <div class="actions">
                        <a class="btn btn-primary" href="{{ url('/stock/show') }}/{{$detail->stock_id}}"> Back</a>
                    </div>
          </div>
          <div class="portlet-body form">

          	<div class="row">
          		<div class="col-md-2"></div>
          		<div class="col-md-5" id="qrcode">
						{!! DNS2D::getBarcodeSVG($detail->code, "QRCODE",3.5,3.5) !!}
          		</div>
          		<div class="col-md-5">
          			<button id="printQrcode" class="btn btn-primary fa fa-print" onclick='printDiv("qrcode");'>Print</button>
          		</div>
          	</div>
          	<hr>
          	<div class="row">
          		<div class="col-md-2"></div>
          		<div class="col-md-5" id="barcode">
          			<div class="row">
						{!! DNS1D::getBarcodeSVG("$detail->code","C39",1,60) !!}
          			</div>
						<span style="margin-left: 25px;">{{$detail->code}}</span>
          		</div>
          		<div class="col-md-5">
          			<button id="printBarCode" class="btn btn-primary fa fa-print" onclick='printDiv("barcode");'>Print</button>
          		</div>
          	</div>
          </div>
      </div>  
    </div>
  </div>  
@endsection

@section('footerscript')
	<script type="text/javascript">
		
		function printDiv(divName) 
		{

		  var divToPrint=document.getElementById(divName);

		  var newWin=window.open('','Print-Window');

		  newWin.document.open();
		  console.log(divToPrint);

		  newWin.document.write('<html><body onload="window.print()">'+divToPrint.innerHTML+'</body></html>');

		  newWin.document.close();

		  setTimeout(function(){newWin.close();},10);

		}
	</script>
@endsection
