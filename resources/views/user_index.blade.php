@extends('adminMaster')
@section('title','Application User Management')
@section('headerscript')
<style>
	.float{
		position:fixed;
		width:60px;
		height:60px;
		bottom:40px;
		right:40px;
		background-color:#3fd5c0;
		color:#FFF;
		border-radius:50px !important;
		text-align:center;
		z-index: 9999;
	}
	.float:hover{

		transition: all 1s;
		transform: translateZ(10px);

    transform: rotateY(-180deg);
		box-shadow: 0 14px 28px rgba(0,0,0,0.25), 0 10px 10px rgba(0,0,0,0.22);
		color:#fff;
	}

	.my-float{
		font-size: 30px;
		margin-top: 25px;
		margin-left: 2px;
	}
</style>
@endsection

@section('page-content')
	<div class="page-content-wrapper">
	<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <a href="{{ url('dashboard') }}">Home</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <span>Application User Management</span>
                    </li>
                </ul>
            </div>
            <div class="page-title"></div>
			<div class="portlet light bordered">
				<div class="portlet-title">
					<div class="caption font-green-haze">
						<i class="icon-social-dropbox font-green-haze"></i>
						<span class="caption-subject bold uppercase">Application User Management</span>
					</div>

				 	<div class="tools"> </div>
				</div>
				@if ($message = Session::get('success'))
					<div class="alert alert-success">
						<p>{{ $message }}</p>
					</div>
				@endif

				 <table class="table table-striped table-bordered table-hover" id="sample_1">
					<thead>
						<tr>
						<th>No</th>
						<th>Name</th>
						<th>Email</th>
						<th>Roles</th>
						<th width="280px">Action</th>
					</tr>

					</thead>
					<tbody>	
						@foreach ($data as $key => $user)
				<tr>
					<td>{{ ++$i }}</td>
					<td>{{ $user->name }}</td>
					<td>{{ $user->email }}</td>
					<td>
						@if(!empty($user->roles))
							@foreach($user->roles as $v)
								<label class="label label-success pull-right">{{ $v->name }}</label>
							@endforeach
						@endif
					</td>
					<td>
						@if (Auth::user()->can(['user-edit']))
							<a class="btn btn-primary btn-sm" href="{{ url('adminUser/edit/'.$user->id) }}"><i class="fa fa-edit"></i> Edit</a>
						
						@endif
						@if (Auth::user()->can(['user-delete']))
							{!! Form::open(['url' => 'adminUser/delete/'.$user->id,'style'=>'display:inline']) !!}
			            	{!! Form::button('<i class="fa fa-trash-o"></i> Delete', ['type' => 'submit','class' => 'btn btn-danger btn-sm']) !!}
			        	{!! Form::close() !!}
						@endif	
					</td>
				</tr>
				@endforeach
					</tbody>
				</table>

				{!! $data->render() !!}
				<!-- END SAMPLE FORM PORTLET-->
			</div>
		</div>
		<a href="{{ url('adminUser/create') }}" class="float" title="Create New User">
			<i class="fa fa-plus my-float"></i>
		</a>
	</div>
@endsection
