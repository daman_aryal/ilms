
<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();
// Route::get('/home', 'HomeController@index');

//dashboard for admin
// Route::get('dashboard/',function(){return view('admin_index');})->middleware('first_login');

//update password
Route::get('password/edit', 'UserController@password_edit');
Route::post('password/update', 'UserController@password_update');
Route::post('/generatePass','HomeController@password');


	Route::get('home/autocomplete', 'StockDetailController@auto');


Route::group(['middleware' => ['auth']], function() {
	Route::group(['prefix'=>'transaction'], function()
			{
				Route::get('/', 'TransactionController@index');
				Route::get('/request/{stock_id}', 'TransactionController@request_getById');
				Route::post('/request/store', 'TransactionController@request_store');
				Route::post('/request/reserve', 'TransactionController@reserve_store');
				Route::get('/dispatch/{transaction_id}/{stock_id}/{stock_detail_id}', 'TransactionController@dispatch_stock');
				Route::get('/returned/{transaction_id}/{stock_id}/{stock_detail_id}', 'TransactionController@returned');
			});


	Route::get('editProfile', 'StudentController@editProfile');
	Route::post('updateProfile', 'StudentController@updateProfile');

	// Route::get('/','abc@abc');
	Route::get('setting','HomeController@setting');
	Route::get('/','HomeController@index');
	
	Route::resource('users','UserController');
	
	
	Route::get('create',['uses'=>'HomeController@create']);

		Route::group(['prefix'=>'dispatch'], function()
		{
			Route::get('/', 'DispatchController@index');
			Route::post('/store', ['as' => 'dispatch.store', 'uses' => 'DispatchController@store']);
		});
		Route::group(['prefix'=>'fine'], function()
		{
			Route::get('/', 'FineController@index');
			Route::get('/userDetail','FineController@userDetail');
			Route::post('/payment', ['as' => 'fine.payment', 'uses' => 'FineController@payment']);
			Route::get('/print','FineController@printReceipt');

		});

		Route::group(['prefix'=>'adminUser', 'middleware' => 'check_membertype' ], function()
		{
			Route::get('/', 'UserController@index');
			Route::get('/create', 'UserController@create');
			Route::get('/edit/{id}', 'UserController@edit');
			Route::post('/store', 'UserController@store');
			Route::post('/update/{id}', 'UserController@update');
			Route::post('/delete/{id}', 'UserController@destroy');
		});

		Route::group(['prefix'=>'role','middleware'=>'user_type:su'], function()
		{
			Route::get('/', 'RoleController@index');
			Route::get('/create', 'RoleController@create');
			Route::get('show/{id}', 'RoleController@show');
			Route::get('/edit/{id}', 'RoleController@edit');
			Route::post('/store', 'RoleController@store');
			Route::post('/update/{id}', 'RoleController@update');
			Route::post('/delete/{id}', 'RoleController@destroy');
		});


	Route::group(['prefix'=>'libarian', 'middleware'=>'user_type:libarian,su'], function()
	{
		Route::get('/', 'LibarianController@index');
		Route::get('/create', 'LibarianController@create');
		Route::get('show/{id}', 'LibarianController@show');
		Route::get('/edit/{id}', 'LibarianController@edit');
		Route::post('/store', 'LibarianController@store');
		Route::post('/update/{id}', 'LibarianController@update');
		Route::post('/delete/{id}', 'LibarianController@destroy');
	});

	Route::group(['prefix'=>'home'], function()
	{
		Route::get('/', 'StudentController@dashboard');
		Route::get('/transaction/status', 'StudentController@transaction_status');
		Route::get('/book','StudentController@book');
		Route::get('/record','StudentController@record');
		Route::get('/request','RequestController@index');
		Route::post('/request','RequestController@store');
		Route::get('/request/approve/{slug}','RequestController@edit');
		Route::get('/pending','StudentController@pending');
		Route::post('/searchbook','StudentController@searchBook');

	});
		Route::get('/book/detail/{name}','StudentController@bookdetail');

	

	

	

  Route::group(['prefix'=>'api'],function(){

      Route::post('/author/store',['as' => 'author.store','uses' => 'Api\AuthorController@store']);
      Route::get('/author/getauthor',['as' => 'author.get', 'uses' => 'Api\AuthorController@getAuthor']);

      Route::post('/publication/store',['as' => 'publication.store','uses' => 'Api\PublicationController@store']);
      Route::get('/publication/getpublication',['as' => 'publication.get', 'uses' => 'Api\PublicationController@getPublication']);

      Route::get('/subject/getsubject',['as' => 'subject.get', 'uses' => 'Api\SubjectController@getSubject']);
      Route::post('/subject/store',['as' => 'subject.store', 'uses' => 'Api\SubjectController@storeSubject']);

      Route::get('/genre/getgenre',['as' => 'genre.get', 'uses' => 'Api\GenreController@get']);
      Route::post('/genre/store',['as' => 'genre.store', 'uses' => 'Api\GenreController@store']);

      Route::post('/stockdetail/updatestatus', 'Api\StockDetailController@updateStatus');
      Route::post('/dispatch/{transaction_id}/{stock_id}/{stock_detail_id}', 'Api\TransactionController@dispatch_stock');
      Route::get('/return/{transaction_id}/{stock_id}/{stock_detail_id}/{user_id}', 'Api\TransactionController@return_stock');
      Route::get('/user/get_users',['as' => 'user.get', 'uses' => 'Api\UserController@getUser']);

      Route::get('/year/get_batch',['as' => 'year.get', 'uses' => 'Api\YearController@getBatch']);

      Route::get('/member_type/get_code',['as' => 'member.get', 'uses' => 'Api\MemberTypeController@getCode']);
      Route::get('/member_type_staff/get_code',['as' => 'member.get', 'uses' => 'Api\MemberTypeControllerStaff@getCode']);
      Route::get('/program/getClass',['as' => 'program.get', 'uses' => 'Api\ClassController@getClass']);
      Route::get('/program/getSection',['as' => 'section.get', 'uses' => 'Api\SectionController@getSection']);

  });

 Route::group(['middleware'=>'check_membertype'],function(){
 	
 			Route::get('fine/receipt','FineController@fineRecipt');
 			Route::get('/dashboard', 'HomeController@index');
			Route::get('/libUser', 'LibUserController@index');
			Route::get('/libUser/create', 'LibUserController@create');
			Route::get('/fine_index','ReportController@fine_index');
			Route::post('/fine_report','ReportController@fineReport')->name('fineReport');
			Route::get('/fine_print','ReportController@finePrint');
			Route::post('/transaction_report','ReportController@transactionReport');
			Route::get('/transaction_index','ReportController@transaction_index');
			Route::get('/book_report','FineController@bookReport');
		 	Route::group(['prefix'=>'basicUser'], function()
				{
					Route::get('/', 'BasicUserController@index');
					Route::get('/create', 'BasicUserController@create');
					Route::get('/edit/{id}', 'BasicUserController@edit');
					Route::post('/store', 'BasicUserController@store');
					Route::post('/update/{id}', 'BasicUserController@update');
					Route::post('/delete/{id}', 'BasicUserController@destroy');
				});
				Route::group(['prefix'=>'staffUser'], function()
				{
					Route::get('/', 'StaffUserController@index');
					Route::get('/create', 'StaffUserController@create');
					Route::get('/edit/{id}', 'StaffUserController@edit');
					Route::post('/store', 'StaffUserController@store');
					Route::post('/update/{id}', 'StaffUserController@update');
					Route::post('/delete/{id}', 'StaffUserController@destroy');
				});

			 	Route::group(['prefix'=>'stock'], function()
				{
					Route::get('/', 'StockController@index');
					Route::get('/create', 'StockController@create');
					Route::get('show/{id}', 'StockController@show');
					Route::get('/edit/{id}', 'StockController@edit');
					Route::post('/store', 'StockController@store');
					Route::post('/update/{id}', 'StockController@update');
					Route::post('/delete/{id}', 'StockController@destroy');
					Route::get('/barcode/{code}', 'StockController@getBarcode');
				});

			  Route::group(['prefix' => 'stockdetail'], function(){
			    // Route::post('/delete/{id}','StockDetailController@destroy');
			    Route::post('/delete/{id}',['as' => 'stockdetail.destroy', 'uses' => 'StockDetailController@destroy']);
			    Route::post('/store',['as' => 'stockdetail.store', 'uses' => 'StockDetailController@store']);
			  });

 			

		 	Route::group(['prefix'=>'reservation'], function()
			{
				Route::get('/','TransactionController@reserve_index');
			});
			
			Route::group(['prefix'=>'fine'], function()
			{
				Route::get('/','FineController@index');
			});
 			Route::resource('year','YearController');
			Route::resource('membertype','MemberTypeController');
			Route::resource('program','ProgramController');
			Route::resource('class','ClassController');
			Route::resource('section','SectionController');
			Route::resource('department','DepartmentController');
			Route::resource('sector','SectorController');
			Route::resource('subject','SubjectController');
			Route::resource('genre','GenreController');
			Route::resource('policy','PolicyController');
			Route::resource('author','AuthorController');
			Route::resource('publication','PublicationController');
			Route::get('myProfile/edit/{id}','UserController@myProfileEdit');

 });
			


});
