$(document).ready(function() {

    $('#advsearch').click(function(){
             $('#advoption').slideToggle();
    });
  $("#owl-example").owlCarousel({
        	items:4,
        	
        	autoPlay : 5200,
			stopOnHover : true,
			 pagination : false

        	});

  $("#owl-example1").owlCarousel({
        	items:4,
        	autoPlay : 5400,
			stopOnHover : true,
			pagination : false
        	});

  $("#owl-example2").owlCarousel({
        	items:4,
        	autoPlay : 5600,
			stopOnHover : true,
			pagination : false
        	});
  $("#owl-example3").owlCarousel({
        	items:4,
        	autoPlay : 5800,
			stopOnHover : true,
			pagination : false
        	});


  $('#getBook').on('click',function(e){
     e.preventDefault();
    var dataCode = $('#getBook').attr('data-stock-id');
    var from = $('#date_from_borrow').val();
    var to =$('#date_to_borrow').val();
    // console.log('hello');
    console.log(dataCode);
    $.ajax({
      url: '../../transaction/request/store',
      type: "POST",
      timeout: 1000,
      async: false,
      headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
      data: {dataCode: dataCode, from: from, to: to},
      success: function( result ) {

        if (result.reserved === "success") {
          toastr.success(result.msg, 'Success: ');
          $('#getBook').attr('disabled','true');
          location.href = "/home/record/";
          
        }else{
          toastr.error(result.msg, 'Failed: The Reuest Cannot be Proceed. Please Contact System Admin! ');
        }
      }
    });
   });

$("#reserve_book").click(function(){
    var reserve_this = $(this);
    //reserve stock
    $("#transaction_reserve").on('submit', function(e){
      var that =$(this);
      e.preventDefault();
      var data = $(this).serializeArray();
      console.log('here');
      // console.log(data);
      var reserve_stock = $.post('../../transaction/request/reserve', data, function(result){
          // console.log(result);

          if(result.reserved === "success"){
            /**
             * 1) after save is success
             * 2) just hide the model
             * 3) show the error section by making attr [hidden=false]
             * 4) add success message, [stock_title] contain success msg
             */
            toastr.success(result.msg, 'Success:');
            $('#getBookModal').modal('hide');
            //disbled reserve item
            reserve_this.parent().html("<button class='btn btn-danger' disabled>Reserved request</button>");
            //make form empty
            $('#reserve_book').attr('disabled','true');
            location.href = "/home/record/";

          }else{
            $(".invalid_date").html("<strong>"+ result.msg +"</strong>");
          }

          that.trigger("reset");
      });

    });
  });
});
