(function(){
	$(".reserve_btn").click(function(){

		$(".invalid_date").empty();
		var reserve_this = $(this);
		//change stock_id dynamic
		$.get('transaction/request/'+$(this).attr('data-stock-id'), function(result){
			$('#stock_detail_byid').html(result);
		});

		$.ajaxSetup({
	        header:$('meta[name="_token"]').attr('content')
	    })

		//reserve stock
		$("#transaction_reserve").on('submit', function(e){
			var that =$(this);
			e.preventDefault();
			var data = $(this).serializeArray();
			var reserve_stock = $.post('transaction/request/store', data, function(result){
					if(result.reserved === "success"){
						/**
						 * 1) after save is success
						 * 2) just hide the model
						 * 3) show the error section by making attr [hidden=false]
						 * 4) add success message, [stock_title] contain success msg
						 */
						$('#reserve_form').modal('hide');
						$(".success_hidden").attr('hidden', false);
						$(".success").text(result.stock_title);

						//disbled reserve item
						reserve_this.parent().html("<button class='btn btn-danger' disabled>Reserved request</button>");
						//make form empty

					}else{
						$(".invalid_date").html("<p>This item is booked till <strong>"+ result.book_to +"</strong></p>");
					}

					that.trigger("reset");
			});

			//handle all error
			reserve_stock.error(function (errors) {

				error = errors.responseJSON;
				$.each(error, function(key, value){

					//show hidden file
					$(".error_hidden").attr('hidden', false);

					/**
					 * if only error exit
					 * check the same error is exit
					 * or not
					 */
					if($(".error li").length > 0){
						/**
						 * if only same error is alredy exit
						 * [error_exit.length] will me more than 0
						 * if error not exit add error
						 */

						var error_exit = $(".error li:contains('"+value+"')");
						if(error_exit.length === 0){
							$(".error").append("<li>"+value+"</li>");
						}
					}else{
						$(".error").append("<li>"+value+"</li>");
					}
				});
			});
		});
	});
}());
