$(document).ready(function() {
  /*******
  * for handling cancel in modal
  * remove error span in modal after cancel
  *********/
  $('#authorModal').on('hide.bs.modal', function(e){
    $("#authorError").empty();
    $('#authorForm').trigger('reset');
    $("#authorInput").removeClass('has-error');
    $("#authorInput").addClass('has-success');
  });

  $('#subjectModal').on('hide.bs.modal', function(e){
    $("#subjectError").empty();
    $('#subjectName').val("");
    $("#subjectInput").removeClass('has-error');
    $("#subjectInput").addClass('has-success');
  });

  $('#publicationModal').on('hide.bs.modal', function(e){
    $("#publicationError").empty();
    $('#publicationName').val("");
    $("#publicationInput").removeClass('has-error');
    $("#publicationInput").addClass('has-success');
  });

  $('#genreModal').on('hide.bs.modal', function(e){
    $("#genreError").empty();
    $('#genreName').val("");
    $("#genreInput").removeClass('has-error');
    $("#genreInput").addClass('has-success');
  });

  /******
  *	for adding new author
  */
  $('#addAuthor').on('click', function (e) {
    e.preventDefault();

    var authorName = $('#authorName').val();
    $.ajax({
      url: '../api/author/store',
      type: "POST",
      processData: false,
      contentType: false,
      timeout: 1000,
      async: false,
      headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
      data: new FormData($('#authorForm')[0]),
      beforeSend: function() {
        $('#authorForm').prop('disabled', true);
        $('#authorName').prop('disabled', true);
        $('#authorLink').prop('disabled', true);
        // $('#authorAbout').prop('disabled', true);
        // $('#addAuthor').prop('disabled', true);
        $('#authorCancel').prop('disabled', true);
        $("#fountainG").show();
      },
      success: function( msg ) {

        if (msg.status == 'true') {
          $("#fountainG").hide();



          $('#authorForm').prop('disabled', false);
          $('#authorName').prop('disabled', false);
          $('#authorLink').prop('disabled', false);
          $('#authorAbout').prop('disabled', false);
          $('#authorForm').trigger('reset');
          $("#authorError").empty();
          $("#authorInput").removeClass('has-error');
          $("#authorInput").addClass('has-success');

          $("#authorModal").removeClass("in");
          $('.modal-backdrop').remove();
          $('#authorModal').modal('hide');
          // $('body').removeClass('modal-open');

          // $('#authorModal').modal('hide');
          // $("#authorModal").removeClass("in");
          // $(".modal-backdrop").remove();

          // $('#authorImage').val(" ");
          toastr.success(msg.msg);
          // $('#authorCancel').submit();
        }else {
          toastr.error(msg.msg);
        }
      },
      error: function( msg ){
        $("#authorError").empty();
        $('#authorName').prop('disabled', false);
        $('#authorLink').prop('disabled', false);
        $('#authorAbout').prop('disabled', false);
        $('#addAuthor').prop('disabled', false);
        $('#authorCancel').prop('disabled', false);
        $("#fountainG").hide();
        error = msg.responseJSON;
        $.each(error, function(key, value){
          $("#authorInput").removeClass('has-success');
          $("#authorInput").addClass('has-error');
          $("#authorError").append(value[0]);
          $("#authorName").focus();
        });
      }
    });
  });

  /******
  *	for adding new publication
  */
  $('#addPublication').on('click', function (e) {
      e.preventDefault();

      var publicationName = $('#publicationName').val();
      $.ajax({
          url: '../api/publication/store',
          type: "POST",
          headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
          data: {publicationName: publicationName},
          success: function( msg ) {
            if (msg.status == 'true') {
              $("#publicationModal").removeClass("in");
              $(".modal-backdrop").remove();
              $('#publicationModal').modal('hide');
              $('#publicationName').val("");
              toastr.success(msg.msg, 'Success: ');
              $('#publicationNameForm').focus();

              $("#publicationError").empty();
              $("#publicationInput").removeClass('has-error');
              $("#publicationInput").addClass('has-success');
            }else {
              toastr.error(msg.msg);
            }
          },
          error: function( msg ){
            error = msg.responseJSON;
            $("#publicationError").empty();
            $.each(error, function(key, value){
              $("#publicationInput").removeClass('has-success');
              $("#publicationInput").addClass('has-error');
              $("#publicationError").append(value[0]);
              $("#publicationName").focus();
            });
          }
      });
  });

  /******
  *	for adding new genre
  */
  $('#addGenre').on('click', function (e) {
      e.preventDefault();

      var genreName = $('#genreName').val();
      $.ajax({
          url: '../api/genre/store',
          type: "POST",
          headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
          data: {genreName: genreName},
          success: function( msg ) {
            if (msg.status == 'true') {
              $('#genreName').val("");
              $("#genreModal").removeClass("in");
              $(".modal-backdrop").remove();
              $('#genreModal').modal('hide');
              toastr.success(msg.msg, 'Success: ');
              $('#genreNameForm').focus();

              $("#genreError").empty();
              $("#genreInput").addClass('has-success');
              $("#genreInput").removeClass('has-error');
            }else {
              toastr.error(msg.msg);
            }
          },
          error: function( msg ){
            error = msg.responseJSON;
            $.each(error, function(key, value){
              $("#genreInput").removeClass('has-success');
              $("#genreInput").addClass('has-error');
              $("#genreError").append(value[0]);
              $("#genreName").focus();
            });
          }
      });
  });

  /******
  *	for adding new subject
  */
  $('#addSubject').on('click', function (e) {
      e.preventDefault();

      var subjectName = $('#subjectName').val();
      $.ajax({
          url: '../api/subject/store',
          type: "POST",
          headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
          data: {subjectName: subjectName},
          success: function( msg ) {
              if (msg.status == 'true') {
                $("#subjectModal").removeClass("in");
                $(".modal-backdrop").remove();
                  $('#subjectModal').modal('hide');
                  $('#subjectName').val("");
                  toastr.success(msg.msg, 'Success: ');
                  $('#subjectNameForm').focus();

                  $("#subjectError").empty();
                  $("#subjectInput").addClass('has-success');
                  $("#subjectInput").removeClass('has-error');
              }else {
                  toastr.error(msg.msg);
              }
          },
          error: function( msg ){
            error = msg.responseJSON;
            $.each(error, function(key, value){
              $("#subjectInput").removeClass('has-success');
              $("#subjectInput").addClass('has-error');
              $("#subjectError").append(value[0]);
              $("#subjectName").focus();
            });
          }
      });
  });



  /******
  *	for typeahead in author name
  */

  var author = new Bloodhound({
    datumTokenizer: Bloodhound.tokenizers.obj.whitespace('author_name'),
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    remote: {
      url: '/api/author/getauthor?author_name=%QUERY',
      wildcard: '%QUERY'
    }
  });

  $('.typeaheadAuthorName').typeahead({
    hint: false,
    highlight: true,
    minLength: 1
    },
    {
    name: 'author_name',
    display: 'author_name',
    source: author
  });

  /******
  *	for typeahead in publication name
  */
  var publication = new Bloodhound({
    datumTokenizer: Bloodhound.tokenizers.obj.whitespace('publication_name'),
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    remote: {
      url: '/api/publication/getpublication?publication_name=%QUERY',
      wildcard: '%QUERY'
    }
  });

  $('.typeaheadPublication').typeahead({
    hint: false,
    highlight: true,
    minLength: 1
    },
    {
    name: 'publication_name',
    display: 'publication_name',
    source: publication
  });

  /******
  *	for typeahead in subject name
  */
  var subject = new Bloodhound({
    datumTokenizer: Bloodhound.tokenizers.obj.whitespace('subject_name'),
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    remote: {
      url: '/api/subject/getsubject?subject_name=%QUERY',
      wildcard: '%QUERY'
    }
  });

  $('.typeaheadSubject').typeahead({
      hint: false,
      highlight: true,
      minLength: 1
    },
    {
      name: 'subject_name',
      display: 'subject_name',
      source: subject
    });

  /******
  *	for typeahead in genre name
  */
  var genre = new Bloodhound({
    datumTokenizer: Bloodhound.tokenizers.obj.whitespace('genre_name'),
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    remote: {
      url: '/api/genre/getgenre?genre_name=%QUERY',
      wildcard: '%QUERY'
    }
  });

  $('.typeaheadGenre').typeahead({
      hint: false,
      highlight: true,
      minLength: 1
    },
    {
      name: 'genre_name',
      display: 'genre_name',
      source: genre
  });

  $('#stockForm').on('keypress', function(e) {
    var keyCode = e.keyCode || e.which;
    if (keyCode === 13) {
      e.preventDefault();
      return false;
    }
  });


});
