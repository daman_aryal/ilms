$(document).ready(function() {
   
$('.dropify').dropify();
    	$('#advsearch').click(function(){
		$('#advoption').slideToggle();
	});
     
      $("#owl-example").owlCarousel({
            	autoPlay : 5200,
            	navigation : true,
    			stopOnHover : true,
    			 pagination : false
            	});

      $("#owl-example1").owlCarousel({
            	items:4,
            	navigation : true,
            	autoPlay : 5400,
    			stopOnHover : true,
    			pagination : false
            	});

      $("#owl-example2").owlCarousel({
            	items:4,
            	navigation : true,
            	autoPlay : 5600,
    			stopOnHover : true,
    			pagination : false
            	});
      $("#owl-example3").owlCarousel({
            	items:4,
            	autoPlay : 5800,
    			stopOnHover : true,
    			pagination : false
            	});
     
    });
    $('#generate').on('click',function(e){
         e.preventDefault();
            $.ajax({
                url: '/generatePass',
                type: "POST",
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success: function( result ) {
                    $('#password_confirm').val(result);
                    $('#password').val(result);
                    $('#view_pass').html(result);
                    $('#generated').show();
                }
            });

    });
        $('#sgenerate').on('click',function(e){
         e.preventDefault();
            $.ajax({
                url: '/generatePass',
                type: "POST",
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success: function( result ) {
                    $('#spassword_confirm').val(result);
                    $('#spassword').val(result);
                    $('#sview_pass').html(result);
                    $('#sgenerated').show();
                }
            });

    });