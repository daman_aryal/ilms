<?php

namespace App\Injection;
use Request;


class SideNav{
	public function active_nav($value)
	{
		if(strpos(Request::path(), $value) !== false){
			return "active";
		}

	}
	public function expand_nav($value)
	{
		if(strpos(Request::path(), $value) !== false){
			return "active";
		}

	}
	public function span_nav($value)
	{
		if(strpos(Request::path(), $value) !== false){
			return "open";
		}

	}
}