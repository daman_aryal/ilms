<?php
namespace App\Injection;

use App\Role;
use App\Location;
use App\LocationUser;
use Auth;

class UserForm{

	protected $role;

	public function __construct(){
		$this->role =  Role::get();
	}

	public function su()
	{
		$su = $this->role->where('user_type_id', 1)->pluck('display_name', 'id');
		return $su;
	}

	public function admin()
	{
		$admin = $this->role->pluck('display_name', 'id');
		return $admin;
	}
	public function location(){
		if (Auth::user()->id != 1 ) {
			$location = LocationUser::where('user_id',Auth::user()->id)->first();
			$location_name = Location::find($location->location_id);
			return $location_name->title;
			
		}else{
			return null;
		}

	}

	// public function libarian()
	// {
	// 	$libarian = $this->role->where('user_type_id', 3)->pluck('display_name', 'id');
	// 	return $libarian;
	// }
	
	// public function student()
	// {
	// 	$student = $this->role->where('user_type_id', 4)->pluck('display_name', 'id');
	// 	return $student;
	// }

}