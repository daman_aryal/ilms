<?php 

namespace App\Injection;

use App\Permission;
use App\Role;
use App\UserType;
use Auth;
use DB;

class RoleForm{

	protected $permission;

	public function __construct(){
		$this->permission = Permission::get();
	}

	public function user_model(){
		$user_model = $this->permission->where('catagory','user-catagory')->pluck('name', 'id');
		return $user_model;
	}
	
	public function role_model(){
		$role_model = $this->permission->where('catagory','role-catagory')->pluck('name', 'id');
		return $role_model;
	}
	
	public function request_model(){
		$request_model = $this->permission->where('catagory','request-catagory')->pluck('name', 'id');
		return $request_model;
	}

	public function stock_model(){
		$stock_model = $this->permission->where('catagory','stock-catagory')->pluck('name', 'id');
		return $stock_model;
	}

	public function user_type(){
		$user_type = UserType::get();
		return $user_type;
	}

	public function checked_permission($permission, $role_id){
		$permission = DB::table('permission_role')
						->where('role_id',$role_id)		
						->where('permission_id', $permission)
						->exists();
		if($permission){
			return true;
		}else{
			return false;
		}				
	}

	public function checked_usertype($user_type, $role_id){
		$is_usertype = Role::where('user_type_id', $user_type)
						->where('id',$role_id)		
						->exists();
		if($is_usertype){
			return true;
		}else{
			return false;
		}				
	}
}