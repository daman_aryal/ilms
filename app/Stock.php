<?php

namespace App;
use App\StockDetail;
use App\Author;
use App\Publication;
use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Database\Eloquent\Model;

class Stock extends Model
{

    use SoftDeletes;

    protected $fillable = [
        'type_id', 'quantity', 'active', 'added_by','updated_by','index','code'
    ];

    protected $dates = ['deleted_at'];

     /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function images()
    {
        return $this->morphMany( 'App\Image', 'imageable' );
    }

    public function featured()
    {
        return $this->hasOne( 'App\Featured' );
    }


    public function rel_stockDetail()
    {
        return $this->hasMany('App\StockDetail','stock_id','id');
    }

    public function rel_author(){

        return $this->belongsToMany('App\Author','pivot_author_stock_details');
    }

    public function rel_stocktype()
    {
        return $this->hasOne('App\StockType','id','type_id');
    }


    public function getQuantity($stockId){
      return StockDetail::where('stock_id',$stockId)->count();
    }

    public function getPublicationName($publication_id){
      $publication = Publication::select('publication_name')->where('id',$publication_id)->first();
      return $publication->publication_name;
    }
    
    public function getAuthorName(){
      $authorName = array();
      foreach ($this->rel_author as $key => $author_name) {
        $authorName[] = $author_name->author_name;
      }
      return $authorName;
    }

     // @if(!$stock_info->images->isEmpty())
     //                                <img src="/{{ $stock_info->images[0]->path }}" />
     //                            @else
     //                              <img src="/img/no-cover.jpg" />
     //                            @endif

    public function getBookImage($id = null ){

        if ($id != null) {

          $stock = Stock::find($id);
          if (!$stock->images->isEmpty()) {
              $image = $stock->images[0]->path;
              return $image;
          }else{
              $image = 'img/no-cover.jpg';
              return $image;
          }
        }

         if (!$this->images->isEmpty()) {
            $image =  $this->images[0]->path;
            return $image; 
         }else{
           $image = 'img/no-cover.jpg';
           return $image;
         }
    }

    protected static function boot() {
        parent::boot();

        static::deleting(function($stock) {
            $stock->featured()->delete();
        });
    }
}
