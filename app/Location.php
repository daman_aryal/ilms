<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{

    protected $fillable = [
    					'title',
    					'description',
    					'status',
    					'slug',
    				];
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
