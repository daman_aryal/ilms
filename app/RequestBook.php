<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RequestBook extends Model
{
	protected $table = 'requests';
    protected $fillable = [
    					'title',
    					'publication',
    					'author',
    					'description',
    					'url',
    					'status',
    					'created_by',
    					'updated_by',
                        'slug',
    				];

    protected $dates = ['deleted_at'];

    public function tag()
    {
        return $this->hasMany('App\RequestTag','request_id','id');
    }
}
