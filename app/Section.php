<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Section extends Model
{
     /**
     * Section belongs to class
     * @return class return class collection
     */
    public function ilmsClass(){
        return $this->belongsTo('App\IlmsClass', 'class_id', 'id');
    }
}
