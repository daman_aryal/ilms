<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use App\StaffUser;

class User extends Authenticatable
{
    use Notifiable;
    use EntrustUserTrait;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    // protected $hidden = [ 'images'];
    // protected $morphClass = 'User';
    protected $fillable = [
        'name', 'email', 'password', 'address','contact',
        'code','created_by','updated_by','active','first_login'
    ];

    /**
     * The attributes that should be hidden for arrays.ss
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'images',
    ];

    /**
     * user relation
     * to user type
     */
    public function user_type(){
        if(isset($this->roles[0])){
        $role = Role::where('id', $this->roles[0]->id)->value('user_type_id');
        $user_type = UserType::where('id', $role)->value('name');
        return $user_type;
        }else{
            
            return false;
        }
          
    }

    public function staffUser(){
        return $this->hasMany('App\StaffUser');
    }

    public function basicUser(){
        return $this->hasMany('App\BasicUser');
    }

    public function images()
    {
        return $this->morphOne('App\Image', 'imageable');
    }

    public function requestTag(){
        return $this->hasMany('App\RequestTag');
    }
   
    public function location()
    {
        return $this->hasOne('App\Location');
    }

    public static function generateRandomClue($length = 16) {
        $b32    = "234567QWERTYUIOPASDFGHJKLZXCVBNM";
        $s  = "";
      for ($i = 0; $i < $length; $i++)
              $s .= $b32[rand(0,31)];
        return $s;
    }
    public function GetUserCode($id)
    {
      $code = User::select('code')->where('id',$id)->first();
      return $code->code;
    }
}
