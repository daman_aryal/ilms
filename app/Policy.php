<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Policy extends Model
{
    //
     protected $table = 'policies';
     protected $fillable = [
        'no_of_days', 'no_of_books', 'per_day_for_week', 'per_day_more_than_week', 'description'
    ];

    // public function MemberType()
    // {
    //     return $this->belongsTo('App\MemberType');
    // }
    public function membertype()
    {
        return $this->belongsToMany('App\MemberType', 'member_type_policies');
    }
}
