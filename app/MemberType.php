<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class MemberType extends Model
{
	
    protected $table = 'member_type';

    //  public function policy()
    // {
    //     return $this->hasOne('App\Policy');
    // }
     public function policy()
    {
        return $this->belongsToMany( 'App\Policy', 'member_type_policies' );
    }
}
