<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FineAccount extends Model
{
    protected $table = 'fine_account';

    protected $fillable = [
		 'amount','user_id','received_by','received_date','receipt_no'
	];

	 public function chargedTo()
	{
		return $this->hasOne('App\User', 'id', 'user_id');
	}
	
	public function getReceived($id)
	{
		$user = User::find($id);

		if($user)
		{
			return $user->name;
		}
		else
		{
			return "-";
		}
	}
}
