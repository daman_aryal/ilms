<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Author extends Model
{

    protected $appends = array('image_url');

    protected $fillable = [
        'author_name', 'link', 'persona_information'
    ];
    protected $hidden = [ 'images'];
    protected $morphClass = 'Author';

     public function stock()
    {
        return $this->belongsToMany('App\Stock', 'pivot_author_stock_details');
    }

    public function images()
    {
        return $this->morphOne('App\Image', 'imageable');
    }


 // code for $this->mimeType attribute
 public function getImageUrlAttribute() {
     if ($this->images) {
         $path = $this->images->path;
     }else {
       $path = 'img/profile_null.png';
     }
     return $path;
 }

}
