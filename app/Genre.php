<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Genre extends Model
{


  protected $fillable = [	'genre_name'];

  /**
     * Class belongs to subject
     * @return subject return subject collection
     */
    public function subject(){
        return $this->belongsTo('App\Subject', 'subject_id', 'id');
    }

}
