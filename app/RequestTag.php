<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RequestTag extends Model
{
    
    protected $fillable = [
    					'request_id',
    					'user_id',
    				];

	protected $dates = ['deleted_at'];

    public function request_book()
    {
        return $this->belongsTo('App\RequestBook');
    }

    public function user(){
    	return $this->belongsTo('App\User');
    }

    public function scopeUnread($query)
	{
	    return $query->where('is_read', '=', 0);
	}

	public function getUserName($id){
		$user = User::find($id);
		return $user->name; 
	}
}
