<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sector extends Model
{
	
    protected $fillable = [
        'title',
        'slug',
        'active'
    ]; 

}
