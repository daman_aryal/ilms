<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    protected $fillable = [	'subject_name','slug','active','sector_id'];


    /**
     * Class belongs to sector
     * @return sector return sector collection
     */
    public function sector(){
        return $this->belongsTo('App\Sector', 'sector_id', 'id');
    }
}
