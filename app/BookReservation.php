<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookReservation extends Model
{
    //
     protected $fillable = [
         'book_from','book_to','user_id','code','stock_id','status','stock_detail_id','active','updated_by'
    ];
   
    /**
	 * reservation hasone user
	 * @return collection return user collection
	 */
	public function user()
	{
		return $this->hasOne('App\User', 'id', 'user_id');
	}

}
