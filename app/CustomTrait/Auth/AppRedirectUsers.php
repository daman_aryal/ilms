<?php

namespace App\CustomTrait\Auth;
use Auth;

trait AppRedirectUsers
{
    /**
     * Get the post register / login redirect path.
     *
     * @return string
     */
    public function redirectPath()
    {
        //if active
        if (Auth::user()->user_type()) {
            if(Auth::user()->user_type() == "su"){
                return 'dashboard/';
            }elseif(Auth::user()->user_type() == "librarian"){
                return 'dashboard/';   
            }elseif(Auth::user()->user_type() == "admin"){
                 return 'dashboard/';   
            }elseif(Auth::user()->user_type() == "student"){
                return 'home/';  
            }   
        }else{
            return 'home/';
        }
    	
        /*return property_exists($this, 'redirectTo') ? $this->redirectTo : '/home';*/
    }
}
