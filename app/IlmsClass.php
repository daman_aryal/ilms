<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IlmsClass extends Model
{
     protected $table = 'class';
    /**
     * Class belongs to program
     * @return program return program collection
     */
    public function program(){
        return $this->belongsTo('App\Program', 'program_id', 'id');
    }

    /**
     * Class has section
     * @return section return section collection
     */
    public function section()
    {
        return $this->hasOne('App\Section','id','section_id');
    }
}
