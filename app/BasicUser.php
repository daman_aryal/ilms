<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BasicUser extends Model
{
    // protected $hidden = [ 'images'];
    protected $fillable = [
    						'user_id',
    						'date_of_birth',
    						'gaurdian_name',
                            'first_name',
                            'last_name',
    						'relation',
    						'year_id',
    						'program_id',
    						'class_id',
    						'section_id',
    						'roll_number',
    						'valid_till',
    						'member_code',
    						'member_type_id'
    						];

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function memberType()
    {
        return $this->hasOne('App\MemberType', 'id', 'member_type_id');
    }

    public function program()
    {
        return $this->hasOne('App\Program', 'id', 'program_id');
    }


    // public function images()
    // {
    //     return $this->morphOne('App\Image', 'imageable');
    // }
}
