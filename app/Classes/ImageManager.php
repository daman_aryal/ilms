<?php

namespace App\Classes;

use Intervention\Image\ImageManagerStatic as Images;
class ImageManager
{
    private static $THUMB_PATH = 'images/thumbnails/';

    /**
     * @param $image
     * @param $path
     * @param null $name
     * @return string
     */
    public static function upload($image, $path, $name = NULL)
    {
        $extension = $image->getClientOriginalExtension();
        if ( empty( $name ) ) {
            $imageName =time().'.'.$extension;

            while ( file_exists( $path.$imageName ) ){
                $imageName = sha1(time()).'.'.$extension;
            }

        } else {
            $imageName = time().'-'.$name.'.'.$extension;
            if(file_exists($path.$imageName))
                unlink($path.$imageName);

        }
        Images::make($image)->save($path.$imageName);
        return $imageName;
    }


}
