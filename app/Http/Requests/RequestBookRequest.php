<?php

namespace App\Http\Requests;
use Auth;
use Illuminate\Foundation\Http\FormRequest;
use App\RequestBook;

class RequestBookRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
            $limit = 5;              
            return [
                'user_name'=>['required','array','between:5,14'],
                'title'=> 'required',
                'publication'=>'required',
                'author'=>'required',
                'url'=>'url',
                ];
    }

    public function messages()   
    {               
        return ['user_name.size'=>'Required number of tags must be :between'];        
        return ['user_name.required'=>'You must tag someone'];
    }

    public function fillRequest(){
        // dd("sdfdsf");
        $inputs = [
            'title'=>$this->title,
            'publication'=>$this->publication,
            'author'=>$this->author,
            'description'=>$this->description,
            'url'=>$this->url,
            'status'=>0,
            'slug'=> $this->setUniqueSlug($this->title,0),
            'created_by'=>Auth::user()->id,
            'updated_by'=>Auth::user()->id,
        ];

        return $inputs;
    }

    // public function fillTag(){

    //     return $inputs[
    //         'request_id' => $this->title,
    //         'user_id' =>$request->publication,
    //     ];
    // }

         /**
     * Recursive routine to set a unique slug.
     *
     * @param string $title
     * @param mixed $extra
     */
    protected function setUniqueSlug( $title, $extra )
    {
        $slug = str_slug( $title . '-' . $extra );
        if ( $text= RequestBook::whereSlug( $slug )->exists() ) {
            $slug = $this->setUniqueSlug( $title, $extra + 1 );
            return $slug;
        }
        return $slug;
    } 
}
