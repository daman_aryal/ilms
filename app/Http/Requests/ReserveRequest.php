<?php

namespace App\Http\Requests;

use App\StockDetail;
use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;


class ReserveRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'from' => 'required',
            'to' => 'required'
        ];
    } 

    public function inputData()
    {   
        $stock_id = $this->stock_detail_id;
        $stock_detail = StockDetail::find($stock_id);
        $input = [
            'code' => $stock_detail->code,
            'reference_id' => 0,
            'user_id' => Auth::user()->id,
            'status' => '0',
            'stock_id' => $stock_detail->stock->id,
            'stock_detail_id' => $stock_id,
            'created_by' => Auth::user()->id,
            'book_from' =>$this->from,
            'book_to' =>  $this->to,
            'updated_by' => null
        ];
       return $input;
    }
}
