<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Auth;
use Hash;


class StafUserUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // 'password' => 'required',
            'password_confirm' => 'same:password',
            'permanent_address' => 'required',
            'temporary_address' => 'required',
            'contact' => 'required | regex : /^[0-9-+]+$/',
            'first_name' => 'required | regex : /^[a-zA-Z ]+$/',
            'last_name' => 'required | regex : /^[a-zA-Z ]+$/',
            'email' => 'required|email',
            // 'member_type_id' => 'required',
            // 'member_code' => 'required',
            'code' => 'required'
        ];
    }

    public function updateUsers(){
        $inputs = [
                    'name' => $this->first_name." ".$this->last_name,
                    'email' => $this->email,
                    'updated_by' =>Auth::user()->id,
                    'address' => $this->permanent_address,
                    'contact' =>$this->contact
                  ];

        if ($this->password) {
            $inputs['password'] = Hash::make($this->password);
        }

        return $inputs;
    }

    public function updateStaffUsers(){

        $inputs = [
                    'first_name' =>$this->first_name,
                    'last_name' => $this->last_name,
                    'date_of_birth'=>$this->date_of_birth,
                    'permanent_address'=>$this->permanent_address,
                    'temporary_address'=>$this->temporary_address,
                    'department_id'=>$this->department_id,
                    'designation'=>$this->designation,
                    'job_type'=>$this->job_type,
                    'joined_date'=>$this->joined_date,
                    'valid_till'=>$this->valid_till
                    ];

        return $inputs;
    }
}
