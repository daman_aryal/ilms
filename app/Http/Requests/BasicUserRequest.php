<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Hash;
use Auth;
use App\MemberType;
use DB;

class BasicUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'year_id' => 'required',
            'password' => 'required',
            'password_confirm' => 'required|same:password',
            'address' => 'required',
            'contact' => 'required | regex : /^[0-9-+]+$/',
            'first_name' => 'required | regex : /^[a-zA-Z ]+$/',
            'last_name' => 'required | regex : /^[a-zA-Z ]+$/',
            'email' => 'required|email|unique:users,email',
            'member_type_id' => 'required',
            // 'member_code' => 'required',
            // 'code' => 'required'
        ];
    }

    public function fillUser($code){
        // dd($this);
        $inputs = [
                'name' => $this->first_name." ".$this->last_name,
                'email' => $this->email,
                'password' => Hash::make($this->password),
                'created_by' =>Auth::user()->id,
                'active' => '1',
                'first_login' => '0',
                'address' => $this->address,
                'contact' =>$this->contact,
                'code' =>$code,
                  ];


        return $inputs;
    }

    public function fillBasicUsers($user_id, $member_code){

        $inputs = [
                    'user_id' => $user_id,
                    'date_of_birth'=>$this->date_of_birth,
                    'gaurdian_name'=>$this->gaurdian_name,
                    'first_name' =>$this->first_name,
                    'last_name' =>$this->last_name,
                    'relation'=>$this->relation,
                    'year_id'=>$this->year_id,
                    'program_id'=>$this->program_id,
                    'class_id'=>$this->class_id,
                    'section_id'=>$this->section_id,
                    'roll_number'=>$this->roll_number,
                    'valid_till'=>$this->valid_till,
                    'member_code'=>$member_code,
                    'member_type_id'=>$this->member_type_id,
                    ];



        return $inputs;
    }
}

