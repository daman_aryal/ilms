<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\StockType;
use App\Author;
use Auth;
use DB;


class StockCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'quantity' => 'required',
            // 'active'    => 'required',
            'index' => 'required',
            // 'code' => 'required',
            'author_name'   =>  'required',
            // 'link' => 'required',
            // 'persona_information'    => 'required',
            'publication_name'   => 'required',
            // 'code'   =>  'required',
            // 'stock_id' => 'required',
            'title'    => 'required',
            'edition'   => 'required',
            'isbn'  => 'required',
            'publication_date'  => 'required',
            'plot_summary' => 'required',
            'language'  => 'required',
            'subject' =>'required',
            'sector' => 'required',
            'stock_format'    => 'required',
            'pages' => 'required',
            // 'dimension' => 'required',
            'dewey' =>  'required',
            'country'   => 'required'
        ];
    }

    public function FillStock()
    {

        $inputs = [
            'type_id'   =>  '1',
            'active'    => '1',
            'index' => $this->index,
            'added_by'     => Auth::user()->id,
            'updated_by'     => Auth::user()->id,
            'code' => null,


        ];
        return $inputs;
    }

    public function FillStockDetail($sid,$pid,$code,$location_id)
    {

        $inputs = [
            'code'   =>  $code,
            'stock_id' => $sid,
            'title'    => title_case($this->title),
            'sector_id' => $this->sector,
            'subject_id' => $this->subject,
            'genre_id' => $this->genre,
            'edition'   => $this->edition,
            'publication_id'    => $pid,
            'active'    => '1',
            'created_by'     => Auth::user()->id,
            'updated_by'     => Auth::user()->id,
            'isbn'  => $this->isbn,
            'publication_date'  => $this->publication_date,
            'reservation_status' => 0,
            'plot_summary' => $this->plot_summary,
            'language'  => $this->language,
            'subject' =>$this->subject,
            'genre' =>$this->genre,
            'stock_format'    => $this->stock_format,
            'pages' => $this->pages,
            'dimension' => $this ->dimension,
            'dewey' =>  $this->dewey,
            'country'   => $this->country,
            'available_status' =>'1',
            'book_lock_status' => '0',
            'location_id' => $location_id,
        ];
        // dd($inputs);
        return $inputs;
    }

      public function getAuthor()
    {
        $name = $this->author_name;
        $author = Author::select('id')->where('author_name',$name)->first();
      return $author->id;
    }


}
