<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\IlmsClass;
use App\Program;
use Db;

class ClassController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $classes = IlmsClass::all();

        return view('class.index',compact('classes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $programs = Program::pluck('title', 'id');

       return view('class.create',compact('programs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $this->validate($request, [
            'title' => 'required',
            'program_id' => 'required',
        ]);
        $class = new IlmsClass();
        $class->title = $request->input('title');
        $class->slug = $this->setUniqueSlug($class->title,0);
        $class->description = $request->input('description');
        $class->program_id = $request->input('program_id');
        $class->status = $request->get( 'status')? 1: 0;
        $class->save();
        
       $classes = IlmsClass::all();

        return view('class.index',compact('classes'))->with('success','Class created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     *  @param  string $slug
     * @return \Illuminate\Http\Response
     */
   public function edit($slug)
    {
        $class = IlmsClass::whereSlug($slug)->first();
        $programs = Program::pluck('title', 'id');
        
        return view('class.edit',compact('class','programs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string $slug
     * @return \Illuminate\Http\Response
     */
     public function update(Request $request, $slug)
    {
        $this->validate($request, [
            'title' => 'required',
            'program_id' => 'required',
        ]);
        $class = IlmsClass::whereSlug($slug)->first();
        $class->title = $request->input('title');
        $class->description = $request->input('description');
        $class->program_id = $request->input('program_id');
        $class->status = $request->get( 'status')? 1: 0;
        $class->save();
$classes = IlmsClass::all();

        return view('class.index',compact('classes'))->with('success','Class updated successfully');
        

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  string $slug
     * @return \Illuminate\Http\Response
     */
    public function destroy($slug)
    {
          IlmsClass::whereSlug($slug)->delete();

        return redirect()->back()
                        ->with('success','Class deleted successfully');
    }

      /**
     * Recursive routine to set a unique slug.
     *
     * @param string $title
     * @param mixed $extra
     */
    protected function setUniqueSlug( $title, $extra )
    {
        $slug = str_slug( $title . '-' . $extra );
        if ( $text= IlmsClass::whereSlug( $slug )->exists() ) {
            $slug = $this->setUniqueSlug( $title, $extra + 1 );
            return $slug;
        }
        return $slug;
    } 
}
