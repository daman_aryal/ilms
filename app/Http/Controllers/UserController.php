<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Role;
use App\User;
use Auth;
use DB;
use Hash;
use Illuminate\Http\Request;
use App\LocationUser;
use App\Location;


class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (Auth::user()->id == 1) {
         
       $data = User::where('code',null)->orderBy('id','DESC')->paginate(5);
       return view('user_index',compact('data'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
        }else{

       $data = User::where('code',null)->where('id','!=','1')->orderBy('id','DESC')->paginate(5);
       return view('user_index',compact('data'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
        }
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $location = Location::pluck('title','id');
        return view('user_create')->withLocations($location);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $this->validate($request, [
            'name' => 'required | regex : /^[a-zA-Z ]+$/',
             'email' => 'required|email|unique:users,email',
            'password' => 'required',
            'password_confirm' => 'required|same:password',
            'address' => 'required',
            'contact' => 'required | regex : /^[0-9-+]+$/',
            'role' => 'required',
            'location_id' => 'required'
        ],
        [
            'name.regex' => 'Name only should contain letter and space'
        ]);

       DB::transaction(function () use ($request) {

            $input = $request->all();
            $input['password'] = Hash::make($input['password']);
            $input['created_by'] = Auth::user()->id;
            $input['active'] = '0';
            $input['first_login'] = '0';
            
            $user = User::create($input);
            $location = $request->location_id;
            // dd($location);
            LocationUser::create([
                    'user_id' => $user->id,
                    'location_id' => $location,
                ]);

            
            /**
             * 1) get roles from $request->role[]
             * 2) we get array, include in "" value i.e. placeholder
             * 3) avoid this "" data using array_except, array_except 
             * filter form key, so flip array for making "" key
             * 4) if $role is 1 then save else return
             * @var array
             */
            $roles = array_except(array_flip($request->get('role')), "");
            if(count($roles) == 1){
                $user->roles()->attach(array_keys($roles));
            }else{
                return back()->withInput()->with(['multiple_role'=>'please select only one role']);
            }
        

        });
        return redirect('adminUser/')->with('success','User created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        return view('user_show',compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user_id = $id;
        $user = User::find($id);
        $locations = Location::pluck('title','id');
        // dd($locations);
        $roles = Role::pluck('display_name','id');
        $pivotLocation = LocationUser::where('user_id',$id)->first();
        // dd();
        // dd($user);

        $userRole = $user->roles->pluck('id','id')->toArray();
        return view('user_edit',compact('user','roles','userRole', 'user_id','locations','pivotLocation'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       
        if ($id == 1) {
           $this->validate($request, [
            'name' => 'required | regex : /^[a-zA-Z ]+$/',
            'address' => 'required',
            'contact' => 'required | regex : /^[0-9-+]+$/',
        ]);
        }else{
            $this->validate($request, [
            'name' => 'required | regex : /^[a-zA-Z ]+$/',
            'address' => 'required',
            'contact' => 'required | regex : /^[0-9-+]+$/',
            'location_id' => 'required'
        ]);    
        }
        

        $input = $request->all();
        if ($request->password) {
            $input['password'] = Hash::make($input['password']);
        }

        $input['created_by'] = Auth::user()->id;
        $input['active'] = '0';
        $input['first_login'] = '0';
        
        $user = User::find($id);
        $user->update($input);

        // $user = User::create($input);
        // $location = $request->location_id;
         if ($id != 1) {
            $location = LocationUser::where('user_id',$user->id)->first();
            $location->update([
                'location_id' => $request->location_id,
            ]);
        }

        /**
         * 1) get roles from $request->role[]
         * 2) we get array, include in "" value i.e. placeholder
         * 3) avoid this "" data using array_except, array_except 
         * filter form key, so flip array for making "" key
         * 4) if $role is 1 then save else return
         * @var array
         */
        
        $empty_role = array_where($request->get('role'), function ($value, $key) {
            if($value !== ""){
                return 1;
            }
        });

        if(count($empty_role) > 0){
            $roles = array_except(array_flip($request->get('role')), "");
            if(count($roles) == 1){
                $user->roles()->sync(array_keys($roles));
            }else{
                return back()->withInput()->with(['multiple_role'=>'please select only one role']);
            }
        }
        

        /**
         * email should not be same to other while updateing
         * but could be same to same user
         */
        $email_exit = User::where('email', $request->get('email'))
                            ->where('id', '!=', $user->id)
                            ->exists();
        if($email_exit){
            return back()->with(['email_exit'=>'Email already exit']);
        }

        
        return redirect('adminUser')->with('success','User Updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::find($id)->delete();
        return redirect()->back()
                        ->with('success','User deleted successfully');
    }

    public function password_edit(){
        return view('password_edit');    
    }

    public function password_update(Request $request){
        $this->validate($request, [
            'old_password' => 'required',
            'password' => 'required',
            'password_confirm' => 'required|same:password'
        ]);


        $user = User::find(Auth::user()->id);
        if(Hash::check($request->get('old_password'), $user->password)){
            $user->update(['password' => bcrypt($request->get('password')),'first_login'=>'1']);

            return redirect('dashboard/')->with(['password_updated'=>'password successfully updated']);
        }
        return redirect()->back()->with(['password_not_match'=>'password doesnot math']);
    }
    public function myProfileEdit($id){
        $user = User::find($id);

        return view('admineditprofile',compact('user'));

    }
}