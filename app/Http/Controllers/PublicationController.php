<?php

namespace App\Http\Controllers;
use App\Publication;

use Illuminate\Http\Request;

class PublicationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $publications = Publication::all();
        return view('publication.index',compact('publications'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         return view('publication.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $this->validate($request, [
            'publication_name' => 'required'
        ]);
        $publication = new Publication();
        $publication->publication_name = $request->input('publication_name');
        $publication->slug = $this->setUniqueSlug($publication->publication_name,0);
        $publication->save();
        
         $publications = Publication::all();
        return view('publication.index',compact('publications'))->with('success','Publication created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

   /**
     * Show the form for editing the specified resource.
     *
     * @param  string $slug
     * @return \Illuminate\Http\Response
     */
   public function edit($slug)
    {
        $publication = Publication::whereSlug($slug)->first();
        
        return view('publication.edit',compact('publication'));
    }

   /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string $slug
     * @return \Illuminate\Http\Response
     */
     public function update(Request $request, $slug)
    {
         $this->validate($request, [
            'publication_name' => 'required'
        ]);
        $publication = Publication::whereSlug($slug)->first();
        $publication->publication_name = $request->input('publication_name');
        $publication->save();

         $publications = Publication::all();
        return view('publication.index',compact('publications'))->with('success','Publication updated successfully');
        

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  string $slug
     * @return \Illuminate\Http\Response
     */
    public function destroy($slug)
    {
         Publication::whereSlug($slug)->delete();

        return redirect()->back()
                        ->with('success','Publication deleted successfully');
    }

       /**
    * Recursive routine to set a unique slug.
     *
     * @param string $publication_name
     * @param mixed $extra
     */
    protected function setUniqueSlug( $publication_name, $extra )
    {
        $slug = str_slug( $publication_name . '-' . $extra );
        if ( $text= Publication::where('slug', $slug )->exists() ) {
            $slug = $this->setUniqueSlug( $publication_name, $extra + 1 );
            return $slug;
        }
        return $slug;
    } 
}
