<?php

namespace App\Http\Controllers;
use App\Image;
use Illuminate\Http\Request;
use App\Http\Requests\BasicUserRequest;
use App\Http\Requests\BasicUserUpdateRequest;
use App\User;
use App\BasicUser;
use DB;
use App\Year;
use App\MemberType;
use App\Section;
use App\IlmsClass;
use App\Program;
use Intervention\Image\ImageManagerStatic as Images;
// use App\Image;
use File;

class BasicUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allUser = User::with('basicUser')->get();
        return view('basicuser.index',compact('allUser'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $year = Year::pluck('year','id');
        $memberType = MemberType::pluck('title','id');
        $section = Section::pluck('title','id');
        $class = IlmsClass::pluck('title','id');
        $program = Program::pluck('title','id');
        
        return view('basicuser.create')->withYears($year)
                                      ->withMemberTypes($memberType)
                                      ->withSections($section)
                                      ->withClasses($class)
                                      ->withPrograms($program)
                                      ->withEdit("false");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BasicUserRequest $request)
    {
        // dd($request);
        DB::transaction(function() use ($request){
            // dd($request);
            $member_type_id = $request->get('member_type_id');
            $code = MemberType::select('code')->where('id',$member_type_id)->first();
            // dd($code);
            $maxMemberCode = DB::select( DB::raw( "select max(t.member_code) as max_member_code from (select member_code from basic_users where member_type_id = $member_type_id) as t  " ));
            $member_code = $maxMemberCode[0]->max_member_code+1;
            $full_code = $code->code.$member_code;
            // dd($full_code);


            $user = User::create($request->fillUser($full_code));
            $basicUser = BasicUser::create($request->fillBasicUsers($user->id, $member_code));

            if(!File::exists( public_path('img') )) {
            File::makeDirectory( public_path('img') );
            }
            if (!File::exists( public_path('img/basic_user') )) {
              File::makeDirectory( public_path('img/basic_user') );
            }


            if ($request->hasFile('user_image')) {
              $userImage = $request->file('user_image');
              $extension = $userImage->getClientOriginalExtension();
              $filename = time();
              $location = public_path('img/basic_user/'.$filename.$extension);
              Images::make($userImage)->save($location);

              $image = new Image;
              $image->name = $filename;
              $image->path = 'img/basic_user/'.$filename.'.'.$extension;
              $user->images()->save($image);
            }
        });

        return redirect()->back()
                     ->with('success','User Created Successfully');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::with('basicUser')->with('images')->findOrFail($id);
        // dd($user);
        $basicUser = BasicUser::where('user_id',$id)->first();
        $year = Year::pluck('year','id');
        $memberType = MemberType::pluck('title','id');
        $section = Section::pluck('title','id');
        $program = Program::pluck('title','id');
        $class = IlmsClass::select('title','id')->where('program_id',$basicUser->program_id)->get()->pluck('title','id');
        $batch = Year::select('batch')->where('id',$basicUser->year_id)->first();
        
        return view('basicuser.edit',compact('user'))->withYears($year)
                                      ->withMemberTypes($memberType)
                                      ->withSections($section)
                                      ->withClasses($class)
                                      ->withPrograms($program)
                                      ->withBatch($batch->batch)
                                      ->withEdit("true");
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(BasicUserUpdateRequest $request, $id,User $user)
    {
        // dd($user);
        DB::transaction(function() use ($request, $id){
            $user = User::findOrFail($id);
            $User = BasicUser::where('user_id',$id)->first();


              /**
             * email should not be same to other while updateing
             * but could be same to same user
             */
            $email_exist = User::where('email', $request->get('email'))
                                ->where('id', '!=', $user->id)
                                ->exists();
            if($email_exist){
                return back()->with(['email_exist'=>'Email already exist']);
            }


            if ($request->hasFile('user_image')) {
              // dd("if");
              $userImage = $request->file('user_image');
              if($user->images){

                // $filename = time().'.'.$userImage->getClientOriginalExtension();
                // $location = public_path('img/basic_user/'.$filename);
                // Images::make($userImage)->save($location);

                // $image = new Image;
                // $image->name = $filename;
                // $image->path = 'img/basic_user/'.$filename;
                
                // dd();
                $user->images->upload($userImage,'img/basic_user/');
              }else{
                
                $user->images()->create(['name' => time().'-'.str_slug(pathinfo($userImage->getClientOriginalName(), PATHINFO_FILENAME)), 'path' => ""])->upload($userImage,'img/basic_user/');
              }
            }else{
              if($user->images){
                $user->images->delete();
                // $user->images->removeImage();
              }

            }


            User::where('id',$id)->update($request->updateUsers());
            BasicUser::where('user_id',$id)->update($request->updateBasicUsers());


        });

        return redirect('basicUser')
                     ->with('success','User Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
