<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Genre;
use App\Subject;
use DB;

class GenreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $genres = Genre::all();

        return view('genre.index',compact('genres'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $subjects = subject::pluck('subject_name','id');
        
         return view('genre.create',compact('subjects'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'genre_name' => 'required|unique:genres',
            'subject_id' => 'required',
        ]);
        $genre = new Genre();
        $genre->genre_name = $request->input('genre_name');
        $genre->slug = $this->setUniqueSlug($genre->genre_name,0);
        $genre->subject_id = $request->input('subject_id');
        $genre->active = $request->get( 'active')? 1: 0;
        $genre->save();
        
         $genres = Genre::all();

        return view('genre.index',compact('genres'))->with('success','Genre created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  string $slug
     * @return \Illuminate\Http\Response
     */
   public function edit($slug)
    {
        $genre = Genre::whereSlug($slug)->first();
        $subjects = Subject::pluck('subject_name','id');
        
        return view('genre.edit',compact('genre','subjects'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string $slug
     * @return \Illuminate\Http\Response
     */
     public function update(Request $request, $slug)
    {
        $this->validate($request, [
            'genre_name' => 'required',
            'subject_id' => 'required',
        ]);
        $genre = Genre::whereSlug($slug)->first();
        $genre->genre_name = $request->input('genre_name');
        $genre->subject_id = $request->input('subject_id');
        $genre->active = $request->get( 'active')? 1: 0;
        $genre->save();

         $genres = Genre::all();

        return view('genre.index',compact('genres'))->with('success','Genre updated successfully');
        

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  string $slug
     * @return \Illuminate\Http\Response
     */
    public function destroy($slug)
    {
          Genre::whereSlug($slug)->delete();

        return redirect()->back()
                        ->with('success','Genre deleted successfully');
    }

      /**
     * Recursive routine to set a unique slug.
     *
     * @param string $title
     * @param mixed $extra
     */
    protected function setUniqueSlug( $title, $extra )
    {
        $slug = str_slug( $title . '-' . $extra );
        if ( $text= Genre::whereSlug( $slug )->exists() ) {
            $slug = $this->setUniqueSlug( $title, $extra + 1 );
            return $slug;
        }
        return $slug;
    } 
}
