<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\StaffUser;
use DB;
use App\Image;
use Intervention\Image\ImageManagerStatic as Images;
use File;
use App\MemberType;
use App\Department;
use App\Http\Requests\StaffUserRequest;
use App\Http\Requests\StafUserUpdateRequest;

class StaffUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allUser = User::with('staffUser')->where('active','1')->get();
        return view('staff.index',compact('allUser'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $memberType = MemberType::pluck('title','id');
        $department = Department::pluck('title','id');
        return view('staff.create')->withMemberTypes($memberType)
                                   ->withDepartment($department)
                                   ->withEdit("false");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StaffUserRequest $request)
    {
        DB::transaction(function() use ($request){

            $member_type_id = $request->get('member_type_id');

            $code = MemberType::select('code')->where('id',$member_type_id)->first();
            // $code = BasicUser::where('member_type_id',$member_type_id)->get();

            $maxMemberCode = DB::select( DB::raw( "select max(t.member_code) as max_member_code from (select member_code from staff_users where member_type_id = $member_type_id) as t  " ));
            // dd($maxMemberCode);

            $member_code = $maxMemberCode[0]->max_member_code+1;
            // dd($member_code);

            $full_code = $code->code.$member_code;

            $user = User::create($request->fillUser($full_code));
            StaffUser::create($request->fillStaffUsers($user->id, $member_code));

            if(!File::exists( public_path('img') )) {
            File::makeDirectory( public_path('img') );
            }
            if (!File::exists( public_path('img/staff_user') )) {
              File::makeDirectory( public_path('img/staff_user') );
            }


            if ($request->hasFile('user_image')) {
              $userImage = $request->file('user_image');
              $extension = $userImage->getClientOriginalExtension();
              $filename = time();
              $location = public_path('img/staff_user/'.$filename.$extension);
              Images::make($userImage)->save($location);

              $image = new Image;
              $image->name = $filename;
              $image->path = 'img/staff_user/'.$filename.'.'.$extension;
              $user->images()->save($image);
            }
        });

        return redirect()->back()
                     ->with('success','User Created Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::with('staffUser')->with('images')->findOrFail($id);
        // dd($user);
        $staffUser = StaffUser::where('user_id',$id)->first();
        $memberType = MemberType::pluck('title','id');
        $department = Department::pluck('title','id');
        return view('staff.edit',compact('user'))->withMemberTypes($memberType)
                                                 ->withDepartment($department)
                                                 ->withEdit("true");
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StafUserUpdateRequest $request, $id)
    {
        DB::transaction(function() use ($request, $id){
            // dd($id);
            $user = User::findOrFail($id);
            $staffUser = StaffUser::where('user_id',$id)->first();


              /**
             * email should not be same to other while updateing
             * but could be same to same user
             */
            $email_exist = User::where('email', $request->get('email'))
                                ->where('id', '!=', $user->id)
                                ->exists();
            if($email_exist){
                return back()->with(['email_exist'=>'Email already exist']);
            }

            if ($request->hasFile('user_image')) {
              $userImage = $request->file('user_image');
              // $filename = time().'.'.$userImage->getClientOriginalExtension();
              // $location = public_path('img/basic_user/'.$filename);
              // Images::make($userImage)->save($location);

              // $image = new Image;
              // $image->name = $filename;
              // $image->path = 'img/basic_user/'.$filename;
              
              // dd();
              if($user->image){
                $user->images->upload($userImage,'img/staff_user/');
              }else{
                $user->images()->create(['name' => time().'-'.str_slug(pathinfo($userImage->getClientOriginalName(), PATHINFO_FILENAME)), 'path' => ""])->upload($userImage,'img/staff_user/');
              }
            }else{
              if($user->images){
                $user->images->delete();
                // $user->images->removeImage();
              }
            }


            User::where('id',$id)->update($request->updateUsers());
            StaffUser::where('user_id',$id)->update($request->updateStaffUsers());
        });

        return redirect('staffUser')
                     ->with('success','User Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
