<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Subject;
use App\Sector;

class SubjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subjects = Subject::all();

        return view('subject.index',compact('subjects'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $sectors = Sector::pluck('title','id');
        
         return view('subject.create',compact('sectors'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'subject_name' => 'required|unique:subjects',
            'sector_id' => 'required',
        ]);
        $subject = new Subject();
        $subject->subject_name = $request->input('subject_name');
        $subject->slug = $this->setUniqueSlug($subject->subject_name,0);
        $subject->sector_id = $request->input('sector_id');
        $subject->active = $request->get( 'active')? 1: 0;
        $subject->save();
        
        $subjects = Subject::all();

        return view('subject.index',compact('subjects'))->with('success','Subject created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  string $slug
     * @return \Illuminate\Http\Response
     */
   public function edit($slug)
    {
        $subject = Subject::whereSlug($slug)->first();
        $sectors = Sector::pluck('title','id');
        
        return view('subject.edit',compact('subject','sectors'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string $slug
     * @return \Illuminate\Http\Response
     */
     public function update(Request $request, $slug)
    {
        $this->validate($request, [
            'subject_name' => 'required',
            'sector_id' => 'required',
        ]);
        $subject = Subject::whereSlug($slug)->first();
        $subject->subject_name = $request->input('subject_name');
        $subject->sector_id = $request->input('sector_id');
        $subject->active = $request->get( 'active')? 1: 0;
        $subject->save();
        $subjects = Subject::all();

        $subjects = Subject::all();

        return view('subject.index',compact('subjects'))->with('success','subject updated successfully');
        

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  string $slug
     * @return \Illuminate\Http\Response
     */
    public function destroy($slug)
    {
          Subject::whereSlug($slug)->delete();

        return redirect()->back()
                        ->with('success','Subject deleted successfully');
    }

      /**
     * Recursive routine to set a unique slug.
     *
     * @param string $title
     * @param mixed $extra
     */
    protected function setUniqueSlug( $title, $extra )
    {
        $slug = str_slug( $title . '-' . $extra );
        if ( $text= Subject::whereSlug( $slug )->exists() ) {
            $slug = $this->setUniqueSlug( $title, $extra + 1 );
            return $slug;
        }
        return $slug;
    } 
}
