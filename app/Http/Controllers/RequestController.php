<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\RequestBook;
use App\RequestTag as Tag;
use App\Http\Requests\RequestBookRequest;
use DB;
use Auth;

class RequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('student_make_request');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RequestBookRequest $request)
    {

        DB::transaction(function() use ($request){
            // dd($request);
            $request_book = RequestBook::create($request->fillRequest());
            foreach ($request->user_name as $key => $user_id) {
                $tags = new Tag();
                $tags->user_id = $user_id;
                $tags->is_read = 0;
                $tags->request_book()->associate($request_book);
                $tags->save();
            }
        });

        return redirect()->back()->withSuccess(['Your request has been published.']); 

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        $request = RequestBook::whereSlug($slug)->first();
        // dd($request);
        return view('student_approve_request')->withRequest($request);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}
