<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BasicUser;
use App\StaffUser;
use App\User;
use DB;
use App\Year;
use App\MemberType;
use App\Section;
use App\IlmsClass;
use App\Program;
use App\Location;
use App\Policy;
use Intervention\Image\ImageManagerStatic as Images;
// use App\Image;
use File;
use App\Department;
;


class LibUserController extends Controller
{
    //

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $year = Year::all();
        $user = User::all();
        $program = Program::pluck('title','id');
        $department = Department::pluck('title','id');
        $location = Location::pluck('title','id');
        $policy = Policy::pluck('id','description');
        $memberType = MemberType::pluck('title','id');
        $basicUser = User::with('basicUser')->get();
        $staffUser = User::with('staffUser')->get();
        return view('libuser.index',compact('user','year','program','basicUser','staffUser','memberType','policy','location','department'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $location = Location::pluck('title','id');
        $policy = Policy::pluck('description','id');
         $year = Year::pluck('year','id');
        $memberType = MemberType::pluck('title','id');
        // $section = Section::pluck('title','id');
        // $class = IlmsClass::pluck('title','id');
        $program = Program::pluck('title','id');
		$department = Department::pluck('title','id');
        return view('libuser.create',compact('policy','location'))->withYears($year)
                                      ->withMemberTypes($memberType)
                                      // ->withSections($section)
                                      // ->withClasses($class)
                                      ->withPrograms($program)
                                      ->withDepartment($department)
                                      ->withEdit("false");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
