<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Section;
use App\IlmsClass;
use DB;

class SectionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sections = Section::all();

        return view('section.index',compact('sections'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $classes = IlmsCLass::pluck('title','id');
        
         return view('section.create',compact('classes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $this->validate($request, [
            'title' => 'required',
            'class_id' => 'required',
        ]);
        $section = new Section();
        $section->title = $request->input('title');
        $section->slug = $this->setUniqueSlug($section->title,0);
        $section->description = $request->input('description');
        $section->class_id = $request->input('class_id');
        $section->status = $request->get( 'status')? 1: 0;

        // dd($section);
        $section->save();
        
        $sections = Section::all();

        return view('section.index',compact('sections'))->with('success','Section created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  string $slug
     * @return \Illuminate\Http\Response
     */
   public function edit($slug)
    {
        $section = Section::whereSlug($slug)->first();
        $classes = IlmsCLass::pluck('title','id');
        
        return view('section.edit',compact('section','classes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string $slug
     * @return \Illuminate\Http\Response
     */
     public function update(Request $request, $slug)
    {
        $this->validate($request, [
            'title' => 'required',
            'class_id' => 'required',
        ]);
        $section = Section::whereSlug($slug)->first();
        $section->title = $request->input('title');
        $section->description = $request->input('description');
        $section->class_id = $request->input('class_id');
        $section->status = $request->get( 'status')? 1: 0;
        $section->save();

        $sections = Section::all();

        return view('section.index',compact('sections'))->with('success','Section updated successfully');
        

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  string $slug
     * @return \Illuminate\Http\Response
     */
    public function destroy($slug)
    {
          Section::whereSlug($slug)->delete();

        return redirect()->back()
                        ->with('success','Section deleted successfully');
    }

      /**
     * Recursive routine to set a unique slug.
     *
     * @param string $title
     * @param mixed $extra
     */
    protected function setUniqueSlug( $title, $extra )
    {
        $slug = str_slug( $title . '-' . $extra );
        if ( $text= Section::whereSlug( $slug )->exists() ) {
            $slug = $this->setUniqueSlug( $title, $extra + 1 );
            return $slug;
        }
        return $slug;
    } 
}
