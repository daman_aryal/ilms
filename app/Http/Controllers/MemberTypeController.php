<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MemberType;
use App\Policy;
use DB;
use App\MemberTypePolicy;

class MemberTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $members = MemberType::all();
        return view('membertype.index',compact('members'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       $policy = Policy::all();
       $member = null;
        return view('membertype.create',compact('policy','member'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
         $this->validate($request, [
            'title' => 'required',
            'code' => 'required|unique:member_type',
        ]);
         
         DB::transaction(function() use ($request){
            $title = $request->input('title');
            $membertype = new MemberType();
            $membertype->title = $request->input('title');
            $membertype->code = $request->input('code');
            $membertype->slug = $this->setUniqueSlug($title,0);
            $membertype->description = $request->input('description');
            $membertype->status = $request->get( 'status')? 1: 0;
            // dd($membertype);
            $membertype->save();
            // dd($membertype->id);
            foreach ($this->getPolicy($request) as $policy) {
            $membertype->policy()->attach($policy);
            }
            // $membertype_policies = new MemberTypePolicy;
            // $membertype_policies->member_type_id = $membertype->id;
            // $membertype_policies->policy_id = $request->policy;
            // $membertype_policies->save(); 
        });
        
         $members = MemberType::all();
        return view('membertype.index',compact('members'))->with('success','Member Type created successfully');
    }

    /**
     * Display the specified resource.
     * 
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        $member = MemberType::whereSlug($slug)->first();

        $policy = Policy::all();
        
        return view('membertype.edit',compact('member','policy'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $slug)
    {
        $this->validate($request, [
            'title' => 'required',
            'code' => 'required',
        ]);
        $membertype = MemberType::whereSlug($slug)->first();
        $membertype->title = $request->input('title');
        $membertype->code = $request->input('code');
        $membertype->description = $request->input('description');
        $membertype->status = $request->get( 'status')? 1: 0;
        $membertype->save();
        MemberType::find($membertype->id)->policy()->detach();
            foreach ($this->getPolicy($request) as $policy) {
            $membertype->policy()->attach($policy);
            }

        $members = MemberType::all();
        return view('membertype.index',compact('members'))->with('success','Member Type updated successfully');
        

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($slug)
    {
        MemberType::whereSlug($slug)->delete();

        return redirect()->back()
                        ->with('success','Member Type deleted successfully');
    }

       /**
     * Recursive routine to set a unique slug.
     *
     * @param string $title
     * @param mixed $extra
     */
    protected function setUniqueSlug( $title, $extra )
    {
        $slug = str_slug( $title . '-' . $extra );
        if ( $text=MemberType::whereSlug( $slug )->exists() ) {
            $slug = $this->setUniqueSlug( $title, $extra + 1 );
            return $slug;
        }
        return $slug;
    } 

    public function getPolicy(Request $request){
        return $request->policy;
    }
    
}
