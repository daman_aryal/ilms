<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use App\User;
use App\BasicUser;
use App\StaffUser;
use App\Transaction;
use Carbon\Carbon;
use App\LocationUser;
// use App\Transaction;
use App\Stock;
use App\StockDetail;
use Illuminate\Support\Facades\Validator;

class DispatchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::all();
        $basic = BasicUser::all();
        $staff = StaffUser::all();
        return view('dispatch.index',compact('user','basic','staff'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Transaction $transaction)
    {
        // dd($request->user);
        // // dd($request->from);
        // $this->validate($request,[
        //         'title'=> 'required',
        //         'from'=>'required|date',
        //         'to'=>'required|date',
        //         'user'=>'required'

        //     ]);
        // dd($request->to);

        $validator = Validator::make($request->all(), [
                'title'=> 'required',
                'from'=>'required|date',
                'to'=>'required|date',
                'user'=>'required'
        ]);

        if ($validator->fails()) {
            // return response()->json(['dispatched'=>'unsuccess','msg'=>'Something went wrong with your inputs.']);
            return response()->json(['dispatched'=>'unsuccess','msg'=>$validator]);
        }

        $datetime1 = Carbon::parse($request->from); 
        $datetime2 = Carbon::parse($request->to);
        // dd($request->to);
        $interval = $datetime1->diffInDays($datetime2);
        // dd($interval);
        $user_id = $request->user;
        $location_id = LocationUser::where('user_id',Auth::user()->id)->first();
        // dd($location_id);
        if ($transaction->checkDate($user_id) < $interval) {

            return response()->json(['dispatched'=>'unsuccess','msg'=>'You Are not allowed to hold book For Such time ']);
        }       

        if ((empty($request->from) && empty($request->to)) || ($request->from > $request->to)) {
            return response()->json(['dispatched'=>'unsuccess','msg'=>'Please enter proper date']);
        }
        // dd($transaction->checkNumber($user_id));
        // dd($transaction->checkNumber($user_id));
        if ($transaction->checkNumber($user_id)) {
            return response()->json(['dispatched'=>'unsuccess','msg'=>'You Have already borrowed Maximun number of books. ']);    
        }
        $code = $request->title;

        $stockMaster = StockDetail::select('stock_id','id','active','available_status','book_lock_status')->where('code',$code)->first();
        // dd($stockMaster->available_status);
        if (!isset($stockMaster)) {
            return response()->json(['dispatched'=>'error','msg'=>'Book not available in database.']);
        }

        //check if book is in transaction i.e. outside the library
        // $checkAvalability = StockDetail::where('code',$code)->first();
        // dd($checkAvalability->available_status);
        if($stockMaster->active == 0){
            $response = array(
              'dispatched' => 'damaged',
              'msg' => 'Book is currently damaged or unavailable.'
            );
            return response()->json($response);
        }

        //check if all book is already requested for and if book is available for force dispatch
        $checkBookLock = StockDetail::where('stock_id',$stockMaster->stock_id)->where('book_lock_status',0)->where('active',1)->exists();
        $aa = StockDetail::where('stock_id',$stockMaster->stock_id)->where('book_lock_status',0)->where('active',1)->first();
        // dd($aa->id);
        if(!$checkBookLock){
            $response = array(
              'dispatched' => 'unavailable',
              'msg' => 'All books are requested and unavailable.'
            );
            return response()->json($response);
        }



        $input = [
            'code' => $request->title,
            'reference_id' => 0,
            'user_id' => $user_id,
            'status' => '2',
            'stock_id' => $stockMaster->stock_id,
            'stock_detail_id'=>$stockMaster->id,
            'created_by' => $user_id,
            'location_id'=>$location_id->location_id,
            'mac_address'=>null,
            'comp_name'=>gethostbyaddr($_SERVER['REMOTE_ADDR']),
            'book_from' =>$request->from,
            'book_to' =>  $request->to,
            'direct_dispatch_status' =>1,
            'updated_by' => null
        ];

        $dbTransaction = DB::transaction(function() use ($input,$stockMaster,$code){

              //now locking another book if entered code matches another virtual book code
            // dd($stockMaster->available_status);
          if ($stockMaster->book_lock_status == 1 && $stockMaster->available_status == 1) {
            // dd($stockMaster->stock_id);
            $lock_another = StockDetail::where('stock_id',$stockMaster->stock_id)->where('book_lock_status',0)->where('active',1)->first();
            // dd($lock_another->book_lock_status);
            $lock_another->update(['book_lock_status'=> 1]);
          }


        StockDetail::where('id',$stockMaster->id)->update(['available_status'=>0, 'book_lock_status' => 1]);
                            $transaction = Transaction::create($input);
                            $transaction->update(['reference_id'=>$transaction->id]);
                            
                        });
        return response()->json(['dispatched'=>'success','msg'=>'Sucessfully dispatched this book']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
