<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\MemberType;
use App\StaffUser;
use DB;

class MemberTypeControllerStaff extends Controller
{
        public function getCode(Request $request){

    	if (null==($request->get('member_type_id'))) {
    		return response()->json(['code' => "" ,'member_code' => ""]);
    	}else{
    		
	    	$member_type_id = $request->get('member_type_id');

	    	$code = MemberType::select('code')->where('id',$member_type_id)->first();
	    	// $code = BasicUser::where('member_type_id',$member_type_id)->get();

	    	$maxMemberCode = DB::select( DB::raw( "select max(t.member_code) as max_member_code from (select member_code from staff_users where member_type_id = $member_type_id) as t  " ));
	    	// dd($maxMemberCode);

	    	$member_code = $maxMemberCode[0]->max_member_code+1;

	    	$full_code = $code->code."-".$member_code;
	    	

	    	return response()->json(['code' => $full_code ,'member_code' => $member_code]);
    	}
    }
}
