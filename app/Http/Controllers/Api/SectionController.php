<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Section;

class SectionController extends Controller
{
    public function getSection(Request $request){
    	// dd($request->get('class_id'));
    	if (null==($request->get('class_id'))) {
	    	return response()->json("");
	   	}else{
	    	$class_id = $request->get('class_id');
	    	// dd($class_id);
	    	$section = Section::select('id','title')->where('class_id',$class_id)->where('status',1)->get();

	    	return response()->json($section);
	   		
	   	}
    }
}
