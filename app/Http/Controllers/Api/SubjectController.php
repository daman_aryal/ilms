<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Subject;
use Validator;

class SubjectController extends Controller
{
    // public function getSubject(Request $request){
    //   $query = $request->get('subject_name');
    //   $subject = Subject::select('subject_name')->where('subject_name', 'like', "%$query%")
    //                                         ->get();
    //   return response()->json($subject);
    // }

    public function storeSubject(Request $request){

      $this->validate($request, [
        'subjectName' => 'required',
      ]);
      $countSubject = Subject::where('subject_name',$request ->subjectName)->get()->count();
      if ( $countSubject > 0) {
        $response = array(
              'status' => 'false',
              'msg' => 'Subject Already In Database.',
          );
        return \Response::json($response);
      }else {
        $subject = new Subject;

        $subject ->subject_name = $request ->subjectName;

        $subject ->save();

        $response = array(
          'status' => 'true',
          'msg' => 'Subject Added Successfully.',
        );

        return \Response::json($response);
      }


    }

    public function getSubject(Request $request){
      // dd($request->sector_id);
      $subject = Subject::select('id','subject_name')->where('sector_id',$request->sector_id)->where('active',1)->get();
      // dd($subject[0]->id);
      return \Response::json($subject);
    }
}
