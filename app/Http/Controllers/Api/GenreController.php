<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Genre;

class GenreController extends Controller
{
  // public function get(Request $request){
  //   $query = $request->get('genre_name');
  //   $genre = Genre::select('genre_name')->where('genre_name', 'like', "%$query%")
  //                                         ->get();
  //   return response()->json($genre);
  // }

  public function store(Request $request){

    $this->validate($request, [
      'genreName' => 'required',
    ]);

    $genre_exists = Genre::where('genre_name',$request ->genreName)->exists();
    if ($genre_exists) {
      $response = array(
            'status' => 'false',
            'msg' => 'Genre Already In Database.',
        );
      return \Response::json($response);
    }else {

      $genre = new Genre;

      $genre ->genre_name = $request ->genreName;

      $genre ->save();

      $response = array(
        'status' => 'true',
        'msg' => 'Genre Added Successfully.',
      );

      return \Response::json($response);
    }
  }

  public function get(Request $request){

          // dd($request->sector_id);
      $genre = Genre::select('id','genre_name')->where('sector_id',$request->sector_id)->where('active',1)->get();
      // dd($genre[0]->id);
      return \Response::json($genre);
  }
}
