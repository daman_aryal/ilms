<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\IlmsClass;

class ClassController extends Controller
{
    public function getClass(Request $request){
    	if (null==($request->get('program_id'))) {
	    	return response()->json("");
	   	}else{
	    	$program_id = $request->get('program_id');
	    	// dd($program_id);
	    	$class = IlmsClass::select('id','title')->where('program_id',$program_id)->where('status',1)->get();

	    	return response()->json($class);
	   		
	   	}
    }
}
