<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Publication;

class PublicationController extends Controller
{
    public function getPublication (Request $request){
            $query = $request->get('publication_name');
            $publication = Publication::select('publication_name')->where('publication_name', 'like', "%$query%")
                                                  ->get();
            return response()->json($publication);

    }

    public function store(Request $request){

      $this->validate($request, [
        'publicationName' => 'required',
      ]);

      $countPublication = Publication::where('publication_name',$request ->publicationName)->exists();
      if ($countPublication) {
        $response = array(
              'status' => 'false',
              'msg' => 'Publication Already In Database.',
          );
        return \Response::json($response);
      }else {

        $publication = new Publication;

        $publication ->publication_name = $request ->publicationName;
        $publication->slug = $this->setUniqueSlug($publication->publication_name,0);

        $publication ->save();

        $response = array(
          'status' => 'true',
          'msg' => 'Publication Added Successfully.',
        );

        return \Response::json($response);
      }
    }

       /**
    * Recursive routine to set a unique slug.
     *
     * @param string $publication_name
     * @param mixed $extra
     */
    protected function setUniqueSlug( $publication_name, $extra )
    {
        $slug = str_slug( $publication_name . '-' . $extra );
        if ( $text= Publication::whereSlug( $slug )->exists() ) {
            $slug = $this->setUniqueSlug( $publication_name, $extra + 1 );
            return $slug;
        }
        return $slug;
    } 
}
