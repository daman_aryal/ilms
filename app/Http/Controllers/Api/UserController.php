<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;

class UserController extends Controller
{
    public function getUser(Request $request){

      $query = $request->get('user_name');
                                               
      $users = User::select('id','name','code')->whereNotNull('code')->where('name', 'like', "%$query%")
                                               ->orWhere('code','like',"%$query%")
                                               ->get();


      return \Response::make(json_encode(array(
          'items' => $users,

      ), JSON_PRETTY_PRINT))->header('Content-Type', "application/json");
      // return response()->json($authors);
    }
}
