<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Author;
use App\Image;
use Intervention\Image\ImageManagerStatic as Images;
use DB;
use File;


class AuthorController extends Controller
{
    public function store(Request $request){

      // dd($request);
      // DB::transaction(function () use ($request) {

        $this->validate($request, [
          'authorName' => 'required',
        ]);


        $author = new Author;

        $author ->author_name          = title_case($request ->authorName);
        $author ->link                 = $request ->authorLink;
        $author ->persona_information  = $request ->authorAbout;
        $author->slug = $this->setUniqueSlug($author->author_name,0);

        $author ->save();

        if(!File::exists( public_path('img') )) {
            File::makeDirectory( public_path('img') );
        }
        if (!File::exists( public_path('img/author') )) {
          File::makeDirectory( public_path('img/author') );
        }


        if ($request->hasFile('author_image')) {
          $authorImage = $request->file('author_image');
          $extension = $authorImage->getClientOriginalExtension();
          $filename = time();
          $location = public_path('img/author/'.$filename.'.'.$extension);
          // dd($location);
          Images::make($authorImage)->save($location);

          $image = new Image;
          $image->name = $filename;
          $image->path = 'img/author/'.$filename.'.'.$extension;
          $author->images()->save($image);
        }
      // });
      $response = array(
            'status' => 'true',
            'msg' => 'Author created successfully'
        );

      return response()->json($response);
    }

    public function getAuthor(Request $request){

      $query = $request->get('author_name');
      $authors = Author::select('id','author_name','persona_information')->where('author_name', 'like', "%$query%")
                                            ->get();

      foreach ($authors as $key => $author) {
        $author->getImageUrlAttribute();
      }

      return \Response::make(json_encode(array(
          'items' => $authors,

      ), JSON_PRETTY_PRINT))->header('Content-Type', "application/json");
      // return response()->json($authors);
    }

        /**
    * Recursive routine to set a unique slug.
     *
     * @param string $title
     * @param mixed $extra
     */
    protected function setUniqueSlug( $title, $extra )
    {
        $slug = str_slug( $title . '-' . $extra );
        if ( $text= Author::whereSlug( $slug )->exists() ) {
            $slug = $this->setUniqueSlug( $title, $extra + 1 );
            return $slug;
        }
        return $slug;
    } 
}
