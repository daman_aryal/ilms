<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Transaction;
use Carbon\Carbon;
use DB;
use App\StockDetail;
use Auth;
use App\LocationUser;
use App\BookReservation;
use App\Fine;


class TransactionController extends Controller
{
  public function dispatch_stock(Request $request, $transaction_id, $stock_id,$stock_detail_id, Transaction $transaction)
    {
      $code = $request->get('code');
      $checkCode = StockDetail::where('code',$code)->where('stock_id',$stock_id)->exists();
      // dd($checkCode);
      if (!$checkCode) {
          $response = array(
            'status' => 'error',
            'msg' => 'Book code doesnot match.'
          );
          return response()->json($response);
      }

      DB::transaction(function() use ($transaction_id, $stock_id,$stock_detail_id, $transaction, $code){
        $transaction->create_new_stauts($transaction_id, 2,$code);
        StockDetail::where('id',$stock_detail_id)->update(['available_status'=>0, 'book_lock_status' => 1]);
      });

      $response = array(
            'status' => 'true',
            'msg' => 'Succesfully Dispatched.'
      );
      return response()->json($response);
    }

    public function return_stock(Request $request,$transaction_id, $stock_id,$stock_detail_id,$user_id, Transaction $transaction){
      $code = $request->get('code');
      $book_to = $request->get('date_to');
      $checkTransaction = new Transaction;
      //fine calculation code start  
      $todayDate = Carbon::now();
      $datetime2 = Carbon::parse($book_to);

      if($todayDate > $datetime2)
      {
         $day = $todayDate->diffInDays($datetime2);
         $priceDay = $checkTransaction->getFine($user_id)->per_day_for_week;
         $priceWeek = $checkTransaction->getFine($user_id)->per_day_more_than_week;
         $fine = 0;
         if ($day < 7)
         $fine = $day * $priceDay;
         else
         $fine = $day * $priceWeek;
      }
      else
      {
         $fine = 0;
         $day=0;
      }
      //fine calculation end
      $book = Transaction::where('code', $code)
                  ->latest()
                  ->get()
                  ->max();
     if ($fine != 0) {
       $input = [
            'transaction_id'=>$transaction_id,
            'amount' => $fine,
            'user_id' => $user_id,
            'paid' => 0,
            'duration' =>$day
        ]; 
     }
     else
     {

     $input = 0;
     }
        

      if ($book->status == 2) {
        DB::transaction(function() use ($transaction_id, $stock_id,$stock_detail_id, $transaction,$input,$fine){

          $transaction->create_new_stauts($transaction_id, 4);

          StockDetail::where('id',$stock_detail_id)->update(['active'=>1,'available_status'=>1, 'book_lock_status' => 0]);
          if($fine!=0){
            $fine_entry = Fine::create($input);
            }
        });

         $response = array(
              'status' => 'true',
              'msg' => 'Succesfully Returned.'
        );
    
      }
      else
      {
         $response = array(
            'status' => 'error',
            'msg' => 'Book Code did not match.'
         );
      }
      return response()->json($response);

    }

}
