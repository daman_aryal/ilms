<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\StockDetail;

class StockDetailController extends Controller
{
    public function updateStatus(Request $request){
      // dd($request->status);
      $stockdetail = StockDetail::where('id',$request->id)->first();

      if ($request->status == "true") {
        $status = 1;
      }
      else{
        $status = 0;
      }
      $stockdetail->active = $status;
      $stockdetail->save();

      $response = array(
            'status' => 'success',
            'msg' => 'Author created successfully',
        );

      return response($response);
    }
}
