<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Year;

class YearController extends Controller
{
    public function getBatch(Request $request){
	    if (null==($request->get('year_id'))) {
	    	return response()->json("");
	   	}else{
	    	$year_id = $request->get('year_id');

	    	$batch = Year::select('batch')->where('id',$year_id)->first();

	    	return response()->json($batch->batch);
	   		
	   	}
	}
}
