<?php

namespace App\Http\Controllers;
use App\Author;
use App\Image;
use Intervention\Image\ImageManagerStatic as Images;
use DB;
use File;

use Illuminate\Http\Request;

class AuthorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $authors = Author::all();
        return view('author.index',compact('authors'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('author.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
         $this->validate($request, [
            'author_name' => 'required'
        ]);
        $author = new Author();
        $author->author_name = title_case($request->input('author_name'));
        $author->slug = $this->setUniqueSlug($author->author_name,0);
        $author->link = $request->input('link');
        $author->persona_information = $request->input('persona_information');
        $author->save();

        if(!File::exists( public_path('img') )) {
            File::makeDirectory( public_path('img') );
        }
        if (!File::exists( public_path('img/author') )) {
          File::makeDirectory( public_path('img/author') );
        }


        if ($request->hasFile('author_image')) {
          $authorImage = $request->file('author_image');
          $filename = time();
          $location = public_path('img/author/'.$filename);
          Images::make($authorImage)->save($location);

          $image = new Image;
          $image->name = $filename;
          $image->path = 'img/author/'.$filename.'.'.$authorImage->getClientOriginalExtension();
          $author->images()->save($image);
        }
        $authors = Author::all();
        return view('author.index',compact('authors')->with('success','Author created successfully'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

   /**
     * Show the form for editing the specified resource.
     *
     * @param  string $slug
     * @return \Illuminate\Http\Response
     */
   public function edit($slug)
    {
        $author = Author::whereSlug($slug)->first();
        
        return view('author.edit',compact('author'));
    }

     /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string $slug
     * @return \Illuminate\Http\Response
     */
     public function update(Request $request, $slug)
    {
        $this->validate($request, [
            'author_name' => 'required'
        ]);
        $author = Author::whereSlug($slug)->first();
        $author->author_name = title_case($request->input('author_name'));
        $author->link = $request->input('link');
        $author->persona_information = $request->input('persona_information');
        $author->save();

        if ($request->hasFile('author_image')) {
              // dd("if");
              $authorImage = $request->file('author_image');
              if($author->images){

                // $filename = time().'.'.$authorImage->getClientOriginalExtension();
                // $location = public_path('img/basic_author/'.$filename);
                // Images::make($authorImage)->save($location);

                // $image = new Image;
                // $image->name = $filename;
                // $image->path = 'img/basic_author/'.$filename;
                
                // dd();
                $author->images->upload($authorImage,'img/author/');
              }else{
                $author->images()->create(['name' => time().'-'.str_slug(pathinfo($authorImage->getClientOriginalName(), PATHINFO_FILENAME)), 'path' => ""])->upload($authorImage,'img/author/');
              }
            }else{
              if($author->images){
                $author->images->delete();
                // $user->images->removeImage();
              }

            }
        
        $authors = Author::all();
        return view('author.index',compact('authors')->with('success','Author Updated successfully');
    }

     /**
     * Remove the specified resource from storage.
     *
     * @param  string $slug
     * @return \Illuminate\Http\Response
     */
    public function destroy($slug)
    {
         Author::whereSlug($slug)->delete();

        return redirect()->back()
                        ->with('success','Author deleted successfully');
    }

       /**
    * Recursive routine to set a unique slug.
     *
     * @param string $title
     * @param mixed $extra
     */
    protected function setUniqueSlug( $title, $extra )
    {
        $slug = str_slug( $title . '-' . $extra );
        if ( $text= Author::whereSlug( $slug )->exists() ) {
            $slug = $this->setUniqueSlug( $title, $extra + 1 );
            return $slug;
        }
        return $slug;
    } 
}
