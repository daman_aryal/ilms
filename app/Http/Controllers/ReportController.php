<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Fine;
use App\FineAccount;
use App\User;
use App\LocationUser;
use App\Transaction;
use App\MemberType;

class ReportController extends Controller
{
	public function fine_index()
	{
		$users = User::pluck('name', 'id')->where('code',null);
		return view('report.fine_index',compact('users'));
	}

	public function transaction_index()
	{
		$users = User::pluck('name', 'id');
		return view('report.transaction_index',compact('users'));
	}

    public function fineReport(Request $request)
    {
    	$this->validate($request, [
            'to' => 'required',
            'from' => 'required',
        ]);
        $basicUser = User::with('basicUser')->get();
        $staffUser = User::with('staffUser')->get();
        $memberType = MemberType::pluck('title','id');
    	$date_to= $request->get('to');
    	$date_from= $request->get('from');
    	$to = $request->get('to');
    	$from = $request->get('from');
    	$report_g_user = LocationUser::select('location_id')->where('user_id',Auth::user()->id)->first();
    	$title = "Fine Collection Report";
    	if($request->get('users')){

        $user = $request->get('users');
    	$user_name = User::find($user);
    	$name = $user_name->name; 
    	}
    	$name = null; 
    	if($report_g_user->location_id ==1){
    		$location = "Bharatpur-13(kailashnagar), Chitwan, Nepal";
    	}
    	else{
    		$location = "Bharatpur-10, Chitwan, Nepal";	
    	}
    	if ($request->get('all') == "on") {
    	$fines = FineAccount::where('created_at','>=',$date_from)->where('created_at','<=',$date_to)->get();
    	}
    	else{

    	$fines = FineAccount::where('created_at','>=',$date_from)->where('created_at','<=',$date_to)->where('received_by',$user)->get(); 
    	}
        // dd($fines);
        $finee = new Fine;
        $title = "Fine Collection Report";
    	return view('report.fine_report',compact('fines','finee','location','name','date_to','date_from','title'));
    }

    public function finePrint()
    {

    }

    public function transactionReport(Request $request)
    {
    	$this->validate($request, [
            'to' => 'required',
            'from' => 'required',
        ]);
    	$date_to= $request->get('to');
    	$date_from= $request->get('from');
    	$report_g_user = LocationUser::select('location_id')->where('user_id',Auth::user()->id)->first();
    	if($report_g_user->location_id ==1){
    		$location = "Bharatpur-13(kailashnagar), Chitwan, Nepal";
    	}
    	else{
    		$location = "Bharatpur-10, Chitwan, Nepal";	
    	}
    	if($request->get('all'))
    	{
    		$name= null;
    		$transactions = Transaction::select()->where('created_at','>=',$date_from)->where('created_at','<=',$date_to)->get();
    	}
    	else
    	{
    		if($request->get('status') == "dispatched")
    		{
    			if($request->get('users'))
	    		{
			    	$user= $request->get('users');
			    	$report_user = User::find($user);
			    	$name = $report_user->name;
		    	}
		    	else
		    	{

		    		$name=null;
		    	}
		    	if(!$report_user->code)
		    	{
		        	$transactions = Transaction::select()->where('created_at','>=',$date_from)->where('created_at','<=',$date_to)->where('created_by',$user)->where('status',2)->get();
		    	}
		    	else
		    	{
		    		$transactions = Transaction::select()->where('created_at','>=',$date_from)->where('created_at','<=',$date_to)->where('user_id',$user)->where('status',2)->get();
		    	}	
	    		
    		}
    		elseif ($request->get('status') == "returned") 
    		{
    			if($request->get('users'))
	    		{
			    	$user= $request->get('users');
			    	$report_user = User::find($user);
			    	$name = $report_user->name;
		    	}
		    	else
		    	{

		    		$name=null;
		    	}
		    	if(!$report_user->code)
		    	{
		        	$transactions = Transaction::select()->where('created_at','>=',$date_from)->where('created_at','<=',$date_to)->where('created_by',$user)->where('status',4)->get();
		    	}
		    	else
		    	{
		    		$transactions = Transaction::select()->where('created_at','>=',$date_from)->where('created_at','<=',$date_to)->where('user_id',$user)->where('status',4)->get();
		    	}	
    		}
    		else
    		{
    			if($request->get('users'))
	    		{
			    	$user= $request->get('users');
			    	$report_user = User::find($user);
			    	$name = $report_user->name;
		    	}
		    	else
		    	{

		    		$name=null;
		    	}
		    	if(!$report_user->code)
		    	{
		        	$transactions = Transaction::select()->where('created_at','>=',$date_from)->where('created_at','<=',$date_to)->where('created_by',$user)->get();
		    	}
		    	else
		    	{
		    		$transactions = Transaction::select()->where('created_at','>=',$date_from)->where('created_at','<=',$date_to)->where('user_id',$user)->get();
		    	}	
    		}
    	}
    	
        // dd($transactions);
        $trans = new Fine;
        $title = "Transaction Report";
    	return view('report.transaction_report',compact('transactions','trans','title','name','location'));	
    }

    public function bookReport()
    {
    	return view('report.book_report');
    }
}
