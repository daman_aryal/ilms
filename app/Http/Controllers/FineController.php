<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Fine;
use App\FineAccount;
use Carbon\Carbon;
use App\User;
use App\BasicUser;
use App\StaffUser;
use App\LocationUser;
use Auth;
use DB;
use Illuminate\Support\Facades\Validator;


class FineController extends Controller
{
    public function index()
    {
    	return view('fine.index');
    }

    public function userDetail(Request $request)
    {
    	$user_id = $request->get('user');
    	$fine = Fine::where('user_id',$user_id)->where('paid',0)->sum('amount');
        $fineAcc = FineAccount::where('user_id',$user_id)->sum('amount');
        $due = $fine - $fineAcc;
        if ($fineAcc >= $fine) {
            $data = [
                'total_amount' => 'Fines Fully Paid',
                'due' => 'Fines Fully Paid'

            ];
        }else {
            $data= [
                'total_amount' => $fine,
                'due' => $due
            ];
        }
    	return $data;
    }

    public function payment(Request $request)
    {
    	 $validator = Validator::make($request->all(), [
                'user'=> 'required',
                'payment'=>'required|numeric'
        ]);

        if ($validator->fails()) {
            // return response()->json(['dispatched'=>'unsuccess','msg'=>'Something went wrong with your inputs.']);
            return redirect()->back()->with(['status'=>'false','msg'=>$validator]);
        }

    	$user_id = $request->get('user');
    	$payment = $request->get('payment');
        $fine = Fine::where('user_id',$user_id)->where('paid',0)->sum('amount');
        $fineAcc = FineAccount::where('user_id',$user_id)->sum('amount');
    	$receipt_no = date('Ymd').'-'.$user_id;
    	$total_pay = $fineAcc + $payment;
        $due = $fine - $total_pay;
        $input = [
            'user_id' => $user_id,
            'amount' => $payment,
            'received_by' => Auth::user()->id,
            'received_date' => Carbon::now(),
            'receipt_no'=>$receipt_no
        ];

        if ($fine == $total_pay) {
             DB::transaction(function() use ($input,$user_id){

                $fine_entry = FineAccount::create($input);
                $fine_entry = Fine::where('user_id',$user_id)->update(['paid' => 1]);
            });
        }

         DB::transaction(function() use ($input){
            $fine_entry = FineAccount::create($input);
        });
        $receipt_user = new User;
        $basic = BasicUser::where('user_id',$user_id)->first();
        $staff = StaffUser::where('user_id',$user_id)->first();
        $location_id = LocationUser::where('user_id',$user_id)->first();
        $name = User::find($user_id);
         $data = [
            'user' => $name->name,
            'amount' => $payment,
            'received_by' => Auth::user()->name,
            'received_date' => Carbon::now(),
            'receipt_no'=>$receipt_no,
            'user_type' => $receipt_user->GetUserCode($user_id),
            'total' => $fine,
            'due' => $due
         ];
         
        if($location_id ==1){
            $data['location'] = "Central";
            $data['address'] = "Bharatpur-13(kailashnagar), Chitwan, Nepal";

        }
        else
        { 
            $data['location'] = "Hospital";
            $data['address'] = "Bharatpur-10, Chitwan, Nepal"; 

        } 
        if($staff)
        {
            $data[ 'department' ] = $staff->department->title;
            $data[ 'program' ] = null;

        }
        else
        {
            $data[ 'department' ] = null;
            $data[ 'program' ] = $basic->program->title;
            
        }
        return view('fine.receipt',compact('data'));
    }
}
