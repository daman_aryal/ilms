<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Sector;

class SectorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sectors = Sector::all();
        return view('sector.index',compact('sectors'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         return view('sector.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $this->validate($request, [
            'title' => 'required|unique:sectors'
        ]);
        $sector = new Sector();
        $sector->title = $request->input('title');
        $sector->slug = $this->setUniqueSlug($sector->title,0);
        $sector->active = $request->get( 'active')? 1: 0;
        $sector->save();
        $sectors = Sector::all();
        
        $sectors = Sector::all();->with('success','Sector created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  string $slug
     * @return \Illuminate\Http\Response
     */
   public function edit($slug)
    {
        $sector = Sector::whereSlug($slug)->first();
        
        return view('sector.edit',compact('sector'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string $slug
     * @return \Illuminate\Http\Response
     */
     public function update(Request $request, $slug)
    {
         $this->validate($request, [
            'title' => 'required'
        ]);
        $sector = Sector::whereSlug($slug)->first();
        $sector->title = $request->input('title');
        $sector->active = $request->get( 'active')? 1: 0;
        $sector->save();
        $sectors = Sector::all();

                return view('sector.index',compact('sectors'))->with('success','Sector updated successfully');
        

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  string $slug
     * @return \Illuminate\Http\Response
     */
    public function destroy($slug)
    {
         Sector::whereSlug($slug)->delete();

        return redirect()->back()
                        ->with('success','Sector deleted successfully');
    }

       /**
    * Recursive routine to set a unique slug.
     *
     * @param string $title
     * @param mixed $extra
     */
    protected function setUniqueSlug( $title, $extra )
    {
        $slug = str_slug( $title . '-' . $extra );
        if ( $text= Sector::whereSlug( $slug )->exists() ) {
            $slug = $this->setUniqueSlug( $title, $extra + 1 );
            return $slug;
        }
        return $slug;
    } 
}
