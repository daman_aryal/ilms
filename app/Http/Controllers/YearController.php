<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Year;
use DB;

class YearController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $years = Year::all();
        return view('year.index',compact('years'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('year.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
         $this->validate($request, [
            'year' => 'required',
            'batch' => 'required|numeric',
        ]);
        $year = new Year();
        $year->year = $request->input('year');
        $year->batch = $request->input('batch');
        $year->description = $request->input('description');
        $year->slug = $this->setUniqueSlug($year->year,$year->batch,0);
        $year->status = $request->get( 'status')? 1: 0;

        // dd($year);
        $year->save();
        $years = Year::all();
        
        return view('year.index',compact('years'))->with('success','Year and Batch created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  string $slug
     * @return \Illuminate\Http\Response
     */
   public function edit($slug)
    {
        $year = Year::whereSlug($slug)->first();
        
        return view('year.edit',compact('year'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string $slug
     * @return \Illuminate\Http\Response
     */
     public function update(Request $request, $slug)
    {
        $this->validate($request, [
            'year' => 'required',
            'batch' => 'required|numeric',
        ]);
        $year = Year::whereSlug($slug)->first();
        $year->year = $request->input('year');
        $year->batch = $request->input('batch');
        $year->description = $request->input('description');
        $year->status = $request->get( 'status')? 1: 0;
        $year->save();
        $years = Year::all();

        return view('year.index',compact('years'))->with('success','Year and Batch updated successfully');
        

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  string $slug
     * @return \Illuminate\Http\Response
     */
    public function destroy($slug)
    {
         Year::whereSlug($slug)->delete();

        return redirect()->back()
                        ->with('success','Year and Batch deleted successfully');
    }

       /**
     * Recursive routine to set a unique slug.
     *
     * @param string $year
     * @param string $batch
     * @param mixed $extra
     */
    protected function setUniqueSlug( $year,$batch,$extra )
    {
        $slug = str_slug( $year.$batch. '-' . $extra );
        if ( $text=Year::whereSlug( $slug )->exists() ) {
            $slug = $this->setUniqueSlug( $year,$batch,$extra + 1 );
            return $slug;
        }
        return $slug;
    } 
}
