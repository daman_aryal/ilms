<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Program;
use DB;

class ProgramController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $programs = Program::all();
        return view('program.index',compact('programs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('program.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $this->validate($request, [
            'title' => 'required'
        ]);
        $program = new Program();
        $program->title = $request->input('title');
        $program->slug = $this->setUniqueSlug($program->title,0);
        $program->description = $request->input('description');
        $program->status = $request->get( 'status')? 1: 0;
        $program->save();
        $programs = Program::all();
        
         $programs = Program::all();->with('success','Program created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     *  @param  string $slug
     * @return \Illuminate\Http\Response
     */
   public function edit($slug)
    {
        $program = Program::whereSlug($slug)->first();
        
        return view('program.edit',compact('program'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string $slug
     * @return \Illuminate\Http\Response
     */
     public function update(Request $request, $slug)
    {
        $this->validate($request, [
            'title' => 'required',
        ]);
        $program = Program::whereSlug($slug)->first();
        $program->title = $request->input('title');
        $program->description = $request->input('description');
        $program->status = $request->get( 'status')? 1: 0;
        $program->save();
        $programs = Program::all();

                return view('program.index',compact('programs'))->with('success','Program updated successfully');
        

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  string $slug
     * @return \Illuminate\Http\Response
     */
    public function destroy($slug)
    {
         Program::whereSlug($slug)->delete();

        return redirect()->back()
                        ->with('success','Program deleted successfully');
    }

      /**
     * Recursive routine to set a unique slug.
     *
     * @param string $title
     * @param mixed $extra
     */
    protected function setUniqueSlug( $title, $extra )
    {
        $slug = str_slug( $title . '-' . $extra );
        if ( $text=Program::whereSlug( $slug )->exists() ) {
            $slug = $this->setUniqueSlug( $title, $extra + 1 );
            return $slug;
        }
        return $slug;
    } 
}
