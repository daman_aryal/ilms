<?php

namespace App\Http\Controllers;

use App\Author;
use App\Http\Requests;
use App\Http\Requests\StockCreateRequest;
use App\PivotAuthorStockDetail;
use App\Publication;
use App\Stock;
use App\StockDetail;
use App\StockType;
use DB;
use Illuminate\Http\Request;
use App\Subject;
use App\Genre;
use Intervention\Image\ImageManagerStatic as Images;
use Milon\Barcode\Facades\DNS2DFacade as DNS2D;
use Milon\Barcode\Facades\DNS1DFacade as DNS1D; 
use App\Image;
use App\Sector;
use File;
use Session;
use App\Featured;
use Auth;
use App\LocationUser;


class StockController extends Controller
{
	public function index()
	{

      	$stock = Stock::with('rel_stockDetail')->get();
       return view('stock_index',compact('stock','author'));
	}

	public function create(){
	$stock = new StockDetail;
    $stock_type = StockType::pluck('type','id')->toArray();
    $sectors = Sector::where('active',1)->pluck('title','id');
		return view('stock_create',compact('stock_type','stock'))->withSectors($sectors);
	}

	public function store(StockCreateRequest $request){
	// public function store(Request $request){
		// dd($request);
		DB::transaction(function() use ($request){
			$user_location = LocationUser::where('user_id',Auth::user()->id)->first();
			$location_id = $user_location->location_id;
			// dd($location->location_id);
			$stock = Stock::create($request->FillStock());
			if ($request->book_featured == "on") {
				$featured = new Featured();
				$featured->stock_id = $stock->id;
				$featured->created_by = Auth::user()->id;
				$featured->save();
			}
			// dd($featured);

			// dd($request->author_name);
			foreach ($request->author_name as $author) {
						// dd($author);
      			$stock->rel_author()->attach($author);
  			}
				// dd($stock);

			if ($request->hasFile('front')) {
				$frontImage = $request->file('front');
				$filename = time().'.'.$frontImage->getClientOriginalExtension();

				if(!File::exists( public_path('img') )) {
				    File::makeDirectory( public_path('img') );
				}
				if (!File::exists( public_path('img/book') )) {
					File::makeDirectory( public_path('img/book') );
				}
				if (!File::exists( public_path('img/book/front') )) {
					File::makeDirectory( public_path('img/book/front') );
				}
				if (!File::exists( public_path('img/book/back') )) {
					File::makeDirectory( public_path('img/book/back') );
				}

				$location = public_path('img/book/front/'.$filename);
				Images::make($frontImage)->save($location);

				$image = new Image;
				$image->name = $filename;
				$image->path = 'img/book/front/'.$filename;
				$stock->images()->save($image);
			}

			if ($request->hasFile('back')) {
				$frontImage = $request->file('back');
				$filename = time().'.'.$frontImage->getClientOriginalExtension();
				$location = public_path('img/book/back/'.$filename);
				Images::make($frontImage)->save($location);

				$image = new Image;
				$image->name = $filename;
				$image->path = 'img/book/back/'.$filename;
				$stock->images()->save($image);
			}




			// $author = Author::select('id')->where('author_name',$request->author_name)->first();

			// $subject_id = Subject::select('id')->where('subject_name',$request->subject_name)->first();
			// $genre_id = Genre::select('id')->where('genre_name',$request->genre_name)->first();
			// dd($genre_id);
			$publication = Publication::select('id')->where('publication_name',$request->publication_name)->first();

			/***
			* get the latest code  
			*/
			$stockdetailInfo = StockDetail::select('code')->orderBy('id','desc')->first();

			/**
			* for first entry of stock 
			*/
			if ($stockdetailInfo != null) {
				$n = preg_replace("/[^0-9]/","",$stockdetailInfo->code);
			}else{
				$n=0;
			}

			
			for ($i=0; $i < $request->quantity; $i++) {
						$n++;
						$code = "CMC".'-'.$n;
						
						$stockDetail = StockDetail::create($request->FillStockDetail($stock->id,$publication->id,$code,$location_id));

			}
		});
		Session::flash('message', "Stock Created Succesfully.");
		return redirect()->back();
	}

	public function show($id)
    {
    			$stockInfo = Stock::with('images','rel_author')->find($id);
    			// dd($stockInfo->rel_author);
    			// dd(!empty($stockInfo->images));
				$stock = StockDetail::where('stock_id',$id)->with('stock')->get();
				// dd($stock);
				$stockid = $id;
				return view('stock_show',compact('stock'))->withStockid($stockid)->withStockInfo($stockInfo);
    }

  public function edit($id)
    {
        $stock_id = $id;

        $stock = Stock::find($stock_id);
        return view('stock_edit',compact('stock','stock_id'));
    }


 public function update(StockCreateRequest $request, $id)
    {

        DB::transaction(function() use ($request,$id){
         $stock = Stock::find($id);
         $stock->update($request->FillStock());
         $author = Author::find($stock->rel_stockDetail->rel_pivot->author_id);
         $author->update($request->FillAuthor());
         $publication = Publication::find($stock->rel_stockDetail->publication_id);
         $publication->update($request->FillPublication());
         $stockDetail = StockDetail::find($stock->rel_stockDetail->id);
         $stockDetail->update($request->FillStockDetail($stock->id,$publication->id));
         $pivot_author_stockdetail = PivotAuthorStockDetail::find($stock->rel_stockDetail->rel_pivot->id);
         $pivot_author_stockdetail->update(['author_id'=>$author->id,'stock_detail_id' => $stockDetail->id]);
         // $this->uploadFeaturedImage($request, $stock);
        });


          return back()->withInput()->with('success','Stock updated successfully');
     }


 	public function destroy($id)
    {
        // DB::table('stocks')->where('id',$id)->delete();
				Stock::where('id',$id)->delete();
				StockDetail::where('stock_id',$id)->delete();

        return redirect()->back()
                        ->with('success','Stock deleted successfully');
    }

    public function getBarcode($code){

    	// echo DNS1D::getBarcodeHTML("$code","C39+",3,33);
    	// echo DNS2D::getBarcodeHTML("$code", "QRCODE",3,3);
    	$detail = StockDetail::find($code);
    	// dd($detail);
    	return view('barcode')->withDetail($detail);
    	// echo '<img src="data:image/png,' . DNS1D::getBarcodePNG("4", "C39+") . '" alt="barcode"   />';
    }

		// public function getCode(){
		// 	// $dt = new DateTime();
		// 	// echo $dt->format('Y-m-d H:i:s');
		// 	// return DB::select( DB::raw( "select IFNULL(concat(a,'-',maxnum+1),concat(CURRENT_DATE,'-1'))value from (select b.a,max(substring(code,12))maxnum from (select DISTINCT(substring(code,1,10))a,code from stock_details where substring(code,1,10)=CURRENT_DATE)b)c" ));
		// 	// code = $dt->format('Y-m-d')."-1"."-".$i;
		// 	$code = DB::select( DB::raw("select IFNULL(concat(a,'-',maxnum+1),concat(CURRENT_DATE,'-1'))value from (select b.a,max(substring(code,12))maxnum from (select DISTINCT(substring(code,1,10))a,code from stock_details where substring(code,1,10)=CURRENT_DATE)b)c" ));
		// 	dd($code);
		// }


	// private function uploadFeaturedImage($request, $stock)
 //    {


 //        if ($request->hasFile('front'))
 //        {
 //            $image1 = $request->file('front');

 //            if ($stock->image)
 //            {
 //                $stock->image->upload($image1);
 //            } else
 //            {
 //               $stock->image()->create(['front' => cleanFileName($image1)])->upload($image1);
 //            }
 //        }

 //        if ($request->hasFile('back'))
 //        {
 //            $image2 = $request->file('back');

 //            if ($stock->image)
 //            {
 //                $stock->image->upload($image2);
 //            } else
 //            {
 //               $stock->image()->create(['back' => cleanFileName($image2)])->upload($image2);
 //            }
 //        }
 //   }
}
