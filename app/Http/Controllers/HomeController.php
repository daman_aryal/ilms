<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Year;
use App\Subject;
use App\Program;
use App\Genre;
use App\Department;
use App\MemberType;
use App\Sector;
use App\IlmsClass;
use App\Section;
use App\Author;
use App\Publication;
use App\StaffUser;
use App\User;
use App\Stock;
use App\StockDetail;
use App\BasicUser;
use Carbon\Carbon;
use App\FineAccount;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$ip=$_SERVER['REMOTE_ADDR'];
        // $mac_string = shell_exec("arp -a $ip");
        // $mac_array = explode(" ",$mac_string);
        // dd($mac_array);`
        // dd($ip." - ".$mac);
        // dd(exec(ipconfig));
        
       $hostname = gethostbyaddr($_SERVER['REMOTE_ADDR']);

       $countStaff = StaffUser::all()->count();

       $countStudent = BasicUser::all()->count();

       $memberid = MemberType:: where('title', '=', 'teacher')->pluck('id');
       $countTeacher = StaffUser:: where('member_Type_id', '=', $memberid)->count();

       $countDepartment= Department::all()->count();

       $countStock = StockDetail::all()->count();
       $currentDate= Carbon::now();
       $countfine=FineAccount::whereRaw('Date(received_date) = CURDATE()')->sum('amount');
       $counts =array();
       $counts['staffNo'] = $countStaff;
       $counts['studentNo'] = $countStudent;
       $counts['teacherNo'] = $countTeacher;
       $counts['departmentNo'] = $countDepartment;
       $counts['stockNo'] = $countStock;
       $counts['fineNo']=$countfine;
       
    if (Auth::user()->user_type()) {
        if(Auth::user()->user_type() == "su"){
            return view('admin_index', compact('counts'));
        }elseif(Auth::user()->user_type() == "librarian"){
            return view('admin_index', compact('counts'));   
        }elseif(Auth::user()->user_type() == "admin"){
             return view('admin_index', compact('counts'));   
        }elseif(Auth::user()->user_type() == "student"){
            return redirect('home');  
        }else{
            return '/';
        }
         }else{
            return redirect('home');
        }
       // return view('admin_index');
    }
    public function setting(){

        $countSubject = Subject::all()->count();
        $countYear    = Year::all()->count();
        $countProgram = Program::all()->count();
        $countGenre = Genre::all()->count();
        $countDepartment = Department::all()->count();
        $countMemberType = MemberType::all()->count();
        $countSector = Sector::all()->count();
        $countIlmsClass = IlmsClass::all()->count();
        $countSection = Section::all()->count();
        $countAuthor = Author::all()->count();
        $countPublication = Publication::all()->count();
        $count = array();
        $count['subject']=$countSubject;
        $count['year'] =$countYear;
        $count['program']=$countProgram;
        $count['genre']=$countGenre;
        $count['department']=$countDepartment;
        $count['membertype']=$countMemberType;
        $count['sector']=$countSector;
        $count['ilmsclass']=$countIlmsClass;
        $count['section']=$countSection; 
        $count['author']=$countAuthor;
        $count['publication']=$countPublication;        
        return view('setting.index',compact('count'));
    }

    public function password(){
        $user = new User;
        $key = $user->generateRandomClue(6);
        return $key;
    }
}
