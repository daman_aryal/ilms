<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Stock;
use App\StockDetail;
use App\Transaction;
use Auth;
use Hash;
use App\BasicUser;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\User;
use Session;
use App\Featured;

class StudentController extends Controller
{
    public function dashboard(Transaction $transaction){
<<<<<<< HEAD
    	/**
    	 * stock deatil with
    	 * eagar loding realated stock and publication
    	 * @var [type]
    	 */
        $stock_details = StockDetail::with('stock','publication', 'transaction')
                                      ->orderBy('stock_id', 'DESC')
                                      ->get();
        $stock_details_with_status= [];

            foreach($stock_details as $stock_detail){
                /**
                 * 1) transaction_status function will return
                 * latest status of stock detial form transaction table
                * @var [type]
                 */
                
                /**
                 * 1) if qunatity is not equal to 0
                 *    i.e. stock until available, so we dont have to check the booked date
                 *    of related stock_detail, its till available
                 * 2) if stock_detail is not in transaction table
                 *     we dont have to check available date, i.e stock is availble
                 */
                    if($transaction->transaction_status($stock_detail->id) !== null
                        && Stock::where('id', $stock_detail->stock_id)->value('quantity') == 0)
                    {
                        $transaction = $transaction->transaction_status($stock_detail->id);   
                        $stock_detail['to'] = $transaction->book_to->toFormattedDateString();
                        $stock_detail['reference_id'] = $transaction->reference_id;
                        $stock_details_with_status[] = $stock_detail;
                    }else{
                        $stock_details_with_status[] = $stock_detail;
                    }
            }
        return view('student_dashboard', compact('stock_details_with_status'));
=======
        $countTransaction = Transaction::all()->count();
        $countBooks = StockDetail::all()->count();
        $user_id = Auth::user()->id;

        $featuredStock = Featured::with('stock')->latest()->limit(8)->get();


        $latestStock = Stock::select('id')->latest()->limit(8)->with(array('rel_stockDetail'=>function($query){ $query->select('title','publication_date','plot_summary','stock_id')->where('active',1)->get();}))->with(array('images'=>function($query){ $query->select('path','imageable_id');}))->get();
        // dd($latestStock);
        // $latestStock = Stock::latest()->limit(8)->with(array('rel_stockDetail'=>function($query){ $query->select('title','code','stock_id')->where('active',1)->get();}))->get();
        // $latestStock = Stock::with('rel_stockDetail')->where('active',1)->get();
        // dd(($latestStock));
        $booksUserHaveID = DB::select(DB::raw("select distinct t.code,t.id,t.stock_detail_id,t.book_from, t.book_to from (SELECT * FROM transactions WHERE code NOT IN (select distinct code from transactions where status =4)) as t where status = 2 and user_id=".$user_id));
        $booksUserReturned = Transaction::where('user_id',$user_id)->where('status','4')->get();
        // dd($booksUserHaveID[0]);
        // dd($featuredStock[0]->stock->rel_stockDetail[0]->title);
        $countUsers = User::all()->count();
        $stockDetail = new StockDetail();
        // dd($booksUserHaveID[0]->stock_detail_id);
        // dd($stockDetail->getStockDetail($booksUserHaveID[0]->stock_detail_id));
        return view('student_dashboard')->withTransactionCount($countTransaction)
                                        ->withUserCount($countUsers)
                                        ->withBookCount($countBooks)
                                        ->withFeaturedStock($featuredStock)
                                        ->withReturned($booksUserReturned)
                                         ->withBooksUserHave($booksUserHaveID)
                                         ->withStockInfo($stockDetail)
                                         ->withLatestBooks($latestStock);
                                        ;
    }

    public function book()
    {

        $books = StockDetail::distinct()->get(['stock_id','title','plot_summary','edition']);
        $stock = new Stock;
        // dd($books);
        return view('student_books',compact('books'))->withStock($stock);
    }

    public function record()
    {
        $checkTransaction = new Transaction;
        $user_id =  Auth::user()->id;
        // dd($user_id);
        $booksUserReturned = Transaction::where('user_id',$user_id)->where('status','4')->get();
        $booksUserIssued = Transaction::where('user_id',$user_id)->where('status','1')->latest()->get()->groupBy('reference_id');
        $aa = Transaction::select('id','book_to', 'book_from','stock_detail_id')
                            ->where('user_id', $user_id)
                            ->latest()
                            ->get()
                            ->max();
        // $booksUserIssued = Transaction::where([
        //                             ['user_id','=',$user_id],
        //                             ['status','=','0'],
        //                             ['reference_id','=',$booksUserReturned->reference_id]
        //   ])->get();
        // dd($booksUserIssued);

        // return view('student_record', compact('booksUserIssued'))->withReturned($booksUserReturned);

        $stockDetail=new StockDetail;

        $booksUserHaveID = DB::select(DB::raw('select distinct t.code,t.stock_detail_id,t.book_from, t.book_to from (SELECT * FROM transactions WHERE code NOT IN (select distinct code from transactions where status =4)) as t where status = 2 and user_id='.$user_id));

        // $booksUser
        // dd($booksUserHaveID);

        // $booksUserHave = select()->distinct()->get('t.code','t.stock_detail_id')->
        return view('student_record',compact('booksUserIssued','checkTransaction'))->withReturned($booksUserReturned)
                                     ->withBooksUserHave($booksUserHaveID)
                                     ->withStockInfo($stockDetail);
    }


    public function pending()
    {
        return view('student_pending');
    }
// this function is for search code
    public function bookdetail($code)
    {
        // dd($code);

        // dd($code);
        $string = explode("-", $code);
        $bookCode = $string[1];
        $transaction = new Transaction;
        $stock = new Stock;
        $getStockMaster = Stock::where('id',$bookCode)->first();
        // dd($getStockMaster->ge);
        $stockMasterId = $getStockMaster->id;
        // dd($getStockMaster);
        $getAvailableBook = StockDetail::where('stock_id',$getStockMaster->id)->where('available_status',1)->where('book_lock_status',0)->where('active',1)->first();
        // dd($getAvailableBook);
        if ($getAvailableBook) {
            $book = $getAvailableBook;
            $book['transactionStatus'] = '0';
            // dd($book);
            return view('student_book_detail',compact('book','getStockMaster','stock'));
        }else{

            $book = StockDetail::where('stock_id',$getStockMaster->id)->first();
            $book['transactionStatus'] = '2';
            return view('student_book_detail',compact('book','getStockMaster','stock'));

        }
        // dd($book);

        // if ($book->available_status == 1) {
        //     return view('student_book_detail',compact('book'));
        // }else{

        //     if (isset($book)) {
        //         return view('student_book_detail',compact('book'));
        //     }else{
        //         $getBook = StockDetail::where('stock_id',$stock_id)->get();
        //          foreach ($getBook as $key => $book) {
        //             $transactionStatus = $transaction->reservedDate($book->id);
        //             // dd($transactionStatus->book_to);
        //             if (!isset($transactionStatus->book_to)) {
        //                  *
        //                  *   here transactionStatus is set to 0 for in order to inform that booking is available

        //                 $book['transactionStatus'] = '0';
        //                 return view('student_book_detail',compact('book'));
        //             }else{
        //                 // if book is reserved and not available at all.
        //                 /**
        //                  *   here transactionStatus is set to 2 for view only later here date should be compared
        //                 */
        //                $book['transactionStatus'] = '2';
        //                 return view('student_book_detail',compact('book'));
        //             }
        //         }
        //     }
        // }
        // // dd($book->id);
        // $transactionStatus = $transaction->transaction_status($book->id);
        // // dd($transactionStatus->status);
        // $status = array('0' => 0,'1'=>1, '2'=>2, '3'=>3 );
        // if (isset($transactionStatus)) {
        //    if ($transactionStatus->status == 4) {
        //       return view('student_book_detail',compact('book'));
        //    }elseif (in_array($transactionStatus->status, $status) ) {
        //        $bookList = StockDetail::where('stock_id',$book->stock_id)->where('id','!=',$book->id)->get();
        //       foreach ($bookList as $key => $book) {
        //            $transactionStatus = $transaction->transaction_status($book->id);
        //            if (isset($transactionStatus)) {
        //             // dd('here');
        //                if ($transactionStatus->status == 4) {
        //                   return view('student_book_detail',compact('book'));
        //                 }else{
        //                     $book['transactionStatus'] = '0';
        //                     return view('student_book_detail',compact('book'));
        //                 }
        //            }else{

        //             return view('student_book_detail',compact('book'));
        //            }
        //       }

        //    }
        // }else{
        // }


>>>>>>> 3048c40f61220d557f360aa7074850b435f91197
    }

    public function transaction_status(Request $request){
        $transaction = Transaction::where('created_by', Auth::user()->id)
                                            ->get()
                                            ->groupBy('reference_id');
        $lts_transaction_status = [];
        foreach($transaction as $lts){
            $lts_transaction_status[] = $lts->max();
        }
        /**
         * manual pagination for
         * group by fetch
         * @var LengthAwarePaginator
         */
        $page = Input::get('page', 1); // Get the ?page=1 from the url
        $perPage = 10; // Number of items per page
        $offset = ($page * $perPage) - $perPage;

        $lts_transaction_status_paginate =  new LengthAwarePaginator(
            array_slice($lts_transaction_status, $offset, $perPage, true), // Only grab the items we need
            count($lts_transaction_status), // Total items
            $perPage, // Items per page
            $page, // Current page
            ['path' => $request->url(), 'query' => $request->query()] // We need this so we can keep all old query parameters from the url
        );
        //store the same reference id transaction
		return view('student_transaction', compact('lts_transaction_status_paginate'));
	}

    public function searchBook(Request $request){

        $min_length = 3;
        $query = $request->search_book;
        // dd($query);
        // $result = StockDetail::


    // you can set minimum length of the query if you want
     
    if(strlen($query) >= $min_length){ // if query length is more or equal minimum length then
         
         // "SELECT * FROM articles
         //    WHERE (`title` LIKE '%".$query."%') OR (`text` LIKE '%".$query."%')"

        // $raw_results = DB::select( DB::raw("select distinct title,stock_id, plot_summary, publication_date from stock_details WHERE (`title` LIKE '%".$query."%') OR (`plot_summary` LIKE '%".$query."%')" ));

        // $raw_results = StockDetail::where('title', 'LIKE', "%$query%")->where('active','1')->distinct()->get(['title','edition','stock_id','plot_summary','publication_date']);
        $raw_results = DB::table('stock_details')
            ->select('stock_details.title','stock_details.edition','stock_details.stock_id','stock_details.plot_summary','stock_details.publication_date','publications.publication_name','pivot_author_stock_details.author_id','authors.author_name')
            ->join('publications', 'publications.id', '=', 'stock_details.publication_id')
            ->join('pivot_author_stock_details','pivot_author_stock_details.stock_id','=','stock_details.stock_id')
            ->join('authors','authors.id','=','pivot_author_stock_details.author_id')
            ->where('title', 'LIKE', "%$query%")
            ->orWhere('publication_name', 'LIKE', "%$query%")
            ->orWhere('author_name', 'LIKE', "%$query%")
            ->where('active','1')
            ->distinct()->get(['title','edition','stock_id','plot_summary']);
         $stock = new Stock;

         return view('student_book_search')->withResults($raw_results)
                                            ->withStock($stock)
                                            ->withQuery($query);
         
    }
    else{ // if query length is less than minimum
        $raw_results = null;
        $stock = null;
         return view('student_book_search')->withResults($raw_results)
                                            ->withErrors(["Minimum length is $min_length"])
                                            ->withStock($stock)
                                            ->withQuery($query);

    }







    }
    public function editProfile(){
        // dd(Auth::user()->id);
        // $user = BasicUser::select('id','first_name','last_name','date_of_birth')->where('user_id',Auth::user()->id)->first();
        // dd($user);
        // dd(Auth::user()->password);
        return view('editprofile');
    }

    public function updateProfile(Request $request){
        $this->validate($request, [
            'password_old' => 'required',
            'password' => 'required',
            'password_confirm' => 'required|same:password'
        ]);
        // dd(Auth::user()->password);

        $password_old = $request->password_old;
        $new_Password = $request->password;

        $user = User::find(Auth::user()->id);

        if (Hash::check($password_old, Auth::user()->password)) {
            $new_Password = Hash::make($new_Password);
            $user->update(['password' => $new_Password]);
            
            Session::flash('message', "Password Changed Successfully");
            return redirect()->back();  
        }else{
            return redirect()->back()->withErrors(['Your password was incorrect.']);  
        }

        // return redirect('/')->with('success','Profile Updated Successfully');

    }
}
