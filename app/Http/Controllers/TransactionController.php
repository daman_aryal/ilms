<?php

namespace App\Http\Controllers;
use App\Http\Requests\ReserveRequest;
use App\Stock;
use App\StockDetail;
use App\Transaction;
use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Input;
use App\BookReservation;
use App\LocationUser;

class TransactionController extends Controller
{

    public function request_getById($stock_id){
    	$stock_detail = StockDetail::find($stock_id);
		return view('stock_detail_byid', compact('stock_detail'));
	}

	public function request_store(Request $request, Transaction $transaction){

			$datetime1 = Carbon::parse($request->from); 
    		$datetime2 = Carbon::parse($request->to);
    		$interval = $datetime1->diffInDays($datetime2);
    	$user_id = Auth::user()->id;
    	$location_id = LocationUser::where('user_id',$user_id)->first();
		if ($transaction->checkDate(Auth::user()->id) < $interval) {

			return response()->json(['reserved'=>'unsuccess','msg'=>'You Are not allowed to hold book For Such time ']);
		}		

		if ((empty($request->from) && empty($request->to)) || ($request->from > $request->to)) {
			return response()->json(['reserved'=>'unsuccess','msg'=>'Please enter proper date']);
		}

		if ($transaction->checkNumber(Auth::user()->id)) {
			return response()->json(['reserved'=>'unsuccess','msg'=>'You Have already borrowed Maximun number of books. ']);	
		}

		$stockMaster = Stock::find($request->dataCode);
		$stockDetail = StockDetail::where('stock_id',$stockMaster->id)->where('book_lock_status',0)->first();
		if (!isset($stockDetail)) {
			return response()->json(['reserved'=>'unsuccess','msg'=>'You Have already Borrowed this book Please reload ']);
		}
		
        $input = [
            'code' => $stockDetail->code,
            'reference_id' => 0,
            'user_id' => Auth::user()->id,
            'status' => '0',
            'stock_id' => $stockMaster->id,
            'stock_detail_id'=>$stockDetail->id,
            'created_by' => Auth::user()->id,
            'location_id'=>null,
            'mac_address'=>null,
            'comp_name'=>gethostbyaddr($_SERVER['REMOTE_ADDR']),
            'book_from' =>$request->from,
            'book_to' =>  $request->to,
            'updated_by' => null
        ];

		$dbTransaction = DB::transaction(function() use ($input, $transaction, $request, $stockMaster,$stockDetail,$location_id){
							$transaction = Transaction::create($input);
							$transaction->update(['reference_id'=>$transaction->id]);
							$input = [
							            'code' => $stockDetail->code,
							            'reference_id' => $transaction->id,
							            'user_id' => Auth::user()->id,
							            'status' => '1',
							            'stock_id' => $stockMaster->id,
							            'stock_detail_id'=>$stockDetail->id,
							            'created_by' => Auth::user()->id,
							            'book_from' =>$request->from,
							            'book_to' => $request->to,
							            'location_id'=>null,
							            'mac_address'=>null,
							            'comp_name'=>gethostbyaddr($_SERVER['REMOTE_ADDR']),
							            'updated_by' => null
						       		 ];
					        $transaction = Transaction::create($input);
					        $stockDetail->book_lock_status = 1;
					        // $stockDetail->active = 0;
					        $stockDetail->save();
						});
		return response()->json(['reserved'=>'success','msg'=>'sucessfully requested to borrow this book']);
	}

	public function reserve_store(Request $request){

		$transaction = new Transaction;

			$datetime1 = Carbon::parse($request->from); 
    		$datetime2 = Carbon::parse($request->to);
    		$interval = $datetime1->diffInDays($datetime2);

		if ($transaction->checkDate(Auth::user()->id) < $interval) {
			
			return response()->json(['reserved'=>'unsuccess','msg'=>'You Are not allowed to hold book For Such time ']);
		}	

		if ((empty($request->from) && empty($request->to)) || ($request->from > $request->to)) {
			return response()->json(['reserved'=>'unsuccess','msg'=>'Please enter proper date']);
		}
		$bookReservation = new BookReservation;
		$stock_detail = Stock::find($request->bookId);
		$stockDetailData = StockDetail::where('stock_id',$stock_detail->id)->where('reservation_status','0')->where('active','0')->where('available_status','0')->get();
		$getDetailData = StockDetail::where('stock_id',$stock_detail->id)->where('reservation_status','0')->where('active','0')->where('available_status','0')->first();

		if (isset($getDetailData)) {
			$trans = $transaction->reservedDate($stock_detail->id,$stockDetailData,$request->from, $getDetailData->id);
		}
		// dd($getDetailData);
		
		if (!isset($getDetailData)) {
			// $transNew = BookReservation::select()->where('stock_id',$stock_detail->id)->where('active',1)->where('status','0')->get()->min('book_to');
			return response()->json(['reserved'=>'unsuccess','msg'=>'Your Request cannot be fulfilled at the moment.']);
		}
		$getAllDate  = array();
		// if (isset($transNew)) {	
		// 	$getAllDate[0]= $transNew;
		// }else{
		foreach ($trans as $key => $getDate) {
			// dd($getDate);
			$newDate = Carbon::parse($getDate->book_to)->format('Y-m-d');
			$getAllDate[$getDate->code] = $newDate;
		}
		$min = min($getAllDate);
		if (min($getAllDate) > $request->from) {
			return response()->json(['reserved'=>'failed','msg'=>'Date is already taken']);
		}else{
			// foreach ($getAllDate as $key => $value) {
			// 	if($value == $min){
			// 		$reserve_code = $key;
			// 	}
			// }
			
			 $input = [
			            'book_from' => $request->from,
			            'book_to' => $request->to,
			            'user_id' => Auth::user()->id,
			            'code' => $getDetailData->code,
			            'stock_id' => $stock_detail->id,
			            'status' => '0',
			            'stock_detail_id' =>$getDetailData->id,
			            'location_id'=>null,
			            'mac_address'=>null,
			            'comp_name'=>gethostbyaddr($_SERVER['REMOTE_ADDR']),
			            'active' =>  1,
			            'updated_by' => null
		       		];
       		DB::transaction(function() use ($input, $bookReservation, $getDetailData){
				$transaction = BookReservation::create($input);
				$getDetailData->reservation_status = 1;
				$getDetailData->save();
			});
			return response()->json(['reserved'=>'success','msg'=>'sucessfully Reserved']);
			}
		}

	public function index(Request $request){
		$stock_detail = new StockDetail;
		$checkTransaction = new Transaction;
		/**
		 * fetch transaction as per reference_id
		 * @var [type]
		 */
		$transactions = Transaction::all()->groupBy('reference_id');
		$latest_transaction = [];
		/**
		 * fetch transaction
		 * with latest status
		 */

		foreach($transactions as $group_transaction){
            $latest_transaction[] = $group_transaction->max();
        }
    // dd($latest_transaction);
		/**
		 * paginate stocked detail
		 * fetched by groupBy eloqunet query
		 * @var [type]
		 */
		$page = Input::get('page', 1); // Get the ?page=1 from the url
        $perPage = 10; // Number of items per page
        $offset = ($page * $perPage) - $perPage;

        $transaction =  new LengthAwarePaginator(
            array_slice($latest_transaction, $offset, $perPage, true), // Only grab the items we need
            count( $latest_transaction), // Total items
            $perPage, // Items per page
            $page, // Current page
            ['path' => $request->url(), 'query' => $request->query()] // We need this so we can keep all old query parameters from the url
        );

		return view('transaction_index', compact('transaction','stock_detail','checkTransaction'));
	}

  public function dispatch_stock($transaction_id, $stock_id,$stock_detail_id, Transaction $transaction)
	{
		DB::transaction(function() use ($transaction_id, $stock_id,$stock_detail_id, $transaction){

			// dd(Auth::user()->id);
			$transaction->create_new_stauts($transaction_id, 2);
      		StockDetail::where('id',$stock_detail_id)->update(['available_status'=>0, 'book_lock_status' => 1]);
		});
		return redirect()->back();
	}


  public function returned($transaction_id, $stock_id,$stock_detail_id, Transaction $transaction){
		DB::transaction(function() use ($transaction_id, $stock_id,$stock_detail_id, $transaction){
			$transaction->create_new_stauts($transaction_id, 4);

      StockDetail::where('id',$stock_detail_id)->update(['available_status'=>1, 'book_lock_status' => 0]);
		});

		return redirect()->back();
	}

   public function reserve_index(){
   		$reservation = BookReservation::all();
   		$stock_detail = new StockDetail;

   		return view('transaction_reserve',compact('reservation','stock_detail'));
   }
}
