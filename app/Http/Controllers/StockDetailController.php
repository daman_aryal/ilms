<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use App\StockDetail;
use App\Image;
use App\Stock;
use App\Author;
use App\PivotAuthorStockDetail;
use App\LocationUser;

class StockDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

      $this->validate($request, [
        'quantity' => 'required|numeric'
      ]);

      DB::transaction(function() use ($request){

          $stockdetail =  StockDetail::select('code','stock_id','title','edition','publication_id','active',
                                              'isbn','publication_date','plot_summary','language','sector_id','subject_id','genre_id','stock_format','available_status','reservation_status','book_lock_status',
                                              'pages','dimension','dewey','country')
                                              ->where('stock_id',$request->stock_id)->first();
          $stockdetailInfo = stockDetail::select('code')->orderBy('id','desc')->first();
          $n = preg_replace("/[^0-9]/","",$stockdetailInfo->code);
          // dd($n);

          $code = "CMC";
          $user_location = LocationUser::where('user_id',Auth::user()->id)->first();
          $location_id = $user_location->location_id;
          // dd($location_id);
          for ($i=0; $i < $request->quantity ; $i++) {
            $n++;
            $newStockDetail = new StockDetail();
            $newStockDetail ->location_id = $location_id;
            $newStockDetail ->code = $code."-".$n;
            $newStockDetail ->stock_id = $stockdetail->stock_id ;
            $newStockDetail ->title = $stockdetail->title ;
            $newStockDetail ->reservation_status = 0;
            $newStockDetail ->edition = $stockdetail->edition ;
            $newStockDetail ->publication_id = $stockdetail->publication_id ;
            $newStockDetail ->active = 1 ;
            $newStockDetail ->isbn = $stockdetail->isbn ;
            $newStockDetail ->book_lock_status = 0;
            $newStockDetail ->available_status = 1;
            $newStockDetail ->publication_date = $stockdetail->publication_date ;
            $newStockDetail ->plot_summary = $stockdetail->plot_summary ;
            $newStockDetail ->language = $stockdetail->language ;
            $newStockDetail ->sector_id = $stockdetail->sector_id ;
            $newStockDetail ->subject_id = $stockdetail->subject_id ;
            $newStockDetail ->genre_id = $stockdetail->genre_id ;
            $newStockDetail ->stock_format = $stockdetail->stock_format ;
            $newStockDetail ->pages = $stockdetail->pages ;
            $newStockDetail ->dimension = $stockdetail->dimension ;
            $newStockDetail ->dewey = $stockdetail->dewey ;
            $newStockDetail ->country = $stockdetail->country ;
            $newStockDetail ->created_by= Auth::user()->id;
            $newStockDetail ->updated_by = Auth::user()->id;

            $newStockDetail->save();
          }

        });

        return redirect()->back()
                        ->with('success','Stock Quantity Added Successfully');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }
    // for auto complete
    public function auto(Request $request){

      $StockArray = array();

      $query = $request->stock_detail;
      // $res = DB::table('stock_details')
      //       ->select('stock_details.title','stock_details.edition','stock_details.stock_id','stock_details.plot_summary','publications.publication_name','pivot_author_stock_details.author_id','authors.author_name')
      //       ->join('publications', 'publications.id', '=', 'stock_details.publication_id')
      //       ->join('pivot_author_stock_details','pivot_author_stock_details.stock_id','=','stock_details.stock_id')
      //       ->join('authors','authors.id','=','pivot_author_stock_details.author_id')
      //       ->where('title', 'LIKE', "%$query%")
      //       ->orWhere('publication_name', 'LIKE', "%$query%")
      //       ->orWhere('author_name', 'LIKE', "%$query%")
      //       ->where('active','1')
      //       ->distinct()->get(['title','edition','stock_id','plot_summary']);

      $res = DB::table('stock_details')
            ->select('stock_details.title','stock_details.edition','stock_details.stock_id','stock_details.plot_summary','publications.publication_name')
            ->join('publications', 'publications.id', '=', 'stock_details.publication_id')
            // ->join('pivot_author_stock_details','pivot_author_stock_details.stock_id','=','stock_details.stock_id')
            // ->join('authors','authors.id','=','pivot_author_stock_details.author_id')
            ->where('active','1')
            ->where('title', 'LIKE', "%$query%")
            ->orWhere('publication_name', 'LIKE', "%$query%")
            // ->orWhere('author_name', 'LIKE', "%$query%")
            ->distinct()->get(['title','edition','stock_id','plot_summary']);

      // $res = StockDetail::where('title', 'LIKE', "%$query%")->where('active','1')->distinct()->get(['title','edition','stock_id','plot_summary']);



      // dd($res);
        foreach($res as $index=>$stock_detail_title){
          $stockMaster = Stock::where('id',$stock_detail_title->stock_id)->first();
          $pivotAuthor = PivotAuthorStockDetail::where('stock_id',$stockMaster->id)->first();
          $authorName = Author::where('id',$pivotAuthor->author_id)->first();
            $StockArray[$index] = [
                'code'  => 'CMC-'.$stockMaster->id,
                'title' => $stock_detail_title->title,
                'authorName' =>$authorName->author_name,
                'edition'   =>$stock_detail_title->edition,
                'plot_summary' => $stock_detail_title->plot_summary,
            ];

        }
        // dd($StockArray);
        return response()->json($StockArray);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      // DB::table('stock_details')->where('id',$id)->delete();
      StockDetail::find($id)->delete();

      return redirect()->back()
                      ->with('success','Stock deleted successfully');
    }
}
