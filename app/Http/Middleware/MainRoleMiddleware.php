<?php

namespace App\Http\Middleware;

use App\Role;
use App\UserType;
use Auth;
use Closure;

class MainRoleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, ...$params)
    {
       if(!array_has(array_flip($params), Auth::user()->user_type())){
             return redirect("/");
        }
        return $next($request);
    }
}
