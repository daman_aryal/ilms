<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class ChangePasswordFirst
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::user()->first_login == 0){
            return redirect('password/edit');
        }
        return $next($request);
    }
}
