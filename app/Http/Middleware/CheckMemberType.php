<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class CheckMemberType
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
    
        if (Auth::user()->code != null ) {
            return abort(404);
        }else{
            return $next($request);
        }
    }
}
