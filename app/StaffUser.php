<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StaffUser extends Model
{
    protected $fillable = ['user_id',
                            'first_name',
                            'last_name',
    					   'date_of_birth',
    					   'permanent_address',
    					   'temporary_address',
    					   'department_id',
    					   'designation',
    					   'job_type',
    					   'joined_date',
    					   'valid_till',
    					   'member_code',
    					   'member_type_id'
    						];

    protected $dates = ['deleted_at'];

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function department()
    {
        return $this->hasOne('App\Department', 'id', 'department_id');
    }
}
