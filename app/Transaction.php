<?php

namespace App;
use Auth;
use Illuminate\Database\Eloquent\Model;
use DB;
use App\User;
use App\MemberTypePolicy;
use App\Policy;

class Transaction extends Model
{
	protected $dates = ['book_from', 'book_to', 'renew_from', 'renew_to'];

	protected $fillable = [
		'code', 'user_id', 'reference_id', 'status',
		'stock_id', 'stock_detail_id', 'created_by', 'updated_by',
		'book_from', 'book_to', 'renew_from', 'renew_to','comp_name','location_id','direct_dispatch_status'
	];


	/*protected $dates = ['book_from', 'book_to', 'renew_from', 'renew_to'];*/

	/**
	 * transaction has on stock id
	 * @return [type] [description]
	 */
	public function stock(){
		return $this->hasOne('App\Stock', 'id', 'stock_id');
	}

	/**
	 * transaction hasone stock detail id
	 * @return stock detail connection
	 */
	public function stock_detail()
	{
		return $this->hasOne('App\StockDetail', 'id', 'stock_detail_id');
	}

	/**
	 * transaction hasone user
	 * @return collection return user collection
	 */
	public function reserved_by()
	{
		return $this->hasOne('App\User', 'id', 'created_by');
	}

	// public function user()
	// {
	// 	return $this->hasOne('App\BasicUser', 'id', 'user_id');
	// }


	public function modified_by()
	{
		return $this->hasOne('App\User', 'id', 'updated_by');
	}


	public function transaction_status($stockMasterID,$stockDetailData,$dateFrom){


		foreach ($stockDetailData as $key => $singleData) {


		}


			$status = Transaction::where('stock_detail_id', $stockMasterID)
								->latest()
								->get()
								->max();
			return $status;

	}

	public function issuedDate($reference_id){
		$date = Transaction::where([
																['user_id','=',Auth::user()->id],
																['status','=','0'],
																['reference_id','=',$reference_id]
			])->first();
		return $date->created_at->toDateString();
	}

	public function reservedDate($stockMasterID,$stockDetailData,$dateFrom,$detailId){
		$status = array();
		foreach ($stockDetailData as $key => $singleData) {
		$stat = Transaction::select('id','book_to', 'book_from','stock_detail_id','code')
							->where('stock_detail_id', $singleData->id)
							->latest()
							->get()
							->max();
		if($stat){
			$status[$key] = $stat;
		}


		}
		return $status;
	}

	/**
	 * fetch the latest satus
	 * of common ref id
	 * @param  [type] $arg    [description]
	 * @param  [type] $ref_id [description]
	 * @return [type]         [description]
	 */
	public function transaction_status_same_ref($arg, $ref_id){
		if($this->where('stock_detail_id', $arg)->exists()){
			$status = Transaction::where('stock_detail_id', $arg)
								->where('reference_id', $ref_id)
								->latest()
								->get()
								->max();
			return $status;
		}
	}

	/**
	 * update particular
	 * row
	 */
	public function create_new_stauts($stock_detail_id, $status, $code=null){
		/**
		 * find transaction containing
		 * give stock
		 * @var [type]
		 */
		$transaction = Transaction::where('id', $stock_detail_id)->first()->toArray();
	/**
		 * update by 2
		 * 2 => dispatched
		 */
	    $location = LocationUser::select('location_id')->where('user_id',Auth::user()->id)->first();
     	$comp_name = gethostbyaddr($_SERVER['REMOTE_ADDR']);

		$transaction['status'] = $status;
		$transaction['location_id'] = $location->location_id;
		$transaction['comp_name'] = $comp_name;
	    // dd($transaction);
		$transaction['updated_by'] = Auth::user()->id;
		if (isset($code)) {
			$transaction['code'] = $code;
		}
		return $this->create($transaction);
	}
	public function checkNumber($id){

		$count = DB::select(DB::raw('select distinct t.code,t.stock_detail_id,t.book_from, t.book_to from (SELECT * FROM transactions WHERE code NOT IN (select distinct code from transactions where status =4)) as t where status = 2 and user_id = '.$id.''));
		$staffuser = User::find($id)->staffUser;
		$basicuser = User::find($id)->basicUser;
		if ($staffuser) {
			foreach ($staffuser as $key => $user) {
				$membertypeId = $user->member_type_id;		
			}
		}
		if ($basicuser) {
			foreach ($basicuser as $key => $user) {
				$membertypeId = $user->member_type_id;		
			}
		}
		$pivot = MemberTypePolicy::where('member_type_id',$membertypeId)->first();
		$policy = Policy::find($pivot->policy_id);
		// echo $policy->no_of_books."<".count($count);
		if (empty($count)) {
			return false;
		}
		if (!$policy->no_of_books < count($count)) {
			return false; 
		}else{
			return true;
		}
		
	}
	public function checkDate($id){

		$staffuser = User::find($id)->staffUser;
		$basicuser = User::find($id)->basicUser;
		if ($staffuser) {
			foreach ($staffuser as $key => $user) {
				$membertypeId = $user->member_type_id;		
			}
		}
		if ($basicuser) {
			foreach ($basicuser as $key => $user) {
				$membertypeId = $user->member_type_id;		
			}
		}
		$pivot = MemberTypePolicy::where('member_type_id',$membertypeId)->first();
		$policy = Policy::find($pivot->policy_id);

		
			return $policy->no_of_days; 
	}

	public function getFine($id){

		$staffuser = User::find($id)->staffUser;
		$basicuser = User::find($id)->basicUser;
		if ($staffuser) {
			foreach ($staffuser as $key => $user) {
				$membertypeId = $user->member_type_id;		
			}
		}
		if ($basicuser) {
			foreach ($basicuser as $key => $user) {
				$membertypeId = $user->member_type_id;		
			}
		}
		$pivot = MemberTypePolicy::where('member_type_id',$membertypeId)->first();
		$policy = Policy::find($pivot->policy_id);

		
			return $policy; 
		}
}
