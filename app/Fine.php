<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Fine extends Model
{

	protected $fillable = [
		 'transaction_id','amount','user_id','paid','duration'
	];
    
    public function chargedTo()
	{
		return $this->hasOne('App\User', 'id', 'user_id');
	}
	
	public function bookFrom()
	{
		return $this->hasOne('App\Transaction', 'id', 'reference_id');
	}

    public function getFine($id){

		$staffuser = User::find($id)->staffUser;
		$basicuser = User::find($id)->basicUser;
		if ($staffuser) {
			foreach ($staffuser as $key => $user) {
				$membertypeId = $user->member_type_id;		
			}
		}
		if ($basicuser) {
			foreach ($basicuser as $key => $user) {
				$membertypeId = $user->member_type_id;		
			}
		}
		$pivot = MemberTypePolicy::where('member_type_id',$membertypeId)->first();
		$policy = Policy::find($pivot->policy_id);

		
			return $policy; 
		}

	public function getReceived($id)
	{
		$user = User::find($id);

		if($user)
		{
			return $user->name;
		}
		else
		{
			return "-";
		}
	}
}
