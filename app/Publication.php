<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Publication extends Model
{
    protected $fillable = [
        'publication_name'
    ];

    public function images()
    {
        return $this->morphmany('App\Image', 'imageable');
    }

}
