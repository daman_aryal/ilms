<?php

function trans_status($value){
	if($value == 0){
		return strip_tags("<label class='label label-info'>requested</label>");
	}elseif($value == 1){
		return strip_tags("<label class='label label-primary'>issued</label>");
	}elseif($value == 2){
		return strip_tags("<label class='label label-danger'>dispatched</label>");
	}elseif($value == 3){
		return strip_tags("<label class='label label-success'></label>");
	}else{
		return "invalid status";
	}
}