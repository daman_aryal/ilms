<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PivotAuthorStockDetail extends Model
{
    protected $fillable = [
        'author_id', 'stock_detail_id'
    ];

}
