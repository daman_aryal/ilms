<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Relations\Relation;
use App\User;
use Auth;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Relation::morphMap([
            'author' => 'App\Author',
            'user' => 'App\User',
            'book' => 'App\Stock'
        ]);

        view()->composer('*', function ($view)
        {
            $user = request()->user();
            if ($user != null) {
                $unreadNotifications = $user->requestTag()->unread()->get();
                $view->with('unreadNotifications', $unreadNotifications);
            }
        });

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->environment() == 'local') {
            $this->app->register('Laracasts\Generators\GeneratorsServiceProvider');
        }
    }
}
