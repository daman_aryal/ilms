<?php

namespace App;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Featured extends Model
{

	use SoftDeletes;
	protected $dates = ['deleted_at'];

    public function stock()
    {
        return $this->belongsTo( 'App\Stock' );
    }
}
